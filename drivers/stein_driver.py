import numpy as np
import sys
import tensorflow as tf
import os
import pickle as pkl

from mpi4py import MPI 
comm = MPI.COMM_WORLD
rank = comm.Get_rank()

########################################################################################################################################
########################################################################################################################################
#save the state of the generator on rank 0 to
# initialize it to the same value in another run.
########################################################################################################################################
########################################################################################################################################
# Command line arguments to set run specs as well as provide paths for saving location, code linking etc
from argparse import ArgumentParser
parser = ArgumentParser(add_help=True, description="...")
parser.add_argument('-problem_name',dest = 'problem_name',required=True, help="problem name",type=str)

parser.add_argument('-low_rank',dest = 'low_rank',required= True,help='low_rank',type = int)
parser.add_argument('-itermax',dest = 'itermax',required= True,help='iter_max',type = int)
parser.add_argument('-outer_itermax',dest = 'outer_itermax',required= False,default = 100,help='outer_iter_max',type = int)
parser.add_argument('-projected',dest = 'projected',required= True,help='projected True or False',type = int)
parser.add_argument('-alpha',dest = 'alpha',required= False,default = 1.0,help='alpha',type = float)
parser.add_argument('-n_particles',dest = 'n_particles',required= True,help='n_particles',type = int)
parser.add_argument('-over_sample',dest = 'over_sample',required= False,default = 0,help='over_sample',type = int)
parser.add_argument('-line_search',dest = 'line_search',required= True,help='Armijo Line Search default is true',type = int)
parser.add_argument('-common_projector',dest = 'common_projector',required= False, default = 0,help='Common projector boolean',type = int)
parser.add_argument('-gradient_termination',dest = 'gradient_termination',required= False, default = 1,help='gradient_termination True or False',type = int)
parser.add_argument('-gauss_newton',dest = 'gauss_newton',required= False,default = 0,help='GN FML',type = int)
parser.add_argument('-record_stats',dest = 'record_stats',required= False,default = 0,help='record_stats',type = int)
parser.add_argument('-svn',dest = 'path_to_svn',required=False,\
        default = '.', help="path to steinvariationmethods/, required!",type=str)
parser.add_argument('-nns',dest = 'path_to_nns',required=False,default = '../nn_stein/', help="path to nn_stein/, required!",type=str)
parser.add_argument('-log_checkpoint_frequency',dest = 'log_checkpoint_frequency',required= False,default = 1,help='threads',type = int)
parser.add_argument('-logger_dir',dest = 'logger_dir',required=False,default = 'tf_logging/', help="logger dir",type=str)
parser.add_argument('-cov_path',dest = 'cov_path',required=False,default = './', help="path to covariance numpy array",type=str)
parser.add_argument('-mean_path',dest = 'mean_path',required=False,default = './', help="path to mean numpy array",type=str)
parser.add_argument('-problem_type',dest = 'problem_type',required=False, default = 'mnist', help="problem specification",type=str)
parser.add_argument('-subspace',dest = 'subspace',required=False, default = 'hessian', help="subspace choose hessian or active",type=str)
parser.add_argument('-search_direction',dest = 'search_direction',required=False, default = 'gradient',\
         help="search direction type choose newton or gradient",type=str)


########################################################################################################################################
########################################################################################################################################

parser.add_argument('-n_threads',dest = 'n_threads',required= False,default = 1,help='threads',type = int)
parser.add_argument("-n_filters", dest='n_filters',nargs='+',default = [2,2], required=False, help="n filters for conv",type=int)
parser.add_argument("-filter_sizes", dest='filter_sizes',nargs='+',default = [4,4], required=False, help="filter sizes for conv",type=int)

parser.add_argument('-n_train',dest = 'n_train',required= False,default = 100,help='n_train',type = int)



args = parser.parse_args() #


########################################################################################################################################
########################################################################################################################################
# Import from Stein
sys.path.append(args.path_to_svn)
sys.path.append(args.path_to_svn +'/steinvariationalmethods')
from steinvariationalmethods import *
from distributions import *
import prngs.parallel as random
from stein import *
########################################################################################################################################
########################################################################################################################################
#Set and record random seed for reproducibility
if comm.Get_rank() == 0:
    try:
        state = pkl.load(open('prngs_state.pkl', 'rb'))
    except:
        state = random.get_state()
        pkl.dump(state, open('prngs_state.pkl', 'wb'))
else:
    state = None
random.set_state(state)


########################################################################################################################################
########################################################################################################################################


####################################################################################################################

if args.problem_type == 'mnist':
    # Import from tensorflow library
    sys.path.append(args.path_to_svn +'/neural_network')
    from neural_network import *
    settings = {}
    settings['n_filters'] = args.n_filters
    settings['filter_sizes'] = args.filter_sizes
    settings['n_threads'] = args.n_threads
    n_train = args.n_train
    likelihood = load_mnist_problem(settings, n_train, NN = GenericCED,comm = comm)
    mu, sigma_inv = xavier_gaussian_prior(likelihood)
    prior = Gaussian(mu,sigma_inv,comm = comm)

########################################################################################################################################
########################################################################################################################################

if rank == 0:
    print('Size of configuration space',likelihood.dims)
# Sample from "Xavier Gaussian Prior"


n_particles = args.n_particles
n_dim = likelihood.dims
problem_name = args.problem_name
print("n_dim",n_dim)

########################################################################################################################################
########################################################################################################################################
# Assuming that in the preceding block of code a prior and likelihood are defined the poster can be instantiated
# Posterior definition is not problem spcific
posterior = Posterior(prior,likelihood)

########################################################################################################################################
########################################################################################################################################
# Assuming that in the preceding block of code a prior and likelihood are defined the poster can be instantiated
# Posterior definition is not problem spcific
########################################################################################################################################
########################################################################################################################################
# STEIN TIME
########################################################################################################################################
########################################################################################################################################

try:
    # Loads particles from the Guassian prior from file on rank 0, broadcast, and then copy portion of array for local
    all_xs = load_from_file(n_particles, n_dim, comm)
    local_xs = prior.get_ownership(all_xs)
    print("Loaded from file")
except:
    local_xs = prior.sample(n_particles)
    all_xs = prior.allgather(local_xs) #the prior knows how many samples are in all the local_xs
    # Saves the samples from rank 0 for reproducibility for our tests
    save_to_file(all_xs, comm)
    print("New samples, saved to file")
    print("all_xs",all_xs.shape[0])
    print("my_portion", local_xs.shape[0])

if args.projected:
    problem_name = args.problem_name + '_projected'
else:
    problem_name = args.problem_name

settings = SteinSettings()

settings['n_particles'] = args.n_particles
settings['problem_name']  = problem_name
settings['logger_dir'] = args.logger_dir
settings['log_checkpoint_frequency'] = args.log_checkpoint_frequency
settings['low_rank'] = args.low_rank
settings['itermax'] = args.itermax
settings['outer_itermax'] = args.outer_itermax
settings['projected'] = bool(args.projected)
settings['gradient_termination'] = bool(args.gradient_termination)
settings['common_projector'] = bool(args.common_projector)
settings['line_search'] = bool(args.line_search)
settings['over_sample'] = args.over_sample
settings['gauss_newton'] = bool(args.gauss_newton)
settings['record_stats'] = bool(args.record_stats)
settings['cov_path'] = args.cov_path
settings['mean_path'] = args.mean_path
settings['has_analytical_moments'] = False
settings['alpha_default'] = args.alpha
settings['print_eigenvalues'] = False
settings['search_direction'] = args.search_direction

if settings['search_direction'] == 'gradient':
    settings['subspace'] = 'active'
else:
    settings['subspace'] = args.subspace

Stein(prior,posterior,all_xs, local_xs,settings, comm)
########################################################################################################################################
########################################################################################################################################
