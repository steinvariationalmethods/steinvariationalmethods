import numpy as np
import sys
# import tensorflow as tf
import os
import pickle as pkl
from mpi4py import MPI 
from scipy.sparse import diags
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
########################################################################################################################################
########################################################################################################################################
#save the state of the generator on rank 0 to
# initialize it to the same value in another run.
########################################################################################################################################
########################################################################################################################################
# Command line arguments to set run specs as well as provide paths for saving location, code linking etc
from argparse import ArgumentParser
parser = ArgumentParser(add_help=True, description="...")

parser.add_argument('-problem_name',dest = 'problem_name',required=True, help="problem name",type=str)
parser.add_argument('-n_dim',dest = 'n_dim',required= True,help='ndim',type = int)

parser.add_argument('-low_rank',dest = 'low_rank',required= True,help='low_rank',type = int)
parser.add_argument('-itermax',dest = 'itermax',required= True,help='iter_max',type = int)
parser.add_argument('-alpha',dest = 'alpha',required= False,default =1.0,help='iter_max',type = float)

parser.add_argument('-projected',dest = 'projected',required= True,help='projected True or False',type = int)
parser.add_argument('-n_particles',dest = 'n_particles',required= True,help='n_particles',type = int)

parser.add_argument('-n_obs',dest = 'n_obs',required= True,help='n_particles',type = int)

parser.add_argument('-common_projector',dest = 'common_projector', required= False,default = 0,help='common_projector', type=int)
parser.add_argument('-over_sample',dest = 'over_sample',required= False,default = 0,help='over_sample',type = int)
parser.add_argument('-line_search',dest = 'line_search',required= False,default = 0,help='Armijo Line Search default is true',type = int)
parser.add_argument('-gradient_termination',dest = 'gradient_termination',required= False,default = 0,help='gradient_termination True or False',type = int)
parser.add_argument('-gauss_newton',dest = 'gauss_newton',required= False,default = 0,help='GN FML',type = int)
parser.add_argument('-svn',dest = 'path_to_svn',required=False,\
        default = '.', help="path to steinvariationmethods/, required!",type=str)
parser.add_argument('-nns',dest = 'path_to_nns',required=False,default = '../nn_stein/', help="path to nn_stein/, required!",type=str)

parser.add_argument('-log_checkpoint_frequency',dest = 'log_checkpoint_frequency',required= False,default = 1,help='threads',type = int)
parser.add_argument('-logger_dir',dest = 'logger_dir',required=False,default = 'logging/', help="logger dir",type=str)
# parser.add_argument('-cov_path',dest = 'cov_path',required=False,default = './', help="path to covariance numpy array",type=str)
# parser.add_argument('-mean_path',dest = 'mean_path',required=False,default = './', help="path to mean numpy array",type=str)

args = parser.parse_args() #
########################################################################################################################################
########################################################################################################################################
# Import from Stein
sys.path.append(args.path_to_svn)
from distributions import *
import prngs.parallel as random
from stein import *
########################################################################################################################################
########################################################################################################################################
#Set and record random seed for reproducibility
if comm.Get_rank() == 0:
    try:
        state = pkl.load(open('prngs_state.pkl', 'rb'))
    except:
        state = random.get_state()
        pkl.dump(state, open('prngs_state.pkl', 'wb'))
else:
    state = None
random.set_state(state)

####################################################################################################################
def DiscreteLaplace(n_dim):
    diag = np.zeros((n_dim));
    lower = np.zeros((n_dim-1));

    h = 1/n_dim
    for i in range(n_dim):
        diag[i] = np.sqrt((i+2)/(i+1))
    for i in range(n_dim-1):
        lower[i] = - np.sqrt((i+1)/(i+2));
    diag /= h 
    lower /= h
    return diag,lower


n_dim = args.n_dim
n_mix = 20
n_particles = args.n_particles

diag,lower = DiscreteLaplace(n_dim)
mu = np.zeros(n_dim)
prior = LaplaceOperatorGaussian(mu, lower, diag, init_allocations_size=n_particles, comm=comm)


likelihood = SinLinearLikelihood(prior,n_mix, n_obs=args.n_obs, noise_sigma=1., init_allocations_size=n_particles, comm=comm)
posterior = Posterior(prior,likelihood)

# def post_moments(posterior):
#     K = prior.hess_inv_neg_logpdf_action(np.zeros(n_dim),np.identity(n_dim))
#     a = posterior.likelihood._a#/ posterior.likelihood._noise_sigma
#     D_part = a@(K@a.T)
#     D = np.linalg.inv(np.identity(n_mix)*posterior.likelihood._noise_sigma2+D_part)
#     cov = K - K@(a.T@(D@(a@K)))
#     m = posterior.likelihood._obs/posterior.likelihood._noise_sigma2 * np.sum(cov@a.T,axis=1)
#     return m, cov

# posterior.mean,posterior.cov = post_moments(posterior)




########################################################################################################################################
########################################################################################################################################
# Assuming that in the preceding block of code a prior and likelihood are defined the poster can be instantiated
# Posterior definition is not problem spcific
########################################################################################################################################
########################################################################################################################################
# STEIN TIME
########################################################################################################################################
########################################################################################################################################
# all_xs = xs.copy()
# local_xs = xs.copy()

try:
    raise Exception('Dont want a data race' )
    # Loads particles from the Guassian prior from file on rank 0, broadcast, and then copy portion of array for local
    all_xs = load_from_file(n_particles, n_dim, comm)
    local_xs = prior.get_copied_ownership(all_xs, n_particles)
    print("Loaded from file")
except:
    local_xs = prior.sample(n_particles)
    all_xs = prior.allgather(local_xs) #the prior knows how many samples are in all the local_xs
    # Saves the samples from rank 0 for reproducibility for our tests
    save_to_file(all_xs, comm)
    print("New samples, saved to file")
    print("all_xs",all_xs.shape[0])
    print("my_portion", local_xs.shape[0])

print('all_xs.shape',all_xs.shape, 'on rank', rank)
print('loacl_xs.shape',local_xs.shape, 'on rank', rank)


# print(np.trace(np.cov(local_xs,rowvar=False)))
# print(np.trace(prior.centered_moment(2)))
# print(np.trace(posterior.cov))
# dddd

if args.projected:
    problem_name = args.problem_name + '_projected'
else:
    problem_name = args.problem_name

settings = SteinSettings()

settings['n_particles'] = args.n_particles
settings['problem_name']  = problem_name
settings['logger_dir'] = args.logger_dir
settings['log_checkpoint_frequency'] = args.log_checkpoint_frequency
settings['low_rank'] = args.low_rank
settings['itermax'] = args.itermax

settings['optimize_in_subspace'] = bool(args.projected)
settings['gradient_termination'] = bool(args.gradient_termination)
settings['line_search'] = bool(args.line_search)
settings['gauss_newton'] = bool(args.gauss_newton)
settings['common_projector'] = bool(args.common_projector)
settings['record_stats'] = False

settings['over_sample'] = args.over_sample
settings['alpha'] = args.alpha
settings['debugging'] = False






Stein(prior,posterior,all_xs, local_xs,settings, comm)
########################################################################################################################################
########################################################################################################################################
