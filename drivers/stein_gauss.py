import numpy as np
import scipy
import scipy.sparse as spspa
import sys
# import tensorflow as tf
import os
import pickle as pkl
from mpi4py import MPI 
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
########################################################################################################################################
########################################################################################################################################
#save the state of the generator on rank 0 to
# initialize it to the same value in another run.
########################################################################################################################################
########################################################################################################################################
# Command line arguments to set run specs as well as provide paths for saving location, code linking etc
from argparse import ArgumentParser
parser = ArgumentParser(add_help=True, description="-b batch size (int) -p population_size (int) -alpha (float)")
parser.add_argument('-svn',dest = 'path_to_svn',required=False,\
 		default = '.', help="path to steinvariationmethods/, required!",type=str)
parser.add_argument('-nns',dest = 'path_to_nns',required=False,default = '../nn_stein/', help="path to nn_stein/, required!",type=str)
parser.add_argument('-n_particles',dest = 'n_particles',required= False,default = 16,help='threads',type = int)
parser.add_argument('-log_checkpoint_frequency',dest = 'log_checkpoint_frequency',required= False,default = 1,help='threads',type = int)
parser.add_argument('-logger_dir',dest = 'logger_dir',required=False,default = 'logging/', help="logger dir",type=str)
parser.add_argument('-problem_name',dest = 'problem_name',required=False,default = 'my_problem', help="problem name",type=str)

args = parser.parse_args() #
########################################################################################################################################
########################################################################################################################################
# Import from Stein
sys.path.append(args.path_to_svn)
from distributions import *
import prngs.parallel as random
from datastructures import *
from metrics import *
from kernels import *
from algorithms import *
from misc import *
import scipy
import scipy.sparse as spspa
from scipy.sparse.linalg import LinearOperator
########################################################################################################################################

def pprint(*args):
    if comm is None or comm.Get_rank() == 0:
        print(*args)

class PriorCovarianceLinearOperator(LinearOperator):
    def __init__(self, sigma_diag):
        dtype = sigma_diag.dtype
        N = sigma_diag.shape[0]
        shape = (N,N)
        super().__init__(dtype, shape)
        self._sigma_diag = sigma_diag
    def _matvec(self, vec):
        return self._sigma_diag * vec
    
# class LowRankEigendecompLinearOperator(LinearOperator):
#     def __init__(self, Ut, d):
#         dtype = d.dtype
#         assert(Ut.ndim ==2)
#         N = Ut.shape[1]
#         shape = (N,N)
#         super().__init__(dtype, shape)
#         self._Ut = Ut
#         self._d = d
#     def _matvec(self, vec):
#         return self._Ut.T @ (self._d *(self._Ut @ vec))
 


########################################################################################################################################
#Set and record random seed for reproducibility
if comm.Get_rank() == 0:
    try:
        state = pkl.load(open('prngs_state.pkl', 'rb'))
    except:
        state = random.get_state()
        pkl.dump(state, open('prngs_state.pkl', 'wb'))
else:
    state = None
random.set_state(state)

####################################################################################################################
def random_mu_and_sigma_fast_decay(n_dim, comm): #just testing random gaussians right now
    if comm is None or comm.Get_rank() == 0:
        mu = random.uniform(-2.,2., size=(n_dim,))
        sigma_inv = np.ones(n_dim)#np.arange(1, n_dim+1)**(-1.)#4.*np.ones(n_dim)#np.arange(1, n_dim+1)**(-1.)#np.exp(-2.*np.arange(1, n_dim+1)/n_dim)#
    else:
        mu = np.empty(n_dim, dtype='d')
        sigma_inv = np.empty(n_dim, dtype='d')
    comm.Bcast(mu, root=0)
    comm.Bcast(sigma_inv, root=0)
    return mu, sigma_inv

def random_mus_and_sigmas_fast_decay(mu,sigma_inv,n_mix,n_dim, comm): #just testing random gaussians right now
    if comm is None or comm.Get_rank() == 0:
        mus = random.uniform(-2.,2., size=(n_mix,n_dim))#mu + random.normal(0.,4*sigma_inv, size=(n_mix,n_dim))##random.uniform(-2.,2., size=(n_mix,n_dim)) #mu + np.random.normal(0.,sigma_inv, size=(n_mix,n_dim))#
        # n = np.sort(sigma_inv)
#         n = np.arange(1, n_dim+1)**(-3.)
#         np.random.shuffle(n)
        sigma_invs = np.array([4*np.ones(n_dim) for i in range(n_mix)])
#         np.random.shuffle(sigma_inv) 
#         for i in range(np.int(n_mix/2)):
#             sigma_invs[i] = sigma_inv.copy()+n
            
#         sigma_invs = np.array([sigma_inv for i in range(n_mix)])#+ n
        
        
    else:
        mus = np.empty((n_mix,n_dim), dtype='d')
        sigma_invs = np.empty((n_mix,n_dim), dtype='d')
    comm.Bcast(mus, root=0)
    print(sigma_invs.shape)
    comm.Bcast(sigma_invs, root=0)
    return mus, sigma_invs


n_mix = 10
n_dim = 5
n_particles = 512

mu, sigma_inv = random_mu_and_sigma_fast_decay(n_dim, comm)
mus, sigma_invs = random_mus_and_sigmas_fast_decay(mu,sigma_inv,n_mix, n_dim, comm)
prior = Gaussian(mu,sigma_inv, init_allocations_size=n_particles, comm=comm)
posterior = GaussianMixture(mus, sigma_invs, init_allocations_size=n_particles, comm=comm)
posterior.prior = prior

# problem_name = data_set + str(args['n_filters'][0]) +'_'+ str(len(args['n_filters']))
problem_name = args.problem_name

########################################################################################################################################
########################################################################################################################################
# Assuming that in the preceding block of code a prior and likelihood are defined the poster can be instantiated
# Posterior definition is not problem spcific

########################################################################################################################################
########################################################################################################################################
# Begin Stein Algorithm
# The following is general
########################################################################################################################################
########################################################################################################################################
# Stein Initialization
########################################################################################################################################
#Set and record random seed for reproducibility
if comm.Get_rank() == 0:
    try:
        state = pkl.load(open('prngs_state.pkl', 'rb'))
    except:
        state = random.get_state()
        pkl.dump(state, open('prngs_state.pkl', 'wb'))
else:
    state = None
random.set_state(state)
try:
    # Loads particles from the Guassian prior from file on rank 0, broadcast, and then copy portion of array for local
    all_xs = load_from_file(n_particles, n_dim, comm)
    local_xs = prior.get_ownership(all_xs)
    print("Loaded from file")
except:
    local_xs = prior.sample(n_particles)
    all_xs = prior.allgather(local_xs) #the prior knows how many samples are in all the local_xs
    # Saves the samples from rank 0 for reproducibility for our tests
    save_to_file(all_xs, comm)
    print("New samples, saved to file")
    print("all_xs",all_xs.shape[0])
    print("my_portion", local_xs.shape[0])


post_mean = posterior.centered_moment(1)
# post_cov = posterior.centered_moment(2)
print(local_xs)
prior_mean = Allreduce_mean_axis_0(local_xs, comm)
if comm is None or comm.Get_rank() == 0:
    mean_diff = np.sum((prior_mean- post_mean)**2*sigma_inv)/np.sum(post_mean**2*sigma_inv)
else:
    mean_diff = np.empty(0, dtype='d')
if comm is not None: comm.Bcast(mean_diff, root=0)
pprint("mean_diff", mean_diff)
pprint("post_mean", post_mean)
pprint("prior_mean", prior_mean)

# prior_cov = np.cov(xs,rowvar=False)
# print("prior measure", measure)
# print("prior mean",mean)
# print("prior mean shape",mean.shape)

# Define run specs
# Should this be implemented as a dictionary and read in from file?

itermax = 100
stepsize = 1
new_subspace = True #if optimize_in_subspace is False, new_subspace must be True
common_projector = False
preconditioner = True #Only takes effect if optimize_in_subspace is False
# num_new_per_sample = 0
# add_particle_at = 1000000

#LINE SEARCH PARAMETERS
line_search = False
alpha_default = 1.0
max_backtracking_iter = 10
c_armijo = 1e-4
gdm_tolerance = 1e-18
r, p = 4, 1 #lowrank, oversampling
local_N, N, d = local_xs.shape[0], all_xs.shape[0], n_dim #abbreviations

optimize_in_subspace = True

# metrics = HessianMetric(model, optimize_in_subspace, init_allocations_size=N, low_rank=r)
metric = AveragedHessianMetric(posterior, project=optimize_in_subspace, init_allocations_size=local_N, low_rank=r, oversampling=p)
kernel = GaussianKernel(metric, init_allocations_size=(local_N, N)) 

if not optimize_in_subspace:
    #Prior Covariance Operator, i.e. H^{-1} applys
    PriorCovPreconditioner = PriorCovarianceLinearOperator(prior.sigma)                  #identical on each rank
#Time ALL major steps going forward!!!
# Initialise average computational time
timeave = 0;

########################################################################################################################################
########################################################################################################################################
# Array Allocations
if optimize_in_subspace:
    #Only need to change these if r changes!!!
    mgJ = np.empty((local_N, r)) #local and global versions?
    HJ = np.empty((local_N, r, r)) #local and global versions?
    post_hess = np.empty((local_N, r, r)) #resize????????
    post_grad = np.empty((local_N, r)) #local and broadcasted?
    galerkin_newton_step = np.empty((local_N,r))
    Q = np.empty((local_N, r))
    lmbda_local = np.empty((local_N, r))            
    Ut_s_local = np.empty((local_N, r, d))


    Ut_s_global = np.empty((N, r, d))
    coeffs_global = np.empty((N, r))
    perp_global = np.empty((N, d))
    proj_global =np.empty((N, d))


    # Global arrays
    global_post_grad = np.empty((N,r))
    global_post_hess = np.empty((N,r,r))
    global_grad_kernel = np.empty((N,N,r))
    global_sum_grad_kernel_cols = np.empty((N,r))
    global_Q = np.empty((N,r))
    lmbda_global = np.empty((N, r))            
    Ut_s_global = np.empty((N, r, d))
            
else:
    mgJ = np.empty((N, d))
    HJ = np.empty((N, d, d))
    post_grad = np.empty((N, d))
    galerkin_newton_step = np.empty((N, d))
    Q = np.empty((N, d))
#     post_hess = np.empty((N, posterior.dims, posterior.dims))
    # Global arrays
    # Josh there are probably more global arrays needed here?
    global_post_grad = np.empty((N, d))

step_norm_old = np.inf
########################################################################################################################################
########################################################################################################################################
# Logging
if comm is None or comm.Get_rank() == 0:
    logger = {}
    logger['n_particles'] = n_particles
    logger['ndim'] = n_dim
    logger ['name'] = problem_name
    # To be logged at each iteration on rank 0
    logger['iteration_duration'] = {}
    logger['mean_error'] = {}
    logger['covariance_error'] = {}
    logger['ave_neg_logpdf'] = {}
    logger['dkl_gradient_norm'] = {}
    logger['alpha'] = {}
    logger['avg_newton_step_norm'] = {}

if not optimize_in_subspace:
    if comm is None or comm.Get_rank() == 0:
        logger['prior_cov_prec_step_norm'] = {}

if comm is None or comm.Get_rank() == 0:
    logger_name = args.logger_dir+problem_name
    if not os.path.isdir(args.logger_dir):
        try:
            os.makedirs(args.logger_dir)
        except:
            pass

########################################################################################################################################
# If using pandas:
if comm is None or comm.Get_rank() == 0:

    try:
        import pandas as pd
        def save_logger(logger,out_name):
            logger_df = pd.DataFrame(logger)
            logger_df.to_csv(out_name+'logger.csv')
            print('Saved file to '+out_name+'logger.csv')
    # If not, numpy 
    except:
        print('Implement a logger saver that doesnt use pandas or (conda or pip) install pandas will take care of that issue')
        pass

########################################################################################################################################
########################################################################################################################################
# Begin Stein Iteration
for k in range(itermax):
    tic = MPI.Wtime()
    if comm.Get_rank() == 0:
        print("Begin calculation of gradient at iteration", k)
    # Calculate gradient
    grad_neg_log_pdfs, neg_log_pdfs = posterior.grad_neg_logpdf(local_xs, projections=(not new_subspace))  #NOT ACCMULATED GRADIENT AND NEGLOGPDF! (LOCAL)
    print('Gradient calculation took ', MPI.Wtime() - tic, 's on rank',rank)
    if optimize_in_subspace:
###############################Subspace Projection#####################################################################  
        if new_subspace:
            randomized_posterior_hess_eigendecomp(prior, local_xs, metric.random_projection, r, lmbda_local, Ut_s_local, posterior)

            #unless we change r, in the line above, we dont need to do reallocations ever.
            if common_projector:
                #do all this on rank 0
                ind = np.unravel_index(np.argsort(-lmbda_s, axis=None), lmbda_s.shape)
                x_choose = ind[0][0:r]
                col_choose = ind[1][0:r]
                Ut_choose = Ut_s[x_choose,col_choose,:]
                orthogonal_basis = scipy.linalg.orth(Ut_choose.T).T
                print(orthogonal_basis.shape)
                Ut_s[:] = orthogonal_basis  #does this work in parallel?

            local_xs = splitspace_vectors(local_xs, Ut_s_local)

            #Now send all this info to the global all_xs
            allgather(Ut_s_local, Ut_s_global,comm)
            allgather(local_xs.coeffs, coeffs_global,comm)
            allgather(local_xs.perp, perp_global,comm)
            allgather(local_xs.proj, proj_global,comm)
            
            all_xs = splitspace_vectors()
            all_xs._projectors = Ut_s_global
            all_xs._perp = perp_global
            all_xs._proj = proj_global
            all_xs._coeffs = coeffs_global
            all_xs._shape = (N, d)

            print("now a split space thing")
####################Prepare for MainAlgorithm##############################################################################   
            #assign the matrix for the metric"
            for i in range(local_N):
                post_hess[i] = np.diag(lmbda_local[i])
                post_grad[i] = local_xs.projectors[i] @ grad_neg_log_pdfs[i]
            kernel.metric.matrices = post_hess           
    else:
        post_grad[:] = grad_neg_log_pdfs
####################MainAlgorithm##############################################################################   
    # Calculate Kernel gram matrix
    grad_kernel, kernel_matrix = kernel.calculate_grad_and_gram_matrix(local_xs, all_xs, projections=optimize_in_subspace)
    # Gather the grad_kernel
    allgather(grad_kernel, global_grad_kernel,comm)
    if optimize_in_subspace:
        post_hess[:] = kernel.metric.matrices 
    else:
        lmbda_s[:], Ut_s[:] = kernel.metric.matrices
    sum_kernel_rows = np.sum(kernel_matrix, axis=1, keepdims=True) # Nx 1, could keep is 1 x N if need be.        
#         if comm is not None and comm.Get_rank() ==0:
#             mgJ[:] = post_grad #copy the gradient onto each worker,
    # All gather the post grad
    allgather(post_grad, global_post_grad,comm)
    # Calculate - gradient(J)
    for i in range(local_N): #THE N BELOW SHOULD BE THE GLOBAL N!
        mgJ[i] = -(kernel_matrix[i] @ global_post_grad)/N + np.mean(grad_kernel[i] , 0) #mean on each process filling in rows of mgJ
    print('||mgJ|| after', np.linalg.norm(mgJ))
    sum_grad_kernel_cols  = np.sum(-grad_kernel, axis=1)/sum_kernel_rows  #need the sum of hte kernel rows on each process/
    # Gather sum_grad_kernel_cols
    allgather(sum_grad_kernel_cols,global_sum_grad_kernel_cols,comm)
    # Calculate Hessian (dense or Low Rank Eigendecomposition)
    if optimize_in_subspace:
        # Create dense Hessians and do r x r Hessian solves 
        # Gather post_hess
        allgather(post_hess, global_post_hess,comm)
        for i in range(local_N): #need to accumulate post hess??????
            HJ[i] = (global_post_hess.T @ (kernel_matrix[i]) + global_grad_kernel[:,i].T @ global_sum_grad_kernel_cols)/N #grad_kernel[i].T @ sum_grad_kernel_col/N can be precomputed ans passed in
    else:
        # Low rank action for CG solves
        HJ_ActionOps = [None] * N
        for i in range(local_N):
            #NOT COMPLETE!!!, requires working out  #LowRankEigendecompLinearOperator better
            HJ_ActionOps[i] = LowRankEigendecompLinearOperator(Ut_s[i], lmbda_s[i], kernel_matrix[i], sum_k, grad_kernel[i].T, )#define the Hessian Action Operators
    # Solve the Newton system for the step (LU or CG)
    if optimize_in_subspace:
        for i, (Hessian_i, minus_gradient_i) in enumerate(zip( HJ, mgJ)):
            Q[i] = np.linalg.solve(Hessian_i, minus_gradient_i)
    else:
        pass
        ## PLEASE FIX ME
        # for newton_step_i, HessianActionOp_i, minus_gradient_i in zip(Q, HJ_ActionOps, mgJ):
        #     newton_step_i, info = spspa.linalg.cg(HessianActionOp_i, minus_gradient_i, tol=1e-05, maxiter=10, M=PriorCovPreconditioner)
    # All gather the Qs after the computation
    allgather(Q,global_Q,comm)
    # Galerkin projection of the step (normalized K)
    for i in range(local_N):
        galerkin_newton_step[i] = (kernel_matrix[i].T @ global_Q)/sum_kernel_rows[i]
    # Line search Globalization
    alpha = alpha_default
    if line_search:
        terminate = False
        average_cost_old = Reduce_mean_scalars_to_rank_0(neg_log_pdfs,  comm)
        for _ in range(max_backtracking_iter):
            if optimize_in_subspace:
                trial_x = splitspace_vectors(local_xs.full,local_xs.projectors)
            else:
                trial_x = local_xs
            trial_x += alpha * galerkin_newton_step 

            local_cost_new, local_grad_inner_step = posterior.neg_logpdf(trial_x), np.sum(post_grad*galerkin_newton_step, axis =1) #post_grad or mgJ???            
            average_grad_inner_step = Reduce_mean_scalars_to_rank_0(local_grad_inner_step, comm)
            average_cost = Reduce_mean_scalars_to_rank_0(local_cost_new, comm)
            if comm is None or comm.Get_rank() == 0:
                if average_cost < average_cost_old - alpha*c_armijo*average_grad_inner_step or (-average_grad_inner_step <= gdm_tolerance):
                    terminate = True
            terminate = comm.bcast(terminate, root=0) if comm is not None else terminate
            if terminate:
                break
            else:
                alpha *= 0.5
                print("old cost ", average_cost_old, "new cost ", average_cost,"armijo_condition ",alpha*c_armijo*average_grad_inner_step,"gdm ",average_grad_inner_step)
        else:
            if comm is not None and comm.Get_rank() ==0:
                print("Warning, backtracked to max backtracking iters")
    #Take Newton Step: Update the particle locations
    # In place add has been checked and works


    # galerkin_newton_step = np.ones(galerkin_newton_step)
    
    local_xs += alpha*galerkin_newton_step

    allgather(local_xs.coeffs, coeffs_global, comm)
    allgather(local_xs.proj, proj_global, comm)

    #set these without computing things
    all_xs._coeffs = coeffs_global
    all_xs._proj = proj_global

    #HOW DO WE DO THIS, because need to automatically do the projection etc
    #need to accumulate the all_xs and then assign all_xs.coeffs
    # Gather the _xs
    # print(80*"#") 
    # print("Before", np.linalg.norm(local_xs.coeffs), np.linalg.norm(all_xs.coeffs))

#update coeffs
    # print("After", np.linalg.norm(local_xs.coeffs), np.linalg.norm(all_xs.coeffs))
    # print(80*"#") 



    #update the values???
    # Access convergence
    step_norm = np.mean(np.linalg.norm(galerkin_newton_step, axis=1)*alpha)
    average_step_norm = Allreduce_mean_scalars(step_norm, comm)
    if np.abs(step_norm_old - average_step_norm)<0.1:
        if optimize_in_subspace:
            new_subspace = True
            print("New subspace at iteration ", k)
    step_norm_old = average_step_norm
    if optimize_in_subspace and new_subspace:
        local_xs = local_xs.full    
        all_xs = all_xs.full
    average_neg_log_pdf = Allreduce_mean_scalars(neg_log_pdfs,  comm)
    # average_neg_log_pdf = np.mean(Reduce_mean_scalars_to_0(neg_log_pdfs,  comm))
    grad_norm = np.linalg.norm(mgJ)
    average_grad_norm = Allreduce_mean_scalars(grad_norm, comm)

    # logger['ave_neg_logpdf'][k] = average_neg_log_pdf[0]
    # post_cov = posterior.centered_moment(2)
    sample_mean = Allreduce_mean_axis_0(local_xs, comm)
    if comm is None or comm.Get_rank() == 0:
        mean_diff = np.sum((sample_mean- post_mean)**2*sigma_inv)/np.sum(post_mean**2*sigma_inv)
    else:
        mean_diff = np.empty(0, dtype='d')
    if comm is not None: comm.Bcast(mean_diff, root=0)
    # pprint("sample_mean", sample_mean)
    
    # pprint(mean_diff)
    if comm is None or comm.Get_rank() == 0:
        logger['dkl_gradient_norm'][k] = average_grad_norm[0]
        logger['dkl_gradient_norm'][k] = average_grad_norm[0]
        logger['avg_newton_step_norm'][k] = average_step_norm[0]
        logger['alpha'][k] = alpha
        logger['iteration_duration'] = MPI.Wtime() - tic
    pprint(80*'#')
    pprint('it',k,"mean error", mean_diff, "ave_step_norm:", average_step_norm[0],"ave_neg_logpdf:", average_neg_log_pdf[0], "grad_norm", average_grad_norm[0],"stepsize:", alpha)
    pprint(80*'#')

    if k == itermax:
        print('Maximum number of iterations has been reached.\n')
    if (comm.Get_rank() ==0) and (k% args.log_checkpoint_frequency == 0):
        save_logger(logger,logger_name)
########################################################################################################################################
########################################################################################################################################
timeave += MPI.Wtime() - tic
timeave = timeave / itermax;
print("time cost ", timeave)
if comm.Get_rank() == 0:
    save_logger(logger,logger_name)