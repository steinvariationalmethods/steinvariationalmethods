import numpy as np
import scipy
import time
import scipy.sparse as spspa
from scipy.sparse.linalg import LinearOperator
import sys
import tensorflow as tf
import os
import pickle as pkl

from mpi4py import MPI 
comm = MPI.COMM_WORLD

rank = 0

#save the state of the generator on rank 0 to
# initialize it to the same value in another run.


from argparse import ArgumentParser

parser = ArgumentParser(add_help=True, description="-b batch size (int) -p population_size (int) -alpha (float)")
parser.add_argument('-svn',dest = 'path_to_svn',required=False,\
 		default = '.', help="path to steinvariationmethods/, required!",type=str)
parser.add_argument('-nns',dest = 'path_to_nns',required=False,default = '../nn_stein/', help="path to nn_stein/, required!",type=str)
parser.add_argument('-data_set',dest = 'data_set',required=False, default = 'mnist', help="data set specification",type=str)
parser.add_argument('-n_threads',dest = 'n_threads',required= False,default = 1,help='threads',type = int)
parser.add_argument('-n_ws',dest = 'n_ws',required= False,default = 2,help='threads',type = int)
parser.add_argument("-n_filters", dest='n_filters',nargs='+',default = [2,2], required=False, help="n filters for conv",type=int)
parser.add_argument("-filter_sizes", dest='filter_sizes',nargs='+',default = [4,4], required=False, help="filter sizes for conv",type=int)



args = parser.parse_args() #
threads = args.n_threads
# Import from Stein
sys.path.append(args.path_to_svn)
from distributions import *
import prngs.parallel as random
from datastructures import *
from metrics import *
from kernels import *
from algorithms import *
#Set and record random seed for reproducibility
if comm.Get_rank() == 0:
    try:
        state = pkl.load(open('prngs_state.pkl', 'rb'))
    except:
        state = random.get_state()
        pkl.dump(state, open('prngs_state.pkl', 'wb'))
else:
    state = None
random.set_state(state)

sys.path.append(args.path_to_nns)
from mnist import load_mnist
from tfLikelihood import tfLikelihood
from neuralNetwork import GenericCED, GenericCAE


class PriorCovarianceLinearOperator(LinearOperator):
    def __init__(self, sigma_diag):
        dtype = sigma_diag.dtype
        N = sigma_diag.shape[0]
        shape = (N,N)
        super().__init__(dtype, shape)
        self._sigma_diag = sigma_diag
    def _matvec(self, vec):
        return self._sigma_diag * vec
    
class LowRankEigendecompLinearOperator(LinearOperator):
    def __init__(self, Ut, d):
        dtype = d.dtype
        assert(Ut.ndim ==2)
        N = Ut.shape[1]
        shape = (N,N)
        super().__init__(dtype, shape)
        self._Ut = Ut
        self._d = d
    def _matvec(self, vec):
        return self._Ut.T @ (self._d *(self._Ut @ vec))


def save_to_file(x, comm):
    print(x.ndim)
    assert(x.ndim ==2)
    n_particles, dimension = x.shape
    if comm is None or comm.Get_rank() == 0:
        filename =str(n_particles)+"_"+str(dimension)+".npy"
        np.save(filename, x)
    if comm is not None: comm.Barrier()
        
        
        
def load_from_file(n_particles, dimension, comm): #NOT USING HDF5 or anything for now. sipmly numpy array loaded on rank 0                                                                                           
    shape = (n_particles, dimension)
    samples, window = np_float64_shared_array(shape, comm)
    
    filename =str(n_particles)+"_"+str(dimension)+".npy"
    print("filename = %s" % filename)
    if os.path.exists(filename):
        if comm is None or comm.Get_rank() == 0:
            filename =str(n_particles)+"_"+str(dimension)+".npy"
            samples[:] = np.load(filename)
        comm.Barrier()
        return samples, window
    else:
        print("UH OH: no file to read koomie")
        raise ValueError("no existing file to read")



def xavier_gaussian_prior(likelihood,comm):
    w_dimension = likelihood.dims
    n_layers = likelihood.n_layers
    w_dims = likelihood.w_dims

    # Mean vector for weights
    mu = np.zeros(w_dimension)
    # Diagonal covariance for weights (represented as vector)
    varinv_coefficients = []
    weight_var = 1.
    bias_var = 1.
    for i in range(n_layers):
        varinv_coefficients.append(weight_var)
        varinv_coefficients.append(bias_var)
        weight_var *=0.5
        bias_var *= 0.5

#     print(varinv_coefficients)
    sigma_inv = np.zeros(w_dimension)
    index = 0
    for coef,dim in zip(varinv_coefficients,w_dims):
        sigma_inv[index:index+dim] = coef*np.ones(dim)
        index += dim
    return  Gaussian(mu,sigma_inv,comm = comm)

N_data = 500
data_set = args.data_set

if data_set == 'mnist':

    xs = np.load(args.path_to_nns+'/mnist/mnist_all_images'+str(rank)+'.npy') 
    ys = np.load(args.path_to_nns+'/mnist/mnist_all_images_noise'+str(rank)+'.npy')
    sigma_noise = np.load(args.path_to_nns+'/mnist/mnist_sigma_noise'+str(rank)+'.npy')

elif data_set == 'cahn_hill':
    xs = np.load(args.path_to_nns+'/cahn_hill/cahn_hill_all_u0s'+str(rank)+'.npy') 
    ys = np.load(args.path_to_nns+'/cahn_hill/cahn_hill_all_uTs'+str(rank)+'.npy')
    sigma_noise = np.load(args.path_to_nns+'/cahn_hill/cahn_hill_sigma_noise'+str(rank)+'.npy')

input_shape = [None]+list(xs[0].shape)
output_shape = [None]+list(ys[0].shape)
# print(input_shape)


settings = {}
settings['input_shape'] = input_shape
settings['output_shape'] = output_shape
settings['n_filters'] = args.n_filters
settings['filter_sizes'] = args.filter_sizes
settings['sigma_noise'] = sigma_noise
settings['n_threads'] = args.n_threads
settings['dtype'] = tf.float32
settings['loss_type'] = 'least_squares'
data = [xs,ys]

likelihood = tfLikelihood(settings,GenericCED,data,comm = comm)
if rank == 0:
    print('Size of configuration space',likelihood.dims)
# Sample from "Xavier Gaussian Prior"
prior = xavier_gaussian_prior(likelihood,comm)
n_particles = args.n_ws
n_dim = likelihood.dims

measure = prior.centered_moment(0)
mean = prior.centered_moment(1)  
cov = prior.centered_moment(2)  
print("prior measure", measure)
print("prior mean",mean)
print("prior mean shape",mean.shape)

try:
    # Loads particles from the Guassian prior from file 
    # into a shared memory array.
    x, x_window = load_from_file(n_particles, n_dim, comm)
    print("Loaded from file")
except:
    comm.Barrier()
    x = prior.sample(n_particles)
    # Saves the samples from rank 0 for reproducibility for our tests
    save_to_file(x, comm)
    print("New samples, saved to file")


xs = prior.sample(n_particles)
posterior = Posterior(prior,likelihood)


########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################

itermax = 200
stepsize = 1

change_dim = True #if projection is False, change_dim must be True
projection = True
preconditioner = True #Only takes effect if projection is False
common_basis = True
line_search = True
max_backtracking_iter = 10
add_num = 0
add_particle_at = 1000000

p = 10 #oversampling
r = 20 #low rank
x = xs.copy()
N = x.shape[0]


#Time ALL major steps going forward!!!


    
# # Initialise particle maximum shifts
# maxshift = np.zeros(N)
# maxmaxshift_old = np.inf

# Initialise average computational time
timeave = 0;

# metrics = HessianMetric(posterior, projection, init_allocations_size=N, low_rank=r)
metrics = AveragedHessianMetric(posterior, projection, init_allocations_size=N, low_rank=r)
kernels = GaussianKernel(metrics, init_allocations_size=N)
Omega = random.normal(size=(r+p, posterior.dims))/posterior.dims
if not projection:
    #Prior Covariance Operator, i.e. H^{-1} applys
    Prec = PriorCovarianceLinearOperator(prior.sigma) 
               
            
#Parallel Comptaible arrays for -gradient(J) and Hessian(J)
lmbda_s, lmbda_s_window = prior.init_array((N, r))
Ut_s, Ut_s_window = prior.init_array((N, r, posterior.dims))
g_mlpt, g_mlpt_window = prior.init_array((N, posterior.dims))
mgJ, mgJ_win = prior.init_array((N, posterior.dims))
if projection:
    HJ, HJ_win = prior.init_array((N, posterior.dims, posterior.dims))
gnH, gnH_win = prior.init_array((N, posterior.dims, posterior.dims))
g_mlpt, g_mlpt_win = prior.init_array((N, posterior.dims))
step, step_win = prior.init_array((N, posterior.dims))
Q, Q_win = prior.init_array((N, posterior.dims))
            
step_norm_old = 10.

#For parallel assignment
my_slice = prior.assign_slice(x.shape[0])
start_idx, end_idx = prior._start_idx, prior._end_idx
print('Starting Stein Iteration')
for k in range(itermax):
    if k>1:
        print('Finished iteration ',k,' and it took ', time.time() - tic, 's')
    tic = time.time()
    print('Starting gradient calculation for iteration ',k)
    grad_neg_log_pdfs, neg_log_pdfs = posterior.grad_neg_logpdf(x, projections=(not change_dim))
    print('Terminating gradient calculation for iteration ',k, ' and it took ', time.time() - tic, 's')
    if projection:
        if change_dim:
##########################AddParticles########################################################### 
            if np.mod(k+1, add_particle_at) == 0:
                #allocate x_new first using prior.init_array()
                #x_new, x_new_window = LocalLaplaceApproximationSamples(add_num, x, prior, posterior, Omega, lambda_s, Ut_s)

                #ALL BELOW CODE COULD BE DONE WITH A FUNCTION LIKE ABOVE ^ :
                print("add particles here")
                #  parallel_randomized_posterior_hess_eigendecomp(prior, x, Omega, r, lmbda_s, Ut_s, posterior=posterior)
                # new_particle_sampler = LaplaceApproxGaussian(lmbda_s, Ut_s, map=None, prior), define this class object (one, or one for each lambda???)
                # Use new_particle_sampler.H_inv_action...  acting on grad_neg_log_pdfs to get x_map = x - H_inv action(grad_neg_log_pdfs)
                # set LaplaceApproxGaussian.mu = x_map
                # xnew should be (add_num) samples from LowRankGaussian then
                # where the sampler allocates a shared memory array and samples add_num * num_particles
                
                #JOSH< MAKE THIS PARALLEL COMPATIBLE. fill in a preallocated xnew
                xnew = np.empty((0,posterior.dims))
                xnew_window = None
                for i in range(start_idx, end_idx): #This should be done without a loop at all
                    gnH = posterior.hess_neg_logpdf_action(x[i], np.identity(posterior.dims), gaussnewton=True, idx=i)
#                     print(posterior._grad_neg_logpdf.shape)
                    x_map = x[i] - np.linalg.solve(gnH,grad_neg_log_pdfs[i])
                    assert(np.allclose( gnH, gnH.T))
                    cov = np.linalg.inv(gnH)
                    xnew = np.append(xnew,np.random.multivariate_normal(x_map,cov,add_num),axis = 0)
                
                #These are the full grad/neg_log pdfs
                grad_neg_log_pdfs, neg_log_pdfs = posterior.grad_neg_logpdf(xnew, projections=(not change_dim), append_result=True)
                #NEXT:
                #we need to expand the following to store the new particle eigendecomps!!
                lmbda_s, lmbda_s_window = prior.init_array((N, r)) #prior.expand_array(lmbda_s, (N, r), lmbda_s_window)
                Ut_s, Ut_s_window = prior.init_array((N, r, posterior.dims)) #prior.expand_array(lmbda_s,(N, r, posterior.dims), lmbda_s_window)
                #Do the eigendecomposition for xnew
                #.....
                
                #Expand x and insert xnew, x here is not a split_space vector yet!!
                x, x_window = prior.append(x, xnew, shared_array_mpi_window=x_window, new_var_shared_array_mpi_window=xnew_window)
                #For parallel assignment
                my_slice = prior.assign_slice(x.shape[0])
                start_idx, end_idx = prior._start_idx, prior._end_idx
                N = x.shape[0]
######################RandomizedEigenvalueDecomposition############################################
            else:
                parallel_randomized_posterior_hess_eigendecomp(prior, x, Omega, r, lmbda_s, Ut_s, posterior=posterior)
#####################CommonBasis###################################################################  
            if common_basis:
                #do all this on rank 0
                ind = np.unravel_index(np.argsort(-lmbda_s, axis=None), lmbda_s.shape)
                x_choose = ind[0][0:r]
                col_choose = ind[1][0:r]
                Ut_choose = Ut_s[x_choose,col_choose,:]
                orthogonal_basis = scipy.linalg.orth(Ut_choose.T).T
                print(orthogonal_basis.shape)
                Ut_s[:] = orthogonal_basis  #does this work in parallel?
####################Prepare for MainAlgorithm##############################################################################   
            x = splitspace_vectors(x, projectors=Ut_s, comm=comm) 
            
            #Parallel Comptaible arrays for -gradient(J) and Hessian(J)
            mgJ, mgJ_win = prior.init_array((x.num_vecs, x.projected_dims))
            if projection:
                HJ, HJ_win = prior.init_array((x.num_vecs, x.projected_dims, x.projected_dims))
            gnH, gnH_win = prior.init_array((N, r, r))
            g_mlpt, g_mlpt_win = prior.init_array((N, r))
            step, step_win = prior.init_array((N,r))
            Q, Q_win = prior.init_array((N, r))
        
            #assign the matrix for the metric
            for i in range(start_idx, end_idx):
                gnH[i] = np.diag(lmbda_s[i])       #np.diag(lmbda_new + 1.)  #Ut_new.T @ (np.diag(lmbda_new + 1.) @ Ut_new)
                g_mlpt[i] = x.projectors[i] @ grad_neg_log_pdfs[i]
            kernels.metric.matrices = gnH           
 
            # MPIFree(lmbda_s_window)
            # MPIFree(Ut_s_window)
            
            
    else:
        g_mlpt[:] = grad_neg_log_pdfs

####################MainAlgorithm##############################################################################   
    
    ####Calculate Kernel gram matrix
    gkernel, kernel = kernels.calculate_grad_and_gram_matrix(x, x, projections=projection)
    if projection:
        gnH[:] = kernels.metric.matrices
    else:
        lmbda_s[:], Ut_s[:] = kernels.metric.matrices
    
    sum_k = 1.#np.sum(kernel,axis=1)
    sum_kernel_rows, sum_kernel_rows_win= prior.init_array((kernel.shape[0],1))
    sum_kernel_rows[my_slice] = np.sum(kernel[my_slice], axis=1, keepdims=True) #Nx 1, could keep is 1 x N if need be.        

    
#         if comm is not None and comm.Get_rank() ==0:
#             mgJ[:] = g_mlpt #copy the gradient onto each worker, for now assume concurrent access is cheap
    
    
    ####Calculate - gradient(J)
    for i in range(start_idx, end_idx):
        mgJ[i] = -(kernel[i] @ g_mlpt)/N + np.mean(gkernel[i] , 0) #mean on each process filling in rows of mgJ
    
    sum_gkernel_cols,sum_gkernel_cols_win = prior.init_array((gkernel.shape[1],gkernel.shape[2]))
    sum_gkernel_cols[my_slice] = np.sum(gkernel[:, my_slice], axis=0)/sum_kernel_rows[my_slice]  #need the sum of hte kernel rows on each process/
            #copy kernel onto all processes???
    
    #### Calculate Hessian (dense or Low Rank Eigendecomposition)
    if projection:
        ####Create dense Hessians and do r x r Hessian solves 
        for i in range(start_idx, end_idx):
            HJ[i] = (gnH.T @ (kernel[i] * sum_k) + gkernel[i].T @ sum_gkernel_cols)/N #gkernel[i].T @ sum_gkernel_col/N can be precomputed ans passed in
    else:
        HJ_ActionOps = [None] * N
        for i in range(start_idx, end_idx):
            #NOT COMPLETE!!!, requires working out  #LowRankEigendecompLinearOperator better
            HJ_ActionOps[i] = LowRankEigendecompLinearOperator(Ut_s[i], lmbda_s[i], kernel[i], sum_k, gkernel[i].T, )#define the Hessian Action Operators
    
    #### Solve the Newton system for the step (LU or CG)
    if projection:
        for i in range(start_idx, end_idx):
            Q[i] = np.linalg.solve(HJ[i], mgJ[i])
    else:
        for i in range(start_idx, end_idx):
            Q[i], info = spspa.linalg.cg(HJ_ActionOps[i], mgJ[i], tol=1e-05, maxiter=10, M=Prec)
       
    ##### Galerkin projection of the step (normalized)
    for i in range(start_idx, end_idx):
        step[i]=(kernel[i].T @ Q)/sum_kernel_rows[i]
    
    ## Line search
    descent = 0
    alpha = 1.
    c_armijo = 1e-4
    gdm_tolerance = 1e-18
    n_backtrack = 0
    if line_search:
        cost_old = neg_log_pdfs.copy()
        if not projection:
            trial_x, trial_x_window = prior.init_array(x.shape)
        while descent == 0 and n_backtrack < max_backtracking_iter:
            if projection:
                trial_x = splitspace_vectors(x.full,x.projectors, comm=None)
                trial_x.coeffs += alpha * step
            else:
                trial_x[:] = x + alpha * step
                
            cost_new = posterior.neg_logpdf(trial_x)
            gstep = np.sum(g_mlpt*step,axis =1)
#             gstep = np.sum(g_mlpt.T @ step,axis =0)
#                 print("old cost ", np.mean(cost_old), "new cost ", np.mean(cost_new),"add ",alpha*c_armijo*np.mean(gstep),"gstep ",np.mean(gstep))
            if np.mean(cost_new) < np.mean(cost_old)-alpha*c_armijo*np.mean(gstep) or (-np.mean(gstep)<=gdm_tolerance):
                descent = 1
            else:
                n_backtrack += 1
                alpha *= 0.5
                if projection:
                    #deallocate splitspace_vectors
                    pass
               
    #Take Newton Step: Update the particle locations
    if projection:
        x.coeffs += alpha*step
    else:
        #x[my_slice] += alpha * step[my_slice]
        x[:] += alpha*step

        
    #Assess convergence
    step_norm = np.mean(np.linalg.norm(step,axis=1)*alpha )
    if np.abs(step_norm_old-step_norm)<0.5:
        if projection:
            change_dim = True
            print("change dim at ", k)
    else:
        if projection:
            change_dim = False
    step_norm_old = step_norm.copy() 

#     change_dim = k % 10 == 0
    if projection and change_dim: #set the particles back to full space particles (does this work with shared allocations?)
        x = x.full    
    
    #Reporting, printing, saving intermediate results to disk!!!!!
    print(k,"step_norm:", step_norm,"ave_neg_logpdf:", np.mean(neg_log_pdfs), "grad_norm", np.linalg.norm(mgJ),"stepsize:", alpha)

    if k == itermax:
        print('Maximum number of iterations has been reached.\n')
        
timeave += time.time() - tic

timeave = timeave / itermax;
print("time cost ", timeave)


