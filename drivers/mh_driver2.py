import numpy as np
import sys
import os
import pickle as pkl

try:
    from mpi4py import MPI 
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
except:
    comm = None
    rank = 0

########################################################################################################################################
# Command line arguments to set run specs as well as provide paths for saving location, code linking etc
from argparse import ArgumentParser
parser = ArgumentParser(add_help=True, description="...")
parser.add_argument('-problem_name',dest = 'problem_name',required=True, help="problem name",type=str)

parser.add_argument('-record_stats',dest = 'record_stats',required= False,default = 0,help='record_stats',type = int)
parser.add_argument('-svn',dest = 'path_to_svn',required=False,\
        default = '.', help="path to steinvariationmethods/, required!",type=str)
parser.add_argument('-log_checkpoint_frequency',dest = 'log_checkpoint_frequency',required= False,default = 1,help='threads',type = int)
parser.add_argument('-logger_dir',dest = 'logger_dir',required=False,default = 'tf_logging/', help="logger dir",type=str)
parser.add_argument('-cov_path',dest = 'cov_path',required=False,default = './', help="path to covariance numpy array",type=str)
parser.add_argument('-mean_path',dest = 'mean_path',required=False,default = './', help="path to mean numpy array",type=str)
parser.add_argument('-problem_type',dest = 'problem_type',required=False, default = 'covertype_logistic', help="problem specification",type=str)
parser.add_argument('-subspace',dest = 'subspace',required=False, default = 'hessian', help="subspace choose hessian or active",type=str)


########################################################################################################################################
########################################################################################################################################

parser.add_argument('-n_threads',dest = 'n_threads',required= False,default = 1,help='threads',type = int)
parser.add_argument("-n_filters", dest='n_filters',nargs='+',default = [2,2], required=False, help="n filters for conv",type=int)
parser.add_argument("-filter_sizes", dest='filter_sizes',nargs='+',default = [4,4], required=False, help="filter sizes for conv",type=int)
parser.add_argument('-n_data',dest = 'n_data',required= False,default = 8,help='n_data',type = int)

args = parser.parse_args() #

########################################################################################################################################
########################################################################################################################################
# Import from Stein
sys.path.append(args.path_to_svn)
sys.path.append(args.path_to_svn +'/steinvariationalmethods')
from steinvariationalmethods import *
from distributions import *
import prngs.parallel as random
from mcmc import *
from stein import *
########################################################################################################################################
########################################################################################################################################

########################################################################################################################################
# save the state of the random number generator on rank 0 to
# initialize it to the same value in another run.
########################################################################################################################################
#Set and record random seed for reproducibility
if rank is 0:
    try:
        state = pkl.load(open('prngs_state.pkl', 'rb'))
    except:
        state = random.get_state()
        pkl.dump(state, open('prngs_state.pkl', 'wb'))
else:
    state = None
random.set_state(state)


########################################################################################################################################
########################################################################################################################################


####################################################################################################################
# import tensorflow as tf

# if args.problem_type == 'mnist':
#     # Import from tensorflow library
#     sys.path.append(args.path_to_svn +'/tf_models')
#     from tf_models import *
#     stein_settings = {}
#     stein_settings['n_filters'] = args.n_filters
#     stein_settings['filter_sizes'] = args.filter_sizes
#     stein_settings['n_threads'] = args.n_threads
#     n_data = args.n_data
#     likelihood = load_mnist_problem(stein_settings, n_data, NN = genericCED,comm = comm)
#     mu, sigma_inv = xavier_gaussian_prior(likelihood)
#     prior = Gaussian(mu, sigma_inv, comm = comm)


# elif args.problem_type == 'mnist_logistic':
#     sys.path.append(args.path_to_svn +'/tf_models')
#     from tf_models import *
#     stein_settings = {}
#     stein_settings['n_threads'] = args.n_threads
#     n_data = args.n_data
#     likelihood = load_mnist_problem(stein_settings, n_data, NN = logistic_function,comm = comm)
#     mu = np.zeros(likelihood.w_dims)
#     sigma_inv = np.ones(likelihood.w_dims)
#     prior = Gaussian(mu, sigma_inv, comm = comm)
# elif args.problem_type == 'covertype_logistic':
#     sys.path.append(args.path_to_svn +'/tf_models')
#     from tf_models import *
#     stein_settings = {}
#     stein_settings['n_threads'] = args.n_threads
#     n_data = args.n_data
#     likelihood = load_covertype_problem(stein_settings, n_data, NN = logistic_function,path = args.path_to_svn +'/tf_models',comm = comm)
#     mu = np.zeros(likelihood.w_dims)
#     sigma_inv = np.ones(likelihood.w_dims)
#     prior = Gaussian(mu, sigma_inv, comm = comm)

# ########################################################################################################################################
# ########################################################################################################################################

# if rank is 0:
#     print('Size of configuration space',likelihood.dims)
# # Sample from "Xavier Gaussian Prior"


# n_dim = likelihood.dims
# print('n_dim=',n_dim)
problem_name = args.problem_name

########################################################################################################################################
########################################################################################################################################
# Assuming that in the preceding block of code a prior and likelihood are defined the poster can be instantiated
# Posterior definition is not problem spcific
n_dim = 5;
n_comp = 4;
mu = np.zeros(n_dim)
sigma_inv =0.5*np.ones(n_dim)
prior = Gaussian(mu, sigma_inv, comm = comm)
mu = np.array([[1.,1.,1.,1.,1.],[-1.,-2.,0.,3.,1.],[0.,0.,0.,0.,0.],[-0.5,1.,-2.,-3.,2.]])#np.random.uniform(-5,5,(n_comp,n_dim))
sigma_inv = np.array([[1.,1.,1.,1.,1.],[0.7,0.5,1.,0.5,2.],[2.,2.,2.,2.,2.],[1.,1.,2.,0.7,0.7]])
# sigma_inv = np.random.uniform(0,10,(n_comp,n_dim))
posterior = GaussianMixture(mu,sigma_inv)#Gaussian(mu+3., sigma_inv, comm = comm)#Posterior(prior,likelihood)
pklfile = open('mean_5.pkl', 'wb')
print("mean", np.mean(mu,axis=0),posterior.centered_moment(1))
pkl.dump(posterior.centered_moment(1), pklfile)
pklfile.close()
pklfile = open('cov_5.pkl', 'wb')
pkl.dump(posterior.centered_moment(2), pklfile)
pklfile.close()
########################################################################################################################################
########################################################################################################################################
# Assuming that in the preceding block of code a prior and likelihood are defined the poster can be instantiated
# Posterior definition is not problem spcific
########################################################################################################################################
########################################################################################################################################
# MH Time
########################################################################################################################################
########################################################################################################################################
time = Wtime()
mh_function(posterior)
all_time = Wtime() - time
print("time ", all_time)

print("true mean", posterior._mean)
print("true cov", posterior.centered_moment(2))




