import numpy as np
import sys
# import tensorflow as tf
import os
import pickle as pkl
from mpi4py import MPI 
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
########################################################################################################################################
########################################################################################################################################
#save the state of the generator on rank 0 to
# initialize it to the same value in another run.
########################################################################################################################################
########################################################################################################################################
# Command line arguments to set run specs as well as provide paths for saving location, code linking etc
from argparse import ArgumentParser
parser = ArgumentParser(add_help=True, description="...")

parser.add_argument('-problem_name',dest = 'problem_name',required=True, help="problem name",type=str)
parser.add_argument('-n_dim',dest = 'n_dim',required= True,help='ndim',type = int)

parser.add_argument('-low_rank',dest = 'low_rank',required= True,help='low_rank',type = int)
parser.add_argument('-itermax',dest = 'itermax',required= True,help='iter_max',type = int)

parser.add_argument('-projected',dest = 'projected',required= True,help='projected True or False',type = bool)
parser.add_argument('-n_particles',dest = 'n_particles',required= True,help='n_particles',type = int)

parser.add_argument('-common_projector',dest = 'common_projector', required= True,help='common_projector', type=bool)
parser.add_argument('-over_sample',dest = 'over_sample',required= False,default = 0,help='over_sample',type = int)
parser.add_argument('-line_search',dest = 'line_search',required= False,default = False,help='Armijo Line Search default is true',type = bool)
parser.add_argument('-gradient_termination',dest = 'gradient_termination',required= False,default = False,help='gradient_termination True or False',type = bool)
parser.add_argument('-gauss_newton',dest = 'gauss_newton',required= False,default = False,help='GN FML',type = bool)
parser.add_argument('-svn',dest = 'path_to_svn',required=False,\
        default = '.', help="path to steinvariationmethods/, required!",type=str)
parser.add_argument('-nns',dest = 'path_to_nns',required=False,default = '../nn_stein/', help="path to nn_stein/, required!",type=str)

parser.add_argument('-log_checkpoint_frequency',dest = 'log_checkpoint_frequency',required= False,default = 1,help='threads',type = int)
parser.add_argument('-logger_dir',dest = 'logger_dir',required=False,default = 'logging/', help="logger dir",type=str)
parser.add_argument('-cov_path',dest = 'cov_path',required=False,default = './', help="path to covariance numpy array",type=str)
parser.add_argument('-mean_path',dest = 'mean_path',required=False,default = './', help="path to mean numpy array",type=str)

args = parser.parse_args() #
########################################################################################################################################
########################################################################################################################################
# Import from Stein
sys.path.append(args.path_to_svn)
from distributions import *
import prngs.parallel as random
from stein import *
########################################################################################################################################
########################################################################################################################################
#Set and record random seed for reproducibility
if comm.Get_rank() == 0:
    try:
        state = pkl.load(open('prngs_state.pkl', 'rb'))
    except:
        state = random.get_state()
        pkl.dump(state, open('prngs_state.pkl', 'wb'))
else:
    state = None
random.set_state(state)

####################################################################################################################
def random_mu_and_sigma_fast_decay(n_dim, comm): #just testing random gaussians right now
    if comm is None or comm.Get_rank() == 0:
        mu = random.uniform(-2.,2., size=(n_dim,))
        sigma_inv = np.arange(1, n_dim+1)**(-1.)#4.*np.ones(n_dim)#np.arange(1, n_dim+1)**(-1.)#np.exp(-2.*np.arange(1, n_dim+1)/n_dim)#
    else:
        mu = np.empty(n_dim, dtype='d')
        sigma_inv = np.empty(n_dim, dtype='d')
    comm.Bcast(mu, root=0)
    comm.Bcast(sigma_inv, root=0)
    return mu, sigma_inv

def random_mus_and_sigmas_fast_decay(mu,sigma_inv,n_mix,n_dim, comm): #just testing random gaussians right now
    if comm is None or comm.Get_rank() == 0:
        mus = random.uniform(-2.,2., size=(n_mix,n_dim))#mu + np.random.normal(0.,5*sigma_inv, size=(n_mix,n_dim))##random.uniform(-2.,2., size=(n_mix,n_dim)) #mu + np.random.normal(0.,sigma_inv, size=(n_mix,n_dim))#
        n = np.sort(sigma_inv)
#         n = np.arange(1, n_dim+1)**(-3.)
#         np.random.shuffle(n)
        sigma_invs = np.array([sigma_inv for i in range(n_mix)])
#         np.random.shuffle(sigma_inv) 
#         for i in range(np.int(n_mix/2)):
#             sigma_invs[i] = sigma_inv.copy()+n
            
#         sigma_invs = np.array([sigma_inv for i in range(n_mix)])#+ n
        
        
    else:
        mus = np.empty((n_mix,n_dim), dtype='d')
        sigma_invs = np.empty((n_mix,n_dim), dtype='d')
    comm.Bcast(mus, root=0)
    print(sigma_invs.shape)
    comm.Bcast(sigma_invs, root=0)
    return mus, sigma_invs


n_mix = 10
n_dim = args.n_dim
n_particles = args.n_particles

mu, sigma_inv = random_mu_and_sigma_fast_decay(n_dim, comm)
mus, sigma_invs = random_mus_and_sigmas_fast_decay(mu,sigma_inv,n_mix, n_dim, comm)
# n_mix = np.load('parameters/n_mix.npy')
# n_dim = np.load('parameters/n_dim.npy')
# n_particles = np.load('parameters/n_particles.npy')
# mu = np.load('parameters/mu.npy')
# mus = np.load('parameters/mus.npy')
# sigma_inv = np.load('parameters/sigma_inv.npy')
# sigma_invs = np.load('parameters/sigma_invs.npy')
# args.n_particles = n_particles
# xs = np.load('parameters/xs.npy')


prior = Gaussian(mu,sigma_inv, init_allocations_size=n_particles, comm=comm)
posterior = GaussianMixture(mus, sigma_invs, init_allocations_size=n_particles, comm=comm)
posterior.prior = prior

# problem_name = data_set + str(args['n_filters'][0]) +'_'+ str(len(args['n_filters']))


########################################################################################################################################
########################################################################################################################################
# Assuming that in the preceding block of code a prior and likelihood are defined the poster can be instantiated
# Posterior definition is not problem spcific
########################################################################################################################################
########################################################################################################################################
# STEIN TIME
########################################################################################################################################
########################################################################################################################################
# all_xs = xs.copy()
# local_xs = xs.copy()

try:
    # Loads particles from the Guassian prior from file on rank 0, broadcast, and then copy portion of array for local
    all_xs = load_from_file(n_particles, n_dim, comm)
    local_xs = prior.get_copied_ownership(all_xs, n_particles)
    print("Loaded from file")
except:
    local_xs = prior.sample(n_particles)
    all_xs = prior.allgather(local_xs) #the prior knows how many samples are in all the local_xs
    # Saves the samples from rank 0 for reproducibility for our tests
    save_to_file(all_xs, comm)
    print("New samples, saved to file")
    print("all_xs",all_xs.shape[0])
    print("my_portion", local_xs.shape[0])

if args.projected:
    problem_name = args.problem_name + '_projected'
else:
    problem_name = args.problem_name

settings = SteinSettings()

settings['n_particles'] = args.n_particles
settings['problem_name']  = problem_name
settings['logger_dir'] = args.logger_dir
settings['log_checkpoint_frequency'] = args.log_checkpoint_frequency
settings['low_rank'] = args.low_rank
settings['itermax'] = args.itermax
settings['optimize_in_subspace'] = args.projected
settings['gradient_termination'] = args.gradient_termination
settings['line_search'] = args.line_search
settings['over_sample'] = args.over_sample
settings['gauss_newton'] = args.gauss_newton
settings['common_projector'] = args.common_projector
print('setting', args.common_projector)
# settings['cov_path'] = args.cov_path
# settings['mean_path'] = args.mean_path

Stein(prior,posterior,all_xs, local_xs,settings, comm)
########################################################################################################################################
########################################################################################################################################
