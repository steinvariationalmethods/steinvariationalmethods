import numpy as np
import scipy
import time
import scipy.sparse as spspa
import sys
import tensorflow as tf
import os
import pickle as pkl
from mpi4py import MPI 
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
########################################################################################################################################
########################################################################################################################################
#save the state of the generator on rank 0 to
# initialize it to the same value in another run.
########################################################################################################################################
########################################################################################################################################
# Command line arguments to set run specs as well as provide paths for saving location, code linking etc
from argparse import ArgumentParser
parser = ArgumentParser(add_help=True, description="-b batch size (int) -p population_size (int) -alpha (float)")
parser.add_argument('-svn',dest = 'path_to_svn',required=False,\
 		default = '.', help="path to steinvariationmethods/, required!",type=str)
parser.add_argument('-nns',dest = 'path_to_nns',required=False,default = '../nn_stein/', help="path to nn_stein/, required!",type=str)
parser.add_argument('-data_set',dest = 'data_set',required=False, default = 'mnist', help="data set specification",type=str)
parser.add_argument('-n_threads',dest = 'n_threads',required= False,default = 1,help='threads',type = int)
parser.add_argument('-n_particles',dest = 'n_particles',required= False,default = 16,help='threads',type = int)
parser.add_argument("-n_filters", dest='n_filters',nargs='+',default = [2,2], required=False, help="n filters for conv",type=int)
parser.add_argument("-filter_sizes", dest='filter_sizes',nargs='+',default = [4,4], required=False, help="filter sizes for conv",type=int)
parser.add_argument('-log_checkpoint_frequency',dest = 'log_checkpoint_frequency',required= False,default = 1,help='threads',type = int)
parser.add_argument('-logger_dir',dest = 'logger_dir',required=False,default = 'logging/', help="logger dir",type=str)
parser.add_argument('-problem_name',dest = 'problem_name',required=False,default = 'my_problem', help="problem name",type=str)

args = parser.parse_args() #
########################################################################################################################################
########################################################################################################################################
# Import from Stein
sys.path.append(args.path_to_svn)
from distributions import *
import prngs.parallel as random
from datastructures import *
from metrics import *
from kernels import *
from algorithms import *
from misc import *
########################################################################################################################################
########################################################################################################################################
#Set and record random seed for reproducibility
if comm.Get_rank() == 0:
    try:
        state = pkl.load(open('prngs_state.pkl', 'rb'))
    except:
        state = random.get_state()
        pkl.dump(state, open('prngs_state.pkl', 'wb'))
else:
    state = None
random.set_state(state)
########################################################################################################################################
########################################################################################################################################
########################################################################################################################################
# BEGIN PRIOR DEFINITION AND PROBLEM SPECIFIC CODE
# Define prior likelihood posterior etc.
# These are problem specific
########################################################################################################################################
########################################################################################################################################
# Define what should be declared in the following problem definition code block
# Make Problem class? Pass problem into Stein iteration function that takes dictionary of run specs?
########################################################################################################################################
# Import from tensorflow library
sys.path.append(args.path_to_nns)
from mnist import load_mnist
from tfLikelihood import tfLikelihood
from neuralNetwork import GenericCED, GenericCAE

def xavier_gaussian_prior(likelihood,comm):
    w_dimension = likelihood.dims
    n_layers = likelihood.n_layers
    w_dims = likelihood.w_dims

    # Mean vector for weights
    mu = np.zeros(w_dimension)
    # Diagonal covariance for weights (represented as vector)
    varinv_coefficients = []
    weight_var = 1.
    bias_var = 1.
    for i in range(n_layers):
        varinv_coefficients.append(weight_var)
        varinv_coefficients.append(bias_var)
        weight_var *=0.5
        bias_var *= 0.5

#     print(varinv_coefficients)
    sigma_inv = np.zeros(w_dimension)
    index = 0
    for coef,dim in zip(varinv_coefficients,w_dims):
        sigma_inv[index:index+dim] = coef*np.ones(dim)
        index += dim
    return  Gaussian(mu,sigma_inv,comm = comm)

N_data = 500
data_set = args.data_set

if data_set == 'mnist':

    xs = np.load(args.path_to_nns+'/mnist/mnist_all_images'+str(rank)+'.npy') 
    ys = np.load(args.path_to_nns+'/mnist/mnist_all_images_noise'+str(rank)+'.npy')
    sigma_noise = np.load(args.path_to_nns+'/mnist/mnist_sigma_noise'+str(rank)+'.npy')

elif data_set == 'cahn_hill':
    xs = np.load(args.path_to_nns+'/cahn_hill/cahn_hill_all_u0s'+str(rank)+'.npy') 
    ys = np.load(args.path_to_nns+'/cahn_hill/cahn_hill_all_uTs'+str(rank)+'.npy')
    sigma_noise = np.load(args.path_to_nns+'/cahn_hill/cahn_hill_sigma_noise'+str(rank)+'.npy')

input_shape = [None]+list(xs[0].shape)
output_shape = [None]+list(ys[0].shape)
# print(input_shape)

settings = {}
settings['input_shape'] = input_shape
settings['output_shape'] = output_shape
settings['n_filters'] = args.n_filters
settings['filter_sizes'] = args.filter_sizes
settings['sigma_noise'] = sigma_noise
settings['n_threads'] = args.n_threads
settings['dtype'] = tf.float32
settings['loss_type'] = 'least_squares'
data = [xs,ys]

likelihood = tfLikelihood(settings,GenericCED,data,comm = comm)
if rank == 0:
    print('Size of configuration space',likelihood.dims)
# Sample from "Xavier Gaussian Prior"
prior = xavier_gaussian_prior(likelihood,comm)
n_particles = args.n_particles
n_dim = likelihood.dims

# problem_name = data_set + str(args['n_filters'][0]) +'_'+ str(len(args['n_filters']))
problem_name = args.problem_name

########################################################################################################################################
########################################################################################################################################
# Assuming that in the preceding block of code a prior and likelihood are defined the poster can be instantiated
# Posterior definition is not problem spcific
posterior = Posterior(prior,likelihood)

########################################################################################################################################
########################################################################################################################################
# Begin Stein Algorithm
# The following is general
########################################################################################################################################
########################################################################################################################################
# Stein Initialization
########################################################################################################################################
#Set and record random seed for reproducibility
if comm.Get_rank() == 0:
    try:
        state = pkl.load(open('prngs_state.pkl', 'rb'))
    except:
        state = random.get_state()
        pkl.dump(state, open('prngs_state.pkl', 'wb'))
else:
    state = None
random.set_state(state)
try:
    # Loads particles from the Guassian prior from file on rank 0, broadcast, and then copy portion of array for local
    all_xs = load_from_file(n_particles, n_dim, comm)
    local_xs = prior.get_ownership(all_xs)
    print("Loaded from file")
except:
    local_xs = prior.sample(n_particles)
    all_xs = prior.allgather(local_xs) #the prior knows how many samples are in all the local_xs
    # Saves the samples from rank 0 for reproducibility for our tests
    save_to_file(all_xs, comm)
    print("New samples, saved to file")
    print("all_xs",all_xs.shape[0])
    print("my_portion", local_xs.shape[0])


measure = prior.centered_moment(0)
mean = prior.centered_moment(1)  
cov = prior.centered_moment(2)  
# print("prior measure", measure)
# print("prior mean",mean)
# print("prior mean shape",mean.shape)

# Define run specs
# Should this be implemented as a dictionary and read in from file?

itermax = 10000
stepsize = 1
new_subspace = True #if optimize_in_subspace is False, new_subspace must be True
common_projector = False
preconditioner = True #Only takes effect if optimize_in_subspace is False
num_new_per_sample = 0
add_particle_at = 1000000

#LINE SEARCH PARAMETERS
line_search = True
max_backtracking_iter = 10
c_armijo = 1e-4
gdm_tolerance = 1e-18
r, p = 30, 5 #oversampling, lowrank
local_N, N, d = local_xs.shape[0], all_xs.shape[0], n_dim #abbreviations

optimize_in_subspace = True

# metrics = HessianMetric(model, optimize_in_subspace, init_allocations_size=N, low_rank=r)
metric = AveragedHessianMetric(posterior, project=optimize_in_subspace, init_allocations_size=local_N, low_rank=r)
kernel = GaussianKernel(metric, init_allocations_size=(local_N, N)) 

if not optimize_in_subspace:
    #Prior Covariance Operator, i.e. H^{-1} applys
    PriorCovPreconditioner = PriorCovarianceLinearOperator(prior.sigma)                  #identical on each rank
#Time ALL major steps going forward!!!
# Initialise average computational time
timeave = 0;

########################################################################################################################################
########################################################################################################################################
# Array Allocations
if optimize_in_subspace:
    #Only need to change these if r changes!!!
    mgJ = np.empty((local_N, r)) #local and global versions?
    HJ = np.empty((local_N, r, r)) #local and global versions?
    post_hess = np.empty((local_N, r, r)) #resize????????
    post_grad = np.empty((local_N, r)) #local and broadcasted?
    galerkin_newton_step = np.empty((local_N,r))
    Q = np.empty((local_N, r))
    lmbda_local = np.empty((local_N, r))            
    Ut_s_local = np.empty((local_N, r, d))
    # Global arrays
    global_post_grad = np.empty((N,r))
    global_post_hess = np.empty((N,r,r))
    global_grad_kernel = np.empty((N,N,r))
    global_sum_grad_kernel_cols = np.empty((N,r))
    global_Q = np.empty((N,r))
    lmbda_global = np.empty((N, r))            
    Ut_s_global = np.empty((N, r, d))
            
else:
    mgJ = np.empty((N, d))
    HJ = np.empty((N, d, d))
    post_grad = np.empty((N, d))
    galerkin_newton_step = np.empty((N, d))
    Q = np.empty((N, d))
#     post_hess = np.empty((N, posterior.dims, posterior.dims))
    # Global arrays
    # Josh there are probably more global arrays needed here?
    global_post_grad = np.empty((N, d))

step_norm_old = np.inf
########################################################################################################################################
########################################################################################################################################
# Logging

logger = {}
logger['n_particles'] = n_particles
logger['ndim'] = n_dim
logger ['name'] = problem_name
# To be logged at each iteration on rank 0
logger['iteration_duration'] = {}
logger['mean_error'] = {}
logger['covariance_error'] = {}
logger['ave_neg_logpdf'] = {}
logger['dkl_gradient_norm'] = {}
logger['alpha'] = {}
logger['avg_newton_step_norm'] = {}

if not optimize_in_subspace:
    logger['prior_cov_prec_step_norm'] = {}

logger_name = args.logger_dir+problem_name
if not os.path.isdir(args.logger_dir):
    try:
        os.makedirs(args.logger_dir)
    except:
        pass

########################################################################################################################################
# If using pandas:
try:
    import pandas as pd
    def save_logger(logger,out_name):
        logger_df = pd.DataFrame(logger)
        logger_df.to_csv(out_name+'logger.csv')
        print('Saved file to '+out_name+'logger.csv')
# If not, numpy 
except:
    print('Implement a logger saver that doesnt use pandas or (conda or pip) install pandas will take care of that issue')
    pass

########################################################################################################################################
########################################################################################################################################
# Begin Stein Iteration
for k in range(itermax):
    tic = MPI.Wtime()
    if comm.Get_rank() == 0:
        print("Begin calculation of gradient at iteration", k)
    # Calculate gradient
    grad_neg_log_pdfs, neg_log_pdfs = posterior.grad_neg_logpdf(local_xs, projections=(not new_subspace))  #NOT ACCMULATED GRADIENT AND NEGLOGPDF! (LOCAL)
    print('Gradient calculation took ', MPI.Wtime() - tic, 's on rank',rank)
    if optimize_in_subspace:
###############################Subspace Projection#####################################################################  
        if new_subspace:
            randomized_posterior_hess_eigendecomp(prior, local_xs, metric.random_projection, r, lmbda_local, Ut_s_local, posterior)
            #unless we change r, in the line above, we dont need to do reallocations ever.
            if common_projector:
                #do all this on rank 0
                ind = np.unravel_index(np.argsort(-lmbda_s, axis=None), lmbda_s.shape)
                x_choose = ind[0][0:r]
                col_choose = ind[1][0:r]
                Ut_choose = Ut_s[x_choose,col_choose,:]
                orthogonal_basis = scipy.linalg.orth(Ut_choose.T).T
                print(orthogonal_basis.shape)
                Ut_s[:] = orthogonal_basis  #does this work in parallel?
            #Set the new projector(s) for the xs
            local_xs = splitspace_vectors(local_xs, Ut_s_local) 
            # Gather Ut_s
            allgather(Ut_s_local, Ut_s_global,comm)
            all_xs = splitspace_vectors(all_xs, Ut_s_global)
            print("now a split space thing")
####################Prepare for MainAlgorithm##############################################################################   
            #assign the matrix for the metric"
            for i in range(local_N):
                post_hess[i] = np.diag(lmbda_local[i])
                post_grad[i] = local_xs.projectors[i] @ grad_neg_log_pdfs[i]
            kernel.metric.matrices = post_hess           
    else:
        post_grad[:] = grad_neg_log_pdfs
####################MainAlgorithm##############################################################################   
    # Calculate Kernel gram matrix
    grad_kernel, kernel_matrix = kernel.calculate_grad_and_gram_matrix(local_xs, all_xs, projections=optimize_in_subspace)
    # Gather the grad_kernel
    allgather(grad_kernel, global_grad_kernel,comm)
    print('Shape of grad_kernel should be')
    if optimize_in_subspace:
        post_hess[:] = kernel.metric.matrices 
    else:
        lmbda_s[:], Ut_s[:] = kernel.metric.matrices
    sum_kernel_rows = np.sum(kernel_matrix, axis=1, keepdims=True) # Nx 1, could keep is 1 x N if need be.        
#         if comm is not None and comm.Get_rank() ==0:
#             mgJ[:] = post_grad #copy the gradient onto each worker,
    # All gather the post grad
    allgather(post_grad, global_post_grad,comm)
    # Calculate - gradient(J)
    print('||mjg|| before', np.linalg.norm(mgJ))
    for i in range(local_N): #THE N BELOW SHOULD BE THE GLOBAL N!
        mgJ[i] = -(kernel_matrix[i] @ global_post_grad)/N + np.mean(grad_kernel[i] , 0) #mean on each process filling in rows of mgJ
    print('||mjg|| after', np.linalg.norm(mgJ))
    sum_grad_kernel_cols  = np.sum(-grad_kernel, axis=1)/sum_kernel_rows  #need the sum of hte kernel rows on each process/
    # Gather sum_grad_kernel_cols
    allgather(sum_grad_kernel_cols,global_sum_grad_kernel_cols,comm)
    # Calculate Hessian (dense or Low Rank Eigendecomposition)
    if optimize_in_subspace:
        # Create dense Hessians and do r x r Hessian solves 
        # Gather post_hess
        allgather(post_hess, global_post_hess,comm)
        for i in range(local_N): #need to accumulate post hess??????
            HJ[i] = (global_post_hess.T @ (kernel_matrix[i]) + global_grad_kernel[:,i].T @ global_sum_grad_kernel_cols)/N #grad_kernel[i].T @ sum_grad_kernel_col/N can be precomputed ans passed in
    else:
        # Low rank action for CG solves
        HJ_ActionOps = [None] * N
        for i in range(local_N):
            #NOT COMPLETE!!!, requires working out  #LowRankEigendecompLinearOperator better
            HJ_ActionOps[i] = LowRankEigendecompLinearOperator(Ut_s[i], lmbda_s[i], kernel_matrix[i], sum_k, grad_kernel[i].T, )#define the Hessian Action Operators
    # Solve the Newton system for the step (LU or CG)
    if optimize_in_subspace:
        for i, (Hessian_i, minus_gradient_i) in enumerate(zip( HJ, mgJ)):
            Q[i] = np.linalg.solve(Hessian_i, minus_gradient_i)
    else:
        pass
        ## PLEASE FIX ME
        # for newton_step_i, HessianActionOp_i, minus_gradient_i in zip(Q, HJ_ActionOps, mgJ):
        #     newton_step_i, info = spspa.linalg.cg(HessianActionOp_i, minus_gradient_i, tol=1e-05, maxiter=10, M=PriorCovPreconditioner)
    # All gather the Qs after the computation
    allgather(Q,global_Q,comm)
    # Galerkin projection of the step (normalized K)
    for i in range(local_N):
        galerkin_newton_step[i] = (kernel_matrix[i].T @ global_Q)/sum_kernel_rows[i]
    # Line search Globalization
    if line_search:
        alpha = 1.
        terminate = False
        average_cost_old = Reduce_mean_scalars_to_0(neg_log_pdfs,  comm)
        for _ in range(max_backtracking_iter):
            if optimize_in_subspace:
                trial_x = splitspace_vectors(local_xs.full,local_xs.projectors)
            else:
                trial_x = local_xs
            trial_x += alpha * galerkin_newton_step 

            local_cost_new, local_grad_inner_step = posterior.neg_logpdf(trial_x), np.sum(post_grad*galerkin_newton_step, axis =1) #post_grad or mgJ???            
            average_grad_inner_step = Reduce_mean_scalars_to_0(local_grad_inner_step, comm)
            average_cost = Reduce_mean_scalars_to_0(local_cost_new, comm)
            if comm is None or comm.Get_rank() == 0:
                if average_cost < average_cost_old - alpha*c_armijo*average_grad_inner_step or (-average_grad_inner_step <= gdm_tolerance):
                    terminate = True
            terminate = comm.bcast(terminate, root=0) if comm is not None else terminate
            if terminate:
                break
            else:
                alpha *= 0.5
                print("old cost ", average_cost_old, "new cost ", average_cost,"armijo_condition ",alpha*c_armijo*average_grad_inner_step,"gdm ",average_grad_inner_step)
        else:
            if comm is not None and comm.Get_rank() ==0:
                print("Warning, backtracked to max backtracking iters")
    #Take Newton Step: Update the particle locations
    # In place add has been checked and works
    local_xs += alpha*galerkin_newton_step
    #HOW DO WE DO THIS, because need to automatically do the projection etc
    #need to accumulate the all_xs and then assign all_xs.coeffs
    # Gather the _xs 
    allgather(local_xs.coeffs, all_xs.coeffs,comm)
    allgather(local_xs.projectors, all_xs.projectors,comm)

    #update the values???
    # Access convergence
    step_norm = np.mean(np.linalg.norm(galerkin_newton_step, axis=1)*alpha)
    average_step_norm = Allreduce_mean(step_norm, comm)
    if np.abs(step_norm_old - average_step_norm)<0.5:
        if optimize_in_subspace:
            new_subspace = True
            print("New subspace at iteration ", k)
    step_norm_old = average_step_norm
    if optimize_in_subspace and new_subspace:
        local_xs = local_xs.full    
        all_xs = all_xs.full
    average_neg_log_pdf = Allreduce_mean(neg_log_pdfs,  comm)
    # average_neg_log_pdf = np.mean(Reduce_mean_scalars_to_0(neg_log_pdfs,  comm))
    grad_norm = np.linalg.norm(mgJ)
    average_grad_norm = Allreduce_mean(grad_norm, comm)
    avg_step_norm = Allreduce_mean(step_norm,comm)

    logger['ave_neg_logpdf'][k] = average_neg_log_pdf[0]
    logger['dkl_gradient_norm'][k] = average_grad_norm[0]
    logger['avg_newton_step_norm'][k] = avg_step_norm
    logger['alpha'][k] = alpha
    logger['iteration_duration'] = MPI.Wtime() - tic
    print('it',k,'rank',comm.Get_rank(),"step_norm:", step_norm,"ave_neg_logpdf:", average_neg_log_pdf[0], "grad_norm", average_grad_norm[0],"stepsize:", alpha)
    if k == itermax:
        print('Maximum number of iterations has been reached.\n')
    if (comm.Get_rank() ==0) and (k% args.log_checkpoint_frequency == 0):
        save_logger(logger,logger_name)
########################################################################################################################################
########################################################################################################################################
timeave += MPI.Wtime() - tic
timeave = timeave / itermax;
print("time cost ", timeave)
if comm.Get_rank() == 0:
    save_logger(logger,logger_name)