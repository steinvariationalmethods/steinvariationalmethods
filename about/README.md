# stein variational methods

Implements stein variational methods as well as other traditional and state-of-the-art methods for bayesian inference, particularly, posterior sampling

# Requirements
- [emcee](http://dfm.io/emcee/current/)
- [pyhmc](https://pythonhosted.org/pyhmc/)
- [NUTS in Python](https://github.com/mfouesneau/NUTS)
- `numpy`, `matplotlib`, `scipy`, etc. Standard packages for scientific work
in Python.

