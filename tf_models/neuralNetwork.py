from __future__ import absolute_import, division, print_function
import numpy as np
import math
import sys
import tensorflow as tf

def genericCED(x,architecture,seed = 0, dtype = tf.float32):
	# input_shape = input_shape
	n_filters = architecture['n_filters']
	filter_sizes = architecture['filter_sizes']
	# x = tf.placeholder(
	#     tf.float32, input_shape, name='x')
	x_shape = tf.shape(x)
	batch_size = x_shape[0]
	# if len(x_shape) ==3:
	# 	current_input = tf.reshape(x,x_shape+[1])
	# else:
	current_input = x
	batch_size = tf.shape(x)[0]
	n_layers = len(n_filters)
	# %%
	# Build the encoder
	encoder = []
	shapes = []
	for layer_i, n_output in enumerate(n_filters[1:]):
		n_input = current_input.get_shape().as_list()[3]
		shapes.append(current_input.get_shape().as_list())
		W = tf.Variable(
			tf.random_uniform([
				filter_sizes[layer_i],
				filter_sizes[layer_i],
				n_input, n_output],
				-1.0 / math.sqrt(n_input),
				1.0 / math.sqrt(n_input),seed = seed))
		b = tf.Variable(tf.zeros([n_output]))
		encoder.append(W)
		output = tf.nn.softmax(
			tf.add(tf.nn.conv2d(
				current_input, W, strides=[1, 2, 2, 1], padding='SAME'), b))
		current_input = output
	# %%
	# store the latent representation
	z = current_input
	encoder.reverse()
	shapes.reverse()
	# %%
	# Build the decoder using the same weights
	for layer_i, shape in enumerate(shapes):

		n_input = current_input.get_shape().as_list()[3]
		W = tf.Variable(
			tf.random_uniform(tf.shape(encoder[layer_i]),
				-1.0 / math.sqrt(n_input),
				1.0 / math.sqrt(n_input),seed = seed))
		# W = encoder[layer_i]
		b = tf.Variable(tf.zeros([W.get_shape().as_list()[2]]))
		if layer_i == n_layers -2:
			# print(layer_i,'Last layer identity')
			output = tf.identity(tf.add(
				tf.nn.conv2d_transpose(
					current_input, W,
					tf.stack([batch_size, shape[1], shape[2], shape[3]]),
					strides=[1, 2, 2, 1], padding='SAME'), b))

		else:
			# print(layer_i,'Inner layer softmax')
			output = tf.nn.softmax(tf.add(
				tf.nn.conv2d_transpose(
					current_input, W,
					tf.stack([batch_size, shape[1], shape[2], shape[3]]),
					strides=[1, 2, 2, 1], padding='SAME'), b))



		current_input = output
	# %%
	# now have the reconstruction through the network
	y = current_input
	return y

def genericBNCED(x,architecture,seed = 0, epsilon = 1e-8, dtype = tf.float32):
	# input_shape = input_shape
	n_filters = architecture['n_filters']
	filter_sizes = architecture['filter_sizes']
	# x = tf.placeholder(
	#     tf.float32, input_shape, name='x')
	x_shape = tf.shape(x)
	batch_size = x_shape[0]
	# if len(x_shape) ==3:
	# 	current_input = tf.reshape(x,x_shape+[1])
	# else:
	current_input = x
	batch_size = tf.shape(x)[0]
	n_layers = len(n_filters)
	# %%
	# Build the encoder
	encoder = []
	shapes = []
	for layer_i, n_output in enumerate(n_filters[1:]):
		n_input = current_input.get_shape().as_list()[3]
		shapes.append(current_input.get_shape().as_list())
		W = tf.Variable(
			tf.random_uniform([
				filter_sizes[layer_i],
				filter_sizes[layer_i],
				n_input, n_output],
				-1.0 / math.sqrt(n_input),
				1.0 / math.sqrt(n_input),seed = seed))
		b = tf.Variable(tf.zeros([n_output]))
		encoder.append(W)
		output = tf.nn.softmax(
			tf.add(tf.nn.conv2d(
				current_input, W, strides=[1, 2, 2, 1], padding='SAME'), b))
		current_input = output
	# %%
	# store the latent representation
	z = current_input
	encoder.reverse()
	shapes.reverse()
	# %%
	# Build the decoder using the same weights
	for layer_i, shape in enumerate(shapes):

		n_input = current_input.get_shape().as_list()[3]
		W = tf.Variable(
			tf.random_uniform(tf.shape(encoder[layer_i]),
				-1.0 / math.sqrt(n_input),
				1.0 / math.sqrt(n_input),seed = seed))
		# W = encoder[layer_i]
		if layer_i == n_layers -2:
			# print(layer_i,'Last layer identity')
			z = tf.nn.conv2d_transpose(
					current_input, W,
					tf.stack([batch_size, shape[1], shape[2], shape[3]]),
					strides=[1, 2, 2, 1], padding='SAME')
			batch_mean, batch_var = tf.nn.moments(z,[0,1,2])
			z_hat = (z - batch_mean)/tf.sqrt(batch_var + epsilon)
			scale1 = tf.Variable(tf.ones(z_hat.shape[1:]))
			beta1 = tf.Variable(tf.zeros(z_hat.shape[1:]))
			z_hat1 = scale1*z_hat + beta1
			output = tf.identity(z_hat1)

		else:
			# print(layer_i,'Inner layer softmax')
			z = tf.nn.conv2d_transpose(
					current_input, W,
					tf.stack([batch_size, shape[1], shape[2], shape[3]]),
					strides=[1, 2, 2, 1], padding='SAME')
			batch_mean, batch_var = tf.nn.moments(z,[0,1,2])
			z_hat = (z - batch_mean)/tf.sqrt(batch_var + epsilon)
			scale1 = tf.Variable(tf.ones(z_hat.shape[1:]))
			beta1 = tf.Variable(tf.zeros(z_hat.shape[1:]))
			z_hat1 = scale1*z_hat + beta1
			output = tf.identity(z_hat1)



		current_input = output
	# %%
	# now have the reconstruction through the network
	y = current_input
	return y
