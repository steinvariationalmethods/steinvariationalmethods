from . import *
import tensorflow as tf
import numpy as np

def load_mnist_problem(settings, n_train, NN = genericCED,comm = None):

	if NN == logistic_function:
		settings['loss_type'] = 'logistic'
		train_data,test_data,sigma_noise = load_mnist(n_train = n_train,data_type ='.')
	else:
		settings['loss_type'] = 'least_squares'
		train_data,test_data,sigma_noise = load_mnist(n_train = n_train)

	input_shape = [None]+list(train_data[0][0].shape)
	output_shape = [None]+list(train_data[1][0].shape)
	# print(input_shape)
	settings['input_shape'] = input_shape
	settings['output_shape'] = output_shape
	settings['sigma_noise'] = sigma_noise
	settings['dtype'] = tf.float32

	return tfLikelihood(settings,NN,train_data,test_data = test_data,comm = comm)

def load_covertype_problem(settings, n_train, NN = logistic_function,path = '.',comm = None):

	settings['loss_type'] = 'logistic'
	train_data,test_data = load_covertype(path = path,n_train = n_train)
	xs_shape = train_data[0].shape
	ys_shape = train_data[1].shape

	print("ys shape", ys_shape)
	input_shape = [None]+list(xs_shape[1:])
	output_shape = [None]
	print("input", input_shape,"output", output_shape)
	settings['input_shape'] = input_shape
	settings['output_shape'] = output_shape
	settings['sigma_noise'] = 1e-1
	settings['dtype'] = tf.float32

	return tfLikelihood(settings,NN,train_data, test_data,comm = comm)