from __future__ import absolute_import, division, print_function
import tensorflow as tf
import numpy as np

def logistic_function(x,architecture,seed = 0, dtype = tf.float32):
	input_shape = architecture['input_shape']
	output_shape = architecture['output_shape']
	input_shape = [-1] + input_shape[1:]
	# output_shape = [-1] + output_shape[1:]
	n_inputs = np.prod(input_shape[1:])
	# reshaped_input = [-1,n_inputs]
	x = tf.squeeze(x)

	n_outputs = np.prod(output_shape[1:])

	w = tf.Variable(tf.zeros((n_inputs,n_outputs)),name='logistic_weight')
	b = tf.Variable(tf.zeros(n_outputs),name='logistic_bias',trainable=False)
	# affine = tf.reduce_sum(tf.reshape(x, [-1,n_outputs])* w, -1) + b
	affine = tf.matmul(x,w) + b
	# return tf.reshape(y,output_shape), tf.reshape(affine,output_shape)
	# return tf.reshape(affine,output_shape)
	return tf.nn.sigmoid(affine)
