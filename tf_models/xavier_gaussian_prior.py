import numpy as np 

def xavier_gaussian_prior(likelihood):
    w_dimension = likelihood.dims
    n_layers = likelihood.n_layers
    w_dims = likelihood.w_dims

    # Mean vector for weights
    mu = np.zeros(w_dimension)
    # Diagonal covariance for weights (represented as vector)
    varinv_coefficients = []
    weight_var = 1.
    bias_var = 1.
    for i in range(n_layers):
        varinv_coefficients.append(weight_var)
        varinv_coefficients.append(bias_var)
        weight_var *=0.5 # doing oppossite of what claimed in paper, these are precisions, not variances, if one uses them as in the code right now.
        bias_var *= 0.5 # doing oppossite of what claimed in paper

#     print(varinv_coefficients)
    sigma_inv = np.zeros(w_dimension)
    index = 0
    for coef,dim in zip(varinv_coefficients,w_dims):
        sigma_inv[index:index+dim] = coef*np.ones(dim)
        index += dim
    return mu,sigma_inv