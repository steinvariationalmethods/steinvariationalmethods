#
# This file is part of stein variational inference methods class project.
#


from . import mnist
from .mnist import load_mnist

from . import covertype
from .covertype import load_covertype,load_colon

from . import tfLikelihood
from .tfLikelihood import tfLikelihood
from . import neuralNetwork
from .neuralNetwork import genericCED, genericBNCED
from . import logisticRegression
from .logisticRegression import logistic_function
from . import diagonal_decay_gaussian_prior
from .diagonal_decay_gaussian_prior import diagonal_decay_gaussian_prior
from . import load_tfLikelihood
from .load_tfLikelihood import load_mnist_problem,load_covertype_problem