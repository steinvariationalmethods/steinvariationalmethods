from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import sys
import numpy as np
import random


# from statsmodels import robust

def dir_check(dir):
	try:
		os.stat(dir)
	except:
		os.mkdir(dir)

def load_covertype(path = '.',n_train = 256,n_test = 256,seed = 0):
	import scipy.io
	data = scipy.io.loadmat(path+'/covertype.mat')
	x_input = data['covtype'][:, 1:]
	y_input = data['covtype'][:, 0]
	y_input[y_input == 2] = 0

	x_input = np.hstack([x_input, np.ones([len(x_input), 1])])
	N,dim = x_input.shape
	dim = 5

	# Permute the indices before splitting into train and test?
	# total_population_size = n_train + n_test
	# indices = range(total_population_size)
	# test_indices,train_indices 	= np.split(indices,[n_test])

	# random.Random(seed).shuffle(x_input)
	# random.Random(seed).shuffle(y_input)

	x_train = x_input[0:n_train,0:dim]
	y_train = y_input[0:n_train]

	x_test = x_input[n_train:n_train+n_test,0:dim]
	y_test = y_input[n_train:n_train+n_test]
	
	return [x_train,y_train],[x_test,y_test]

def load_colon(path = '.',n_train = 50,n_test = 12,seed = 0):
	import scipy.io
	data = scipy.io.loadmat(path+'/colon_cancer.mat')
	x_input = data['colon_cancer'][:, 1:]
	y_input = data['colon_cancer'][:, 0]
	y_input[y_input == -1] = 0

	x_input = np.hstack([x_input, np.ones([len(x_input), 1])])
	N,dim = x_input.shape
	# dim = 5

	# Permute the indices before splitting into train and test?
	# total_population_size = n_train + n_test
	# indices = range(total_population_size)
	# test_indices,train_indices 	= np.split(indices,[n_test])

	# random.Random(seed).shuffle(x_input)
	# random.Random(seed).shuffle(y_input)

	x_train = x_input[0:n_train,0:dim]
	y_train = y_input[0:n_train]

	x_test = x_input[n_train:n_train+n_test,0:dim]
	y_test = y_input[n_train:n_train+n_test]
	
	return [x_train,y_train],[x_test,y_test]


