from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import sys
import numpy as np
import random


# from statsmodels import robust

def dir_check(dir):
	try:
		os.stat(dir)
	except:
		os.mkdir(dir)

def load_mnist(noise_percentage = 0.05, data_type = 'bayes_ae',n_train = 10000,n_test = 1000,seed = 0):
	try: 
		# read from file

		train_images = np.load('mnist_train_images.npy')	
		train_images_noise = np.load('mnist_train_images_noise.npy')
		train_labels = np.load('mnist_train_labels.npy')

		test_images = np.load('mnist_test_images.npy')	
		test_images_noise = np.load('mnist_test_images_noise.npy')
		test_labels = np.load('mnist_test_labels.npy')

		sigma = np.load('mnist_sigma_noise.npy')
		print(80*'#')
		print('Successfully loaded MNIST data'.center(80))
		print(80*'#')

	except:
		# write to file
		print(80*'#')
		print('Did not load MNIST data'.center(80))
		print(80*'#')
		from tensorflow.examples.tutorials.mnist import input_data
		mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

		total_population_size = n_train + n_test
		all_data = mnist.train.next_batch(total_population_size)
		all_images, all_labels = all_data

		all_images = np.array(all_images)
		all_images_noise = np.array(all_images)
		all_labels = np.array(all_labels)

		sigma = noise_percentage*(all_images.max() - all_images.min())

		all_images_noise += sigma*np.random.randn(*all_images.shape)

		for i in range(2):
			all_images = np.expand_dims(all_images,axis = -1)
			all_images_noise  = np.expand_dims(all_images_noise,axis = -1)
			# labels = np.expand_dims(labels,axis = -1)

		indices = range(total_population_size)
		test_indices,train_indices 	= np.split(indices,[n_test])

		random.Random(seed).shuffle(all_images)
		random.Random(seed).shuffle(all_labels)

		test_images  = np.array(all_images[test_indices])
		test_images_noise = np.array(all_images_noise[test_indices])
		test_labels = np.array(all_labels[test_indices])

		train_images = np.array(all_images[train_indices])
		train_images_noise = np.array(all_images_noise[train_indices])
		train_labels  = np.array(all_labels[train_indices])


		np.save('mnist_train_images.npy',train_images)	
		np.save('mnist_train_images_noise.npy',train_images_noise)
		np.save('mnist_train_labels.npy',train_labels)

		np.save('mnist_test_images.npy',test_images)	
		np.save('mnist_test_images_noise.npy',test_images_noise)
		np.save('mnist_test_labels.npy',test_labels)

		np.save('mnist_sigma_noise.npy',sigma)


	if data_type == 'bayes_ae':
		return [train_images,train_images_noise], [test_images,test_images_noise], sigma
	else:
		return [train_images,train_labels], [test_images,test_labels], 1.0





