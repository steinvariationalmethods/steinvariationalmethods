import tensorflow as tf
import numpy as np
import sys
sys.path.append('../')
from distributions.distribution_base import _Distribution

def assign_ops(w,v):
	# Assign v to w
	update_ops = []
	w_and_v = list(zip(w,v))
	for w,v in reversed(w_and_v):
		with tf.control_dependencies(update_ops):
			update_ops.append(tf.assign(w, v))
	assignment_op = tf.group(*update_ops)
	return assignment_op

def my_flatten(tensor_list):
	flattened_list = []
	for tensor in tensor_list:
		flattened_list.append(tf.reshape(tensor,[np.prod(tensor.shape)]))
	return tf.concat(flattened_list,axis=0)

def initialize_indices(shapes):
	indices = []
	index = 0
	for shape in shapes:
		index += int(np.prod(shape))
		indices.append(index)
	return indices

def tensor_to_vec(x,shapes):
	# Inputs x a tensor
	storage = None
	for x,shape  in zip(x,shapes):

		x_flat = np.reshape(x,int(np.prod(shape)))
		if storage is None:
			storage = x_flat
		else:
			storage = np.concatenate((storage,x_flat))
	return storage

def vec_to_tensor(x,shapes,indices):
	x = np.squeeze(x)
	storage = []
	index_1 = 0
	for index_2,shape in zip(indices,shapes):
		chunk = x[index_1:index_2]
		index_1 = index_2
		reshaped = np.reshape(chunk,shape)
		storage.append(reshaped)
	return storage

def placeholder_like(shapes,name,dtype):
	placeholder = []
	for shape in shapes:
		placeholder.append(tf.placeholder(dtype,shape,name=name))
	return placeholder

class tfLikelihood(_Distribution):

	def __init__(self, settings, neural_network_fn,train_data, test_data = None, init_allocations_size=1, comm=None):
		#############################################################################################################
		# settings is a property, these features are used in network construction
		self._settings = settings
		input_shape = settings['input_shape']
		dtype = settings['dtype']
		# self._data = data
		# "variables"
		self._x = tf.placeholder(dtype,input_shape,name = "input")

		if self.settings['loss_type'] == 'logistic':
			print("doing alogistic")
			self._y_prediction = neural_network_fn(self._x,settings,dtype = dtype)
		else:
			self._y_prediction = neural_network_fn(self._x,settings,dtype = dtype)

		if settings['loss_type'] == 'autoencoder':
			self._y_true = tf.placeholder(dtype, output_shape,name="output")
			self._feed_dict = {self._x:train_data[0],self._y_true:train_data[1]}
			if test_data is None:
				self._test_dict = None
			else:
				self._test_dict = {self._x:test_data[0],self._y_true:test_data[1]}
		else:
			output_shape = settings['output_shape']
			self._y_true = tf.placeholder(dtype, output_shape,name="output")
			self._feed_dict = {self._x:train_data[0],self._y_true:train_data[1]}
			if test_data is None:
				self._test_dict = None
			else:
				self._test_dict = {self._x:test_data[0],self._y_true:test_data[1]}
		self._w = tf.trainable_variables()
		self._flat_w = my_flatten(self._w)

		# properties of the network
		self._n_layers = len(self._w)//2
		shapes = [tuple(int(wii) for wii in wi.shape) for wi in self._w]
		self._w_shapes = shapes
		self._w_dims = [np.prod(shape) for shape in self.w_shapes]
		# initialize the indices to be used in array transformation
		self._indices = initialize_indices(shapes)
		#############################################################################################################
		# Overall vector dimension for "control parameter" w used in super init, not sure if it ever
		# actually gets used
		dimension = np.sum(self.w_dims)
		super().__init__(dimension, init_allocations_size=init_allocations_size, comm=comm)
		#############################################################################################################
		# Initialize the "loss" function or least squares 'misfit' type term in pdf exponential parametrization
		scale = (1./settings['sigma_noise'])**2
		with tf.name_scope('loss'):
			if self.settings['loss_type'] == 'autoencoder':
				self._loss = scale*tf.reduce_mean(tf.pow(self._x-self._y_prediction,2))
			elif self.settings['loss_type'] == 'least_squares':
				self._loss = scale*tf.reduce_mean(tf.pow(self._y_true-self._y_prediction,2))
			elif self.settings['loss_type'] == 'logistic':
				# self._loss = \
				# tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(\
				# 	labels = self._y_true, logits = self._y_prediction), 1)
				# self. _loss = tf.reduce_sum(tf.log(self._y_true*self._y_prediction + (1.-self._y_true) * (1.-self._y_prediction) + 1e-3))
				self. _loss = -tf.reduce_sum(tf.log(tf.multiply(self._y_true,tf.transpose(self._y_prediction)) + tf.multiply((1.-self._y_true) , tf.transpose((1.-self._y_prediction)) )+ 1e-3))#/tf.reduce_mean(tf.log(tf.multiply(self._y_true,tf.transpose(self._y_prediction) )+ tf.multiply((1.-self._y_true) , tf.transpose((1.-self._y_prediction)) )+ 1e-3))

				# print("loss ", self. _loss)
				# print("get hererererererrerere")



		# Define gradient term
		gradient = tf.gradients(self._loss,self._w, name='gradient')
		self._gradient = my_flatten(gradient)


		# Initialize the vector for Hessian mat-vecs & define (g,dw) inner product
		self._w_hat = tf.placeholder(dtype,(dimension),'w_hat')
		self._gT_w_hat = tf.tensordot(self._w_hat,self._gradient,axes = [[0],[0]])

		# Define GN hessian action H_GN_dw = gg^Tdw
		self._H_GN_w_hat = self._gT_w_hat*self._gradient

		# # Define Hessian action Hdw
		H_w_hat = tf.gradients(self._gT_w_hat,self._w, stop_gradients = self._w_hat)
		self._H_w_hat = my_flatten(H_w_hat)

		# Define the GN quadratic form (dw,H_GNdw)
		self._w_hatT_H_GN_w_hat = tf.tensordot(self._w_hat,self._H_GN_w_hat,axes = [[0],[0]])

		# # Define quadratic form (dw,Hdw)
		self._w_hatT_H_w_hat = tf.tensordot(self._w_hat ,self._H_w_hat,axes = [[0],[0]])

		#############################################################################################################
		# How to manage the tf session?
		# Should this be done in a separate function and called at runtime?

		self._sess = tf.Session(config=tf.ConfigProto(inter_op_parallelism_threads = settings['n_threads'],\
													  intra_op_parallelism_threads = 1))
		init = tf.global_variables_initializer()
		self._sess.run(init)

		# if self.comm is not None:
		# 	self.comm.Barrier()

		#############################################################################################################




	@property
	def settings(self):
		return self._settings

	@property
	def n_layers(self):
		return self._n_layers

	@property
	def w_shapes(self):
		return self._w_shapes

	@property
	def w_dims(self):
		return self._w_dims

	@property
	def indices(self):
		return self._indices

	@property
	def feed_dict(self):
		return self._feed_dict

	@property
	def test_dict(self):
		return self._test_dict


	@property
	def w(self):
		assert self._sess is not None
		w = self._sess.run(self._w)
		return tensor_to_vec(w,self.w_shapes)

	# does it make sense to do via @w.setter?
	def assign_w(self,v):
		if type(v)==np.ndarray:
			v = vec_to_tensor(v,self.w_shapes,self.indices)
		assert self._sess is not None
		update_ops = []
		v_and_w = list(zip(v,self._w))
		for v, w in reversed(v_and_w):
			with tf.control_dependencies(update_ops):
				update_ops.append(tf.assign(w, v))
		assignment_op = tf.group(*update_ops)
		self._sess.run(assignment_op)

	def _assign_w(self,v):
		if type(v)==np.ndarray:
			v = vec_to_tensor(v,self.w_shapes,self.indices)
		assert self._sess is not None
		update_ops = []
		v_and_w = list(zip(v,self._w))
		for v, w in reversed(v_and_w):
			with tf.control_dependencies(update_ops):
				update_ops.append(tf.assign(w, v))
		assignment_op = tf.group(*update_ops)
		return assignment_op


	def forward(self, ws):
		assert self._sess is not None
		out = []
		for w in ws:
			w = vec_to_tensor(w,self.w_shapes,self.indices)
			self.assign_w(w)
			fwd = self._sess.run(self._y_prediction,self.feed_dict)
			out.append(fwd)
		out = np.array(out)
		return out
	def ytrue(self, ws):
		assert self._sess is not None
		out = []
		for w in ws:
			w = vec_to_tensor(w,self.w_shapes,self.indices)
			self.assign_w(w)
			fwd = self._sess.run(self._y_true,self.feed_dict)
			out.append(fwd)
		out = np.array(out)
		return out
	def _pdf_impl(self, ws, result):
		assert self._sess is not None
		out = []
		for w in ws:
			w = vec_to_tensor(w,self.w_shapes,self.indices)
			assignment_ops = assign_ops(self._w,w)
			with tf.control_dependencies([assignment_ops]):
				loss_after_assignment = tf.identity(self._loss)
			_,temp = self._sess.run([assignment_ops,loss_after_assignment],self.feed_dict)
			# temp = tensor_to_vec(temp,self.w_shapes)
			out.append(np.exp(-1.0*temp))
		result[:] = out

	def _neg_logpdf_impl(self, ws, result=None):
		assert self._sess is not None
		if np.size(ws.shape) == 2 and result is not None:#type(ws) is list and 
			out = []
			for w in ws:
				w = vec_to_tensor(w,self.w_shapes,self.indices)
				self.assign_w(w)
				loss = self._sess.run(self._loss,self.feed_dict)
				out.append(loss)
			out = np.array(out)
			result[:] =out
		else:
			w = vec_to_tensor(ws,self.w_shapes,self.indices)
			self.assign_w(w)
			loss = self._sess.run(self._loss,self.feed_dict)
			result = loss
			return loss

	def _neg_logpdf_test_impl(self, ws, result=None):
		assert self._sess is not None
		assert self._test_dict is not None
		if np.size(ws.shape) == 2 and result is not None:#type(ws) is list and 
			out = []
			for w in ws:
				w = vec_to_tensor(w,self.w_shapes,self.indices)
				self.assign_w(w)
				loss = self._sess.run(self._loss,self.test_dict)
				out.append(loss)
			out = np.array(out)
			result[:] = out
		else:
			w = vec_to_tensor(ws,self.w_shapes,self.indices)
			self.assign_w(w)
			loss = self._sess.run(self._loss,self.test_dict)
			result = loss
			return loss


	def _grad_neg_logpdf_impl(self, ws, neg_logpdf_result, grad_neg_logpdf_result):
		assert self._sess is not None
		gradients = []
		losses = []
		for w in ws:
			w = vec_to_tensor(w,self.w_shapes,self.indices)
			self.assign_w(w)
			loss,gradient = self._sess.run([self._loss,self._gradient],self.feed_dict)
			gradients.append(gradient)
			losses.append(loss)
			
		gradients = np.array(gradients)
		grad_neg_logpdf_result[:] = gradients

		neg_logpdf_result[:] = np.array(losses)


	def _hess_neg_logpdf_action_impl(self, w, Omega, result, idx=None):
		assert self._sess is not None
		Hdws = []
		w = vec_to_tensor(w,self.w_shapes,self.indices)
		self.assign_w(w)
		for omega in Omega:
			self._feed_dict[self._w_hat] = omega
			Hdws.append(self._sess.run(self._H_w_hat,self.feed_dict))
		result[:] = np.array(Hdws)

	def _ggT_neg_logpdf_action_impl(self, w, Omega, result, idx=None):
		assert self._sess is not None
		ggT_omegas = []
		w = vec_to_tensor(w,self.w_shapes,self.indices)
		self.assign_w(w)
		g = self._sess.run(self._gradient,self._feed_dict)
		for omega in Omega:
			gT_omega = np.inner(g,omega)
			ggT_omegas.append(g*gT_omega)
		result[:] = np.array(ggT_omegas)

	def _hess_neg_logpdf_action_gn_impl(self, w, Omega, result, idx=None):
		assert self._sess is not None
		Hdws = []
		w = vec_to_tensor(w,self.w_shapes,self.indices)
		self.assign_w(w)
		for omega in Omega:
			self._feed_dict[self._w_hat] = omega
			Hdws.append(self._sess.run(self._H_GN_w_hat,self.feed_dict))
		result[:] = np.array(Hdws)


	def _hess_neg_logpdf_quadratic_form_impl(self, w, ys, hessaction_ys_result, result, idx=None):
		assert self._sess is not None
		Hdws = []
		dwHdws = []
		w_tensor = vec_to_tensor(w,self.w_shapes,self.indices)
		self.assign_w(w_tensor)
		diffs = w - ys
		diffs = np.squeeze(diffs)
		for diff in diffs:
			self._feed_dict[self._w_hat] = diff
			Hdw_,dwHdw_ = self._sess.run([self._H_w_hat,self._w_hatT_H_w_hat],self.feed_dict)
			Hdws.append(np.array(Hdw_))
			dwHdws.append(np.array(dwHdw_))
		result[:] = -0.5*np.array(dwHdws)
		hessaction_ys_result[:] = np.array(Hdws)

	def _hess_neg_logpdf_quadratic_form_gn_impl(self, w, ys, hessaction_ys_result, result, idx=None):
		assert self._sess is not None
		Hdws = []
		dwHdws = []
		w_tensor = vec_to_tensor(w,self.w_shapes,self.indices)
		self.assign_w(w_tensor)
		diffs = w - ys
		diffs = np.squeeze(diffs)
		for diff in diffs:
			self._feed_dict[self._w_hat] = diff
			Hdw_,dwHdw_ = self._sess.run([self._H_GN_w_hat,self._w_hatT_H_GN_w_hat],self.feed_dict)
			Hdws.append(np.array(Hdw_))
			dwHdws.append(np.array(dwHdw_))
		result[:] = -0.5*np.array(dwHdws)
		hessaction_ys_result[:] = np.array(Hdws)



