#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein_variational_inference.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#

# from pairwise_fortran import diff_pairwise
from misc import assign_indices

import numpy as np
from numba import jit

@jit(nopython=True, cache=True, fastmath=True)
def diff_pairwise(pts, diffs):
    """
    Compute differences between each pair of pts stored as rows in input
    array `pts` and store in `diffs`. `pts` should have shape (npts, dim) and
    diffs has shape (npts*(npts-1)//2, dim). For performance, both should be
    in row major order.
    """
    npts = pts.shape[0]
    dim = pts.shape[1]
    indx = 0

    for i1 in range(npts-1):
        pt_i1 = pts[i1, 0:dim]
        for i2 in range(i1+1, npts):
            pt_i2 = pts[i2, 0:dim]
            diffs[indx, 0:dim] = pt_i1 - pt_i2
            indx += 1
    return diffs

def get_ij_from_linear_idx(k, n, cached_val):
    i = n - 2 - np.int(np.sqrt(-8*k + cached_val) /2.0 - 0.5)
    j = k + i + 1 - n*(n-1)//2 + (n-i)*((n-i)-1)//2
    return (i, j)
vectorized_get_ij_from_linear_idx = np.vectorize(get_ij_from_linear_idx)

class PairwiseDifferences: #consider subclassing np.ndarray, so that we can index nicely? then use 
    #something like this: https://stackoverflow.com/questions/2572916/numpy-smart-symmetric-matrix
    #Not sure if it will remove a lot of the stuff in this code below if we subclass.
    
    """
    Class wrapping the in-memory representation of the pairwise differences of a set
    of pts as a dense n*(n-1)//2 x dim array.
    """
    def __init__(self, dim, npts=1, comm=None):
        """
        __init__(self, dim, npts=1, comm=None)

        npts: The number of pts that will be differenced. Used for creating the memory buffer. The
        size of npts and the memory buffer may change (be reallocated) if pts is set to points with pts.shape[0] != pts
        dim: The second dimension of the numpy ndarray buffer. Used for checking size of
        function arguments and creating the memory buffer.

        A (n*(n-1)//2, dim) ndarray will be used for storage for all of the pairwise differences.
        The numpy ndarray *may* be a shared memory array.
        """

        # caching values so that indexing wont be expensive
        self._npts = npts
        self._compute_cached_fn_of_n_pts()
        self._compute_n_diffs_from_n_pts()
        self._pts = None #set manually
        self._dim = dim
        self._comm = comm
        self._diffs = self._init_array()

    @property
    def comm(self):
        return self._comm
    
    @property
    def shape(self):
        return self._diffs.shape

    def _compute_cached_fn_of_n_pts(self):
        #call after changing self._npts
        self._cached_fn_of_n_pts = 4*self._npts*(self._npts-1)-7

    def _compute_n_diffs_from_n_pts(self):
        #call after changing self._npts
        # triangular number defining the number of entries of pairwise diffs
        self._n_diffs = self._npts*(self._npts-1)//2

    @property
    def pts(self):
        return self._pts

    def _init_array(self):
        buffer_shape = (self._n_diffs, self._dim)
        if self.comm is None or self.comm.size == 1: #use NullComm() insead...
            return np.zeros(shape=buffer_shape, dtype=np.float64)
        else:
            return np_float64_shared_array(buffer_shape, comm)

    @pts.setter
    def pts(self, pts):
        if not(np.ndim(pts) == 2 and pts.shape[1] == self._dim):
            raise Exception("pts should have shape (#npts, dim)")

        #re-allocations if necessary
        npts = pts.shape[0]
        if npts != self._npts:
            print ("#pts changed, reallocating differences array")
            self._npts = npts
            self._compute_cached_fn_of_n_pts()
            self._compute_n_diffs_from_n_pts()
            self._diffs = self._init_array()
        #set the points
        self._pts = pts
        self._diffs_need_update = True #not using yet...

    def __getitem__(self, key):
        return self._diffs[key]

    def shape(self):
        return (self._size, self._dim)

    @property
    def buffer(self):
        return self._diffs

    def compute_diffs(self):
        "Once self.pts is set, call to compute the differences. If called in parallel, only compute on rank 0"
        if self.comm is None or self.comm.Get_rank() == 0: #later we will check if its faster to do this in parallel
            diff_pairwise(self._pts, self._diffs)

    def _baseindex(self, i):
        "The 1D array index of the first stored element in row i. Note, only the elements above the diagonal are stored"
        # return i * self._npts - i*(i+1)//2 # < -- below was tested to be faster than this from forums and my own tests, by factor of 3 
        #Getting all of the indices can take a a while for n_pts ~ O(1k), so its important this isn't slow.
        return self._n_diffs - (self._npts-i)*((self._npts-i)-1)//2
    
    def get_ij_from_linear_idx(self, k):
        "Given idx k of the pairwise pt differences array, returns the (i, j) row, column indices"
        # Tested up to n = 3000 to be accurate. Did not try any further. 
        # I am not sure at what point the floating point calculations will be problematic.
        return vectorized_get_ij_from_linear_idx(k, self._npts, self._cached_fn_of_n_pts)

    def assign_ik_slices(self):
        "Returns a tuple of tuples: (i, np._s[k_start:j_end]). i is a row, k_start:k_end are k indices, i.e. linear indices"
        start_idx, end_idx = assign_indices(self._size)
        num_remaining_idxs = end_idx - start_idx

        my_slices = []
        i, start_j = self.get_ij_from_linear_idx(start_k)
        while 1:
            #start of a new row:
            num_elements_to_end_of_row = (self._npts - 1 - start_j)
            if num_remaining_idxs <= num_elements_to_end_of_row:
                # we have no more rows assigned to this worker
                my_slices.append((i, np._s[start_k:start_k + num_remaining_idxs]))
                return tuple(my_slices)
            else:
                end_k = start_k + num_elements_to_end_of_row
                my_slices.append((i, np._s[start_k:end_k]))
                i += 1
                num_remaining_idxs -= num_elements_to_end_of_row
                start_j, start_k = i + 1, end_k #start_j is to the right of the i'th diagonal element
        else:
            raise Exception("Something is wrong, with the index assignments!")

        #add the final slice
        # my_slices.append((i, np._s[start_k:self._npts]))
        # return tuple(my_slices)


        #Use my_slice = p_dffs.assign_ik_slices() in 
        #kernel.all_gradients(pdffs) 

    def linear_idx(self, i, j):
        return self._baseindex(i) + (j-i) - 1

    #merge this in with __getitem__ by subclassing an ndarray
    #how to make array indexing so that it isn't just acessing one at a time?
    def get_diff(self, i, j):
        """
        get_diff(self, i, j)

        Get the difference between pts[i,:] and pts[j, :].
        
        Note that if i > j, returns get_diff(self, j, i) without negating the sign.
        The negation is not necessary for the application.
        """
        if not (i >= 0 and i < self._npts and j >= 0 and j < self._npts):
            raise Exception("i and j should both be valid point indices")
        elif i == j:
            return np.zeros(self._dim)
        elif i > j:
            return self.get_diff(j, i)
        else:
            return self._diffs[self.linear_idx(i, j), :]

