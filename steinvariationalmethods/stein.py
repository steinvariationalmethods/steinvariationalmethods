import numpy as np
import scipy
import scipy.sparse as spspa
import sys
import os
import pickle as pkl
import scipy.sparse as spspa
from scipy.sparse.linalg import LinearOperator
from mpi4py.MPI import Wtime
from datastructures import *
from metrics import *
from kernels import *
import scipy as sp
from algorithms import *
from misc import *
########################################################################################################################################
########################################################################################################################################

def SteinSettings():
    settings = {}
    settings['itermax'] = 100
    settings['outer_itermax'] = 100
    settings['projected'] = True
    settings['line_search'] = False
    settings['n_particles'] = None
    settings['low_rank'] = None
    settings['over_sample'] = None
    settings['gradient_termination'] = None
    settings['common_projector'] = False
    settings['preconditioner'] = True
    settings['alpha_default'] = 1.0
    settings['max_backtracking_iter'] = 10
    settings['c_armijo'] = 1e-4
    settings['gdm_tolerance'] = 1e-18
    settings['problem_name'] = None
    settings['rel_gradient_tolerance'] = 1e-4
    settings['gauss_newton'] = None
    settings['cov_path'] = None
    settings['mean_path'] = None
    settings['record_stats'] = True
    settings['has_analytical_moments'] = True
    settings['print_eigenvalues'] = True
    settings['debugging'] = False
    settings['log_checkpoint_frequency'] = 1

    return settings

########################################################################################################################################
class PriorCovarianceLinearOperator(LinearOperator):
    def __init__(self, sigma_diag):
        dtype = sigma_diag.dtype
        N = sigma_diag.shape[0]
        shape = (N, N)
        super().__init__(dtype, shape)
        self._sigma_diag = sigma_diag

    def _matvec(self, vec):
        return self._sigma_diag * vec

class PriorCovarianceLinearOperatorCholesky(LinearOperator):
    def __init__(self, L_cholesky_banded_obj,L):
        dtype = L.dtype
        N = L.shape[0]
        shape = (N,N)
        super().__init__(dtype, shape)
        self._L_cholesky_banded_obj = L_cholesky_banded_obj

    def _matvec(self, vec):
        temp = sp.linalg.cho_solve_banded((self._L_cholesky_banded_obj, True),vec.T, overwrite_b=False).T 
        print('Cholesky Solve:', np.linalg.norm(temp))
        return temp

class LowRankEigendecompLinearOperator(LinearOperator):
    def __init__(self, Ut, d):
        dtype = d.dtype
        assert(Ut.ndim == 2)
        N = Ut.shape[1]
        shape = (N,N)
        super().__init__(dtype, shape)
        self._Ut = Ut
        self._d = d

    def _matvec(self, vec):
        return self._Ut.T @ ((self._Ut.T*self._d).T @ vec)

class TransportHess(LinearOperator):
    def __init__(self,Uts,ds,prior,kernel_matrix_i,global_grad_kernel_i, global_sum_grad_kernel_cols):
        dtype = ds.dtype
        assert(Uts.ndim ==3)
        self.N, r, d = Uts.shape
        shape = (d,d)
        super().__init__(dtype, shape)
        self._Uts = Uts
        self._ds = ds 
        self._global_grad_kernel_i = global_grad_kernel_i
        self._global_sum_grad_kernel_cols = global_sum_grad_kernel_cols
        self._kernel_matrix_i = kernel_matrix_i
        self.prior = prior

    def _matvec(self,vec):
        y = np.zeros_like(vec)
        # print('||vec||', np.linalg.norm(vec))
        for j in range(self.N):
            # hess = LowRankEigendecompLinearOperator(self._Uts[j],self._ds[j])
            Ut = self._Uts[j]
            d = self._ds[j]-1. #!!!!! (is this correct?) assumes the  eigenvalues passed in 'ds' are the posterior hessian eigenvalues, since this supposed to be the likelihood hessian eigenvalues
            hess_vec = self.prior._sigma_inv*vec + self.prior._sigma_inv*( Ut.T @(d * (Ut@(self.prior._sigma_inv*vec))))
            y += (self._kernel_matrix_i[j]*hess_vec)#(hess._matvec(vec)))
        y += (self._global_grad_kernel_i @ self._global_sum_grad_kernel_cols) @vec
        # print('||Hvec||', np.linalg.norm(y/self.N))
        return y/self.N

########################################################################################################################################

def Stein(prior,posterior,all_xs, local_xs, settings,comm):
########################################################################################################################################
########################################################################################################################################
# Stein Initialization
########################################################################################################################################
#Set and record random seed for reproducibility
    # Define run specs
    # Should this be implemented as a dictionary and read in from file?

    projection_time = 0.0
    try:
        rank = comm.Get_rank()
    except:
        rank = 0

    itermax = settings['itermax']
    outer_itermax = settings['outer_itermax']
    common_projector = settings['common_projector']
    preconditioner = settings['preconditioner']
    line_search = settings['line_search']
    alpha_default = settings['alpha_default']
    max_backtracking_iter = settings['max_backtracking_iter']
    c_armijo = settings['c_armijo']
    gdm_tolerance = settings['gdm_tolerance']
    r = settings['low_rank']
    p = settings['over_sample']
    projected = settings['projected']
    n_particles = settings['n_particles']
    problem_name = settings['problem_name']
    log_checkpoint_frequency = settings['log_checkpoint_frequency']
    gradient_termination = settings['gradient_termination']
    gauss_newton = settings['gauss_newton']
    subspace = settings['subspace']



    if 'search_direction' in settings.keys():
        search_direction = settings['search_direction']
    else:
        print('search_direction not specified assuming newton')
        search_direction = 'newton'
    print("Search direction:",search_direction)
    print("Projection:", bool(projected))
    new_subspace = True
    n_dim = prior.dims
    # terminate = None
    if settings['record_stats']:
        if not settings['has_analytical_moments']:
            pprint(settings['mean_path'],comm = comm)
            if rank is 0:
                post_mean = np.load(settings['mean_path'])
                post_cov = np.load(settings['cov_path'])
            else:
                post_mean = np.zeros(n_dim, dtype='d')
                post_cov = np.zeros((n_dim,n_dim), dtype='d')
            comm.Bcast(post_mean, root=0)
            comm.Bcast(post_cov, root=0)
        else:
            post_mean = posterior.centered_moment(1)
            post_cov = posterior.centered_moment(2)
        prior_mean = Allreduce_mean_scalars(local_xs, comm)
        try:
            sigma_inv = prior.sigma_inv
            def mean_error(points, true_mean, prior_sigma_inv):
                if hasattr(local_xs,'full'):
                    estimator_mean = Allreduce_mean_axis_0(points.full, comm)
                else:
                    estimator_mean = Allreduce_mean_axis_0(points, comm)

                if rank is 0:
                    mean_diff = np.sum((estimator_mean - true_mean)**2*prior_sigma_inv)/np.sum(true_mean**2*prior_sigma_inv)
                else:
                    mean_diff = np.zeros(0, dtype='d')

                if comm is not None: comm.Bcast(mean_diff, root=0)
                # pprint("mean_diff", mean_diff, comm= comm)
                # pprint("post_mean", true_mean, comm = comm)
                # pprint("estimator_mean", estimator_mean, comm = comm)
                return mean_diff


        except:
            def mean_error(points, true_mean, L):
                if hasattr(local_xs,'full'):
                    estimator_mean = Allreduce_mean_axis_0(points.full, comm)
                else:
                    estimator_mean = Allreduce_mean_axis_0(points, comm)

                if rank is 0:
                    diff = estimator_mean - true_mean
                    mean_diff = diff.T @(L@(L.T@diff))/(true_mean.T @(L@(L.T@true_mean)))
                else:
                    mean_diff = np.zeros(0, dtype='d')

                if comm is not None: comm.Bcast(mean_diff, root=0)
                # pprint("mean_diff", mean_diff, comm= comm)
                # pprint("post_mean", true_mean, comm = comm)
                # pprint("estimator_mean", estimator_mean, comm = comm)
                return mean_diff

    # points is all_xs
    def covariance_error(points, true_covariance):
        if hasattr(points,'full'):
            estimator_covariance = np.var(points.full, axis = 0)
        else:
            estimator_covariance = np.var(points, axis = 0)
        
        cov_diff = np.linalg.norm(estimator_covariance  - true_covariance, ord=2)
        # pprint("cov_diff", cov_diff, comm= comm)
        return cov_diff

    # Peng wants to do Entrywise Variance instead of covariance error:
    # THIS MEANS WE WILL USE np.var(, axis=...)
    

    local_N, N, d = local_xs.shape[0], all_xs.shape[0], n_dim #abbreviations
    # metrics = HessianMetric(model, projected, init_allocations_size=N, low_rank=r)
    metric = AveragedHessianMetric(posterior, project=projected, init_allocations_size=local_N, low_rank=r, oversampling=p)
    kernel = GaussianKernel(metric, init_allocations_size=(local_N, N)) 

    if not projected:
        #Prior Covariance Operator, i.e. H^{-1} applys
        if hasattr(prior,'L'):
            PriorCovPreconditioner = PriorCovarianceLinearOperatorCholesky(prior._L_cholesky_banded_obj,prior.L)       
        else:
            PriorCovPreconditioner = PriorCovarianceLinearOperator(prior.sigma)                  #identical on each rank
    
    #Time ALL major steps going forward!!!
    # Initialise average computational time
    timeave = 0;

    pprint('Common projector:', common_projector,comm = comm)
    ########################################################################################################################################
    ########################################################################################################################################
    # Array Allocations
    if projected:
        #Only need to change these if r changes!!!
        mgJ = np.zeros((local_N, r)) #local and global versions?
        HJ = np.zeros((local_N, r, r)) #local and global versions?
        post_hess = np.zeros((local_N, r, r)) #resize????????
        post_grad = np.zeros((local_N, r)) #local and broadcasted?
        candidate_step = np.zeros((local_N,r))
        Q = np.zeros((local_N, r))
        if not common_projector:
            lmbda_local = np.zeros((local_N, r))            
            Ut_s_local = np.zeros((local_N, r, d))
            Ut_s_global = np.zeros((N, r, d))
            lmbda_global = np.zeros((N, r))            
        else:
            lmbda_local = np.zeros(r)            
            Ut_s_local = np.zeros((r, d))

            # Ut_s_global = np.zeros((comm.Get_size(), r, d))
            # lmbda_global = np.zeros(comm.Get_size(), r)    

        coeffs_global = np.zeros((N, r))
        perp_global = np.zeros((N, d))
        proj_global =np.zeros((N, d))


        # Global arrays
        global_post_grad = np.zeros((N,r))
        global_post_hess = np.zeros((N,r,r))
        global_grad_kernel = np.zeros((N,N,r))
        global_sum_grad_kernel_cols = np.zeros((N,r))
        global_Q = np.zeros((N,r))

    else:
        mgJ = np.zeros((local_N, d)) #local and global versions?
        HJ = np.zeros((local_N, d, d)) #local and global versions?
        # post_hess = np.zeros((local_N, d, d)) #resize????????

        # lmbda_s = np.zeros((local_N,d))
        # Ut_s = np.zeros((local_N,d,d))

        lmbda_local = np.zeros((local_N, r))            
        Ut_s_local = np.zeros((local_N, r, d))
        
        # lmbda_global = np.zeros((N, r)) 
        # Ut_s_global = np.zeros((N, r, d))

        post_grad = np.zeros((local_N, d)) #local and broadcasted?
        candidate_step = np.zeros((local_N,d))
        Q = np.zeros((local_N, d))

        # Global arrays
        global_post_grad = np.zeros((N,d))
        global_post_hess = np.zeros((N,d,d))
        global_grad_kernel = np.zeros((N,N,d))
        global_sum_grad_kernel_cols = np.zeros((N,d))
        global_Q = np.zeros((N,d))
    # Finished array allocations
    ########################################################################################################################################
    ########################################################################################################################################
    # Initialize quantities used in calculation
    step_norm_old = np.inf
    ########################################################################################################################################
    # Logging
    if rank is 0:
        logger = {}
        logger['n_particles'] = n_particles
        logger['ndim'] = n_dim
        logger ['name'] = problem_name
        # To be logged at each iteration on rank 0
        logger['iteration_duration'] = {}
        logger['mean_error'] = {}
        logger['cov_error'] = {}
        logger['ave_neg_logpdf'] = {}
        logger['ave_neg_logpdf_test'] = {}
        logger['dkl_gradient_norm'] = {}
        logger['alpha'] = {}
        logger['avg_newton_step_norm'] = {}
        logger['outer_it'] = {}
        
        logger['newton_solve_time'] = {}
        logger['kernel_assembly_time'] = {}
        logger['gradient_time'] = {}
        logger['hess_time'] = {}
        logger['total_hess_time'] = {}
        logger['communication_time'] = {}
        logger['eigenvalues'] = {}
        logger['projection_time'] = {}

        logger['sample mean'] ={}
        logger['sample mean ave'] ={}
        # logger['sample cov trace'] ={}
        # Set up logger directory
        logger_dir = settings['logger_dir']
        logger_name = logger_dir+problem_name
        if not os.path.isdir(logger_dir):
            try:
                os.makedirs(logger_dir)
            except:
                print('Did not make the logger directory for some reason')
                pass
    # Define the logger-saver function
    if rank is 0:
        try:
            import pickle
        except:
            print(80*'#')
            print('Please install pickle and re-run if you want to save the logging data')
            print(80*'#')
        else:
            def save_logger(logger,out_name):
                    with open(out_name +'.pkl', 'wb+') as f:
                        pickle.dump(logger, f, pickle.HIGHEST_PROTOCOL)
    ########################################################################################################################################
    ########################################################################################################################################
    # Begin Stein Iteration
    pprintsection('Starting Stein'.center(80),comm  = comm)
    k = 0
    outer_k = -1
    average_grad_norm = [np.inf]
    gradient_tolerance = -np.inf
    iterating = True
    prior_mean = Allreduce_mean_axis_0(local_xs, comm)
    # # prior_diff = np.linalg.norm(posterior.mean-prior_mean)/np.linalg.norm(posterior.mean)
    # # true_cov = np.trace(posterior.cov)
    # These are not used in the algorithm, just used for printing here.
    local_prior_cov = np.var(local_xs, axis= 0)
    if settings['debugging']:
        pprint("inital prior sample: ",np.mean(prior_mean),comm= comm)
    # print(prior_cov)
    # dfdfdfd
    # print("true_ave", np.mean(posterior.mean), "prior: ", np.mean(prior_mean),prior_diff)
    # print("true cov ", true_cov,"prior cov", prior_cov)
    pprint('{0:7} {1:7} {2:7} {3:12} {4:12} {5:12} {6:12} {7:9}'.format('out it'.rjust(6),\
                      'in it'.rjust(6),'total it'.rjust(6),  '-logpdf'.rjust(11),  '-logpdf test'.rjust(11),'||g||'.rjust(11),\
                        '||p||'.rjust(11),'alpha'.rjust(8)),comm = comm)

    while iterating:
        if hasattr(local_xs,'full'):
            sample_mean = Allreduce_mean_axis_0(local_xs.full, comm)
            sample_mean_ave = np.mean(sample_mean)
            # print("shape", all_xs._proj.shape,all_xs._perp.shape)
            sample_cov = np.var(all_xs.full, axis = 0)
            # sample_cov_trace = np.trace(sample_cov)
        else:
            sample_mean = Allreduce_mean_axis_0(local_xs, comm)
            sample_mean_ave = np.mean(sample_mean)
            sample_cov = np.var(all_xs, axis = 0)
            # sample_cov_trace = np.trace(sample_cov)

        k += 1
        it_start = Wtime()
        tic = Wtime()
        if settings['debugging']:
            pprint("Begin calculation of gradient at iteration", k,comm = comm)
        # Calculate gradient
        # print("local_xs", local_xs)
        # grad_neg_log_pdfs, neg_log_pdfs = posterior.prior.grad_neg_logpdf(local_xs, projections=(not new_subspace)) 
        # print("grad_log_pdf ", grad_neg_log_pdfs)
        # print("fwd ", posterior.likelihood.forward(local_xs))
        # print("likelihood", posterior.likelihood.neg_logpdf(local_xs) )
        # ytrue = posterior.likelihood.ytrue(local_xs) 
        # print("ytrue", ytrue ,ytrue.shape)

        # grad_neg_log_pdfs, neg_log_pdfs = posterior.likelihood.grad_neg_logpdf(local_xs, projections=(not new_subspace)) 
        # print("like grad_log_pdf ", grad_neg_log_pdfs,neg_log_pdfs)     
        # grad_neg_log_pdfs, neg_log_pdfs = posterior.prior.grad_neg_logpdf(local_xs, projections=(not new_subspace)) 
        # print("prior grad_log_pdf ", grad_neg_log_pdfs,neg_log_pdfs)   
        grad_neg_log_pdfs, neg_log_pdfs = posterior.grad_neg_logpdf(local_xs, projections = False)#projections=(not new_subspace))  #NOT ACCMULATED GRADIENT AND NEGLOGPDF! (LOCAL)
        neg_log_pdfs_test = posterior.neg_logpdf_test(local_xs)
        # print("grad_log_pdf ", grad_neg_log_pdfs)
        gradient_time = Wtime() -tic
        if settings['debugging']:
            pprint('Gradient calculation took ', gradient_time, comm = comm)
        if projected:
    ###############################Subspace Projection#####################################################################  
            if new_subspace:
                outer_k += 1
                inner_iter = 0
                if settings['debugging']:
                    pprint('What is a Gauss Newton?',gauss_newton, comm= comm)
                # print("random matrix ", metric.random_projection)
                if subspace == 'hessian':
                    randomized_posterior_hess_eigendecomp(prior, local_xs, metric.random_projection,\
                                         r, lmbda_local, Ut_s_local, posterior,gauss_newton, common_projector)
                    # for i in range(10):
                    #     x = np.zeros(10)
                    #     x[i] = 1.
                    #     print(Ut_s_local.shape)
                    #     aa = prior._sigma_inv*x + prior._sigma_inv*( Ut_s_local.T @((lmbda_local-1.) * (Ut_s_local@(prior._sigma_inv*x))))
                    #     print(aa)
                    # dfdfdfd
                    # print("lmbda ", lmbda_local)
                    # dfdfdfd
                    # print("Ut", Ut_s_local)

                elif subspace == 'active':
                    randomized_posterior_ggT_eigendecomp(prior, local_xs, metric.random_projection,\
                                         r, lmbda_local, Ut_s_local, posterior,gauss_newton, common_projector)
                else:
                    raise
                    pass
                if settings['print_eigenvalues']:
                    pprint("lamdba ", lmbda_local, "Ut", Ut_s_local)
                    pprint("lambda", lmbda_local[0]-1.,comm = comm)
                proj0 = Wtime()
                local_xs = splitspace_vectors(local_xs, Ut_s_local,prior)
                projection_time = Wtime() - proj0
                # print('lmbda_local.shape',lmbda_local.shape)
                lmbdas = lmbda_local[0]
                #Now send all this info to the global all_xs
                comm_time0 = Wtime()
                if not common_projector:
                    allgather(Ut_s_local, Ut_s_global,comm)
                allgather(local_xs.coeffs, coeffs_global,comm)
                allgather(local_xs.perp, perp_global,comm)
                allgather(local_xs.proj, proj_global,comm)

                    
                all_xs = splitspace_vectors()
                if not common_projector:
                    all_xs._projectors = Ut_s_global
                else:

                    all_xs._projectors = Ut_s_local
                all_xs._perp = perp_global
                all_xs._proj = proj_global
                all_xs._coeffs = coeffs_global
                all_xs._shape = (N, d)
                # print("perp", all_xs._perp.shape,"proj", all_xs._proj.shape)
                comm_time = Wtime() - comm_time0    
    ####################Prepare for MainAlgorithm##############################################################################   
                #assign the matrix for the metric
                if not common_projector:
                    for i in range(local_N):
                        post_hess[i] = np.abs(np.diag(lmbda_local[i]))
                        post_grad[i] = local_xs.projectors[i] @ (prior._sigma_inv*grad_neg_log_pdfs[i])
                    kernel.metric.matrices = post_hess  
                else:
                    for i in range(local_N):
                        post_grad[i] = local_xs.projectors @ (prior._sigma_inv*grad_neg_log_pdfs[i])                   
  
                new_subspace = False
            else:
                if not common_projector:
                    for i in range(local_N):
                        post_grad[i] = local_xs.projectors[i] @ (prior._sigma_inv*grad_neg_log_pdfs[i])
                else:
                    for i in range(local_N):
                        post_grad[i] = local_xs.projectors @ (prior._sigma_inv*grad_neg_log_pdfs[i])
                inner_iter += 1
                # print("inner", inner_iter)
        # End projection case
        # Begin full space!!
        else:
            outer_k += 1
            inner_iter = 0
            allgather(local_xs, all_xs, comm)
            comm_time = 0
            post_grad[:] = grad_neg_log_pdfs
            ####d_likelihood = d-1####

            # print(80*'#')
            # for k in range(local_xs.shape[0]):
            #     print('Norm of local xs[k]',np.linalg.norm(local_xs[k]), 'rank = ',rank, 'k = ',k)
            # print(80*'#')
            randomized_posterior_hess_eigendecomp(prior, local_xs, metric.random_projection,\
                                         r, lmbda_local, Ut_s_local, posterior,gauss_newton, common_projector=False)
            
            # print(80*'#')
            # print('Hello I am on rank ',rank,'and this is my randomized posterior_hess_eigendecomp')
            # print('lambdas_trace = ',np.sum(lmbda_local))
            # print('Ut_s_local_norm = ',np.linalg.norm(Ut_s_local))
            # print(80*'#')
            # print("lambda", np.sum(lmbda_local[0]>1.),np.sum(lmbda_local[0]>(1.+1e-07)),np.sum(lmbda_local[0]>(1.+1e-09)),np.sum(lmbda_local[0]>(1.+1e-05)))
            # dfdfdfd

            #COMMMUNICATE ALL OF THE D's AND U's, all gather!!!!!!
            kernel.metric.matrices = [lmbda_local, Ut_s_local]

            # kernel.metric = avemetric(u,d)
    ####################MainAlgorithm##############################################################################   
        # Calculate Kernel gram matrix
        posterior.reset_times()
        kernel.reset_times()
        if settings['debugging']:
            try:
                pprint('||local_xs||',np.linalg.norm(local_xs),comm = comm)
                pprint('||all_xs||',np.linalg.norm(all_xs),comm = comm)
            except:
                pass
        # print("projected",projected)
        # print("all_xs ", all_xs.shape)
        # print("local_xs ", local_xs.shape)

        grad_kernel, kernel_matrix = kernel.calculate_grad_and_gram_matrix(local_xs, all_xs, projections=projected, gaussnewton = gauss_newton)
        # print("grad_kernel ", grad_kernel)
        # print("kernel_matrix ", kernel_matrix)
        # dfdfdfdfd
        if settings['debugging']:
            pprint('||grad_kernel||',np.linalg.norm(grad_kernel),comm = comm)
            pprint('||kernel_matrix||',np.linalg.norm(kernel_matrix),comm = comm)

            pprint('kernel matrix',kernel_matrix,comm = comm)

        kernel_time = kernel.kernel_time
        hess_time = posterior.hess_time
        # Gather the grad_kernel
        gkcomm0 = Wtime()
        allgather(grad_kernel, global_grad_kernel,comm)
        if projected:
            post_hess[:] = kernel.metric.matrices 
            # print("do we compute post grad???", post_grad[0])
            # print("post_hess ", post_hess)
        else:
            # These are local to the process!
            lmbda_s, Ut_s = kernel.metric._global_metric_d_s,kernel.metric._global_metric_Ut_s
            # allgather(lmbda_s, lmbda_global, comm)
            # allgather(Ut_s,Ut_s_global,comm)
            lmbdas = lmbda_s
        sum_kernel_rows = np.sum(kernel_matrix, axis=1, keepdims=True) # Nx 1, could keep is 1 x N if need be.     
        # All gather the post grad
        pgcomm0 = Wtime()
        allgather(post_grad, global_post_grad,comm)
        comm_time += Wtime() - pgcomm0

        # Calculate - gradient(J)
        for i in range(local_N): #THE N BELOW SHOULD BE THE GLOBAL N!
            mgJ[i] = -(kernel_matrix[i] @ global_post_grad)/N + np.mean(grad_kernel[i] , 0) #mean on each process filling in rows of mgJ
        # print("mgJ", mgJ)
        # Newton step calculation
        if search_direction == 'newton':
            sum_grad_kernel_cols  = np.sum(-grad_kernel, axis=1)/sum_kernel_rows  #need the sum of hte kernel rows on each process/
            # print("sum_grad_kernel_cols", sum_grad_kernel_cols)
            # Gather sum_grad_kernel_cols
            sgkcomm0 = Wtime()
            allgather(sum_grad_kernel_cols,global_sum_grad_kernel_cols,comm)
            comm_time += Wtime() - sgkcomm0
            # Calculate Hessian (dense or Low Rank Eigendecomposition)
            if projected:
                # Create dense Hessians and do r x r Hessian solves 
                # Gather post_hess
                phcomm0 = Wtime()
                allgather(post_hess, global_post_hess,comm)
                comm_time += Wtime() - phcomm0
                for i in range(local_N): #need to accumulate post hess??????
                    HJ[i] = (global_post_hess.T @ (kernel_matrix[i]) + grad_kernel[i].T @ global_sum_grad_kernel_cols)/N #grad_kernel[i].T @ sum_grad_kernel_col/N can be precomputed ans passed in
            else:               #global_post_hess is N x d x d , kernel matrix[i] is N x 1.    
                # Low rank action for CG solves
                HJ_ActionOps = [None] * N
                # define the Hessian Action Operators
                for i in range(local_N):
                    HJ_ActionOps[i] = TransportHess(Ut_s, lmbda_s, prior, kernel_matrix[i], grad_kernel[i].T, global_sum_grad_kernel_cols)
            # Solve the Newton system for the step (LU or CG)
            newton_time_0 = Wtime()
            if settings['debugging']:
                pprint('Prior to the newton solve ||g|| is',np.linalg.norm(mgJ),comm = comm)
            if projected:
                for i, (Hessian_i, minus_gradient_i) in enumerate(zip( HJ, mgJ)):
                    Q[i] = np.linalg.solve(Hessian_i, minus_gradient_i)
            else:
                for i,(newton_step_i, HessianActionOp_i, minus_gradient_i) in enumerate(zip(Q, HJ_ActionOps, mgJ)):

                    Q[i], info = spspa.linalg.gmres(HessianActionOp_i, minus_gradient_i, tol=1e-05, maxiter=10, M=PriorCovPreconditioner)

            if settings['debugging']:
                pprint('||Q|| after ', np.linalg.norm(Q),comm = comm )
            # All gather the Qs after the computation
            newton_time = Wtime() - newton_time_0
            Qcomm0 = Wtime()
            allgather(Q,global_Q,comm)
            comm_time += Wtime() - Qcomm0
            # Galerkin projection of the step (normalized K)    
            for i in range(local_N):
                candidate_step[i] = (kernel_matrix[i].T @ global_Q)/sum_kernel_rows[i]
                candidate_norm = np.linalg.norm(candidate_step[i])

        # Gradient Step Calculation
        elif search_direction == 'gradient':
            candidate_step = mgJ
            if settings['debugging']:
                for i in range(local_N):
                    print('On rank',rank,'i = ',i,'||grad[i]||=',np.linalg.norm(mgJ[i]))

        if settings['debugging']:
            pprint('||candidate_step|| right after assignment',np.linalg.norm(candidate_step),comm = comm)
        
        # Line search Globalization
        alpha = alpha_default

        if line_search:
            # terminate = False
            average_cost_old = Reduce_mean_scalars_to_rank_0(neg_log_pdfs,  comm)
            for _ in range(max_backtracking_iter):
                if projected:
                    trial_x = splitspace_vectors(local_xs.full,local_xs.projectors,prior)
                else:
                    trial_x = local_xs.copy()

                trial_x += alpha * candidate_step

                local_cost_new, local_grad_inner_step = posterior.neg_logpdf(trial_x), np.sum(-mgJ*candidate_step, axis =1) #post_grad or mgJ???            
                average_grad_inner_step = Reduce_mean_scalars_to_rank_0(local_grad_inner_step, comm)
                average_cost = Reduce_mean_scalars_to_rank_0(local_cost_new, comm)
                if rank is 0:
                    terminate = average_cost < average_cost_old + alpha*c_armijo*average_grad_inner_step or (-average_grad_inner_step <= gdm_tolerance)
                    terminate = float(terminate)
                else:
                    terminate = 0.0

                terminate = bool(Allreduce_mean_scalars(terminate, comm))
                if terminate:
                    break
                else:
                    alpha *= 0.5
                    # pprint("old cost ", average_cost_old, "new cost ", average_cost,"armijo_condition ",alpha*c_armijo*average_grad_inner_step,"gdm ",average_grad_inner_step,comm=comm)
            else:
                if comm is not None and rank ==0:
                    pprint("Warning, backtracked to max backtracking iters",comm=comm)
        # print("xs after ", local_xs.full,alpha)
        #Take Newton Step: Update the particle locations
        # In place add has been checked and works
        proj0 = Wtime()
        if settings['debugging']:
            try:
                pprint('||local_xs|| before',np.linalg.norm(local_xs),comm = comm)
                pprint('||glerkinstep|| before update',np.linalg.norm(candidate_step),comm = comm)
            except:
                pass
        local_xs += alpha*candidate_step

        if settings['debugging']:
            try:
                pprint('||local_xs|| after',np.linalg.norm(local_xs),comm = comm)
            except:
                pass
        projection_time += Wtime() - proj0

        if projected:
            # if not common_projector:
            local_xscomm0 = Wtime()
            allgather(local_xs.coeffs, coeffs_global, comm)
            allgather(local_xs.proj, proj_global, comm)
            comm_time += Wtime() - local_xscomm0

            #set these without computing things
            # if not common_projector:
            all_xs._coeffs = coeffs_global
            all_xs._proj = proj_global
            # else:
            #     all_xs._coeffs = local_xs.coeffs
            #     all_xs._proj = local_xs.proj

        #HOW DO WE DO THIS, because need to automatically do the projection etc
        #need to accumulate the all_xs and then assign all_xs.coeffs
        # Gather the _xs
        # print(80*"#") 
        # print("Before", np.linalg.norm(local_xs.coeffs), np.linalg.norm(all_xs.coeffs))

    #update coeffs
        # print("After", np.linalg.norm(local_xs.coeffs), np.linalg.norm(all_xs.coeffs))
        # print(80*"#") 



        #update the values???
        # Access convergence
        step_norm = np.mean(np.linalg.norm(candidate_step, axis=1)*alpha)
        average_step_norm = Allreduce_mean_scalars(step_norm, comm)

        if np.abs(step_norm_old - average_step_norm)< 1e-4 or inner_iter >= 19:
            if projected:
                new_subspace = True
                # outer_k += 1
                if settings['debugging']:
                    pprint("New subspace at iteration ", k,comm = comm)
        step_norm_old = average_step_norm

        if projected and new_subspace:
            local_xs = local_xs.full    
            all_xs = all_xs.full
            if settings['debugging']:
                pprint("sample: ",sample_mean_ave,comm = comm)
        average_neg_log_pdf = Allreduce_mean_scalars(neg_log_pdfs,  comm)
        # try:
        average_neg_log_pdf_test = Allreduce_mean_scalars(neg_log_pdfs_test,comm)
        # except:
        #     print('In order to see generalization error, please provide test data')
        # average_neg_log_pdf = np.mean(Reduce_mean_scalars_to_0(neg_log_pdfs,  comm))
        grad_norm = np.linalg.norm(mgJ)
        grad_norm_global = np.zeros(shape = comm.Get_size())
        allgather(grad_norm, grad_norm_global, comm)
        average_grad_norm = np.linalg.norm(grad_norm_global)#Allreduce_mean_scalars(grad_norm, comm)
        assert(average_grad_norm > 0.0)
        if k == 0:
            gradient_tolerance = settings['rel_gradient_tolerance']*average_grad_norm

        if settings['record_stats']:
            if hasattr(prior,'L'):
                mean_diff = mean_error(local_xs, post_mean, prior.L)
            else:
                mean_diff = mean_error(local_xs, post_mean, prior.sigma_inv)
            cov_diff = covariance_error(all_xs, post_cov)
        iteration_duration = Wtime() -it_start 
        total_hess_time = posterior.hess_time
        if rank is 0:
            logger['ave_neg_logpdf'][k] = average_neg_log_pdf
            try:
                logger['ave_neg_logpdf_test'][k] = average_neg_log_pdf_test
            except:
                pass
            logger['dkl_gradient_norm'][k] = average_grad_norm
            logger['avg_newton_step_norm'][k] = average_step_norm
            logger['alpha'][k] = alpha
            logger['iteration_duration'][k] = iteration_duration
            logger['outer_it'][k] = outer_k
            if search_direction == 'newton':
                logger['newton_solve_time'][k] = newton_time
            logger['kernel_assembly_time'][k] = kernel_time
            logger['gradient_time'][k] = gradient_time
            logger['hess_time'][k] = hess_time
            logger['total_hess_time'][k] = total_hess_time
            logger['eigenvalues'][k] = lmbdas
            logger['projection_time'][k] = projection_time
            logger['communication_time'][k] =comm_time

            logger['sample mean'][k] =sample_mean
            logger['sample mean ave'][k] =sample_mean_ave
            # logger['sample cov trace'][k] =sample_cov_trace
            if settings['record_stats']:
                logger['mean_error'][k] = mean_diff
                logger['cov_error'][k] = cov_diff

        pprint('{0:7} {1:7} {2:7} {3:1.6e} {4:1.6e} {5:1.6e} {6:1.6e} {7:1.3e}'.format(\
                    str(outer_k).center(8),str(inner_iter).center(8),str(k).center(8),average_neg_log_pdf,average_neg_log_pdf_test,average_grad_norm,average_step_norm,alpha),comm = comm)
        # pprintsection('change_dim iter',outer_k, 'it', k, "ave_step_norm:", average_step_norm,"ave_neg_logpdf:", average_neg_log_pdf, "grad_norm", average_grad_norm,"stepsize:", alpha,comm = comm)
        if new_subspace and settings['record_stats']:
            pprint("mean error", mean_diff,"cov err", cov_diff)

        if (rank ==0) and (k% log_checkpoint_frequency == 0):
            save_logger(logger,logger_name)
        if gradient_termination:
            if projected:
                iterating = (average_grad_norm > gradient_tolerance) and ( outer_k < outer_itermax) and ( k < itermax )
            else:
                iterating = (average_grad_norm > gradient_tolerance) and ( k < itermax )

        else:
            if projected:
                iterating = (outer_k < outer_itermax) and ( k < itermax )
            else:
                iterating = (k < itermax)
    if not iterating:
        if (average_grad_norm < gradient_tolerance):
            pprint('Gradient Norm tolerance has been reached.\n',comm = comm)
        else:
            pprint('Maximum number of iterations has been reached.\n',comm = comm)
    print("final mean", np.mean(local_xs,axis=0))
    print("cov ", np.var(local_xs,axis = 0))
    ########################################################################################################################################
    ########################################################################################################################################
    timeave += Wtime() - tic
    timeave = timeave / itermax;
    pprint("time cost ", timeave,comm = comm)
    if rank == 0:
        save_logger(logger,logger_name)