import numpy as np
import pickle as pkl
from .samplers import EmceeMHSampler

# Metropolis-Hastings only needs one function evaluation per sample. For each distribution, I "tune" the sampler by computing a chain, then using its covariance as the covariance of the proposal density for the production runs.
# 
# In the next block I loop over the distributions, compute 100 realizations of the chain, and pickle them. Skip running this cell after this is done as it is time consuming depending on the number of evals.

# In[7]:



def mh_function(posterior,outname ='MH_all_chains' ,nwalkers = 1,nevals = 400):
    dim = posterior.dims
    sampler = EmceeMHSampler(posterior, 'burnin', 'dim', 'p0',
             burnin=100,dim=posterior.dims,p0=np.random.rand(dim))
    # nevals = nevalsdict[key]
    def get_initial_point():
        try:
            return sampler.distr.sample()
        except:
            try:
                return sampler.distr.prior.sample()
            except:
                return np.random.randn(dim)

    sampler.set_params(cov=1.0, initial_point=get_initial_point(), burnin=200, dim=dim)

    # Adaptation part to find a good empirical covariance for the random walk MH
    print('I am now sampling')
    chain = sampler.sample(200)['chain']
    print('The sampling has concluded')
    if dim > 1:
        sampler.set_param('cov', np.cov(chain.T))
    else:
        sampler.set_param('cov', np.float(np.cov(chain.T)))

    # Now do sampling:
    def compute_chain(initial_point,nsamples):
        sampler.set_param('initial_point', initial_point)
        result = sampler.sample(nsamples)
        return result['chain'],result['accept'] #sampler.sample(nsamples)['acor'],
     
    # all_chains = [chain[-1]] 
    init_point =  [chain[-1]]

    # all_chains = compute_chain(init_point,nevals)
    nsamples = 200
    n_checkpoint = int(nevals/nsamples)
    acor_all = []

    # all_chains = np.append(all_chains,init_point,axis = 0)
    for i in range(n_checkpoint):
        # if np.remainder(i, 100) == 0:
        print("check point, i = ", i*nsamples)
        # initial_point = i * 1000
        # print("init_pont", all_chains[initial_point], compute_chain(all_chains[initial_point],nsamples).shape)
        # small_chain = compute_chain(init_point,nsamples)
        small_chain,acep_rate = compute_chain(init_point,nsamples)
        # small_chain,acor,acep_rate = compute_chain(init_point,nsamples)
        # print("acor", acor)
        print("accept", acep_rate)
        # acor_all.append(acor)
        init_point = [small_chain[-1]]
        # all_chains = np.append(all_chains,init_point,axis = 0)#[compute_chain() for i in range(10)]
        # print("shape", len(all_chains))
        filename = "checkpoint_%d_logis.pkl" % i

        pklfile = open(filename, 'wb')
        pkl.dump(small_chain, pklfile)
        pklfile.close()
    # pklfile = open('acor_logis.pkl', 'wb')
    # pkl.dump(acor_all, pklfile)
    # pklfile.close()


    # Run this cell to load the chains if already computed:

    # In[4]:


    # pklfile = open(outname +'_2.pkl', 'rb')
    # all_chains = pkl.load(pklfile)
    # pklfile.close()



