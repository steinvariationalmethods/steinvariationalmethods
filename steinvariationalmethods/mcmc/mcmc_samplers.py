from __future__ import absolute_import, division, print_function

import math
import numpy as np

class SampleStruct:
    def __init__(self, kernel):
        self.sample = np.zeros(kernel.posterior.dims) #parameter
        self.neg_logpdf = 0
        self.grad_neg_logpdf  = np.zeros(kernel.posterior.dims) #gradient
        self.cov_neg_logpdf_action = np.zeros(kernel.posterior.dims) #Covariance action on gradient

    def assign(self, other):
        self.neg_log_pdf = other.neg_log_pdf
        
        self.sample = other.sample.copy()
        self.grad_neg_logpdf = other.grad_neg_logpdf.copy()
        self.cov_neg_logpdf_action = other.cov_neg_logpdf_action.copy()


class MCMC(object):
    def __init__(self, kernel):
        self.kernel = kernel
        self.parameters = {}
        self.parameters["number_of_samples"]     = 10000000 #10 million
        self.parameters["burn_in"]               = 500
        self.parameters["print_progress"]        = 20
        self.parameters["print_level"]           = 1
        
        #we need to compute the mean and covariance without storing all the particles...
        self.mean_estimate = 0.
        dims = self.kernel.posterior.dims
        self.covariance_estimate = np.zeros((dims, dims)) #accumulate a sum, if the covariance norm gets large, dump to file
        #and start accumulating again     
        
    def run(self, x0):

        if tracer is None:
            tracer = NullTracer()
        number_of_samples = self.parameters["number_of_samples"]
        burn_in = self.parameters["burn_in"]
        
        #initialize current particle and poposal particle
        current = SampleStruct(self.kernel)
        proposed = SampleStruct(self.kernel)
        current.sample = x0 

        self.kernel.init_sample(current)
        
        if self.parameters["print_level"] > 0:
            print( "Burn {0} samples".format(burn_in) )
        sample_count = 0
        naccept = 0
        n_check = burn_in // self.parameters["print_progress"]
        while (sample_count < burn_in):
            naccept +=self.kernel.sample(current, proposed)
            sample_count += 1
            if sample_count % n_check == 0 and self.parameters["print_level"] > 0:
                print( "{0:2.1f} % completed, Acceptance ratio {1:2.1f} %".format(float(sample_count)/float(burn_in)*100,
                                                                         float(naccept)/float(sample_count)*100 ) )
        if self.parameters["print_level"] > 0:
            print( "Generate {0} samples".format(number_of_samples) )
        sample_count = 0
        naccept = 0
        n_check = number_of_samples // self.parameters["print_progress"]
        while (sample_count < number_of_samples):
            naccept += self.kernel.sample(current, proposed)

            # accumulate the mean
            # also accumulate sample * sample.T, so that we can get the covariance

            sample_count += 1
            if sample_count % n_check == 0 and self.parameters["print_level"] > 0:
                print( "{0:2.1f} % completed, Acceptance ratio {1:2.1f} %".format(float(sample_count)/float(number_of_samples)*100,
                                                                         float(naccept)/float(sample_count)*100 ) )       
        return naccept
    
    def consume_random(self):
        number_of_samples = self.parameters["number_of_samples"]
        burn_in = self.parameters["burn_in"]
        
        for ii in range(number_of_samples+burn_in):
            self.kernel.consume_random()


    


class SNmapKernel:
    """
    Stochastic Newton with MAP Hessian
    """
    def __init__(self, posterior, nu):
        """
        - posterior: an object of type posterior
        - nu:    an object of type LowRankGaussian 
        """
        self.posterior = posterior
        self.nu = nu
        self.parameters = {}
        self.parameters["inner_rel_tolerance"]   = 1e-9
        
        self.noise = np.zeros(posterior.dims)
        
        self.discard = np.zeros(posterior.dims)
        self.w       = np.zeros(posterior.dims)

    def init_sample(self, x):
        x.grad_neg_logpdf, x.neg_log_pdf = self.posterior.grad_neg_logpdf(x)
        # self.nu.Hlr.solve(s.Cg, s.g)
        x.cov_neg_logpdf_action = self.nu.hess_inv_neg_logpdf_action(None, x.grad_neg_logpdf, gaussnewton=False) #supposed to do H^{-1} g and store in "cov_neg_logpdf_action" 
    
    def sample(self, current, proposed): 
        proposed.m = self.proposal(current)
        self.init_sample(proposed)
        c2p = self.neg_log_rho(current, proposed)
        p2c = self.neg_log_rho(proposed, current)
        al = c2p - p2c
        if(al > math.log(np.random.rand())):
            current.assign(proposed)
            return 1
        else:
            return 0

    # not sure whats going on here
    def proposal(self, current):
        # self.noise = np.random.normal(np.zeros(self.model.dim),1.)
        # parRandom.normal(1., self.noise)
        self.nu.sample(self.noise, self.w, add_mean=False) #???????? update w here???
        return current.sample - current.cov_neg_logpdf_action + self.w

    def neg_log_rho(self, origin, destination):
        w = destination.sample - origin.sample + origin.cov_neg_logpdf_action
        return origin.neg_log_pdf + self.nu.hess_neg_logpdf_quadratic_forms(np.zeros, w) #

    def consume_random(self):
        self.noise = np.random.normal(np.zeros(self.model.dim),1.)
        np.random.rand()






