import os
import numpy as np
from numba import jit
import time

from distributions import LaplaceApproxGaussian_cholesky as LaplaceApproxGaussian
from algorithms import randomized_double_pass_generalized_eigensolver
import prngs.parallel as random

# Parameters for the stochastic Newton MCMC algorithm: the function
# expects a dictionary with these values. The parameters are very simple -
#     - burn_in: This many steps will be performed and discarded before
#                starting computation of mean and variance.
#     - nsamples: Number of samples used for the mean and variance calculation.
#     - rank_reduction: The factor by which the rank of the covariance is reduced
#                       from full dimensionality when computing the low-rank Gaussian;
#                       e.g. if the full dimension is N x N, the rank used is 
#                       N // rank_reduction.
#     - oversample: The oversampling parameter for the stochastic eigensolver

stochastic_newton_default_params = {
        'burn_in': 100,
        'nsamples': 100000,
        'rank_reduction': 100, 
        'oversample': 5
}

@jit(nopython=True, cache=True)
def online_mean_variance_update(n, mu, cov,sum_of_squares, newval):
    """
    Numerically stable algorithm to compute mean and covariance with a single
    pass over data. cov is divided by `nsamples` at end of computation. An example
    function using this to compute mean and variance of an array of samples:

    def mean_and_covariance(samples):
        N = samples.shape[0]
        d = samples.shape[1]

        mu = np.zeros(d)
        cov = np.zeros((d,d))

        for i in range(N):
            online_mean_variance_update(i, mu, cov, samples[i,:])

        return mu, cov / N
    """
    delta = newval - mu
    mu += delta / (n+1)
    delta2 = newval - mu
    sum_of_squares += np.outer(newval,newval)
    cov += np.outer(delta, delta2)
    return mu, cov, sum_of_squares

def newton_proposal(prior, post, x, grad_neg_log_pi_post, reduction, Omega, low_rank, gaussnewton):
    """
    Compute a low rank Gaussian approximation of the proposal density given by
    eq. 2.15 in the paper outlining the Stochastic Newton MCMC algorithm
    """
    #idx=0 is just a hack to get precomputation from the gradient info

    # Aop is the hessian of the negative log likelihood

    print("Shapes in newton proposal",x.shape)
    if post.likelihood is not None:
        Aop = lambda W: post.likelihood.hess_neg_logpdf_action(x, W, gaussnewton=gaussnewton, idx=0)
    else:
        Aop = lambda W: post.hess_neg_logpdf_action(x, W, gaussnewton=gaussnewton, idx=0) \
                - prior.hess_neg_logpdf_action(x, W, gaussnewton=gaussnewton)

    # Bop is the hessian of the negative log prior
    Binvop = lambda W: prior.hess_inv_neg_logpdf_action(x, W) 
    Bop = lambda W: prior.hess_neg_logpdf_action(x, W, idx=0)

    print('Shape of Omega is ', Omega.shape[0])
    Y = Binvop(Aop(Omega))
    d, Ut = randomized_double_pass_generalized_eigensolver(Aop, Bop, Binvop, Y, low_rank)
    # print('Checking the eigen decomp, UdUt', hess)




    laplace_approx_gaussian = LaplaceApproxGaussian(prior, Ut, np.abs(d), init_allocations_size=1, comm=None)
    laplace_approx_gaussian.mu = x - laplace_approx_gaussian.hess_inv_neg_logpdf_action(x, grad_neg_log_pi_post)[0]
    return laplace_approx_gaussian

# The reduction parameter is the factor by which we reduce the rank when
# constructing the low rank Gaussian approximation.
def newton_chain(prior, post, initial=None, steps=0, reduction=1, oversample=0, gaussnewton=True):
    """
    Implements the Newton MCMC algorithm as a generator. See algorithm 2 in the
    paper. Parameter `steps` is the number of samples that will be drawn.
    """
    if initial is None:
        m = prior.sample()
    else:
        m = initial
    print("Starting newton chain", m.shape)
    # Create random projection matrix first, once, which is used for Low Rank Hessian Eigenvalue Decomposition
    r = post.dims // reduction # The rank of the low rank approximation
    p = oversample
    Omega = random.normal(size=(r+p, post.dims))
    grad_neg_log_pi_post, neg_log_pi_post = post.grad_neg_logpdf(np.array([m]))
    neg_log_pi_post = neg_log_pi_post[0]
    # This is the meaty part that constructs the Laplace Approximation Gaussian:
    # q(m, ⋅) in the notation from the paper.

    proposal = newton_proposal(prior, post, m, grad_neg_log_pi_post, reduction, Omega, r, gaussnewton)
    n = 0
    accepted_amount = 0
    rejected_amount = 0
    while n < steps:
        y = proposal.sample()
        try:
            new_grad_neg_log_pi_post, new_neg_log_pi_post = post.grad_neg_logpdf(np.array([y]))
            new_neg_log_pi_post = new_neg_log_pi_post[0]
            new_proposal = newton_proposal(prior, post, y, new_grad_neg_log_pi_post,
                                           reduction, Omega, r, gaussnewton)
            log_alpha_hat = -neg_log_pi_post -proposal.neg_logpdf(y)[0]+new_neg_log_pi_post +new_proposal.neg_logpdf(m)[0]
            # print(log_alpha_hat)
            log_alpha = min(0,log_alpha_hat)
            if log_alpha > np.log(random.uniform()):
                #Proposal sample accepted
                m = y.copy()
                proposal = new_proposal
                grad_neg_log_pi_post = new_grad_neg_log_pi_post.copy()
                neg_log_pi_post = new_neg_log_pi_post.copy()

                accepted_amount += 1
                acceptance_ratio = 100.*np.float64(accepted_amount)/(accepted_amount + rejected_amount)
                print('iter:', n, 'Accepted!, ratio: ', acceptance_ratio)
                n += 1
                yield m, acceptance_ratio

            else:
                rejected_amount+=1
        except:
            pass
def stochastic_newton_mcmc(prior, post, problem_name, return_chain=False, params=None):
    if params == None:
        params = stochastic_newton_default_params
    if return_chain:
        chain = np.zeros((params['nsamples'], post.dims))

    #Create directory for saving accumulated sample means and covariances 
    file_dir = os.path.dirname(os.path.realpath('__file__'))
    problem_dir = os.path.join(file_dir, problem_name)
    sample_mu_dir = os.path.join(problem_dir,'sample_mu')
    sample_cov_dir = os.path.join(problem_dir,'sample_cov')
    sample_sos_dir = os.path.join(problem_dir,'sum_of_squares')
    chkpt_cov_dir = os.path.join(problem_dir,'checkpointed_covariances')

    if not os.path.exists(problem_dir):
        os.makedirs(problem_dir)
    if not os.path.exists(sample_mu_dir):
        os.makedirs(sample_mu_dir)
    if not os.path.exists(sample_cov_dir):
        os.makedirs(sample_cov_dir)
    if not os.path.exists(sample_sos_dir):
        os.makedirs(sample_sos_dir)
    if not os.path.exists(chkpt_cov_dir):
        os.makedirs(chkpt_cov_dir)

    logger = {}
    logger['mean_of_means'] = {}
    logger['mean'] = {}
    logger['cov_trace'] = {}
    logger['trace_of_sum_of_sqs'] ={}




    try:
        import pandas as pd
        def save_logger(logger,out_name):
            logger_df = pd.DataFrame(logger)
            logger_df.to_csv(out_name+'.csv')
            print('Saved file to '+out_name+'.csv')
    # If not, numpy 
    except:
        print('Implement a logger saver that doesnt use pandas or (conda or pip) install pandas will take care of that issue')
        pass


    print('StochNewton: Performing burnin (%d steps)'%(params['burn_in']))
    start = time.time()
    m = None

    for step, _ in newton_chain(prior, post, steps=params['burn_in'],
                             reduction=params['rank_reduction'],
                             oversample=params['oversample']):
        m = step

    print('StochNewton: Burnin complete. Now drawing %d samples...'%(params['nsamples']))
    print('Burn in took ', time.time() - start, 's')

    mu = np.zeros(post.dims)
    sum_of_squares = np.zeros((post.dims,post.dims))
    cov = np.zeros((post.dims, post.dims))
    i = 0
    for step, acceptance_ratio in newton_chain(prior, post, initial=step, steps=params['nsamples'], 
                             reduction=params['rank_reduction'],
                             oversample=params['oversample']):
        mu, cov, sum_of_squares = online_mean_variance_update(np.float64(i), mu, cov, sum_of_squares, step) #step[0] is a hack since the return of post.sample() gives a (1, dim) object right now

        if return_chain:
            chain[i, :] = step
        i += 1
        logger['mean_of_means'][i] = np.trace(cov / i)
        logger['mean'][i] = np.mean(mu)
        logger['cov_trace'][i] = np.trace(cov / i)
        logger['trace_of_sum_of_sqs'][i] = np.trace(sum_of_squares)
        if (i % params['checkpoint_iter'] == 0):
            # print('Saving current batch sample mu and sample cov to disk at iteration', i)
            # np.save(os.path.join(sample_mu_dir, str(i)), mu)
            # np.save(os.path.join(sample_cov_dir, str(i)), cov / i)
            # np.save(os.path.join(sample_sos_dir,str(i)),sum_of_squares)
            # sum_of_squares[:] = 0.0
            # print("mu estimator norm", np.linalg.norm(mu))
            # print('Covariance estimator norm',np.linalg.norm(cov/i))
            # print("Current acceptance %:", acceptance_ratio)

            save_logger(logger,'mcmc_chain_results')

            #LOG THIS SCALAR AS WELL: np.mean(mu) #mean of the means
            #LOG THIS SCALAR: np.trace(cov / i) #trace of cov calculation
            #LOG THIS SCALAR: np.trace(sum_of_squares) #alternative trace of cov calculation

    if True:

        sample_sos = np.zeros_like(sum_of_squares)
        for i in range(0,params['nsamples'],params['checkpoint_iter']):
            if i == 0:
                pass
            else:
                sample_sos += np.load(sample_sos_dir+'/'+str(i)+'.npy')
                sample_mean = np.load(sample_mu_dir+'/'+str(i)+'.npy')
                checkpointed_covariance = sample_sos/(i) - np.outer(sample_mean,sample_mean)
                np.save(os.path.join(chkpt_cov_dir,str(i)),checkpointed_covariance)
                print('Checkpointed covariance norm at iteration',i,' is ', np.linalg.norm(checkpointed_covariance))


    np.save(os.path.join(sample_mu_dir, str(i)), mu)

    #LOG THIS SCALAR AS WELL: np.mean(mu)
    #LOG THIS SCALAR: np.trace(cov / i)
    np.save(os.path.join(sample_cov_dir, str(i)), np.trace(cov / i))


    if return_chain:
        return mu, cov / params['nsamples'], chain
    else:    
        return mu, cov / params['nsamples']


