import emcee
import numpy as np
# import pyhmc
# import NUTS.emcee_nuts as mcnuts
# from SVN_H import SVN_H as SVN_H_New

class _Sampler:
    "Abstract base class for sampling algorithms"
    def __init__(self, distr, *args, **kwargs):
        """
        __init__(self, *all_params, **default_values)

        The sampler has a 'parameters' member which is initialized to None.
        Non-keyword arguments to the constructor are assumed to be the keys for the
        parameter dictionary that should be supplied before sampling. kwargs specify
        default values for any parameters where this is appropriate.

        Child classes should define their own constructor that calls this base
        constructor with the names of all of the required parameters, and default
        values.

        Before sampling, call 'set_params(**parameters)' to set values for parameters.
        All parameters without a supplied default value will be required.
        """
        # if distr.what_am_i() is not 'Distribution':
        #     raise Exception("Initialize _Sampler with a _Distribution sub-class")
        self.distr = distr
        self.parameters = None
        self.recognized_params = args
        self.default_parameter_values = kwargs
        self.params_set = False

    def sample(self, nsamples):
        """
        Draw nsamples samples using whatever sampling algorithm. The parameters for
        the sampler must be set before calling 'sample'.

        Note that the implementation of the sampling algorithm goes in `sample_impl`;
        this is a base method that just checks if parameters are set, first.
        """
        if not self.params_set:
            raise Exception("Sampler parameters not set")
        return self.sample_impl(nsamples)

    def sample_impl(self, nsamples):
        "Override sample_impl in derived classes to do the actual algorithm implementation"
        raise Exception("sample_impl should be implemented by child classes")

    def set_params(self, **kwargs):
        """
        Given a dictionary with *all* of the parameters required, set the parameters.
        Checks that all parameters without default values are defined.
        """
        self.parameters = kwargs
        for param in self.recognized_params:
            if (not param in self.parameters) and (param in self.default_parameter_values):
                self.parameters[param] = self.default_parameter_values[param]
            elif not param in self.parameters:
                raise Exception("""
                                Parameter "%s" not supplied, and no default value
                                available.
                                """%(param))
        self.params_set = True

    def set_param(self, param, val):
        """
        If the parameters dict has been initialized, this function can be used to
        tweak a single setting.
        """
        if self.parameters is None:
            raise Exception("Use set_params to initialize all parameters")
        self.parameters[param] = val

    def clear_params(self):
        "Clear params so that they must be set again before using the sampler"
        self.parameters = None
        self.params_set = False

class EmceeEnsembleSampler(_Sampler):
    def __init__(self, distr):
        super().__init__(distr,*args,**kwargs)
        
    def sample_impl(self, nsamples):
        params = self.parameters
        sampler = emcee.EnsembleSampler(
            params['nwalkers'], params['dim'], lambda x: self.distr.neg_logpdf(x), 
            params['a']
        )
        sampler.run_mcmc(
            params['p0'], 
            int(np.ceil(nsamples/params['nwalkers'])) + params['burnin']
        )
        return {'chain': sampler.flatchain[params['burnin']:-1, :],
                'accept': sampler.acceptance_fraction,
                #'acor': sampler.acor,
                'lnprob': sampler.flatlnprobability[params['burnin']:-1]
               }

# This is the only one that I am checking


class EmceeMHSampler(_Sampler):
    def __init__(self, distr,*args,**kwargs):
        super().__init__(distr,*args,**kwargs)
    
    def sample_impl(self, nsamples):
        params = self.parameters
        cov = self.matrixify(params['cov'])
        sampler = emcee.MHSampler(cov, params['dim'], lambda x: -self.distr.neg_logpdf(x)[0])
        # print('p0.shape',params['p0'].shape)
        sampler.run_mcmc(params['p0'], nsamples + params['burnin'])
        # print("chain", sampler.flatchain)
        if nsamples >= 100000:
            return {'chain': sampler.flatchain[params['burnin']:-1, :],
                    'accept': sampler.acceptance_fraction,
                    'lnprob': sampler.lnprobability[params['burnin']:-1],
                    'acor': sampler.acor
                   }
        else:
            return {'chain': sampler.flatchain[params['burnin']:-1, :],
                    'accept': sampler.acceptance_fraction,
                    'lnprob': sampler.lnprobability[params['burnin']:-1]
                   }
        
    def matrixify(self, cov):
        if np.isscalar(cov):
            return np.diag(np.repeat(cov, self.parameters['dim']))
        elif cov.shape == (self.parameters['dim'],):
            return np.diag(cov)
        elif cov.shape == (self.parameters['dim'], self.parameters['dim']):
            return cov
        else:
            raise Exception('Covariance has incompatible shape')

class HMCSampler(_Sampler):
    def __init__(self, distr):
        super().__init__(distr, 'p0', 'display', 'nsteps', 'burnin', 'decay',
                'epsilon', 'window', 'persistence', burnin=100, nsteps=10, epsilon=0.2,
                persistence=False, decay=0.9, window=1, display=False)

    def sample_impl(self, nsamples):
        params = self.parameters
        samples = pyhmc.hmc(
            lambda x: (self.distr.neg_logpdf(x), self.distr.grad_neg_logpdf(x)),
            params['p0'], n_samples=nsamples, display=params['display'],
            n_steps=params['nsteps'], n_burn=params['burnin'],
            persistence=params['persistence'], decay=params['decay'],
            epsilon=params['epsilon'], window=params['window']
            )
        return {'chain': samples}

# class NUTSSampler(_Sampler):
#     def __init__(self, distr):
#         super().__init__(distr, 'M', 'burnin', 'p0', 'delta', M=10, burnin=100,
#                 delta=0.2)

#     def sample_impl(self, nsamples):
#         sampler = mcnuts.NUTSSampler(self.dim, self.distr.logpdf,
#                                      self.distr.gradlogpdf)
#         return sampler.run_mcmc(self.parameters['p0'], self.parameters['M'],
#                                 self.parameters['burnin'],
#                                 self.parameters['delta'])

# class SteinSampler(_Sampler): #This implements the Galerkin Projection Block Hessian Stein Sampler
#     def __init__(self, distr):
#         super().__init__(distr, 'stepsize', 'itermax','linear_solve_max_iters','linear_solve_tol','convergence_callback', itermax=100, linear_solve_max_iters=50, linear_solve_tol=1e-4)

#     def sample_impl(self, nsamples):
#         self.prior_samples = self.distr.prior._samplemany(nsamples)
#         prior_samples_stein = self.prior_samples.T.copy() #right now the SVN_H_New takes tranposed samples!!
#         return SVN_H_New(prior_samples_stein, 
#                          self.parameters['stepsize'], 
#                          self.parameters['itermax'], 
#                          self.distr,
#                          self.parameters['convergence_callback'],
#                          # tol0=self.parameters['linear_solve_tol'],
#                          # maxiter0=self.parameters['linear_solve_max_iters'],
#                          )


