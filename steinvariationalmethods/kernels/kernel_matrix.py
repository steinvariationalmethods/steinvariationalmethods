#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein_variational_inference.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#

import numpy as np

class KernelMatrix(np.ndarray):
    """
    Class wrapping the in-memory representation of the kernel matrix of a set
    of points as a dense n*(n-1)//2 shared memory array. 
    """
    def __init__(self, npoints, comm, buf=None):
        """
        __init__(self, npoints, comm, buf=None)

        npoints: The number of points that defines the size of the kernl matrix. Used for checking size
        of function arguments.
        
        comm: The MPI communicator
        buf: *Must* be specified, should be a numpy ndarray with shape (n*(n-1)//2,).
        dim is inferred as buf.shape[1]. This becomes storage for all of the pairwise
        differences.
        """
        if buf is None:
            raise Exception("""
                            Please supply a memory buffer of size (n*(n-1)//2,)
                            to hold the kernel matrix.
                            """)
        elif buf.shape[0] != npoints*(npoints-1)//2:
            raise Exception("Buffer should have space for n*(n-1)//2 scalars")
        elif np.ndim(buf) != 1:
            raise Exception("Buffer should be a 1-dimensional array")

        self._kernel_matrix = buf
        self._npoints = npoints

    @property
    def buffer(self):
        return self._kernel_matrix

    @buffer.setter
    def buffer(self, buf):
        if buf.shape[0] != self._npoints*(self._npoints-1)//2:
            raise Exception("Buffer should have space for n*(n-1)//2 points")
        elif np.ndim(buf) != 1:
            raise Exception("Buffer should be a 1-dimensional array")
        elif buf.shape[1] != self._dim:
            raise Exception("buf.shape[1] != self._dim")

        self._kernel_matrix = buf

    def _baseindex(self, i):
        return i * self._npoints - i*(i+1)//2

    # use views https://stackoverflow.com/questions/2572916/numpy-smart-symmetric-matrix
    # subclass np.ndarray    
    def get_val(self, i, j):
        """
        get_val(self, i, j)

        Get the Kernel matrix value between points[i,:] and points[j, :].
        
        Note that if i > j, returns get_val(self, j, i).
        """
        if not (i >= 0 and i < self._npoints and j >= 0 and j < self._npoints):
            raise Exception("i and j should both be valid point indices")
        elif i == j:
            return 1
        elif i > j:
            return self._kernel_matrix[self._baseindex(j) + (i-j) - 1, :]
        else:
            return self._kernel_matrix[self._baseindex(i) + (j-i) - 1, :]

    #we need to be able to get access to values using [:, i], etc, so subclass nd.array
    def __setitem__(self, (i, j), value):
        pass
