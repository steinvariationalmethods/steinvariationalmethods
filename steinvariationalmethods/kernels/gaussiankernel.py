import numpy as np
from numba import vectorize, guvectorize, float64, jit
from mpi4py.MPI import Wtime


from .kernel_base import _Kernel
   
 #THis could be instead stored in a metric class
 #with the functions 
@guvectorize([(float64[:,:], float64[:], float64[:], float64[:], float64[:])],
         '(dim, rank), (rank), (dim), (dim), ()', nopython=True, cache=True)
def low_rank_val(U, d, x, y, result):
    """
    Evaluate the evaluation of the kernel for p_i, p_j, i.e. if H(p_j) = U D U.T, 
    k_diffs = exp(-0.5* diffs.T U D U.T diffs). 
    """
    diff = x - y
    exponent = -0.5 * np.sum(d*(U.T @ diff)**2.) #this is incorrect. work this out fir diffs...
    result[0] = np.exp(exponent)

@guvectorize([(float64[:,:], float64[:], float64[:], float64[:], float64[:], float64[:])],
         '(dim, rank), (rank), (dim), (dim), (dim), (dim)', nopython=True, cache=True)
def low_rank_grad_val(U, d, x, y, hessian_action_y, result): 
    """
    Evaluate the gradient of kernel at (p_i, p_j) with respect to p_j, i.e. if H(p_j) = U D U.T, 
    grad_j K(diffs) = k(diffs) * diffs.T U D U.T diffs, or
    """
    diff = x - y
    H_diff = (U*d)@(U.T @ diff)
    hessian_action_y[:] = H_diff
    result[:] = np.exp(-0.5 * (diff.T @ H_diff)) * H_diff

#allocate one shared memory Kernel matrix, abstract object? ability to do Kij * etc ij

# init_allocations_size is ipmortant if you want efficiency in the beginning
# otherrise, default is ok...
#init_allocations_size = num particles initial 
#instantiate posterior_distribution(dims, parameters, init_allocations_size, comm)
#instantiate metric(posterior_distribution init_allocations_size)
# Kernel(metric, init_allocations_size)
# stein variational objective gradient/hessian
# tein variational objective.particles
# stein variational objective.add_partilces(nsampmles)
# add_partilces should take the local uDU.T and 
# perform a Gaussian sampling.
# this means, if U D U.T was not done yet, need to do it,
# when particles update (boolean UDU.T is updated as false)
# otherwise, LowRankPriorUpdateGaussian.sample(U,D,\diag Sigma) \ksi + mu to get samples

@guvectorize([(float64[:], float64[:, :])],
         '(N), (N, dim)', nopython=True, cache=True)
def grad_gram_matrix_function(gram_matrix_row, hess_actions):
    for i in range(gram_matrix_row.shape[0]):
        gram_matrix_row_i = np.exp(gram_matrix_row[i])
        gram_matrix_row[i] = gram_matrix_row_i
        hess_actions[i] = gram_matrix_row_i * hess_actions[i]

class GaussianKernel(_Kernel):
    """A Gaussian Kernel, K(p_i,p_j) = exp(-0.5*(p_i-p_j)^T * M * (p_i-p_j)), 
    with precision defined by a metric M (in general a function of p_i)
    NOTICE FOR NOW, we neglect normalization. We may come back to this!
    """
    def __init__(self, metric, init_allocations_size=1):
        super().__init__(metric, init_allocations_size=init_allocations_size)



    #We want to be able to pass in all ys for which to compute the , all diffs (j > i) (i.e. one call to this function fills out a row of the Kernel matrix, and one row of the gradient of the Kernel Matrix)
    def _calculate_grad_and_gram_matrix_impl(self, xs, ys, gram_matrix, grad_gram_matrix, gauss_newton, projections): #N x N x dim shared memory 
        """
        Evaluate the gradient of kernel at (p_i, p_j) with respect to p_j, grad K(p_i, p_j) = k(p_i, p_j) * M(p_i) * (p_i - p_j)
        """
        self.metric.quadratic_forms(xs, ys, grad_gram_matrix, gram_matrix, gauss_newton, projections)

        t0 = Wtime()

        grad_gram_matrix_function(gram_matrix, grad_gram_matrix) 

        self._kernel_time = Wtime() - t0


