from abc import ABC, abstractmethod
import numpy as np

from misc import *

class _Kernel(ABC):
    r"""A Kernel allows for evaluation of the Kernel function at pairs of particles which
    live in parameter space to form the `gram matrix' and gradient of the `gram matrix'"""
    def __init__(self, metric, init_allocations_size=(1,1)):
        assert(len(init_allocations_size) == 2 )
        self._metric = metric
        self._gram_matrix = np.zeros(init_allocations_size)

        N_i, N_j = init_allocations_size
        self._grad_gram_matrix = np.zeros((N_i, N_j, self.metric.dims))

        self._kernel_time = 0.0

        #rowsum gram matrix? columnsum grad gram matrix?
        #rowsum grad gram matrix? columnsum grad gram matrix 

    @property
    def metric(self):
        return self._metric

    @property
    def kernel_time(self):
        return self._kernel_time

    def reset_times(self):
        self._kernel_time = 0.0

    @property
    def comm(self):
        return self._metric.comm

    @property
    def gram_matrix(self):
        #Should be a numpy object that has access to *, the pointwise multiplication
        return self._gram_matrix

    @property
    def grad_gram_matrix(self):
        #Should be a numpy object that has access to *, the pointwise multiplication
        return self._grad_gram_matrix

    def _expand_array(self, array, extra_shape):
        #add zeros to the end of the array given a compatible "extra shape":
        return np.concatenate((array, np.zeros(extra_shape)), axis=0)
        #we expand arrays when the number of particles changes, and when 

    def _assign_size(self, N):
        # assign size of local portion
        return assign_size(N, self.comm)

    def _grad_gram_matrix_allocations_impl(self, xs, ys, projections):
        assert(xs.shape[1] == ys.shape[1])
        N_i = xs.shape[0]
        N_j = ys.shape[0]
        if projections:
            dims = xs.projected_dims
        else:
            dims = self.metric.dims

        if (N_i, N_j, dims) != self._grad_gram_matrix.shape:
            self._gram_matrix = np.zeros((N_i, N_j))
            self._grad_gram_matrix = np.zeros((N_i, N_j, dims))
        else:
            self._gram_matrix[:] = 0
            self._grad_gram_matrix[:] = 0

        
        #no zeroing out necessary here

    def calculate_grad_and_gram_matrix(self, xs, ys, gaussnewton=False, projections=False):
        #xs is local, ys is the whole array locally

        #gradient with respect to ys
        self._grad_gram_matrix_allocations_impl(xs, ys, projections)
        # print('||gram|| before assignment',np.linalg.norm(self._gram_matrix))
        # print('||grad gram|| before assignment',np.linalg.norm(self._grad_gram_matrix))
        self._calculate_grad_and_gram_matrix_impl(xs, ys, self._gram_matrix, self._grad_gram_matrix, gaussnewton, projections)
        # print('||gram|| after assignment',np.linalg.norm(self._gram_matrix))
        # print('||grad gram|| after assignment',np.linalg.norm(self._grad_gram_matrix))
        return self._grad_gram_matrix, self._gram_matrix

    @abstractmethod
    def _calculate_grad_and_gram_matrix_impl(self, xs, ys, gram_matrix, result, eigendecomp, gaussnewton, projections):
        pass
