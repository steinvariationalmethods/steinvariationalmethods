#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein variational inference methods class project.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#
import numpy as np

from scipy.linalg import cholesky, eigh, solve_triangular, qr, rq
from misc import *
import prngs.parallel as random

import time
def randomized_double_pass_eigensolver(Aop, Y, k):
    """
    Randomized algorithm for Hermitian eigenvalue problems
    Returns k largest eigenvalues computed using the randomized algorithm
    
    Parameters:
    -----------
    Aop : {Callable} n x n
          Hermitian matrix operator whose eigenvalues need to be estimated
          y = Aop(w_hat) is the action of A in the direction w_hat 
    Y = Aop(Omega) : precomputed action of Aop on Omega, a m x n Array of (presumably) sampled Gaussian or l-percent sparse random vectors (row)
    k :  int, 
        number of eigenvalues/vectors to be estimated, 0 < k < m
    Returns:
    --------
    
    lmbda : ndarray, (k,)           
        eigenvalues arranged in descending order
    Ut : ndarray, (k, n)
        eigenvectors arranged according to eigenvalues, rows are eigenvectors
    
    References:
    -----------
    .. [1] Halko, Nathan, Per-Gunnar Martinsson, and Joel A. Tropp. "Finding structure with randomness: Probabilistic algorithms for constructing approximate matrix decompositions." SIAM review 53.2 (2011): 217-288.
    .. [2] Algorithm 2 of Arvind paper
    Examples:
    ---------
    >>> import numpy as np
    >>> n = 100
    >>> A = np.diag(0.95**np.arange(n))
    >>> Aop = lambda w_hat: np.dot(A,w_hat)
    >>> k = 10
    >>> p = 5
    >>> Omega = np.random.randn(n, k+p)
    >>> lmbda, Ut = randomized_eigensolver(Aop, Omega, k)
    """
    m, n = Y.shape 
    assert(n >= m >= k) #m = k + p ( p is the oversampling for Omega, to ensure we get a good random projection basis)
    Q, _ = qr(Y.T, mode='economic')
    T =  (Aop(Q.T) @ Q).T #m foward problems , m x m small matrix
    # T = .5*T + .5*T.T

    #Eigen subproblem
    lmbda, V = eigh(T, turbo=True, overwrite_a=True, check_finite=False)
    inds = np.abs(lmbda).argsort()[::-1]
    lmbda = lmbda[inds[0:k]]
    V = V[:, inds[0:k]] #S in the original paper m x m

    #Compute eigenvectors
    Ut = (Q @  V).T 
    return lmbda, Ut

def randomized_single_pass_generalized_eigensolver(Aop, Bop, Binvop, Omega, k): 
    """
    The single pass algorithm for the GHEP as presented in [2].

    Randomized algorithm for Generalized Hermitian eigenvalue problems, i.e. 
    solving Au = lambda Bu
    Returns k largest eigenvalues computed using the randomized algorithm
    
    
    Parameters:
    -----------
    Aop : {Callable} tn x n
          Hermitian matrix operator whose eigenvalues need to be estimated
          y = Aop(w_hat) is the action of A in the direction w_hat 
          
    B  : {Callable} n x n
          Hermetian RHS matrix operator. The eigenvectors u should live
          in the range space of Aop
    Binv: {Callable} n x n
          Inverse of B 
    n : int,
           number of row/columns of the operator A
        
    k :  int, 
        number of eigenvalues/vectors to be estimated
    p :  int, optional
        oversampling parameter which can improve accuracy of resulting solution
        Default: 20
            
    Returns:
    --------
    
    w : ndarray, (k,)
        eigenvalues arranged in descending order
    u : ndarray, (n,k)
        eigenvectors arranged according to eigenvalues such that U^T B U = I_k
    
    References:
    -----------
    [2] Arvind K. Saibaba, Jonghyun Lee, Peter K. Kitanidis, Randomized algorithms for Generalized Hermitian Eigenvalue Problems with application to computing Karhunen-Loeve expansion, Numerical Linear Algebra with Applications, 2010
    Examples:
    ---------
    >>> import numpy as np
    >>> n = 100
    >>> A = np.diag(0.95**np.arange(n))
    >>> Aop = lambda w_hat: np.dot(A,w_hat)
    >>> k = 10
    >>> p = 5
    >>> lmbda, U = randomized_single_pass_generalized_eigensolver(Aop, n, k, p)
    """
    raise Exception("Need to reimplement this function")
    n, m  = Omega.shape
    
    assert(n >= m >= k )
    
    Ybar = Aop(Omega)
        
    Y = Binvop(Ybar)

    # CholQR with W-inner products
    Z,_ = np.linalg.qr(Y)
    BZ = Bop(Z)
        
    R = np.linalg.cholesky( np.dot(Z.T,BZ )) 
    Q = np.linalg.solve(R, Z.T).T #Q = Y*R^-1
    BQ = np.linalg.solve(R, BZ.T).T #WQ = Z * R^-1
    
        
    Xt = np.dot(Omega.T, BQ)
    Wt = np.dot(Ybar.T, Q)
    Tt = np.linalg.solve(Xt,Wt)
                
    T = .5*Tt + .5*Tt.T
        
    d, V = np.linalg.eigh(T)
    
    d_abs = np.abs(d) #sort by absolute value (we want the k largest eigenvalues regardless of sign)
    sort_perm = d_abs.argsort()
        
    sort_perm = sort_perm[::-1]
    
    d = d[sort_perm[0:k]]
    V = V[:, sort_perm[0:k]] 
        
    U = Q @ V
    return d, U


def randomized_double_pass_generalized_eigensolver(Aop, Bop, Binvop, Y, k): 
    """
    The double pass algorithm for the GHEP as presented in [2].

    Randomized algorithm for Generalized Hermitian eigenvalue problems, i.e. 
    solving Au = lambda Bu
    Returns k largest eigenvalues computed using the randomized algorithm
    
    #Algorithm 8 in the Arvind paper
    Parameters:
    -----------
    - :code:`Aop`: the operator for which we need to estimate the dominant generalized eigenpairs.
    - :code:`Bop`: the right-hand side operator.
    - :code:`Binvop`: the inverse of the right-hand side operator.
    - :code:`Omega`: a random gassian matrix with :math:`m \\geq k` columns.
    - :code:`k`: the number of eigenpairs to extract.
    - :code:`s`: the number of power iterations for selecting the subspace.
    
    Aop : {Callable} n x n
          Hermitian matrix operator whose eigenvalues need to be estimated
          y = Aop(w_hat) is the action of A in the direction w_hat 
    Y = AOmega : m x n Array of (presumably) sampled Gaussian or l-percent sparse random vectors (rows)
    B  : {Callable} n x n
          Hermetian RHS matrix operator. The eigenvectors u should live
          in the range space of Aop
    Binv : {Callable} n x n
          Hermetian inverse of RHS matrix operator.
    k :  int, 
        number of eigenvalues/vectors to be estimate
           
    Returns:
    --------
    
    - :code:`lmbda`: the estimate of the :math:`k` dominant eigenvalues of :math:`A`.
    - :code:`U`: the estimate of the :math:`k` dominant eigenvectors of :math:`A,\\, U^T B U = I_k`.
    
    Examples:
    ---------
    >>> import numpy as np
    >>> n = 100
    >>> A = np.diag(0.95**np.arange(n))
    >>> B = np.diag(0.25**np.arange(n))
    >>> Binv = np.diag(1/0.25**np.arange(n))
    >>> Aop = lambda w_hat: np.dot(A,w_hat)
    >>> Bop = lambda w_hat: np.dot(B,w_hat)
    >>> Binvop = lambda w_hat: np.dot(Binv,w_hat)
    >>> Omega = np.random.normal((n,n))
    >>> k = 10
    >>> p = 5
    >>> lmbda, U = randomized_double_pass_generalized_eigensolver(Aop, Bop, Binvop, Omega, k)
    """
    m, n  = Y.shape # m = k+p < N
    assert(n >= m >= k )

    #ALGO 4:
    # CholQR with B-inner products, Y = QR, Q.T B Q = I
    Zt, _ = qr(Y.T, mode='economic') #Y = QR, Z = Q
    BZ = Bop(Zt.T).T
    R = cholesky( np.dot(Zt.T, BZ), lower=True) #R = chol(Y.T B Y)
    Q = solve_triangular(R, Zt.T,lower=True) #Q = Y*R^-1 
    # T = np.zeros((m,m))
    T =  (Aop(Q)@ Q.T) #m x m small matrix, #m forward problems
    # T = .5*T + .5*T.T
    #Eigen subproblem
    lmbda, V = eigh(T, turbo=True, overwrite_a=True,check_finite=False)
    inds = np.abs(lmbda).argsort()[::-1]
    lmbda = lmbda[inds[0:k]]
    V = V[:, inds[0:k]] #S in the original paper m x m
    #Compute eigenvectors
    U = V.T @  Q
    return lmbda, U



def randomized_posterior_ggT_eigendecomp(prior, xs, Omega,  low_rank, lmbdas_result, Uts_result, posterior, gaussnewton, common_projector): 
    assert low_rank > 0

    #Omega size: m x n_dim, n_dim > m > k 
    N = xs.shape[0]
    # print(low_rank,"should be same as ", Omega.shape[0],"for now")
    if posterior.likelihood is not None:
        Aop = lambda Omega : posterior.likelihood.ggT_neg_logpdf_actions(xs, Omega, gaussnewton=gaussnewton)       
    else:
        print('Entering not at the likelihood')
        Aop = lambda Omega : posterior.ggT_neg_logpdf_actions(xs, Omega, gaussnewton=gaussnewton) \
                            - prior.ggT_neg_logpdf_actions(xs, Omega, gaussnewton=gaussnewton)           
    Binvop = lambda AOmega : prior.hess_inv_neg_logpdf_actions(xs, AOmega, gaussnewton=gaussnewton)

    #Do all randomized hessian actions first


    Y = Binvop(Aop(Omega)) #Y is different on each worker
    # print("Y get", Y,Y.shape)
    # print("got Y done")
    #Then do eigendecompositions on each worker
    def Aop(Omega_mat):
        out = np.zeros_like(Omega_mat)
        if posterior.likelihood is not None:
            for i in range(N):
                out += posterior.likelihood.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            out /=N
        else:
            for i in range(N):
                out += posterior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i) - prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            out /=N


        return out

    def Bop(Omega_mat):
        out = np.zeros_like(Omega_mat)
        for i in range(N):
            out += prior.hess_inv_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton)
        out /=N
        return out
    def Binvop(Omega_mat):
        out = np.zeros_like(Omega_mat)
        for i in range(N):
            out += prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
        out /=N
        return out 
    if prior.comm is None or prior.comm.Get_size() == 1:
        Y = np.mean(Y,axis = 0)
    else:
        Y = np.mean(Y,axis = 0)
        print('shape of Y prior to reduction = ',Y.shape)
        Allreduce_mean_axis_0(Y, prior.comm)
        print('shape of Y after reduction = ',Y.shape)


    print('Y shape after mean = ', Y.shape)
    print('Uts_result shape prior to assignment = ', Uts_result.shape)
    lmbdas_result[:], Uts_result[:] = randomized_double_pass_generalized_eigensolver(Aop, Bop, Binvop, Y, low_rank)  
    print('Uts_result shape after assignment = ', Uts_result.shape)
    lmbdas_result[:] += 1 #add the prior contribution to the posterior eigenvalues

    # print("Finished randomized posterior hess eignedecomp")


def randomized_posterior_hess_eigendecomp(prior, xs, Omega,  low_rank, lmbdas_result, Uts_result, posterior, gaussnewton, common_projector): 
    assert low_rank > 0

    #Omega size: m x n_dim, n_dim > m > k 
    N = xs.shape[0]
    # print(low_rank,"should be same as ", Omega.shape[0],"for now")
    if posterior.likelihood is not None:
        Aop = lambda Omega : posterior.likelihood.hess_neg_logpdf_actions(xs, Omega, gaussnewton=gaussnewton)       
    else:
        Aop = lambda Omega : posterior.hess_neg_logpdf_actions(xs, Omega, gaussnewton=gaussnewton) \
                            - prior.hess_neg_logpdf_actions(xs, Omega, gaussnewton=gaussnewton)           
    Binvop = lambda AOmega : prior.hess_inv_neg_logpdf_actions(xs, AOmega, gaussnewton=gaussnewton)
    #Do all randomized hessian actions first

    Y = Binvop(Aop(Omega)) #Y is different on each worker
    # print("Omega", Omega)
    # print("Y before", Y)
    for i in range(N):
        matrix_rank = np.linalg.matrix_rank(Y[i])
        if matrix_rank == 0:
            print(80*'#')
            print('The matrix rank of Y is ',matrix_rank, ' for i =  ',i,'on rank ',prior.comm.Get_rank())
            print(80*'#')
    # print("Y get", Y,Y.shape)
    # print("got Y done")
    #Then do eigendecompositions on each worker
    if not common_projector:
        for i in range(N):
            #works on Omega matrix, not just an omega vector
            if posterior.likelihood is not None:
                Aop_i = lambda Omega_mat : posterior.likelihood.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            else:
                Aop_i = lambda Omega_mat : posterior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i) \
                                    - prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            Binvop_i = lambda Omega_mat : prior.hess_inv_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            Bop_i = lambda Omega_mat : prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            # print("starting a randomized svd")
            lmbdas_result[i], Uts_result[i] = randomized_double_pass_generalized_eigensolver(Aop_i, Bop_i, Binvop_i, Y[i], low_rank)
            # print('lmbdas_result[i]',lmbdas_result[i],'at iteration i ', i )
            # print("Finished a randomized svd")
            lmbdas_result[i] += 1 #add the prior contribution to the posterior eigenvalues
            # print('lmbdas_result[i]',lmbdas_result[i],'at iteration i ', i , ' after the plus 1' )
    else:
        def Aop(Omega_mat):
            out = np.zeros_like(Omega_mat)
            if posterior.likelihood is not None:
                for i in range(N):
                    out += posterior.likelihood.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
                out /=N
            else:
                for i in range(N):
                    out += posterior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i) - prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
                out /=N
            # print("out before", out)
            out = out[np.newaxis,:]
            out = Allreduce_mean_axis_0(out, prior.comm)
            # print("out after", out)

            return out

        def Binvop(Omega_mat):
            out = np.zeros_like(Omega_mat)
            for i in range(N):
                out += prior.hess_inv_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton)
            out /=N
            out = out[np.newaxis,:]
            out = Allreduce_mean_axis_0(out, prior.comm)
            return out
        def Bop(Omega_mat):
            out = np.zeros_like(Omega_mat)
            for i in range(N):
                out += prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            out /=N
            out = out[np.newaxis,:]
            out = Allreduce_mean_axis_0(out, prior.comm)
            return out 
        # Y = np.mean(Y,axis = 0)
        if prior.comm is None or prior.comm.Get_size() == 1:
            Y = np.mean(Y,axis = 0)
        else:
            # Y = np.mean(Y,axis = 0)
            # print("YYY before", Y)
            # print('shape of Y prior to reduction = ',Y.shape)
            Y = Allreduce_mean_axis_0(Y, prior.comm)
            # print('shape of Y after reduction = ',Y.shape)
        # print("YYY after", Y)
        lmbdas_result[:], Uts_result[:] = randomized_double_pass_generalized_eigensolver(Aop, Bop, Binvop, Y, low_rank)
        lmbdas_result[:] += 1 #add the prior contribution to the posterior eigenvalues

    # print("Finished randomized posterior hess eignedecomp")

def randomized_likelihood_hess_eigendecomp(prior, xs, Omega,  low_rank, lmbdas_result, Uts_result, posterior, gaussnewton, common_projector): 
    assert low_rank > 0

    #Omega size: m x n_dim, n_dim > m > k 
    N = xs.shape[0]
    # print(low_rank,"should be same as ", Omega.shape[0],"for now")
    if posterior.likelihood is not None:
        Aop = lambda Omega : posterior.likelihood.hess_neg_logpdf_actions(xs, Omega, gaussnewton=gaussnewton)       
    else:
        Aop = lambda Omega : posterior.hess_neg_logpdf_actions(xs, Omega, gaussnewton=gaussnewton) \
                            - prior.hess_neg_logpdf_actions(xs, Omega, gaussnewton=gaussnewton)           
    Binvop = lambda AOmega : prior.hess_inv_neg_logpdf_actions(xs, AOmega, gaussnewton=gaussnewton)
    #Do all randomized hessian actions first


    Y = Binvop(Aop(Omega)) #Y is different on each worker
    # print("Y get", Y,Y.shape)
    # print("got Y done")
    #Then do eigendecompositions on each worker
    if not common_projector:
        for i in range(N):
            #works on Omega matrix, not just an omega vector
            if posterior.likelihood is not None:
                Aop_i = lambda Omega_mat : posterior.likelihood.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            else:
                Aop_i = lambda Omega_mat : posterior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i) \
                                    - prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            Binvop_i = lambda Omega_mat : prior.hess_inv_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            Bop_i = lambda Omega_mat : prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            # print("starting a randomized svd")
            lmbdas_result[i], Uts_result[i] = randomized_double_pass_generalized_eigensolver(Aop_i, Bop_i, Binvop_i, Y[i], low_rank)
            # print('lmbdas_result[i]',lmbdas_result[i],'at iteration i ', i )
            # print("Finished a randomized svd")
    else:
        def Aop(Omega_mat):
            out = np.zeros_like(Omega_mat)
            if posterior.likelihood is not None:
                for i in range(N):
                    out += posterior.likelihood.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
                out /=N
            else:
                for i in range(N):
                    out += posterior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i) - prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
                out /=N
            return out

        def Bop(Omega_mat):
            out = np.zeros_like(Omega_mat)
            for i in range(N):
                out += prior.hess_inv_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton)
            out /=N
            return out
        def Binvop(Omega_mat):
            out = np.zeros_like(Omega_mat)
            for i in range(N):
                out += prior.hess_neg_logpdf_action(xs[i], Omega_mat, gaussnewton=gaussnewton, idx=i)
            out /=N
            return out 
        Y = np.mean(Y,axis = 0)
        lmbdas_result[:], Uts_result[:] = randomized_double_pass_generalized_eigensolver(Aop, Bop, Binvop, Y, low_rank)

    # print("Finished randomized posterior hess eignedecomp")