from . import generator
from .generator import get_state, set_state, get_generator, rand, randn

