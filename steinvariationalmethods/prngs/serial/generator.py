from randomgen import Xoroshiro128
from .. import _GeneratorBase

class SerialGenerator(_GeneratorBase):
    def __init__(self):
        self.gen = Xoroshiro128()

    def set_state_impl(self, s):
        self.gen.state = s

global_generator = SerialGenerator()

def set_state(state):
    global_generator.state = state
    return state

def get_state():
    return global_generator.state

def get_generator():
    return global_generator.generator

def rand(*args):
    return global_generator.rand(*args)

def randn(*args):
    return global_generator.randn(*args)

