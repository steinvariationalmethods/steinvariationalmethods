
from randomgen import Xorshift1024
from .. import _GeneratorBase

from mpi4py import MPI
comm = MPI.COMM_WORLD





class ParallelGenerator(_GeneratorBase):
    def __init__(self):
        try:
            rank = comm.Get_rank()
        except:
            rank = 0
        if rank == 0:
            self.gen = Xorshift1024()
        else:
            self.gen = None
        self.gen = comm.bcast(self.gen, root=0).jump(rank)

    def set_state_impl(self, s):
        rank = comm.Get_rank()
        if rank == 0:
            self.gen.state = s

        self.gen = comm.bcast(self.gen, root=0).jump(rank)

global_generator = ParallelGenerator()

def set_state(state):
    global_generator.state = state
    return state

def get_state():
    return global_generator.state

def get_generator():
    return global_generator.generator

def rand(*args, **kwargs):
    return global_generator.rand(*args, **kwargs)

def randn(*args, **kwargs):
    return global_generator.randn(*args, **kwargs)

def multivariate_normal(*args, **kwargs):
    return global_generator.multivariate_normal(*args, **kwargs)

def binomial(*args, **kwargs):
    return global_generator.binomial(*args, **kwargs)

def randint(*args, **kwargs):
    return global_generator.randint(*args, **kwargs)

def dirichlet(*args, **kwargs):
    return global_generator.dirichlet(*args, **kwargs)

def exponential(*args, **kwargs):
    return global_generator.exponential(*args, **kwargs)

def gamma(*args, **kwargs):
    return global_generator.gamma(*args, **kwargs)

def geometric(*args, **kwargs):
    return global_generator.geometric(*args, **kwargs)

def laplace(*args, **kwargs):
    return global_generator.laplace(*args, **kwargs)

def lognormal(*args, **kwargs):
    return global_generator.lognormal(*args, **kwargs)

def multinomial(*args, **kwargs):
    return global_generator.multinomial(*args, **kwargs)

def normal(*args, **kwargs):
    return global_generator.normal(*args, **kwargs)

def permutation(*args, **kwargs):
    return global_generator.permutation(*args, **kwargs)

def uniform(*args, **kwargs):
    return global_generator.uniform(*args, **kwargs)

def choice(*args, **kwargs):
    return global_generator.choice(*args, **kwargs)

def l_percent_sparse(*args, **kwargs):
    return global_generator.l_percent_sparse(*args, **kwargs)
