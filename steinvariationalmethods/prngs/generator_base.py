import numpy as np
from scipy.sparse import csr_matrix, csc_matrix

class _GeneratorBase:
    @property
    def state(self):
        return self.gen.state

    @state.setter
    def state(self, s):
        self.set_state_impl(s)

    @property
    def generator(self):
        return self.gen.generator

    def rand(self, *args):
        return self.generator.rand(*args)

    def multivariate_normal(self, *args, **kwargs):
        return self.generator.multivariate_normal(*args, **kwargs)

    def binomial(self, *args, **kwargs):
        return self.generator.binomial(*args, **kwargs)

    def randint(self, *args, **kwargs):
        return self.generator.randint(*args, **kwargs)

    def dirichlet(self, *args, **kwargs):
        return self.generator.dirichlet(*args, **kwargs)

    def exponential(self, *args, **kwargs):
        return self.generator.exponential(*args, **kwargs)

    def gamma(self, *args, **kwargs):
        return self.generator.gamma(*args, **kwargs)

    def geometric(self, *args, **kwargs):
        return self.generator.geometric(*args, **kwargs)

    def laplace(self, *args, **kwargs):
        return self.generator.laplace(*args, **kwargs)

    def lognormal(self, *args, **kwargs):
        return self.generator.lognormal(*args, **kwargs)

    def multinomial(self, *args, **kwargs):
        return self.generator.multinomial(*args, **kwargs)

    def normal(self, *args, **kwargs):
        return self.generator.normal(*args, **kwargs)

    def permutation(self, *args, **kwargs):
        return self.generator.permutation(*args, **kwargs)

    def choice(self, *args, **kwargs):
        return self.generator.choice(*args, **kwargs)
        
    def uniform(self, *args, **kwargs):
        return self.generator.uniform(*args, **kwargs)

    def l_percent_sparse(self, l, size, *args, **kwargs):
        "More or less referencing scikit-learn's version in https://github.com/scikit-learn/scikit-learn/blob/7389dba/sklearn/random_projection.py#L513"
        """This is the l-percent sparse random matrix of shape p x n, also known as the Generalized 
        Achlioptas random sparse matrix often used for random projection.
        l = 2 / 3 corresponds to the Achlioptas distribution.
        l < 2 / 2 is the generalization by Ping Li et al.
        For sparsity :math:`s = 1 / (1 - l)`, the random variable is defined as 
                   { + 1 / sqrt(p)   with probability 1 / 2s
        sqrt(s) *  {   0                              with probability 1 - 1 / s
                   { - 1 / sqrt(p)   with probability 1 / 2s

        Parameters
        ----------
        size : (int, int) 
              : (p, n)
                    p : int,
                        Dimensionality of the target projection space.
                    n : int,
                        Dimensionality of the original space. n > p
        l : float in range ]0, 1], Sparsity of the random variable
            Use l = 2 / 3.0 to reproduce the results from
            Achlioptas, 2001.
        Returns
        -------
        components : array or CSR matrix with shape [p, n]
            The generated Gaussian random matrix.
        See Also
        --------
        SparseRandomProjection
        gaussian_random_matrix
        References
        ----------
        .. [1] Ping Li, T. Hastie and K. W. Church, 2006,
               "Very Sparse Random Projections".
               http://web.stanford.edu/~hastie/Papers/Ping/KDD06_rp.pdf
        .. [2] D. Achlioptas, 2001, "Database-friendly random projections",
               http://www.cs.ucsc.edu/~optas/papers/jl.pdf  
        """
        #requires shape to be a tuple of size 2
        (n, p) = size
        if l == 0: #Rademacher random variable, can skip generation of sparsity locations, since fully dense
            components = self.generator.binomial(1, 0.5, size) * 2 - 1
            return 1 / np.sqrt(p) * components
        else:
            # Generate CSR locations of sparsity/non-zeros
            indices = []
            offset = 0
            indptr = [offset]
            for i in range(p):
                # find the indices of the non-zero components for column i
                nnz_i = self.generator.binomial(n, 1-l)
                indices_i = self.generator.choice(n, nnz_i, replace=False)
                indices.append(indices_i)
                offset += nnz_i
                indptr.append(offset)

            indices = np.concatenate(indices)

            # The probability of the sign is 50/50
            data = self.generator.binomial(1, 0.5, size=np.size(indices)) * 2 - 1

            # build the CSR structure by concatenating the rows
            components = csc_matrix((data, indices, indptr),
                                       shape=size)

            return np.sqrt(1/(1-l)) / np.sqrt(p) * components
