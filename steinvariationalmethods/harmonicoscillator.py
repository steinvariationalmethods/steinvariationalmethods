import numpy as np
from numba import vectorize, jit, float64
from scipy import special
from math import factorial

from distributions import _Distribution, require_parameters

#trying to work out using scipy rather than manually determining the derivatives
#logpdfs seem to be -x^2 -log(pi)/2 + log((H_n/2)**2/n!) - nlog(2), where H_n is the hermite polynomial
#see the update to HarmOsc.ipynb
def H(n, x):
    return special.eval_hermite(n, x)
    
def harm_1d(n, x):
    return 2.**(-n) * H(n, x)**2 * np.exp(-x**2) / np.sqrt(np.pi) / factorial(n)
    
def harm_1d_log(n, x):
    return -n*np.log(2.) - x**2 + np.log(H(n,x)**2 / factorial(n)) - 0.5*np.log(np.pi)
    
def harm_1d_gradlog(n, x):
    return 4*n*H(n-1,x) / H(n, x) - 2*x

def harm_1d_hesslog(n, x):
    hn = H(n, x)
    return 2 * (-4*n**2 * H(n-1,x)**2 + 4*n*(n-1)*hn*H(n-2,x) - hn**2) / hn**2
    
class HarmOscDistr(_Distribution):
    def __init__(self, dim, **kwargs):
        require_parameters('degree', **kwargs)
        super().__init__(dim)
        self.n = kwargs['degree']
        
    def pdf(self, x):
        if self._dim == 1:
            return harm_1d(self.n, x)
        else:
            return np.prod([harm_1d(self.n, y) for y in x])
    
    def logpdf(self, x):
        if self._dim == 1:
            return harm_1d_log(self.n, x)
        else:
            return sum(harm_1d_log(self.n, y) for y in x)
        
    def gradlogpdf(self, x):
        if self._dim == 1:
            return harm_1d_gradlog(self.n, x)
        else:
            return np.array([harm_1d_gradlog(self.n, y) for y in x])
        
    def hess_logpdf_action_impl(self, x, gaussnewton=False):
        if gaussnewton:
            raise NotImplementedYet("The Gauss Newton Hessian is not implemented yet")
        if self._dim == 1:
            return harm_1d_hesslog(self.n, self.hess_eval_pt) * x
        else:
            hess = np.array([harm_1d_hesslog(self.n, y) for y in self.hess_eval_pt])
            return hess * x

################################################################################
# Don't use stuff below here; the class above that's generic should be preferred.
################################################################################

def quantum_harmonic_1d_scipy(n,x):
    return (special.eval_hermite(n,x)**2*(np.exp(-x**2/2)**2)/(factorial(3)*2**3)/np.sqrt(np.pi))

@vectorize([float64(float64)], nopython=True)
def π0(x):
    return np.exp(-x**2) / np.sqrt(np.pi)

@vectorize([float64(float64)], nopython=True)
def π1(x):
    return 2 * x**2 * np.exp(-x**2) / np.sqrt(np.pi)

@vectorize([float64(float64)], nopython=True)
def π2(x):
    return (2*x**4 - 2*x**2 + 1./2) * np.exp(-x**2) / np.sqrt(np.pi)

@vectorize([float64(float64)], nopython=True)
def π3(x):
    return (4./3*x**6 - 4.*x**4 + 3.*x**2) * np.exp(-x**2) / np.sqrt(np.pi)

# @vectorize([float64(float64)], nopython=True)
# def π4(x):
#     return (2*x**4 - 2*x**2 + 1./2) * np.exp(-x**2) / np.sqrt(np.pi)

# @vectorize([float64(float64)], nopython=True)
# def π5(x):
#     return (2*x**4 - 2*x**2 + 1./2) * np.exp(-x**2) / np.sqrt(np.pi)

# @vectorize([float64(float64)], nopython=True)
# def π6(x):
#     return (2*x**4 - 2*x**2 + 1./2) * np.exp(-x**2) / np.sqrt(np.pi)

# @vectorize([float64(float64)], nopython=True)
# def π7(x):
#     return (2*x**4 - 2*x**2 + 1./2) * np.exp(-x**2) / np.sqrt(np.pi)

# @vectorize([float64(float64)], nopython=True)
# def π8(x):
#     return (2*x**4 - 2*x**2 + 1./2) * np.exp(-x**2) / np.sqrt(np.pi)

def π9(x):
    return quantum_harmonic_1d_scipy(9,x)
@vectorize([float64(float64)], nopython=True)
def logπ0(x):
    return -x**2 - np.log(np.pi)/2

@vectorize([float64(float64)], nopython=True)
def logπ1(x):
    return np.log(2/np.sqrt(np.pi)) + np.log(x**2) - x**2

@vectorize([float64(float64)], nopython=True)
def logπ2(x):
    return np.log(np.pi**(-1./2)) + np.log(2*x**4 - 2*x**2 + 1./2) - x**2

@vectorize([float64(float64)], nopython=True)
def dlogπ0(x):
    return -2*x

@vectorize([float64(float64)], nopython=True)
def dlogπ1(x):
    return 2/x - 2*x

@vectorize([float64(float64)], nopython=True)
def ddlogπ0(x):
    return -2.0

@vectorize([float64(float64)], nopython=True)
def ddlogπ1(x):
    return -(2/x**2 + 2)

@vectorize([float64(float64)], nopython=True)
def dlogπ2(x):
    return 2*x*(-2*x**2 + 5)/(2*x**2 - 1)

@vectorize([float64(float64)], nopython=True)
def ddlogπ2(x):
    return -(8*x**4 + 8*x**2 + 10) / (4*x**4 - 4*x**2 + 1)

@vectorize([float64(float64, float64)], nopython=True)
def π01(x, y):
    return π0(x) * π1(y)

@vectorize([float64(float64, float64)], nopython=True)
def logπ01(x, y):
    return logπ0(x) + logπ1(y)

@jit(nopython=True)
def dlogπ01(x, y):
    return np.array((dlogπ0(x), dlogπ1(y)))

@jit(nopython=True)
def ddlogπ01(x, y):
    arr = np.zeros(2, 2)
    arr[0, 0] = ddlogπ0(x)
    arr[1, 1] = ddlogπ1(y)
    return arr

@vectorize([float64(float64, float64)], nopython=True)
def π11(x, y):
    return π1(x) * π1(y)

@vectorize([float64(float64, float64)], nopython=True)
def logπ11(x, y):
    return logπ1(x) + logπ1(y)

@jit(nopython=True)
def dlogπ11(x, y):
    return dlogπ1(np.array(x, y))

@jit(nopython=True)
def ddlogπ11(x, y):
    arr = np.zeros(2, 2)
    arr[0, 0] = ddlogπ1(x)
    arr[1, 1] = ddlogπ1(y)
    return arr

@vectorize([float64(float64, float64)], nopython=True)
def π12(x, y):
    return π1(x) * π2(y)

@vectorize([float64(float64, float64)], nopython=True)
def logπ12(x, y):
    return logπ1(x) + logπ2(y)

@jit(nopython=True)
def dlogπ12(x, y):
    return np.array((dlogπ1(x), dlogπ2(y)))

@jit(nopython=True)
def ddlogπ12(x, y):
    arr = np.zeros(2, 2)
    arr[0, 0] = ddlogπ1(x)
    arr[1, 1] = ddlogπ2(y)
    return arr

@vectorize([float64(float64, float64)], nopython=True)
def π22(x, y):
    return π2(x) * π2(y)

@vectorize([float64(float64, float64)], nopython=True)
def logπ22(x, y):
    return logπ2(x) + logπ2(y)

@jit(nopython=True)
def dlogπ22(x, y):
    return dlogπ2(np.array((x, y)))

@jit(nopython=True)
def ddlogπ22(x, y):
    arr = np.zeros(2, 2)
    arr[0, 0] = ddlogπ2(x)
    arr[1, 1] = ddlogπ2(y)
    return arr

class oscillator_distribution1(_Distribution):
    def __init__(self):
        super().__init__(1)

    def logpdf(self, x):
        return logπ1(x)

    def gradlogpdf(self, x):
        return dlogπ1(x)

    def hess_logpdf_action_impl(self, x):
        return ddlogπ1(self.m) * x

    def pdf(self, x):
        return π1(x)

class oscillator_distribution2(_Distribution):
    def __init__(self):
        super().__init__(1)

    def logpdf(self, x):
        return logπ2(x)

    def gradlogpdf(self, x):
        return dlogπ2(x)

    def hess_logpdf_action_impl(self, x):
        return ddlogπ2(self.m) * x

    def pdf(self, x):
        return π2(x)

class oscillator_distribution01(_Distribution):
    def __init__(self):
        super().__init__(2)

    def logpdf(self, x):
        return logπ01(x[0], x[1])

    def gradlogpdf(self, x):
        return dlogπ01(x[0], x[1])

    def hess_logpdf_action_impl(self, x):
        return np.dot(ddlogπ01(self.m[0], self.m[1]), x)

    def pdf(self, x):
        return π01(x[0], x[1])

class oscillator_distribution11(_Distribution):
    def __init__(self):
        super().__init__(2)

    def logpdf(self, x):
        return logπ11(x[0], x[1])

    def gradlogpdf(self, x):
        return dlogπ11(x[0], x[1])

    def hess_logpdf_action_impl(self, x):
        return np.dot(ddlogπ11(self.m[0], self.m[1]), x)

    def pdf(self, x):
        return π11(x[0], x[1])

class oscillator_distribution12(_Distribution):
    def __init__(self):
        super().__init__(2)

    def logpdf(self, x):
        return logπ12(x[0], x[1])

    def gradlogpdf(self, x):
        return dlogπ12(x[0], x[1])

    def hess_logpdf_action_impl(self, x):
        return np.dot(ddlogπ12(self.m[0], self.m[1]), x)

    def pdf(self, x):
        return π12(x[0], x[1])

class oscillator_distribution22(_Distribution):
    def __init__(self):
        super().__init__(2)

    def logpdf(self, x):
        return logπ22(x[0], x[1])

    def gradlogpdf(self, x):
        return dlogπ22(x[0], x[1])

    def hess_logpdf_action_impl(self, x):
        return np.dot(ddlogπ22(self.m[0], self.m[1]), x)

    def pdf(self, x):
        return π22(x[0], x[1])

oned_distrs = {
    1: oscillator_distribution1(),
    2: oscillator_distribution2()
}

twod_distrs = {
    (0, 1): oscillator_distribution01(),
    (1, 1): oscillator_distribution11(),
    (1, 2): oscillator_distribution12(),
    (2, 2): oscillator_distribution22()
}

def distribution(**kwargs):
    require_parameters('dim', 'degree', **kwargs)
    
    if kwargs['dim'] is 1:
        if (kwargs['degree'] is not 1 and kwargs['degree'] is not 2):
            raise Exception("Only implemented distributions of degree 1 or 2")
        return oned_distrs[kwargs['degree']]
    elif kwargs['dim'] is 2:
        if kwargs['degree'] not in twod_distrs:
            raise Exception("2D distribution of this degree not implemented")
        return twod_distrs[kwargs['degree']]
    else:
        raise Exception("Only implemented 1- and 2-D distributions")

