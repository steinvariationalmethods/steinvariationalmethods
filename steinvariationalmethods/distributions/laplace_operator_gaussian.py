#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein_variational_inference.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#

import prngs.parallel as random
import numpy as np
import scipy.sparse as spspa
import scipy as sp
from .distribution_base import _Distribution

# @guvectorize([(float64[:], float64[:], float64[:], float64[:], float64[:])],
#              '(m), (m), (m), (m), ()', nopython=True, cache=True)
def hess_neg_logpdf_quadratic_form(eval_point, L, y, hessaction_diff_result, result):
    """
    hess_neg_logpdf_quadratic_form(eval_point, sigma_inv, y, hessaction_diff_result, result)

    Compute the action of the Hessian, as evaluated at `eval_point`, on a
    vector `x`.

    Arguments
    =========
    - mu: Mean of the distribution, shape (n,) or (1,).
    - sigma_inv: Inverse covariance, same as above. If a vector, it is the
    diagonal of the inverse covariance matrix.
    - y: (y - eval_point) is the Vector to which H(eval_point) is applied, shape(n,).

    Returns
    =======
    H(eval_point) * x, shape (n,)
    """
    diff = eval_point - y
    hess_action_diff = (L@(L.T@diff))
    hessaction_diff_result[:] = hess_action_diff
    result[0] = diff @ hess_action_diff

def sample(mu, L_cholesky_banded, nsamples):
    """
    sample(mu, sigma, nsamples)

    Draw samples from a normal distribution, given L, where L L.T = C^{-1}, sampling is L^{-1} x + mu where x ~ N(0, 1) 

    Arguments
    =========
    - mu: Mean of the distribution.
    - sigma: Standard deviation.
    - nsamples: Number of samples to draw

    Returns
    =======
    samples - a (nsamples, dim) ndarray of samples.

    To infer dimension, mu should be a vector specifying the mean. You will
    get strange results otherwise.
    """
    samples = np.zeros((nsamples, mu.shape[0]))
    samples[:] = random.normal(0., 1., size=(nsamples, mu.shape[0]))
    for i in range(nsamples):
        samples[i] = sp.linalg.solve_banded((1,0), L_cholesky_banded, samples[i], check_finite=False) + mu   
    return samples

class LaplaceOperatorGaussian(_Distribution):
    def __init__(self, mu, lower, diag, init_allocations_size=1, comm=None):
        super().__init__(mu.shape[0], init_allocations_size=init_allocations_size, comm=comm)
        if mu.shape != diag.shape:
            raise Exception("mu and cholesky factor diagonal's shapes don't match")
        if mu.shape[0] != lower.shape[0] +1 or lower.ndim != 1:
            raise Exception("mu and cholesky factor lower diagonal's shapes don't match")
        self._mu = mu
        self._L = spspa.diags([diag, lower],[0,-1], shape=(mu.shape[0], mu.shape[0]))
        self._L_cholesky_banded_obj = np.array([diag,np.append(lower,np.array([-1.0]))])

    @property
    def mu(self):
        return self._mu

    @property
    def L(self):
        #Cholesky factor of C^{-1}, the precision matrix, sparsely stored (You CAN use @ operator with it)
        return self._L
    
    def centered_moment(self, num):
        if num is 0:
            return 1
        elif num is 1:
            return self._mu
        elif num is 2:
            res = sp.linalg.solve_banded((1,0), self._L_cholesky_banded_obj, np.identity(self.dims), check_finite=False) 
            res = sp.linalg.solve_banded((1,0), self._L_cholesky_banded_obj, res, check_finite=False) 
            return res
        else:
            return 0  

    def _neg_logpdf_impl(self, xs, result):
        for i in range(xs.shape[0]):
            diff= xs[i] - self._mu
            result[i] = 0.5 * (diff.T @ ( self._L @ ( (self._L).T @ diff )))

    def _grad_neg_logpdf_impl(self, xs, neg_logpdf_result, grad_neg_logpdf_result):
        for i in range(xs.shape[0]):
            diff= xs[i] - self._mu
            sigma_inv_diff = ((diff @ self._L) @ self._L.T).T
            neg_logpdf_result[i] = 0.5 * (diff.T @ ( sigma_inv_diff))
            grad_neg_logpdf_result[i] = sigma_inv_diff

    def _hess_neg_logpdf_action_gn_impl(self, eval_point, ys, result, idx=None):
        for i in range(ys.shape[0]):
            result[i] = ((ys[i] @ self._L) @ self._L.T).T

    def _hess_neg_logpdf_action_impl(self, eval_point, ys, result, idx=None):
        for i in range(ys.shape[0]):
            result[i] = ((ys[i] @ self._L) @ self._L.T).T

    def _hess_inv_neg_logpdf_action_impl(self, eval_point, ys, result, idx=None):
        for i in range(ys.shape[0]):
            result[i] = sp.linalg.cho_solve_banded((self._L_cholesky_banded_obj, True), ys[i], overwrite_b=False) 

    def _hess_inv_neg_logpdf_action_gn_impl(self, eval_point, ys, result, idx=None):
        for i in range(ys.shape[0]):
            result[i] = sp.linalg.cho_solve_banded((self._L_cholesky_banded_obj, True), ys[i], overwrite_b=False) 

    #DO WE NEED THIS? STILL NOT SURE?!?!?!?!??!?!?!
    def _hess_neg_logpdf_quadratic_form_impl(self, eval_point, y, hessaction_diffs_result, result, gaussnewton=False, idx=None):
        hess_neg_logpdf_quadratic_form(eval_point, self._L, y, hessaction_diffs_result, result)

    #implementation of sampling for gaussians
    def _sample_impl(self, nsamples, out):
        out[:] = sample(self._mu, self._L_cholesky_banded_obj, nsamples)
