#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein_variational_inference.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#
import numpy as np

from numba import guvectorize, float64, jit

from .distribution_base import _Distribution
from prngs import parallel as random
from misc import *


HALFLOG2PI = 0.5* np.log(2*np.pi)

@guvectorize([(float64[:], float64[:], float64[:], float64[:])], '(), (), (n_mix) -> ()',
        nopython=True, cache=True)
def stable_normalization(weight, halflog2pi, sigma_inv, result): 
    #careful, dont use weights that are tiny or sigma_inv's who's pointwise values are too small
    log_sigma_inv = np.log(sigma_inv)            #HOW DO YOU MAKE THIS A COMPILED CONSTANT
    log_normize_partial = - sigma_inv.shape[0] * halflog2pi[0] + 0.5*np.sum(log_sigma_inv)
    result[0] = np.exp(np.log(weight[0]) + log_normize_partial)

@guvectorize([(float64[:], float64[:], float64[:], float64[:])], '(), (), (n_mix) -> ()',
        nopython=True, cache=True)
def log_normalization(weight, halflog2pi, sigma_inv, result): 
    #careful, dont use weights that are tiny or sigma_inv's who's pointwise values are too small
    log_sigma_inv = np.log(sigma_inv)            #HOW DO YOU MAKE THIS A COMPILED CONSTANT
    log_normize_partial = - sigma_inv.shape[0] * halflog2pi[0] + 0.5*np.sum(log_sigma_inv)
    result[0] = np.log(weight[0]) + log_normize_partial

@guvectorize([(float64[:,:], float64[:,:], float64[:], float64[:],  float64[:], float64[:], float64[:])],
             '(n_mix, dim), (n_mix, dim), (n_mix), (n_mix), (dim), (n_mix), ()', nopython=True, cache=True)
def stable_pdf(mus, sigma_invs, log_normalizations, exponents, x, weighted_pdf_i_s, result):
    n_mix = mus.shape[0]
    for i in range(n_mix):
        diff = x - mus[i, :]
        exponents[i] = -0.5 * (diff.T @ (diff * sigma_invs[i, :])) + log_normalizations[i]
    shift = np.max(exponents)
    for i in range(n_mix):
        weighted_pdf_i = np.exp(exponents[i] - shift)
        weighted_pdf_i_s[i] = weighted_pdf_i #weighted_pdf_i_s pdf_i_s = pdf_i+s * exp(-shift)
        result[0] += weighted_pdf_i
    result[0] = np.exp(np.log(result[0]) + shift) #never a reliable figure, because neglogpdf may still be very large, often underflows to 0

@guvectorize([(float64[:,:], float64[:,:], float64[:], float64[:], float64[:], float64[:], float64[:], float64[:], float64[:])],
             '(n_mix, dim), (n_mix, dim), (n_mix), (n_mix), (dim), (), (), (n_mix), ()', nopython=True, cache=True)
def stable_neg_logpdf(mus, sigma_invs, log_normalizations, exponents, x, pdf, weighted_pdf, weighted_pdf_i_s, result): #exponents
    #-log(sum(exp(exponents))) = -( shift + log(sum(exp(exponents - shift))))
    n_mix = mus.shape[0]
    for i in range(n_mix):
        diff = x - mus[i, :]
        exponents[i] = -0.5 * (diff.T @ (diff * sigma_invs[i, :])) + log_normalizations[i]
    shift = np.max(exponents)
    for i in range(n_mix):
        weighted_pdf_i = np.exp(exponents[i] - shift)
        weighted_pdf_i_s[i] = weighted_pdf_i #weighted_pdf_i_s pdf_i_s = pdf_i+s * exp(-shift)
        weighted_pdf[0] += weighted_pdf_i

    weighted_pdf_val = weighted_pdf[0]
    neglogpdf = - (np.log(weighted_pdf_val) + shift)
    result[0] = neglogpdf
    pdf[0] = np.exp(-neglogpdf) #never a reliable figure, because neglogpdf may still be very large, often underflows to 0


@guvectorize([(float64[:, :], float64[:, :], float64[:, :], float64[:], float64[:], float64[:], float64[:], float64[:],
               float64[:], float64[:], float64[:])],
        '(n_mix, dim), (n_mix, dim), (n_mix, dim), (n_mix), (n_mix), (dim), (), (), (), (n_mix), (dim)', nopython=True, cache=True)
def stable_grad_neg_logpdf(mus, sigma_invs, sigma_inv_diffs, log_normalizations, exponents, x, pdf, weighted_pdf, nlpdf, weighted_pdf_i_s, result):
    n_mix = mus.shape[0]
    for i in range(n_mix):
        diff = x - mus[i, :]
        sigma_invs_diff = sigma_invs[i, :] * diff
        sigma_inv_diffs[i] = sigma_invs_diff
        exponents[i] = -0.5 * (diff.T @ sigma_invs_diff) + log_normalizations[i]
    shift = np.max(exponents)
    for i in range(n_mix):
        weighted_pdf_i = np.exp(exponents[i] - shift)
        weighted_pdf_i_s[i] = weighted_pdf_i
        weighted_pdf[0] += weighted_pdf_i
        result[:] += weighted_pdf_i * sigma_inv_diffs[i]

    weighted_pdf_val = weighted_pdf[0]
    result[:] /= weighted_pdf_val
    neglogpdf = - (np.log(weighted_pdf_val) + shift)
    nlpdf[0] = neglogpdf
    pdf[0] = np.exp(-neglogpdf)

@guvectorize([(float64[:], float64[:, :], float64[:, :], float64[:, :], float64[:], float64[:], float64[:], float64[:], float64[:], float64[:],
               float64[:], float64[:], float64[:])],
    '(dim), (n_mix, dim), (n_mix, dim), (n_mix, dim), (n_mix), (n_mix), (dim), (), (), (), (nmix), (dim), (dim)',
    nopython=True, cache=True)
def stable_hess_neg_logpdf_action(eval_point, mus, sigma_invs, sigma_inv_diffs, log_normalizations, exponents, y, pdf, weighted_pdf, nlpdf, weighted_pdf_i_s, glpdf, result):
    #This version recomputes quantities (presumably) already computed in the gradient computations. Consider using cached_ version
    n_mix = mus.shape[0]
    for i in range(n_mix):
        diff = eval_point - mus[i, :]
        sigma_invs_diff = sigma_invs[i, :] * diff
        sigma_inv_diffs[i, :] = sigma_invs_diff
        exponents[i] = -0.5 * (diff.T @ sigma_invs_diff) + log_normalizations[i]
    shift = np.max(exponents)
    for i in range(n_mix):
        weighted_pdf_i = np.exp(exponents[i] - shift)
        weighted_pdf_i_s[i] = weighted_pdf_i #weighted_pdf_i_s pdf_i_s = pdf_i+s * exp(-shift)
        weighted_pdf[0] += weighted_pdf_i
        sigma_inv_diff_i = sigma_inv_diffs[i, :]
        glpdf[:] += weighted_pdf_i * sigma_inv_diff_i
        result[:]  += weighted_pdf_i * (y * sigma_invs[i, :] - (sigma_inv_diff_i @ y) * sigma_inv_diff_i)

    weighted_pdf_val = weighted_pdf[0]
    glpdf[:] /= weighted_pdf_val
    result[:]  /= weighted_pdf_val

    neglogpdf = - (np.log(weighted_pdf_val) + shift)
    nlpdf[0] = neglogpdf
    pdf[0] = np.exp(-neglogpdf)
    result[:] += glpdf * (glpdf @ y )

#THIS IS REALLY SLOW, should use the cached versions
@guvectorize([(float64[:], float64[:, :], float64[:, :], float64[:, :], float64[:], float64[:], float64[:], float64[:], float64[:], float64[:],
               float64[:], float64[:], float64[:])],
    '(dim), (n_mix, dim), (n_mix, dim), (n_mix, dim), (n_mix), (n_mix), (dim), (), (), (),  (nmix), (dim), (dim)',
    nopython=True, cache=True) 
def stable_hess_neg_logpdf_action_gn(eval_point, mus, sigma_invs, sigma_inv_diffs, log_normalizations, exponents, y, pdf, weighted_pdf, nlpdf, weighted_pdf_i_s, glpdf, result):
    #This version recomputes quantities (presumably) already computed in the gradient computations. Consider using cached_ version
    n_mix = mus.shape[0]
    for i in range(n_mix):
        diff = eval_point - mus[i, :]
        sigma_invs_diff = sigma_invs[i, :] * diff
        sigma_inv_diffs[i, :] = sigma_invs_diff
        exponents[i] = -0.5 * (diff.T @ sigma_invs_diff) + log_normalizations[i]
    shift = np.max(exponents)
    for i in range(n_mix):
        weighted_pdf_i = np.exp(exponents[i] - shift)
        weighted_pdf_i_s[i] = weighted_pdf_i #weighted_pdf_i_s pdf_i_s = pdf_i+s * exp(-shift)
        weighted_pdf[0] += weighted_pdf_i
        glpdf[:] += weighted_pdf_i * sigma_inv_diffs[i, :]
        result[:]  += weighted_pdf_i * y * sigma_invs[i, :]

    weighted_pdf_val = weighted_pdf[0]
    glpdf[:] /= weighted_pdf_val
    result[:] /= weighted_pdf_val

    neglogpdf = - (np.log(weighted_pdf_val) + shift)
    nlpdf[0] = neglogpdf
    pdf[0] = np.exp(-neglogpdf)
    result[:] += glpdf * (glpdf @ y )

@guvectorize([(float64[:, :], float64[:, :], float64[:], float64[:],
    float64[:], float64[:], float64[:])],
    '(n_mix, dim), (n_mix, dim), (dim), (n_mix), (), (dim), (dim)',
    nopython=True, cache=True)
def cached_hess_neg_logpdf_action(sigma_invs, pre_comp_sigma_inv_diff, y, pdf_i_s, cached_pdf, cached_grad_neg_logpdf, result):
    #instead of passing in the evaluation point, we pass in pre_comp_sigma_inv_diff, which was cached in the gradient evaluations 
    for i in range(pdf_i_s.shape[0]):
        pre_comp_sigma_inv_diff_i = pre_comp_sigma_inv_diff[i, :]
        result[:] += pdf_i_s[i] * (y * sigma_invs[i, :] - (pre_comp_sigma_inv_diff_i @ y) * pre_comp_sigma_inv_diff_i)

    result[:] *= 1./cached_pdf[0]
    result[:] += cached_grad_neg_logpdf * (cached_grad_neg_logpdf @ y)

#FASTEST VERSION ON CCGO by far
def cached_hess_neg_logpdf_action_not_vectorized(sigma_invs, pre_comp_sigma_inv_diff, ys, pdf_i, cached_pdf, cached_grad_neg_logpdf, result):
    #instead of passing in the evaluation point, we pass in sigma_inv_diff, which was cached in the gradient evaluations 
    for i in range(pdf_i.shape[0]):
        pdf_i_sigma_invs_i = pdf_i[i] * sigma_invs[i]
        pre_comp_sigma_inv_diff_i = pre_comp_sigma_inv_diff[i]
        pdf_i_pre_comp_sigma_inv_diff_i = pdf_i[i] * pre_comp_sigma_inv_diff_i
        for j in range(ys.shape[0]): #ys is shape (n_ys, dim)
            result[j] += pdf_i_sigma_invs_i * ys[j] - (pdf_i_pre_comp_sigma_inv_diff_i @ ys[j]) * pre_comp_sigma_inv_diff_i

    result[:] /= cached_pdf
    for j in range(ys.shape[0]):
        result[j] += cached_grad_neg_logpdf * (cached_grad_neg_logpdf @ ys[j])

jit_cached_hess_neg_logpdf_action = jit(nopython=True, cache=True)(cached_hess_neg_logpdf_action_not_vectorized)

@guvectorize([(float64[:, :], float64[:], float64[:],
    float64[:], float64[:], float64[:])],
    '(n_mix, dim), (dim), (n_mix), (), (dim), (dim)',
    nopython=True, cache=True)
def cached_hess_neg_logpdf_action_gn(sigma_invs, y, pre_comp_pdf_i_s, cached_pdf, cached_grad_neg_logpdf, result):
    #instead of passing in the evaluation point, we pass in sigma_inv_diff, which was cached in the gradient evaluations 
    for i in range(pre_comp_pdf_i_s.shape[0]):
        result[:] += pre_comp_pdf_i_s[i] * y * sigma_invs[i, :]

    result[:] *= 1./cached_pdf[0]
    result[:] += cached_grad_neg_logpdf * (cached_grad_neg_logpdf @ y )

@jit(nopython=True, cache=True)
def jit_cached_hess_neg_logpdf_action_gn(sigma_invs, ys, pre_comp_pdf_i_s, cached_pdf, cached_grad_neg_logpdf, result):
    #instead of passing in the evaluation point, we pass in sigma_inv_diff, which was cached in the gradient evaluations 
    for i in range(pre_comp_pdf_i_s.shape[0]):
        pdf_i_sigma_invs_i = pre_comp_pdf_i_s[i] * sigma_invs[i]
        for j in range(ys.shape[0]):
            result[j] += (pdf_i_sigma_invs_i * ys[j])

    result[:] *= 1./cached_pdf[0]
    for j in range(ys.shape[0]):
        result[j] += cached_grad_neg_logpdf * (cached_grad_neg_logpdf @ ys[j])

@guvectorize([(float64[:, :], float64[:, :], float64[:], float64[:], float64[:], float64[:],
    float64[:], float64[:], float64[:])],
    '(n_mix, dim), (n_mix, dim), (dim), (dim), (n_mix), (), (dim), (dim), ()',
    nopython=True, cache=True)
def cached_hess_neg_logpdf_quadratic_form_guvectorized(sigma_invs, pre_comp_sigma_inv_diff, x, y, pdf_i_s, cached_pdf, cached_grad_neg_logpdf, H_p_diff, result):
    # ys are pt diffs. This function is mainly for the purpose of fast computation of the exponent for
    #np.exp(-0.5 * diffs.T H(eval_point) diffs)
    #It is NOT presumed that H(eval_point) diffs) has been cached.
    #pre_comp_sigma_inv_diff is the cached Sigma^{-1} @ (x- mus) where x is an eval point and mus are all the means
    for i in range(pdf_i_s.shape[0]):
        p_diff = x - y
        pre_comp_sigma_inv_diff_i = pre_comp_sigma_inv_diff[i]
        H_p_diff_partial = pdf_i_s[i] * (sigma_invs[i] * p_diff - (pre_comp_sigma_inv_diff_i @ p_diff) * pre_comp_sigma_inv_diff_i)
        H_p_diff[:] += H_p_diff_partial
        result[0] += p_diff.T @ H_p_diff_partial
    result[0] *= 1./cached_pdf[0]
    H_p_diff_partial = cached_grad_neg_logpdf * (cached_grad_neg_logpdf @ p_diff)
    H_p_diff[:] += H_p_diff_partial
    result[0] += p_diff.T @ H_p_diff_partial
    result[0] *= -0.5

#SETTLED ON THIS VERSION SINCE ITS FASTEST ON CCGO
def cached_hess_neg_logpdf_quadratic_form(sigma_invs, pre_comp_sigma_inv_diff, x, ys, pdf_i_s, cached_pdf, cached_grad_neg_logpdf, H_p_diffs, result):
    # diffs are pt diffs. This function is mainly for the purpose of fast computation of the exponent for
    #np.exp(-0.5 * diffs.T H(eval_point) diffs)
    #It is NOT presumed that H(eval_point) diffs) has been cached.
    for i in range(pdf_i_s.shape[0]):
        p_diffs = x - ys
        pdf_i_sigma_invs_i = pdf_i_s[i] * sigma_invs[i]
        pre_comp_sigma_inv_diff_i = pre_comp_sigma_inv_diff[i]
        pdf_i_pre_comp_sigma_inv_diff_i = pdf_i_s[i] *  pre_comp_sigma_inv_diff_i
        for j in range(ys.shape[0]):
            H_p_diffs_j_partial = pdf_i_sigma_invs_i * p_diffs[j] - (pdf_i_pre_comp_sigma_inv_diff_i @ p_diffs[j]) * pre_comp_sigma_inv_diff_i
            H_p_diffs[j] += H_p_diffs_j_partial
            result[j] += p_diffs[j].T @ H_p_diffs_j_partial 
    
    H_p_diffs[:] /= cached_pdf
    result[:] /= cached_pdf
    for j in range(ys.shape[0]):
        H_p_diffs_j_partial = cached_grad_neg_logpdf * (cached_grad_neg_logpdf @ p_diffs[j])
        H_p_diffs[j] += H_p_diffs_j_partial
        result[j] += p_diffs[j].T @ H_p_diffs_j_partial
    result[:] *= -0.5

@guvectorize([(float64[:, :], float64[:], float64[:], float64[:], float64[:], float64[:],
    float64[:], float64[:])],
    '(n_mix, dim), (dim), (dim), (n_mix), (), (dim), (dim), ()',
    nopython=True, cache=True)
def cached_hess_neg_logpdf_quadratic_form_gn_guvectorized(sigma_invs, x, y, pdf_i_s, cached_pdf, cached_grad_neg_logpdf, H_p_diff, result):
    # ys are pt diffs. This function is mainly for the purpose of fast computation of the exponent for
    #np.exp(-0.5 * diffs.T H(eval_point) diffs)
    #It is NOT presumed that H(eval_point) diffs) has been cached.
    for i in range(pdf_i_s.shape[0]):
        p_diff = x - y
        H_p_diff_partial = pdf_i_s[i] * sigma_invs[i] * p_diff
        H_p_diff[:] += H_p_diff_partial
        result[0] += p_diff.T @ H_p_diff_partial
    result[0] *= 1./cached_pdf[0]
    H_p_diff_partial = cached_grad_neg_logpdf * (cached_grad_neg_logpdf @ p_diff)
    H_p_diff[:] += H_p_diff_partial
    result[0] += p_diff.T @ H_p_diff_partial
    result[0] *= -0.5

#SETTLED ON THIS VERSION SINCE ITS FASTEST ON CCGO
def cached_hess_neg_logpdf_quadratic_form_gn(sigma_invs, x, ys, pdf_i_s, cached_pdf, cached_grad_neg_logpdf, H_p_diffs, result):
    # diffs are pt diffs. This function is mainly for the purpose of fast computation of the exponent for
    #np.exp(-0.5 * diffs.T H(eval_point) diffs)
    #It is NOT presumed that H(eval_point) diffs) has been cached.
    for i in range(pdf_i_s.shape[0]):
        p_diffs = x - ys
        pdf_i_sigma_invs_i = pdf_i_s[i] * sigma_invs[i]
        for j in range(ys.shape[0]):
            H_p_diffs_j_partial = (pdf_i_sigma_invs_i * p_diffs[j])
            H_p_diffs[j] += H_p_diffs_j_partial
            result[j] += p_diffs[j].T @ H_p_diffs_j_partial
    
    H_p_diffs[:] /= cached_pdf
    result[:] /= cached_pdf
    for j in range(ys.shape[0]):
        H_p_diffs_j_partial = cached_grad_neg_logpdf * (cached_grad_neg_logpdf @ p_diffs[j])
        H_p_diffs[j] += H_p_diffs_j_partial
        result[j] += p_diffs[j].T @ H_p_diffs_j_partial
    result[:] *= -0.5

cached_hess_neg_logpdf_quadratic_form_gn_jit = jit(nopython=True, cache=True)(cached_hess_neg_logpdf_quadratic_form_gn)

def sample(mu, sigma, nsamples):
    """
    sample(mu, sigma, nsamples)

    Draw samples from a normal distribution.

    Arguments
    =========
    - mu: Mean of the distribution.
    - sigma: Standard deviation.
    - nsamples: Number of samples to draw

    Returns
    =======
    samples - a (nsamples, dim) ndarray of samples.

    To infer dimension, mu should be a vector specifying the mean. You will
    get strange results otherwise.
    """
    return np.squeeze(random.normal(mu, sigma, (nsamples, mu.shape[0])))


class GaussianMixture(_Distribution):
    def __init__(self, mus, sigma_invs, init_allocations_size=1, weights=None, comm=None):
        super().__init__(mus.shape[1], init_allocations_size=init_allocations_size, comm=comm)
        
        # Distribution parameters
        if weights is None:
            n = mus.shape[0]
            weights = np.ones(n) / n
        self._weights = weights
        n = weights.shape[0]
        if not (mus.shape[0] == n and sigma_invs.shape[0] == n):
            raise Exception("Number of weights != mus.shape[0] or sigmas.shape[0]")
        if not (mus.shape[1] == sigma_invs.shape[1]):
            raise Exception("Dim of mus and sigmas is not equal")
        self._num_components = n
        self._normalizations = stable_normalization(weights, HALFLOG2PI, sigma_invs)
        self._log_normalizations = log_normalization(weights, HALFLOG2PI, sigma_invs)
        self._sigma_invs = sigma_invs
        self._mus = mus
        self._mean = np.sum(self.weights[:,np.newaxis] * self.mus, axis=0)
        self._mus_centered = (self.mus - self._mean)

        # Allocations for computation and caching of results
        self._weighted_pdf = np.zeros(init_allocations_size)
        self._weighted_pdf_i_s = np.zeros((init_allocations_size, self.num_components))
        self._exponents = np.empty(self.num_components) #, self._exponents_window,  used only as a temporary storage array, this quantity never changes size
        self._sigma_inv_diffs = np.empty((init_allocations_size, self.num_components, self.dims))

    # A Gaussian Mixture is currently deisgned be used as a prior OR as a posterior,
    # which is why we allow it to have a prior
    # We don't use it as a likelihood, and so we don't provide the likelihood setter
    
    @property
    def prior(self):
        return self._prior

    @prior.setter 
    def prior(self, prior):
        self._prior = prior

    @property
    def num_components(self):
        return self._num_components

    @property
    def mus(self):
        return self._mus

    @property
    def weights(self):
        return self._weights

    @property
    def sigma_invs(self):
        return self._sigma_invs

    def centered_moment(self, num):
        if num is 0:
            return 1
        elif num is 1:
            return self._mean
        elif num is 2:
            covariance = np.diag(np.sum(self.weights[:,np.newaxis] * 1./self._sigma_invs, axis=0))+ np.einsum('ij,ik->jk', self.weights[:,np.newaxis] * self._mus_centered, self._mus_centered)
            return covariance
        else:
            raise NotImplementedError("Did not implement these calculations")  

    def _pdf_allocations_impl(self, xs):
        N = xs.shape[0]
        if N != self._weighted_pdf_i_s.shape[0]:
            self._weighted_pdf_i_s = np.zeros((N, self.num_components))
        else:
            self._weighted_pdf_i_s[:] = 0
        super()._pdf_allocations_impl(xs)

    def _neg_logpdf_allocations_impl(self, xs):
        N = xs.shape[0]
        if N !=self._neg_logpdf.shape[0]:
            self._pdf = np.zeros(N)
            self._weighted_pdf = np.zeros(N)
            self._weighted_pdf_i_s  = np.zeros((N, self.num_components))
        else:
            self._pdf[:] = 0
            self._weighted_pdf[:] = 0
            self._weighted_pdf_i_s[:] = 0

        super()._neg_logpdf_allocations_impl(xs)

    def _grad_neg_logpdf_allocations_impl(self, xs, projections, append_result):
        if append_result: #only dealing with this case when we dont have projections
            assert(not projections)
            N_additional = xs.shape[0]
            N = self._grad_neg_logpdf.shape[0]
            assert(N == self._neg_logpdf.shape[0]) #This should be true, if we called this function, since 
            # it is presumed that this function is called by grad_neg_logpdf, when we are appending the result to a 
            #previously called grad_neg_logpdf(xs)

            self._pdf = self._expand_array(self._pdf,  (N_additional,))
            self._weighted_pdf = self._expand_array(self._weighted_pdf, (N_additional,))
            self._weighted_pdf_i_s = self._expand_array(self._weighted_pdf_i_s,  (N_additional,self.num_components))
            self._sigma_inv_diffs= self._expand_array(self._sigma_inv_diffs, (N_additional, self.num_components, self.dims))
        else:
            N = xs.shape[0]
            if N !=self._grad_neg_logpdf.shape[0]:
                self._pdf = np.zeros(N)
                self._weighted_pdf = np.zeros(N)
                self._weighted_pdf_i_s = np.zeros((N, self.num_components))
                self._sigma_inv_diffs = np.empty((N, self.num_components, self.dims))
            else:
                #quantities that need to be zero'd out before computing the gradient
                self._pdf[:] = 0
                self._weighted_pdf[:] = 0
                self._weighted_pdf_i_s[:] = 0

        super()._grad_neg_logpdf_allocations_impl(xs, projections, append_result)

    def _pdf_impl(self, xs, result):
        stable_pdf(self._mus, self._sigma_invs, self._log_normalizations, self._exponents, \
                xs, self._weighted_pdf_i_s, result)

    def _neg_logpdf_impl(self, xs, result):
        stable_neg_logpdf(self._mus, self._sigma_invs, self._log_normalizations, \
            self._exponents, xs, self._pdf, self._weighted_pdf, self._weighted_pdf_i_s, result)

    def _grad_neg_logpdf_impl(self, xs, neg_logpdf_result, grad_neg_logpdf_result):
        stable_grad_neg_logpdf(self._mus, self._sigma_invs, self._sigma_inv_diffs, self._log_normalizations, \
            self._exponents, xs, self._pdf, self._weighted_pdf, \
            neg_logpdf_result, self._weighted_pdf_i_s, grad_neg_logpdf_result)
   
    def _hess_neg_logpdf_action_impl(self, eval_point, ys, result, idx=None):
        assert(idx !=None) #LOCAL INDICES NOW!!!!
        cached_hess_neg_logpdf_action(self._sigma_invs, self._sigma_inv_diffs[idx], ys,\
                     self._weighted_pdf_i_s[idx], self._weighted_pdf[idx], self._grad_neg_logpdf[idx], result)

    def _hess_neg_logpdf_action_gn_impl(self, eval_point, ys, result, idx=None):
        assert(idx !=None)
        cached_hess_neg_logpdf_action_gn(self._sigma_invs, ys,\
                     self._weighted_pdf_i_s[idx], self._weighted_pdf[idx], self._grad_neg_logpdf[idx], result)
        
    def _hess_neg_logpdf_quadratic_form_gn_impl(self, eval_point, ys, hessian_action_diffs_result, result, idx):
        cached_hess_neg_logpdf_quadratic_form_gn(self._sigma_invs, eval_point, ys, self._weighted_pdf_i_s, self._weighted_pdf[idx], self._grad_neg_logpdf, hessian_action_diffs_result, result)

    def _hess_neg_logpdf_quadratic_form_impl(self, eval_point, ys, hessian_action_diffs_result, result, idx):
        cached_hess_neg_logpdf_quadratic_form(self._sigma_invs, self._sigma_inv_diffs[idx], eval_point, ys, self._weighted_pdf_i_s, self._weighted_pdf[idx], self._grad_neg_logpdf, hessian_action_diffs_result, result)
        #_hessaction_diffs, is available for reuse if one does not overwrite them. 

    def _sample_impl(self, nsamples, out):
        if np.all(self._weights == self._weights[0]):
            # all weights are equal same, dont use weighting vector p
            distr_idxs = random.choice(self.num_components, nsamples)
        else:
            distr_idxs = random.choice(self.num_components, nsamples, p=self._weights)
        for i, distr_idx in enumerate(distr_idxs):
            out[i] = sample(self.mus[distr_idx], self.sigma_invs[distr_idxs], 1)
