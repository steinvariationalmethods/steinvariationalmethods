#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein variational inference methods class project.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#

import numpy as np
from math import isclose
from abc import ABC, abstractmethod
from misc import *

from mpi4py import MPI 



class _Distribution(ABC):
    "Abstract base class for (generally non-normalized, i.e. non-probability) distributions"
    def __init__(self, dims, init_allocations_size=1, comm=None):
        """
        All child classes should call this base constructor
        (via super().__init__) to set the class members common to all
        _Distribution objects.
        """
        self._neg_logpdf_evals = 0
        self._grad_neg_logpdf_evals = 0
        self._hess_neg_logpdf_evals = 0
        self._grad_time = 0.0
        self._hess_time = 0.0

        self._dims = dims

        self._comm = comm

        #All necessary allocations for quantities stored in Distribution, for the sake of cached computations
        self._pdf = np.zeros(init_allocations_size)
        self._neg_logpdf = np.zeros(init_allocations_size)
        self._neg_logpdf_test = np.zeros(init_allocations_size)
        self._grad_neg_logpdf = np.zeros((init_allocations_size, self.dims))

        #The sizes of the following are typically not known at the start, or are generally allocated instead by other classes

        self._hessaction_diffs = np.empty((0, self.dims))
        self._hessaction_Omega = np.empty((0, 0, 0)) # <-dont allocate larger in general because we wont use this normally. normally this is handled by the kernel/metric
        self._ggTaction_Omega = np.empty((0,0,0))

        self._quadratic_forms = np.empty((0, 0))
        self._hess_inv_action_ys = np.array((0, self.dims))
        self._hess_inv_action_Omega = np.empty((0, init_allocations_size, self.dims)) # <-dont allocate larger in general because we wont use this normally. normally this is handled by the kernel/metric

        self._projected_grad_neg_logpdf  = np.empty((0, 0))
        self._projected_hessian_matrix = np.empty((0, 0, 0))

        self._log_normalization = None
        self._normalization = None

        self._likelihood = None
        self._prior = None
        

    @property
    def likelihood(self): #None if the distribution happens to be a posterior without a Likelihood, or the distribution is a Prior
        return self._likelihood

    @property
    def prior(self):
        return self._prior

    @property
    def likelihood(self):
        return self._likelihood

    @property
    def comm(self):
        return self._comm
    
    @property  
    def dims(self):
        return self._dims

    def append(self, variable, new_variable):
        return np.concatenate(variable, new_variable, axis=0)

    def _expand_array(self, array, extra_shape):
        #add zeros to the end of the array given a compatible "extra shape":
        return np.concatenate((array, np.zeros(extra_shape)), axis=0)

    def _assign_size(self, N):
        # assign size of local portion
        self._current_nsamples = N
        return assign_size(N, self.comm)

    @property
    def neg_logpdf_evals(self):
        return self._neg_logpdf_evals

    @neg_logpdf_evals.setter
    def neg_logpdf_evals(self, n):
        self._neg_logpdf_evals = n

    @property
    def grad_neg_logpdf_evals(self):
        return self._grad_neg_logpdf_evals

    @grad_neg_logpdf_evals.setter
    def grad_neg_logpdf_evals(self, n):
        self._grad_neg_logpdf_evals = n

    @property
    def hess_neg_logpdf_evals(self):
        return self._hess_neg_logpdf_evals

    @property
    def grad_time(self):
        return self._grad_time

    @property
    def hess_time(self):
        return self._hess_time

    def reset_times(self):
        self._grad_time = 0.0
        self._hess_time = 0.0

    def reset_count_evals(self):
        self._neg_logpdf_evals = 0
        self._grad_neg_logpdf_evals = 0
        self._hess_neg_logpdf_evals = 0

               
    @property
    def fn_evals(self):
        return self._neg_logpdf_evals + self._grad_neg_logpdf_evals + 2*self._hess_neg_logpdf_evals

    @hess_neg_logpdf_evals.setter
    def hess_neg_logpdf_evals(self, n):
        self._hess_neg_logpdf_evals = n

    def sample(self, nsamples=1): #ALL SAMPLES WILL BE on all cores. COLLECTIVE OPTIONS MUST BE USED AFTER NEWTON STEP AND OTHER OPERATIONS
        my_rank_n_samples = self._assign_size(nsamples) 
        samples = np.zeros((my_rank_n_samples, self.dims)) #defines size of your local array #STICK TO multiples OF N PROCS PLEASE!
        self._sample_impl(my_rank_n_samples, samples)
        if nsamples is 1:
            return np.squeeze(samples, axis=0)
        else:
            return samples
    
    def sample0(self, nsamples=1): #ALL SAMPLES WILL BE on all cores. COLLECTIVE OPTIONS MUST BE USED AFTER NEWTON STEP AND OTHER OPERATIONS
        samples = np.zeros((nsamples, self.dims)) #defines size of your local array #STICK TO multiples OF N PROCS PLEASE!
        self._sample_impl(nsamples, samples)
        if nsamples is 1:
            return np.squeeze(samples, axis=0)
        else:
            return samples


    def get_copied_ownership(self, all_samples, size=None): #This function call on a worker will give the worker a copy of its assigned portion of the array
        if size is not None:
            self._current_nsamples = size #?????
        else:
            #all_samples should be identical on each core
            self._current_nsamples = all_samples.shape[0]
        my_slice = assign_slice(self._current_nsamples, self.comm)
        return all_samples[my_slice].copy()

    def allgather(self, local_array, out = None): 
        if out is None:
            out = np.zeros((self._current_nsamples, local_array.shape[1]))
        if self.comm is None or self.comm.Get_size() == 1:
            out[:] = local_array
        else:
            self.comm.Allgather( [local_array, MPI.DOUBLE], [out, MPI.DOUBLE] )
        return out
        
    def _sample_impl(self, nsamples, out):
        raise Exception("Sampling directly from this distribution has not been implemented")

    def _pdf_allocations_impl(self, xs):
        N = xs.shape[0]
        if N != self._pdf.shape[0]:
            self._pdf = np.zeros(N)
        else:
            self._pdf[:] = 0

    def _neg_logpdf_allocations_impl(self, xs):
        N = xs.shape[0]
        if N != self._neg_logpdf.shape[0]:
            self._neg_logpdf = np.zeros(N)
        else:
            self._neg_logpdf[:]

    def _neg_logpdf_test_allocations_impl(self, xs):
        N = xs.shape[0]
        if N != self._neg_logpdf_test.shape[0]:
            self._neg_logpdf_test = np.zeros(N)
        else:
            self._neg_logpdf_test[:]


    def centered_moment(self, num):
        raise NotImplementedError("This Distribution does not have calculation of moments implemented. Use MCMC to calculate")

    def centered_moment_error(self, num, moment_estimator):
        if self.prior is not None and num == 1:
            #Gaussian Prior weighted norm for relative error
            true_mean = self.centered_moment(num)
            diff = true_mean - moment_estimator
            return (diff.T @ (diff *self.prior.sigma_inv))/(true_mean.T @ (true_mean *self.prior.sigma_inv))
        else:
            return np.linalg.norm(self.centered_moment(num) - moment_estimator, ord=2)/np.linalg.norm(self.centered_moment(num), ord=2)  #always the L2 norm or Hilbert Schmidt norm

    
    def pdf(self, xs):
        self._pdf_allocations_impl(xs)
        if hasattr(xs, 'full'):
            self._pdf_impl(xs.full, self._pdf)
        else:
            self._pdf_impl(xs, self._pdf)
        return self._pdf

    def neg_logpdf(self, xs):
        self.neg_logpdf_evals += xs.shape[0] #INTRODUCE NOTION OF PARALLEL EVALS AS WELL!!!!!
        self._neg_logpdf_allocations_impl(xs)
        if hasattr(xs, 'full'):
            self._neg_logpdf_impl(xs.full, self._neg_logpdf)
        else:
            self._neg_logpdf_impl(xs, self._neg_logpdf)
        return self._neg_logpdf

    def neg_logpdf_test(self, xs):
        self._neg_logpdf_test_allocations_impl(xs)
        if hasattr(xs, 'full'):
            self._neg_logpdf_test_impl(xs.full, self._neg_logpdf_test)
        else:
            self._neg_logpdf_test_impl(xs, self._neg_logpdf_test)
        return self._neg_logpdf_test

    #Usually, the user shouldn't have to reimplement this, unless, for efficiency, they allocate other quantities
    def _grad_neg_logpdf_allocations_impl(self, xs, projections, append_result):
        if append_result: #only dealing with this case when we dont have projections
            assert(not projections)
            N_additional = xs.shape[0]
            N = self._grad_neg_logpdf.shape[0]
            assert(N == self._neg_logpdf.shape[0]) #This should be true, if we called this function, since 
            # it is presumed that this function is called by grad_neg_logpdf, when we are appending the result to a 
            #previously called grad_neg_logpdf(xs)
            self._grad_neg_logpdf = self._expand_array(self._grad_neg_logpdf, (N_additional, self.dims))
            self._neg_logpdf = self._expand_array(self._neg_logpdf, (N_additional,))
        else:        
            N = xs.shape[0]
            allocated = False
            if projections:
                (N2, rank, dims) = xs.projectors.shape
                assert(N2 == N)
                if (N2, rank) != self._projected_grad_neg_logpdf.shape:
                    self._projected_grad_neg_logpdf = np.zeros((N, rank))
                else:
                    self._projected_grad_neg_logpdf[:] = 0
                allocated = True

            if N != self._grad_neg_logpdf.shape[0]: 
                self._grad_neg_logpdf = np.zeros((N, self.dims))
                allocated = True
            if N != self._neg_logpdf.shape[0]:
                self._neg_logpdf = np.zeros(N)
                allocated = True
            if not allocated:
                self._grad_neg_logpdf[:] = 0
                self._neg_logpdf[:] = 0

    def _ggT_neg_logpdf_allocations_impl(self, xs):
        N = xs.shape[0]
        allocated = False
        if N != self._grad_neg_logpdf.shape[0]: 
            self._grad_neg_logpdf = np.zeros((N, self.dims))
            self._ggT_neg_logpdf = np.zeros((N, self.dims))
            allocated = True
        if N != self._neg_logpdf.shape[0]:
            self._neg_logpdf = np.zeros(N)
            allocated = True

    def grad_neg_logpdf(self, xs, projections=False, append_result=False):
        self.grad_neg_logpdf_evals += xs.shape[0]
        if append_result:
            last_idx = self._neg_logpdf.shape[0]
        elif projections:
            N = self._grad_neg_logpdf.shape[0]

        self._grad_neg_logpdf_allocations_impl(xs, projections, append_result)

        #xs is assumed to be stored in shared memory if this is run in mpi
        t0 = MPI.Wtime()
        if hasattr(xs, 'full'):
            self._grad_neg_logpdf_impl(xs.full, self._neg_logpdf, self._grad_neg_logpdf)
        else:
            if append_result:
                self._grad_neg_logpdf_impl(xs, self._neg_logpdf[last_idx:], self._grad_neg_logpdf[last_idx:])
            else:
                self._grad_neg_logpdf_impl(xs, self._neg_logpdf, self._grad_neg_logpdf)

        if projections:
            assert(not append_result)
            # sigma_inv = self.prior._sigma_inv
            for i in range(N):
                self._projected_grad_neg_logpdf[i] =  (self._grad_neg_logpdf[i]) @ xs.projectors[i].T

        if self.comm is not None: self.comm.Barrier()   
        self._grad_time += MPI.Wtime() - t0
        if projections:
            return self._projected_grad_neg_logpdf,  self._neg_logpdf
        else:
            return self._grad_neg_logpdf,  self._neg_logpdf

    # def ggT_neg_logpdf(self, xs, projections=False, append_result=False):
    #     self.grad_neg_logpdf_evals += xs.shape[0]
    #     if append_result:
    #         last_idx = self._neg_logpdf.shape[0]
    #     elif projections:
    #         N = self._grad_neg_logpdf.shape[0]

    #     self._ggT_neg_logpdf_allocations_impl(xs, projections, append_result)


    #     #xs is assumed to be stored in shared memory if this is run in mpi
    #     t0 = MPI.Wtime()
    #     self._grad_neg_logpdf_impl(xs.full, self._neg_logpdf, self._grad_neg_logpdf)

    #     self('Gradient calculation works!')

    #     print('grad_neg_logpdf shape = ',self._grad_neg_logpdf.shape)

    #     exit()

    #     if self.comm is not None: self.comm.Barrier()   
    #     self._grad_time += MPI.Wtime() - t0

    #     if projections:
    #         return self._projected_grad_neg_logpdf,  self._neg_logpdf
    #     else:
    #         return self._grad_neg_logpdf,  self._neg_logpdf
            
    def _pdf_impl(self, xs, result):
        #shapes: 
        # xs: (n_xs, dim)
        # result: (dim)
        raise NotImplementedError("This method is not implemented for this distribution")

    def _neg_logpdf_impl(self, xs, result):
        #shapes: 
        # xs: (n_xs, dim)
        # result: (dim)
        raise NotImplementedError("This method is not implemented for this distribution")

    def _neg_logpdf_test_impl(self, xs, result):
        #shapes: 
        # xs: (n_xs, dim)
        # result: (dim)
        raise NotImplementedError("This method is not implemented for this distribution")

    def _grad_neg_logpdf_impl(self, xs, neg_logpdf_result, grad_neg_logpdf_result):
        #shapes: 
        # xs: (n_xs, dim)
        # result: (n_xs, dim)
        #Most people can ignore 'idx', its used if one has cached results
        raise NotImplementedError("This method is not implemented for this distribution")


    #ignore idx most of the time
    def _hess_neg_logpdf_quadratic_form_impl(self, eval_point, ys, hessaction_diffs_result, result, idx=None):
        #shapes: 
        # eval_point: (dim)
        # ys: (n_ys, dim)
        # hessian_action_ys_result: (n_ys, dim)
        # result: (n_ys)
        #Most people can ignore 'idx', its used if one has cached results
        raise NotImplementedError("This method is not implemented for this distribution")

    def _hess_neg_logpdf_quadratic_form_gn_impl(self, eval_point, ys, hessaction_diffs_result, result, idx=None):
        #Most people can ignore 'idx', its used if one has cached results
        raise NotImplementedError("This method is not implemented for this distribution")

    def _hess_neg_logpdf_action_impl(self, eval_point, Omega, result, idx=None):
        #shapes: 
        # eval_point: (dim)
        # ys: (n_ys, dim)
        # result: (n_ys, dim)
        raise NotImplementedError("This method is not implemented for this distribution")

    def _ggT_neg_logpdf_action_impl(self, eval_point, Omega, result, idx=None):
        #shapes: 
        # eval_point: (dim)
        # ys: (n_ys, dim)
        # result: (n_ys, dim)
        raise NotImplementedError("This method is not implemented for this distribution")

    def _hess_neg_logpdf_action_gn_impl(self, eval_point, Omega, result, idx=None):
        raise NotImplementedError("This method is not implemented for this distribution")

    def _hess_inv_neg_logpdf_action_impl(self, eval_point, Omega,  result, idx=None):
        raise NotImplementedError("This method is not implemented for this distribution")

    def _hess_inv_neg_logpdf_action_gn_impl(self, eval_point, Omega,  result, idx=None):
        raise NotImplementedError("This method is not implemented for this distribution")

    def _hess_neg_logpdf_actions_allocations_impl(self, eval_points, Omega):
        N = eval_points.shape[0]
        if Omega.ndim is 3:
            (N2, n_vecs, dim) = Omega.shape
            assert(N == N2)
        else:
            (n_vecs, dim) = Omega.shape
        if (N, n_vecs, dim) != self._hessaction_Omega.shape:
            self._hessaction_Omega = np.zeros((N, n_vecs, dim))
        else:
            self._hessaction_Omega[:] = 0

    def _ggT_neg_logpdf_actions_allocations_impl(self, eval_points, Omega):
        N = eval_points.shape[0]
        if Omega.ndim is 3:
            (N2, n_vecs, dim) = Omega.shape
            assert(N == N2)
        else:
            (n_vecs, dim) = Omega.shape
        if (N, n_vecs, dim) != self._ggTaction_Omega.shape:
            self._ggTaction_Omega = np.zeros((N, n_vecs, dim))
        else:
            self._ggTaction_Omega[:] = 0


    def _hess_inv_neg_logpdf_actions_allocations_impl(self, eval_points, AOmega):
        N = eval_points.shape[0]
        (N2, n_vecs, dim) = AOmega.shape
        assert(N == N2)
        if (N2, n_vecs, dim) !=  self._hess_inv_action_Omega.shape:
            self._hess_inv_action_Omega = np.zeros((N, n_vecs, dim))
        else:
            self._hess_inv_action_Omega[:] = 0

    def _hess_neg_logpdf_matrices_allocations_impl(self, eval_points, projectors):
        N = eval_points.shape[0]
        (N2, rank, dim) = projectors.shape
        assert(N == N2)
        if (N, rank, rank) != self._projected_hessian_matrix.shape:
            self._projected_hessian_matrix = np.zeros((N, rank, rank))
        #Should there be another case where we zero?
        else:
            self._projected_hessian_matrix[:] = 0

    def _hess_neg_logpdf_action_allocations_impl(self, eval_point, ys):
        ys_shape = ys.shape
        if self._hessaction_diffs.shape != ys_shape:
            self._hessaction_diffs = np.zeros(ys.shape)
        else:
            self._hessaction_diffs[:] = 0

    def _hess_inv_neg_logpdf_action_allocations_impl(self, eval_point, ys):
        ys_shape = ys.shape
        if self._hess_inv_action_ys.shape != ys_shape:
            self._hess_inv_action_ys = np.zeros(ys.shape)
        else:
            self._hess_inv_action_ys[:] = 0

    def hess_neg_logpdf_action(self, eval_point, ys, gaussnewton=False, idx=None): #idx indicates which eval_point
        self._hess_neg_logpdf_action_allocations_impl(eval_point, ys)
        t0 = MPI.Wtime()
        if gaussnewton:
            self._hess_neg_logpdf_action_gn_impl(eval_point, ys, self._hessaction_diffs, idx=idx)
        else:
            self._hess_neg_logpdf_action_impl(eval_point, ys, self._hessaction_diffs, idx=idx)
        self._hess_time += MPI.Wtime() - t0
        return self._hessaction_diffs #returns an array local to each worker


    def hess_neg_logpdf_actions(self, eval_points, Omega, result=None, gaussnewton=False): 
        #Mainly used for eigendecomposition
        # if eval_point is N x dim,
        # Omega does NOT need to be a N x r x dim (just assumption of Omega being on each worker), but Omega should be the 
        # same shape on each worker:  r x dim
        # The N is split up into each workers portion of the N
        # The Omega are handled as r x dim objects
        # The result is stored in a N x r x dim shared object

        #all invocations of this function across workers should have disjoint eval point
        self.hess_neg_logpdf_evals += eval_points.shape[0] * Omega.shape[0] * Omega.shape[1]

        if result is None:
            self._hess_neg_logpdf_actions_allocations_impl(eval_points, Omega) #eval points is split up for evaluations
            result = self._hessaction_Omega
        t0 = MPI.Wtime()
        if gaussnewton:
            for i, eval_point in enumerate(eval_points):
                self._hess_neg_logpdf_action_gn_impl(eval_point, Omega, result[i], idx=i)
        else:
            for i, eval_point in enumerate(eval_points):
                self._hess_neg_logpdf_action_impl(eval_point, Omega, result[i], idx=i)
        # if self.comm is not None: self.comm.Barrier()
        self._hess_time += MPI.Wtime() - t0
        return result

    def ggT_neg_logpdf_actions(self, eval_points, Omega, result=None, gaussnewton=False): 
        #Mainly used for eigendecomposition
        # if eval_point is N x dim,
        # Omega does NOT need to be a N x r x dim (just assumption of Omega being on each worker), but Omega should be the 
        # same shape on each worker:  r x dim
        # The N is split up into each workers portion of the N
        # The Omega are handled as r x dim objects
        # The result is stored in a N x r x dim shared object

        #all invocations of this function across workers should have disjoint eval point

        if result is None:
            self._ggT_neg_logpdf_actions_allocations_impl(eval_points, Omega) #eval points is split up for evaluations
            result = self._ggTaction_Omega

        t0 = MPI.Wtime()
        for i, eval_point in enumerate(eval_points):
                self._ggT_neg_logpdf_action_impl(eval_point, Omega, result[i], idx=i)
        # if self.comm is not None: self.comm.Barrier()
        self._hess_time += MPI.Wtime() - t0
        return result

    def hess_inv_neg_logpdf_actions(self, eval_points, AOmega, result=None, gaussnewton=False): 
        #Mainly used for eigendecomposition
        # if eval_point is N x dim,
        # The Omega are handled as r x dim objects
        # The result is stored in a r x dim object

        if result is None:
            self._hess_inv_neg_logpdf_actions_allocations_impl(eval_points, AOmega) #eval points is split up for evaluations
            result = self._hess_inv_action_Omega
        if gaussnewton:
            for i, eval_point in enumerate(eval_points):
                self._hess_inv_neg_logpdf_action_gn_impl(eval_point, AOmega[i], result[i], idx=i)
        else:
            for i, eval_point in enumerate(eval_points):
                self._hess_inv_neg_logpdf_action_impl(eval_point, AOmega[i], result[i], idx=i)
        # if self.comm is not None: self.comm.Barrier()
        return result


    #This function can only really be used if eval_points are split_vector type
    def hess_neg_logpdf_matrices(self, eval_points, result=None, gaussnewton=False):
        # Projected Posterior hessian matrix formed by hitting hessian against basis vectors and projecting:
        # U.T H U + I, this is the projected Posterior hessian in the C^{-1} prior inner product 
        # eval_points.projectors is assumed to be in distributed memory, N x r x dim
        # If projectors are Identity in dim dimensions, Forms full Hessian in d dimensions
        # The prolongated projected hessian would be U U.T H U U.T 
        # otherwise forms projected hessian in r dimensions
        #result is N x r x r
        #all invocations of this function across workers should have disjoint eval points

        self.hess_neg_logpdf_evals += eval_points.shape[0] * eval_points.projectors.shape[0] * eval_points.projectors.shape[1]
        # print('eval_points.shape',eval_points.shape)
        # print('eval_points.projectors', eval_points.projectors.shape)
        self._hess_neg_logpdf_actions_allocations_impl(eval_points, eval_points.projectors) #eval points is split up for evaluations            
        N = eval_points.shape[0]
        many_projectors = eval_points.projectors.ndim is 3
        if many_projectors:
            N2, r, dim = eval_points.projectors.shape
            assert(N == N2)
        else:
            assert(eval_points.projectors.ndim is 2)
            r, dim = eval_points.projectors.shape
        t0 = MPI.Wtime()
        if self.likelihood is None:
            if gaussnewton:
                if many_projectors:
                    for i in range(N):
                        self._hess_neg_logpdf_action_gn_impl(eval_points.full[i], eval_points.projectors[i], self._hessaction_Omega[i], idx=i)
                else:
                    for i in range(N):
                        self._hess_neg_logpdf_action_gn_impl(eval_points.full[i], eval_points.projectors, self._hessaction_Omega[i], idx=i)
            else:
                if many_projectors:
                    for i in range(N):
                        self._hess_neg_logpdf_action_impl(eval_points.full[i], eval_points.projectors[i], self._hessaction_Omega[i], idx=i)
                else:
                    for i in range(N):
                        self._hess_neg_logpdf_action_impl(eval_points.full[i], eval_points.projectors, self._hessaction_Omega[i], idx=i)         
            
            if result is None:
                self._hess_neg_logpdf_matrices_allocations_impl(eval_points, eval_points.projectors)
                result = self._projected_hessian_matrix
            if many_projectors:
                for i in range(N):
                    result[i] = self._hessaction_Omega[i] @ eval_points.projectors[i].T  # r x dim @ dim x r  
                    result[i] += np.identity(r)
            else:
                for i in range(N):
                    result[i] = self._hessaction_Omega[i] @ eval_points.projectors.T  # r x dim @ dim x r  
                    result[i] += np.identity(r)
        else:
            if gaussnewton:
                if many_projectors:
                    for i in range(N):
                        self.likelihood._hess_neg_logpdf_action_gn_impl(eval_points.full[i], eval_points.projectors[i], self._hessaction_Omega[i], idx=i)
                        self.prior._hess_neg_logpdf_action_gn_impl(eval_points.full[i], eval_points.projectors[i], self._hessaction_Omega[i], idx=i)
                else:
                    for i in range(N):
                        self.likelihood._hess_neg_logpdf_action_gn_impl(eval_points.full[i], eval_points.projectors, self._hessaction_Omega[i], idx=i)
                        self.prior._hess_neg_logpdf_action_gn_impl(eval_points.full[i], eval_points.projectors, self._hessaction_Omega[i], idx=i)
            else:
                if many_projectors:
                    for i in range(N):
                        # print("projector ", eval_points.projectors[i])
                        self.likelihood._hess_neg_logpdf_action_impl(eval_points.full[i], eval_points.projectors[i], self._hessaction_Omega[i], idx=i)
                        # print("get here to compute hess likelihood ",self._hessaction_Omega[i])
                        self.prior._hess_neg_logpdf_action_impl(eval_points.full[i],eval_points.projectors[i], self._hessaction_Omega[i], idx=i)
                        # print("totoal ", self._hessaction_Omega[i])
                        # dfdfdfdf
                        # self._hessaction_Omega[i] += hessaction_Omega_tmp
                else:
                    # print("projector",eval_points.projectors)
                    for i in range(N):
                        # print("before", self._hessaction_Omega[i])
                        self.likelihood._hess_neg_logpdf_action_impl(eval_points.full[i], eval_points.projectors, self._hessaction_Omega[i], idx=i)  
                        # print("get here to compute hess likelihood ",self._hessaction_Omega[i])
                        # hessaction_Omega_tmp = np.zeros(self._hessaction_Omega[i].shape)
                        self.prior._hess_neg_logpdf_action_impl(eval_points.full[i], eval_points.projectors, self._hessaction_Omega[i], idx=i) 
                        # self._hessaction_Omega[i] += hessaction_Omega_tmp
                        # print("prior part ", hessaction_Omega_tmp)
                        # print("totoal ", self._hessaction_Omega[i])
                        # dfdfdfdf
            
            if result is None:
                self._hess_neg_logpdf_matrices_allocations_impl(eval_points, eval_points.projectors)
                result = self._projected_hessian_matrix
            if many_projectors:
                for i in range(N):
                    # print("shape ", eval_points.projectors[i].shape,self._hessaction_Omega[i].shape)
                    result[i] = self._hessaction_Omega[i] @ eval_points.projectors[i].T  # r x dim @ dim x r  
                    # result[i] += np.identity(r)
            else:
                for i in range(N):
                    result[i] = self._hessaction_Omega[i] @ eval_points.projectors.T  # r x dim @ dim x r  
                    # result[i] += np.identity(r)


        # if self.comm is not None: self.comm.Barrier() 
        self._hess_time += MPI.Wtime() - t0
        return result

    def hess_inv_neg_logpdf_action(self, eval_point, ys, gaussnewton=False):
        self.hess_neg_logpdf_evals += 1
        self._hess_inv_neg_logpdf_action_allocations_impl(eval_point, ys)
        if gaussnewton:
            self._hess_inv_neg_logpdf_action_gn_impl(eval_point, ys, self._hess_inv_action_ys)
        else:
            self._hess_inv_neg_logpdf_action_impl(eval_point, ys, self._hess_inv_action_ys)
        return self._hess_inv_action_ys

    def _hess_neg_logpdf_quadratic_forms_allocations_impl(self, eval_points, ys):
        n_eval_pts = eval_points.shape[0]
        n_vecs = ys.shape[0]
        if (n_eval_pts, n_vecs) != self._quadratic_forms.shape:
            self._quadratic_forms = np.zeros((n_eval_pts, n_vecs))
            self._hessaction_diffs = np.zeros((n_eval_pts, n_vecs, self.dims))
        else:
            self._quadratic_forms[:] = 0
            self._hessaction_diffs[:] = 0

    def hess_neg_logpdf_quadratic_forms(self, eval_points, ys, hessaction_diffs_result=None, result=None, gaussnewton=False):
        # all invocations of this function across workers should have disjoint eval points
        #exp(-0.5 (xi-xj).T H(i) (x_i - x_j)) for all js, i = eval_point, js = pts, 

        #shapes: 
        # eval_points: (n_eval_pts, dim)
        # ys: (n_ys, dim)
        # hessian_action_diffs: (n_eval_pts, n_ys, dim)
        # result: (n_eval_pts, n_ys)

        self.hess_neg_logpdf_evals += eval_points.shape[0] * ys.shape[0]
        N = eval_points.shape[0]
        if result is None: #actually both should be None, do that logic later
            self._hess_neg_logpdf_quadratic_forms_allocations_impl(eval_points, ys)
            result = self._quadratic_forms
            hessaction_diffs_result = self._hessaction_diffs
        if gaussnewton:
            for i in range(N):
                self._hess_neg_logpdf_quadratic_form_gn_impl(eval_points[i], ys, hessaction_diffs_result[i], result[i], idx=i)
        else:
            for i in range(N):
                self._hess_neg_logpdf_quadratic_form_impl(eval_points[i], ys, hessaction_diffs_result[i], result[i], idx=i)
        # if self.comm is not None: self.comm.Barrier()
        return result, hessaction_diffs_result



class Posterior(_Distribution):
    def __init__(self, prior, likelihood, centered_moments_dict=None):
        super().__init__(prior.dims)
        self._prior = prior
        self._comm = prior.comm
        assert self._comm is likelihood.comm
        self._likelihood = likelihood
        self._centered_moments_dict = centered_moments_dict if centered_moments_dict is not None else dict()
    @property
    def prior(self):
        return self._prior

    @property
    def likelihood(self):
        return self._likelihood
        
    @prior.setter
    def prior(self, prior):
        self._prior = prior

    @likelihood.setter
    def likelihood(self, likelihood):
        self._likelihood = likelihood

    def centered_moment(self, num, moment_estimator):
        centered_moment_val = self._centered_moments_dict.get(num) 
        if type(centered_moment_val) is str:
            return np.load(centered_moment_val)
        else:
            return centered_moment_val

    def pdf(self, xs):
        "Unnormalized pdf of the posterior"
        return np.exp(-self.neg_logpdf(xs))

    def neg_logpdf(self, xs):
        "Unnormalized log of the pdf of the posterior"
        self.neg_logpdf_evals += xs.shape[0]

        return self.prior.neg_logpdf(xs) + self.likelihood.neg_logpdf(xs)

    def neg_logpdf_test(self, xs):
        "Unnormalized log of the pdf of the posterior"
        self.neg_logpdf_evals += xs.shape[0]

        return self.prior.neg_logpdf(xs) + self.likelihood.neg_logpdf_test(xs)

    
    def grad_neg_logpdf(self, xs, projections=False, append_result=False):
        "Unnormalized gradient of the log of the pdf of the posterior"
        self.grad_neg_logpdf_evals += xs.shape[0]

        return  [prior_thing + like_thing for prior_thing, like_thing in zip(self.prior.grad_neg_logpdf(xs, projections=projections, append_result=append_result), self.likelihood.grad_neg_logpdf(xs, projections=projections, append_result=append_result))]
       
    def hess_neg_logpdf_action(self, xs, gaussnewton=False):
        raise Exception("Should never need to call this function for posterior. It will use the likelihood and prior Hessians instead")




