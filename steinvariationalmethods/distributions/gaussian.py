#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein_variational_inference.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#


import numpy as np
from numba import vectorize, guvectorize, float64, jit

from .distribution_base import _Distribution
from prngs import parallel as random

@guvectorize([(float64[:], float64[:])], '(n) -> ()', nopython=True,cache=True)
def normalization(sigma_inv, result):
    """
    normalization(sigma_inv)

    Compute the normalization factor for the Gaussian PDF. sigma_inv is a
    vector containing 1 / diag(Sigma).
    """
    n = sigma_inv.shape[0]
    result[0] = 1. / np.sqrt((2*np.pi)**n / np.prod(sigma_inv)) 

@guvectorize([(float64[:], float64[:])], '(n) -> ()', nopython=True, cache=True)
def log_normalization(sigma, result):
    n = sigma.shape[0]
    result[0] = -0.5 * ( n * np.log(2*np.pi) + np.sum(np.log(sigma)))

@guvectorize([(float64[:], float64[:], float64[:], float64[:])],
             '(dim), (dim), (dim), ()', nopython=True, cache=True)
def pdf(x, mu, sigma_inv, result):
    diff = x - mu
    exponent = -0.5 * (diff.T @ (diff * sigma_inv))
    result[0] = np.exp(exponent)

@guvectorize([(float64[:], float64[:], float64[:], float64[:])],
              '(m), (m), (m), ()', nopython=True, cache=True)
def neg_logpdf(x, mu, sigma_inv, result):
    diff = x - mu
    exponent = 0.5 * (diff.T @ (diff * sigma_inv))
    result[0] = exponent

@guvectorize([(float64[:], float64[:], float64[:], float64[:], float64[:])],
             '(m), (m), (m), (), (m)', nopython=True, cache=True)
def grad_neg_logpdf(x, mu, sigma_inv, neg_logpdf_result, grad_neg_logpdf_result): 
    diff = x-mu
    neg_logpdf_result[0] = 0.5 * (diff.T @ (diff * sigma_inv))
    grad_neg_logpdf_result[:] = diff * sigma_inv

@guvectorize([(float64[:], float64[:], float64[:])],
             '(m), (m), (m)', nopython=True, cache=True)
def hess_neg_logpdf_action(sigma_inv, y, result):
    """
    hess_neg_logpdf_action(eval_point, mu, sigma_inv, y)

    Compute the action of the Hessian, as evaluated at `eval_point`, on a
    vector `y`.

    Arguments
    =========
    - eval_point: Shape (n,); point where the Hessian is evaluated.
    - mu: Mean of the distribution, shape (n,) or (1,).
    - sigma_inv: Inverse covariance, same as above. If a vector, it is the
    diagonal of the inverse covariance matrix.
    - y: Vector to which H(eval_point) is applied, shape(n,).

    Returns
    =======
    H(eval_point) * y, shape (n,)
    """
    result[:] += y * sigma_inv

@guvectorize([(float64[:], float64[:], float64[:])],
             '(m), (m), (m)', nopython=True, cache=True)
def hess_inv_neg_logpdf_action(sigma, y, result):
    """
    hess_inv_neg_logpdf_action(eval_point, mu, sigma_inv, y)

    Compute the action of the inverse of the Hessian, as evaluated at `eval_point`, on a
    vector `y`.

    Arguments
    =========
    - eval_point: Shape (n,); point where the Hessian is evaluated.
    - mu: Mean of the distribution, shape (n,) or (1,).
    - sigma_inv: Inverse covariance, same as above. If a vector, it is the
    diagonal of the inverse covariance matrix.
    - y: Vector to which H(eval_point) is applied, shape(n,).

    Returns
    =======
    H(eval_point) * x, shape (n,)
    """
    result[:] = y * sigma

@guvectorize([(float64[:], float64[:], float64[:], float64[:], float64[:])],
             '(m), (m), (m), (m), ()', nopython=True, cache=True)
def hess_neg_logpdf_quadratic_form(eval_point, sigma_inv, y, hessaction_diff_result, result):
    """
    hess_neg_logpdf_quadratic_form(eval_point, sigma_inv, y, hessaction_diff_result, result)

    Compute the action of the Hessian, as evaluated at `eval_point`, on a
    vector `x`.

    Arguments
    =========
    - mu: Mean of the distribution, shape (n,) or (1,).
    - sigma_inv: Inverse covariance, same as above. If a vector, it is the
    diagonal of the inverse covariance matrix.
    - y: (y - eval_point) is the Vector to which H(eval_point) is applied, shape(n,).

    Returns
    =======
    H(eval_point) * x, shape (n,)
    """
    diff = eval_point - y
    hess_action_diff = (diff * sigma_inv)
    hessaction_diff_result[:] = hess_action_diff
    result[0] = diff @ hess_action_diff

def sample(mu, sigma, nsamples):
    """
    sample(mu, sigma, nsamples)

    Draw samples from a normal distribution.

    Arguments
    =========
    - mu: Mean of the distribution.
    - sigma: Standard deviation.
    - nsamples: Number of samples to draw

    Returns
    =======
    samples - a (nsamples, dim) ndarray of samples.

    To infer dimension, mu should be a vector specifying the mean. You will
    get strange results otherwise.
    """
    return random.normal(mu, sigma, (nsamples, mu.shape[0]))

class Gaussian(_Distribution):
    def __init__(self, mu, sigma_inv, init_allocations_size=1, comm=None):
        super().__init__(mu.shape[0], init_allocations_size=init_allocations_size, comm=comm)
        if mu.shape != sigma_inv.shape:
            raise Exception("mu and sigma's shapes don't match")
        
        # Distribution parameters
        # These live on each worker's local memory and are identical
        self._mu = mu
        self._sigma_inv = sigma_inv
        self._sigma = 1./sigma_inv
        self._sigma_sqrt = self._sigma **(0.5) #standard deviations
        self._normalization = None
        self._log_normalization = None

    @property
    def mu(self):
        return self._mu

    @property
    def sigma_inv(self):
        return self._sigma_inv

    @property
    def sigma(self):
        return self._sigma
    
    def centered_moment(self, num):
        if num is 0:
            return 1
        elif num is 1:
            return self._mu
        elif num is 2:
            return np.diag(self._sigma)
        else:
            return 0  

    @property
    def normalization(self):
        if self._normalization is None:
            self._normalization = np.exp(log_normalization(self._sigma))
        return self._normalization

    @property
    def log_normalization(self):
        if self._log_normalization is None:
            self._log_normalization = log_normalization(self._sigma)
        return self._log_normalization

    def _pdf_impl(self, xs, result):
        pdf(xs, self.mu, self.sigma_inv, result)

    def _neg_logpdf_impl(self, xs, result= None):
        neg_logpdf(xs, self.mu, self.sigma_inv, result)


    def _grad_neg_logpdf_impl(self, xs, neg_logpdf_result, grad_neg_logpdf_result):
        grad_neg_logpdf(xs, self.mu, self.sigma_inv, neg_logpdf_result, grad_neg_logpdf_result)

    def _hess_neg_logpdf_action_gn_impl(self, eval_point, ys, result, idx=None):
        hess_neg_logpdf_action(self.sigma_inv, ys, result)

    def _hess_neg_logpdf_action_impl(self, eval_point, ys, result, idx=None):
        hess_neg_logpdf_action(self.sigma_inv, ys, result)

    def _hess_inv_neg_logpdf_action_impl(self, eval_point, ys, result, idx=None):
        hess_inv_neg_logpdf_action(self.sigma, ys, result)

    def _hess_inv_neg_logpdf_action_gn_impl(self, eval_point, ys, result, idx=None):
        hess_inv_neg_logpdf_action(self.sigma, ys, result)

    def _hess_neg_logpdf_quadratic_form_impl(self, eval_point, y, hessaction_diffs_result, result, gaussnewton=False, idx=None):
        hess_neg_logpdf_quadratic_form(eval_point, self.sigma_inv, y, hessaction_diffs_result, result)

    def normalized_pdf(self, x):
        #For plotting purposes
        return self.normalization * self.pdf(x)

    def normalized_neg_logpdf(self, x):
        #Used for Stochastic Newton MCMC purposes
        return self.neg_logpdf(x) - self.log_normalization

    #implementation of sampling for gaussians
    def _sample_impl(self, nsamples, out):
        out[:] = sample(self.mu, self._sigma_sqrt, nsamples)
