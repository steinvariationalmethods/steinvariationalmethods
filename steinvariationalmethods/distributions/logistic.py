#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein_variational_inference.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#

import prngs.parallel as random
import numpy as np
# from numba import vectorize, guvectorize, float64, jit

# from .distribution_base import _Distribution

def sigmoid(x,theta): 
        y = 1./(1.+np.exp(-np.dot(x,theta)))
        return y

# @guvectorize([(float64[:], float64[:])], '(n) -> ()', nopython=True,cache=True)
# def normalization(sigma_inv, result):
#     """
#     normalization(sigma_inv)

#     Compute the normalization factor for the Gaussian PDF. sigma_inv is a
#     vector containing 1 / diag(Sigma).
#     """
#     n = sigma_inv.shape[0]
#     result[0] = 1. / np.sqrt((2*np.pi)**n / np.prod(sigma_inv))

# @guvectorize([(float64[:], float64[:])], '(n) -> ()', nopython=True, cache=True)
# def log_normalization(sigma_inv, result):
#     n = sigma_inv.shape[0]
#     result[0] = 0.5 * ( - n * np.log(2*np.pi) + np.sum(np.log(sigma_inv)))

# @guvectorize([(float64[:], float64[:], float64[:], float64[:])],
#              '(dim), (dim), (dim), ()', nopython=True, cache=True)
# def pdf(x, mu, sigma_inv, result):
#     diff = x - mu
#     exponent = -0.5 * (diff.T @ (diff * sigma_inv))
#     result[0] = np.exp(exponent)


# @guvectorize([(float64[:], float64[:], float64[:], float64[:])],
#               '(m), (n), (o), ()', nopython=True, cache=True)
# def neg_logpdf(x, theta, obs_y, prior_m,prior_sigma2,result):
#     z = sigmoid(x,theta)
#     dz = z*(1.-z)
# #         print("z", z.shape,"y", self.obs_y.shape)
#     misfit = obs_y*z + (1.-obs_y) * (1.-z) + 1e-3
# #         print("misfit", np.sum(np.log(misfit)))
# #         prior = np.exp(-0.5*np.sum((theta-self.prior_m)**2/self.prior_sigma2))
#     logpost_pdf = np.sum(np.log(misfit))  -0.5*np.sum((theta-prior_m)**2/prior_sigma2)#np.log(prior)
#     result[0] = -logpost_pdf





# @guvectorize([(float64[:], float64[:], float64[:], float64[:], float64[:])],
#              '(m), (n), (o), (), (m)', nopython=True, cache=True)
# def grad_neg_logpdf(x, mu, sigma_inv, neg_logpdf_result, grad_neg_logpdf_result): 
#     result[:]

# @guvectorize([(float64[:], float64[:], float64[:], float64[:], float64[:])],
#              '(m), (n), (o), (m), (m)', nopython=True, cache=True)
# def hess_neg_logpdf_action(eval_point, mu, sigma_inv, y, result):
#     """
#     hess_neg_logpdf_action(eval_point, mu, sigma_inv, y)

#     Compute the action of the Hessian, as evaluated at `eval_point`, on a
#     vector `y`.

#     Arguments
#     =========
#     - eval_point: Shape (n,); point where the Hessian is evaluated.
#     - mu: Mean of the distribution, shape (n,) or (1,).
#     - sigma_inv: Inverse covariance, same as above. If a vector, it is the
#     diagonal of the inverse covariance matrix.
#     - y: Vector to which H(eval_point) is applied, shape(n,).

#     Returns
#     =======
#     H(eval_point) * y, shape (n,)
#     """
#     # result[:] =  y * sigma_inv

# @guvectorize([(float64[:], float64[:], float64[:], float64[:], float64[:])],
#              '(m), (m), (m), (m), (m)', nopython=True, cache=True)
# def hess_inv_neg_logpdf_action(eval_point, mu, sigma, y, result):
#     """
#     hess_inv_neg_logpdf_action(eval_point, mu, sigma_inv, y)

#     Compute the action of the inverse of the Hessian, as evaluated at `eval_point`, on a
#     vector `y`.

#     Arguments
#     =========
#     - eval_point: Shape (n,); point where the Hessian is evaluated.
#     - mu: Mean of the distribution, shape (n,) or (1,).
#     - sigma_inv: Inverse covariance, same as above. If a vector, it is the
#     diagonal of the inverse covariance matrix.
#     - y: Vector to which H(eval_point) is applied, shape(n,).

#     Returns
#     =======
#     H(eval_point) * x, shape (n,)
#     """
#     result[:] = y * sigma

# @guvectorize([(float64[:], float64[:], float64[:], float64[:], float64[:])],
#              '(m), (m), (m), (m), ()', nopython=True, cache=True)
# def hess_neg_logpdf_quadratic_form(eval_point, mu, sigma_inv, x, result):
#     """
#     hess_neg_logpdf_action(eval_point, mu, sigma_inv, x)

#     Compute the action of the Hessian, as evaluated at `eval_point`, on a
#     vector `x`.

#     Arguments
#     =========
#     - eval_point: Shape (n,); point where the Hessian is evaluated.
#     - mu: Mean of the distribution, shape (n,) or (1,).
#     - sigma_inv: Inverse covariance, same as above. If a vector, it is the
#     diagonal of the inverse covariance matrix.
#     - x: Vector to which H(eval_point) is applied, shape(n,).

#     Returns
#     =======
#     H(eval_point) * x, shape (n,)
#     """
#     result[0] = x @ (x * sigma_inv)

# def sample(mu, sigma, nsamples):
#     """
#     sample(mu, sigma, nsamples)

#     Draw samples from a normal distribution.

#     Arguments
#     =========
#     - mu: Mean of the distribution.
#     - sigma: Standard deviation.
#     - nsamples: Number of samples to draw

#     Returns
#     =======
#     samples - a (nsamples, dim) ndarray of samples.

#     To infer dimension, mu should be a vector specifying the mean. You will
#     get strange results otherwise.
#     """
#     return np.squeeze(random.normal(mu, sigma, (nsamples, mu.shape[0])))

class Logistic(object):
    def __init__(self, prior, x_train,y_train, init_allocations_size=1, comm=None):
        # super().__init__(mu.shape[0], init_allocations_size=init_allocations_size, comm=comm)
        # if mu.shape != sigma_inv.shape:
        #     raise Exception("mu and sigma's shapes don't match")
        # # Distribution parameters
        # self._mu = mu
        # self._sigma_inv = sigma_inv
        # self._sigma = 1./sigma_inv
        # self._normalization = None
        # self._log_normalization = None

        # Allocations for computation and caching of results
        # self._hess_inv_action = self.init_array((init_allocations_size, self.dims))
        self.prior = prior
        self.dims = self.prior.dims

        self.x = x_train
        self.obs_y = y_train

    def neg_logpdf(self,theta):
        z = sigmoid(self.x,theta)
        dz = z*(1.-z)
#         print("z", z.shape,"y", self.obs_y.shape)
        misfit = self.obs_y*z + (1.-self.obs_y) * (1.-z) + 1e-3
#         print("misfit", np.sum(np.log(misfit)))
#         prior = np.exp(-0.5*np.sum((theta-self.prior_m)**2/self.prior_sigma2))
        logpost_pdf = np.sum(np.log(misfit))  -0.5*np.sum((theta-self.prior._mu)**2/self.prior._sigma)#np.log(prior)

        return [-logpost_pdf]

    
    # @property
    # def mu(self):
    #     return self._mu
    
    # def centered_moment(self, num):
    #     if num is 0:
    #         return 1
    #     elif num is 1:
    #         return self._mu
    #     elif num is 2:
    #         if np.allclose(self._mu, np.zeros_like(self._mu)):
    #             return 1./self._sigma_inv
    #         else:
    #             covariance = self._sigma
    #             return covariance #perhaps we can only get the covariance action on random vectors (H^-1)
    #     else:
    #         return 0  

    # @property
    # def normalization(self):
    #     if self._normalization is None:
    #         self._normalization = np.exp(log_normalization(self._sigma_inv))
    #     return self._normalization

    # @property
    # def log_normalization(self):
    #     if self._log_normalization is None:
    #         self._log_normalization = log_normalization(self._sigma_inv)
    #     return self._log_normalization

    # def _pdf_impl(self, xs, result):
    #     # pdf(xs, self.mu, self.sigma_inv, result)

    # def _neg_logpdf_impl(self, xs, result):
    #     # neg_logpdf(xs, self.mu, self.sigma_inv, result)

    # def _grad_neg_logpdf_impl(self, xs, neg_logpdf_result, grad_neg_logpdf_result):
    #     # grad_neg_logpdf(xs, self.mu, self.sigma_inv, neg_logpdf_result, grad_neg_logpdf_result)

    # def _hess_neg_logpdf_action_gn_impl(self, eval_point, xs, result, idx=None):
    #     # hess_neg_logpdf_action(eval_point, self.mu, self.sigma_inv, xs, result)

    # def _hess_neg_logpdf_action_impl(self, eval_point, xs, result, idx=None):
    #     # hess_neg_logpdf_action(eval_point, self.mu, self.sigma_inv, xs, result)

    # def _hess_neg_logpdf_quadratic_form_impl(self, eval_point, x, hessaction_ys_result, result, gaussnewton=False, idx=None):
    #     # hess_neg_logpdf_quadratic_form(eval_point, self.mu, self.sigma_inv, x, result)

    # def normalized_pdf(self, x):
    #     #For plotting purposes
    #     return self.normalization * self.pdf(x)

    # def normalized_neg_logpdf(self, x):
        # return self.neg_logpdf(x) - self.log_normalization