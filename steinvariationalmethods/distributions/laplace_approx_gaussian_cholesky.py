#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein_variational_inference.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#


#L @ (L.T x)  => sigma_inv * x 



import prngs.parallel as random
import scipy as sp
import numpy as np
from numba import vectorize, guvectorize, float64, jit
from scipy.sparse import diags
from .distribution_base import _Distribution

# @guvectorize([(float64[:], float64[:], float64[:], float64[:, :], float64[:, :], float64[:])],
             # '(dim), (dim), (dim), (dim, rank), (rank, dim), ()', nopython=True, cache=True)
def pdf(x, mu, L ,Ut, d,result):
    # exp(-0.5 diff.T (R + RU D U^{T}r) diff)
    diff = x - mu
    exponent = - 0.5 *( (diff.T @ (L @ (L.T @ diff )))+ diff.T @ (L @ ( L.T @( Ut.T@ (d* (Ut @ (L @  (L.T @ diff))))))))
    result[0] = np.exp(exponent)

# @guvectorize([(float64[:], float64[:], float64[:], float64[:, :], float64[:, :], float64[:])],
              # '(dim), (dim), (dim), (dim, rank), (rank, dim), ()', nopython=True, cache=True)
def neg_logpdf(x, mu, L,Ut,d,result):
    #0.5 diff.T (R + RU D U^{T}R) diff
    diff = x - mu
    neg_exponent = 0.5 *( (diff.T @ (L @ (L.T @ diff )))+ diff.T @ (L @ ( L.T @( Ut.T@(d*(Ut @ (L @  (L.T @ diff))))))))
    result[0] = neg_exponent

# @guvectorize([(float64[:], float64[:], float64[:], float64[:, :], float64[:, :], float64[:], float64[:])],
             # '(dim), (dim), (dim), (dim, rank), (rank, dim), (), (dim)', nopython=True, cache=True)
def grad_neg_logpdf(x, mu,L,Ut,d, neg_logpdf_result, grad_neg_logpdf_result):
    #(R + RU D U^{T}) diff
    diff = x-mu
    sigma_inv_diff = (L @ (L.T @ diff)) + (L @ ( L.T @( Ut.T@(d*(Ut @ (L @  (L.T @ diff)))))))
    neg_logpdf_result[0] = 0.5 * diff.T @ sigma_inv_diff
    grad_neg_logpdf_result[:] = sigma_inv_diff

# @guvectorize([(float64[:], float64[:, :], float64[:, :], float64[:], float64[:])],
             # '(dim), (dim, rank), (rank, dim), (dim), (dim)', nopython=True, cache=True)
def hess_neg_logpdf_action(L, y,Ut,d, result):
    """
    hess_neg_logpdf_action(eval_point,  prior_sigma_invU, dUt, d, y) 

    Compute the action of the Hessian, as evaluated at `eval_point`, on a
    vector `y`, i.e. (R + RU D U^{T}R) y, where R = prior_sigma_inv

    Arguments
    =========
    - eval_point: Shape (n,); point where the Hessian is evaluated.
    - mu: Mean of the distribution, shape (n,) or (1,).
    - prior_sigma_inv: Prior Inverse covariance, same as above. If a vector, it is the
    diagonal of the inverse covariance matrix.
    - Ut: U.T, the basis vectors for the rowspace of the Posterior Laplace approximation Precision
    - d: the eigenvalues for the posterior Precision
    - y: Vector to which H(eval_point) is applied, shape(n,).
    - the result of the hessian action
    Returns
    =======
    H(eval_point) * y, shape (n,)
    """
    result[:] = L @ (L.T @  y )+ (L @ ( L.T @( Ut.T@(d*(Ut @ (L @  (L.T @ y)))))))

# @guvectorize([(float64[:], float64[:, :], float64[:, :], float64[:], float64[:])],
             # '(dim), (rank, dim), (rank, dim), (dim), (dim)', nopython=True, cache=True)
def hess_inv_neg_logpdf_action(Lsolve, Ut, log_likelihood_gaussian_approx_eigsUt, y, result):
    """
    hess_inv_neg_logpdf_action(prior_sigma, Ut, log_likelihood_gaussian_approx_eigs, y, result)

    Compute the action of the inverse of the Hessian, as evaluated at `eval_point`, on a
    vector `y`, i.e. (R^-1 - U (I + D^{-1})^{-1} U^T) y, where R^-1 = prior_sigma
                i.e. R^-1 y   -  U ((I + D)^{-1} D) U^T)y
    Arguments
    =========
    - eval_point: Shape (n,); point where the Hessian is evaluated.
    - mu: Mean of the distribution, shape (n,) or (1,).
    - sigma_inv: Inverse covariance, same as above. If a vector, it is the
    diagonal of the inverse covariance matrix.
    - y: Vector to which H(eval_point) is applied, shape(n,).

    Returns
    =======
    H(eval_point) * x, shape (n,)     
    """
    result[:] = sp.linalg.cho_solve_banded((Lsolve, True), y.T, overwrite_b=False).T   -   (Ut.T @ ((log_likelihood_gaussian_approx_eigsUt) @ y.T )).T

# @guvectorize([(float64[:], float64[:], float64[:], float64[:, :], float64[:, :], float64[:], float64[:])],
             # '(dim), (dim), (dim), (rank, dim), (rank, dim), (dim), ()', nopython=True, cache=True)
def hess_neg_logpdf_quadratic_form(eval_point, y, L,Ut, d,hess_action_diff_result, result):
    """
    hess_neg_logpdf_action(eval_point, Ut, d, y)

    Compute the quadratic form of the Hessian, as evaluated at `eval_point`, of the
    vector `eval_point - y` i.e. , le
    tting z = (eval_point - y)
            z.T (R^-1 - U (I + D^{-1})^{-1} U^T) z, where R^-1 = prior_sigma
                i.e. z.T R^-1 z   -  z.T  U ((I + D)^{-1} D) U^T)z

    Arguments
    =========
    - eval_point: Shape (n,); point where the Hessian is evaluated.
    - mu: Mean of the distribution, shape (n,) or (1,).
    - sigma_inv: Inverse covariance, same as above. If a vector, it is the
    diagonal of the inverse covariance matrix.
    - x: Vector to which H(eval_point) is applied, shape(n,).

    Returns
    =======
    H(eval_point) * x, shape (n,)
    """
    diff = eval_point - y
    hess_action_diff = L @ (L.T @  y ) +(L @ ( L.T @( Ut.T@(d*(Ut @ (L @  (L.T @ diff)))))))  #prior_sigma * diff -   (Ut.T @ ((log_likelihood_gaussian_approx_eigsUt) @ diff ))  
    hess_action_diff_result[:] = hess_action_diff
    result[0] = diff.T @ hess_action_diff

def sample(L_solve, dim, nsamples):
    """
    sample(sigma, nsamples)

    Draw samples from a 0 mean normal distribution.

    Arguments
    =========
    - sigma: Standard deviation.
    - nsamples: Number of samples to draw

    Returns
    =======
    samples - a (nsamples, dim) ndarray of samples.

    To infer dimension, sigma should be a vector specifying the standard deviation. You will
    get strange results otherwise.
    """
    samples = np.zeros(dim)
    samples = random.normal(0., 1., dim)
    samples = sp.linalg.solve_banded((1,0), L_solve, samples, check_finite=False)
    # for i in range(nsamples):
    #     samples[i] = sp.linalg.solve_banded((1,0), L_solve, samples[i], check_finite=False)
    return samples

# @guvectorize([(float64[:], float64[:], float64[:])], '(rank), (dim) -> ()', nopython=True, cache=True)
def log_normalization(d, sigma, result): #log of the normalization factor in front of the PDF
    """See equation 2.23 of Stochastic Newton MCMC paper
    """
    n = sigma.shape[0]
    result[0] = - 0.5 * ( n * np.log(2*np.pi)) -0.5 * np.log(np.linalg.det(sigma)) \
        + 0.5*np.sum(np.log(1 + d)) #logdet(Covariace)^0.5

class LaplaceApproxGaussian_cholesky(_Distribution):
    """
    Class for the Laplace approximation (Gaussian) of the Posterior given a Gaussian Prior,
    and a Low-rank Hessian approximation of the Covariance of the Likelihood (approximated as a Gaussian)
    computed at a Maximum A Posteriori point, which becomes the Gaussian mean.
    This class provides functionality for approximate Hessian apply, Hessian inverse apply, and Gaussian sampling 
    based on an approximate (randomized projection) low rank factorization of the Hessian.
    
    In particular if :math: `R` is the prior Precision, :math:`d` and :math:`U` are the dominant eigenpairs of
    :math:`H_{\\mbox{log_likelihood}} U[:,i] = d[i] R U[:,i]`
    then we have:
    
    - low rank Hessian apply: :math:`y = ( R + RU D U^{T}R) x.`
    - low rank Hessian solve: :math:`y = (R^-1 - U (I + D^{-1})^{-1} U^T) x.` 
    - low rank Hessian Gaussian sampling: :math:`y = ( I - U S U^{T}R) x`, where :math:`S = I - (I + D)^{-1/2}` and :math:`x \\sim \\mathcal{N}(0, R^{-1}).`
    """
    def __init__(self, prior, Ut, d, mu=None, init_allocations_size=1, comm=None): 
        """
        Construct the Gaussian approximation of the posterior.
        Input:
        - :code:`prior`: the prior 
        - :code:`d`:     the dominant generalized eigenvalues of the Hessian of the log likelihood at the MAP point of the posterior
        - :code:`U`:     the dominant generalized eigenvector of the Hessian of the log likelihood :math:`U^T R U = I.` at the MAP point of the posterior
        - :code:`mu`:  `the` MAP point of the posterior
        """
        super().__init__(Ut.shape[1], init_allocations_size=init_allocations_size, comm=comm)
        if mu is not None:
            if mu.shape[0] != Ut.shape[1]:
                raise Exception("mu and eigenvectors of covariance shapes don't match")
        if Ut.shape[0] != d.shape[0]:
            raise Exception("Precision/Hessian eigenvalue and eigenvector shapes dont match")

        # Distribution parameters
        self._mu = mu
        self._Ut = Ut
        self._L = prior._L
        self._Lsolve = prior._L_cholesky_banded_obj
    
        # self._prior_sigma_invU =  prior._L @ ( (prior._L).T @ Ut.T)#Sigma_prior^{-1} U
        self._prior_sigma = sp.linalg.cho_solve_banded((prior._L_cholesky_banded_obj, True), np.identity(self.dims), overwrite_b=False) 
        # self._prior_sigma_inv = prior._L @ ( (prior._L).T @ np.identity(mu.shape[0]))  #diagonal sigma inv, i.e. R
        # self._dUtR = np.diag(d) @ (Ut @ (prior._L @  (prior._L).T )) #D U.T R
        # self._prior_sigma_sqrt = np.linalg.inv(prior._L@ np.identity(mu.shape[0]))#for sampling
        self._prior_mu = prior._mu
        ones = np.ones( d.shape, dtype=d.dtype )   
        self._log_likelihood_gaussian_approx_eigsUt = np.diag((d / (ones + d))) @ Ut
        self._log_likelihood_gaussian_approx_eigs_sqrtUt = np.diag(ones - np.power(ones + d, -.5)) @  Ut #I - (I + D)^{-0.5}
        self._d = d


    @property
    def normalization(self):
        if self._normalization is None:
            self._normalization = np.exp(log_normalization(self._d, self._prior_sigma))
        return self._normalization

    @property
    def log_normalization(self):
        if self._log_normalization is None:
            self._log_normalization = log_normalization(self._d, self._prior_sigma)
        return self._log_normalization

    @property
    def mu(self):
        return self._mu

    @mu.setter
    def mu(self, mu):
        self._mu = mu

    def centered_moment(self, num):
        if num is 0:
            return 1
        elif num is 1:
            return self._mu
        elif num is 2:
            return diags(self._prior_sigma, 0)-  self._Ut.T @ self._log_likelihood_gaussian_approx_eigsUt#....perhaps we can only get the covariance action on random vectors (H^-1)
        else:
            return 0  

    def _pdf_impl(self, xs, result):
        pdf(xs, self.mu, self._L,self._Ut,self._d, result)

    def _neg_logpdf_impl(self, xs, result):
        neg_logpdf(xs, self.mu, self._L,self._Ut,self._d,result)


    def _grad_neg_logpdf_impl(self, xs, neg_logpdf_result, grad_neg_logpdf_result):
        grad_neg_logpdf(xs, self.mu, self._L,self._Ut,self._d,neg_logpdf_result, grad_neg_logpdf_result)

    def _hess_neg_logpdf_action_gn_impl(self, eval_point, ys, result, idx=None):
        hess_neg_logpdf_action(self._L,self._Ut, self._d,ys, result)

    def _hess_neg_logpdf_action_impl(self, eval_point, ys, result, idx=None):
        hess_neg_logpdf_action(self._L,self._Ut,self._d, ys, result)

    def _hess_inv_neg_logpdf_action_impl(self, eval_point, ys, result):
        hess_inv_neg_logpdf_action(self._Lsolve , self._Ut, self._log_likelihood_gaussian_approx_eigsUt, ys, result)

    def _hess_inv_neg_logpdf_action_gn_impl(self, eval_point, ys, result):
        hess_inv_neg_logpdf_action(self._Lsolve , self._Ut,self._log_likelihood_gaussian_approx_eigsUt, ys, result)

    def _hess_neg_logpdf_quadratic_form_impl(self, eval_point, y, hessaction_diff_result, result, gaussnewton=False, idx=None):
        hess_neg_logpdf_quadratic_form(eval_point, y, self._L,self._Ut,self._d, hessaction_diff_result, result)

    def _sample_impl(self, nsamples, out):
        # low rank Hessian Gaussian sampling: :math:`y = ( I - U S U^{T}R) x`, where :math:`S = I - (I + D)^{-1/2}` and :math:`x \\sim \\mathcal{N}(0, R^{-1}).`
        samples = sample(self._Lsolve, self._dims, nsamples)
        out[:] = samples - ( self._Ut.T @ (self._log_likelihood_gaussian_approx_eigs_sqrtUt @ (self._L @ ( (self._L).T @samples) )))
        if self.mu is not None:
            out[:] += self.mu

    def normalized_neg_logpdf(self, x):
        # Used for Stochastic Newton MCMC
        return self.neg_logpdf(x) - self.log_normalization

    def normalized_pdf(self, x):
        #For plotting purposes
        return self.normalization * self.pdf(x)
