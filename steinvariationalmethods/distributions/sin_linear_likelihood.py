#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein_variational_inference.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#

import prngs.parallel as random
import numpy as np
from numba import vectorize, guvectorize, float64, jit

from .distribution_base import _Distribution


@guvectorize([(float64[:], float64[:], float64[:,:], float64[:], float64[:])],
              '(dim), (), (n_mix,dim), (n_obs), ()', nopython=True, cache=True)
def neg_logpdf(x, noise_sigma2, a, obs, result):
    # exponent = (0.5 /noise_sigma2) * np.sum((a @ x-obs)**2)
    # result[0] = (0.5 /noise_sigma2) * np.sum((a @ x-obs)**2)
    # ax = a@x
    # axax = ax@ax
    Fx =   np.log(x@(a.T@(a@(a.T@(a@x)))))#np.log(np.sum(np.sum(a**2,axis=0)*x)**2)#ax@ax#np.log(axax)
    diff = Fx- obs
    result[0] = (diff.T @ (diff *  (0.5 /noise_sigma2)))


@guvectorize([(float64[:], float64[:], float64[:,:], float64[:], float64[:],float64[:])],
             '(dim), (), (n_mix,dim), (n_obs), (), (dim)', nopython=True, cache=True)
def grad_neg_logpdf(x, noise_sigma2, a , obs, neg_logpdf_result, grad_neg_logpdf_result): 
    # ax = a@x
    # axax = ax@ax
    #sumax = np.sum(np.sum(a**2,axis=0)*x)
    ax = a.T@(a@(a.T@(a@x)))
    logFx = x@ax
    Fx = np.log(logFx)#np.log(sumax**2)#axax#np.log(axax)
    # Fx =  2.*np.log(sumax)#axax#
    diff = Fx - obs
    neg_logpdf_result[0] = (diff.T @ (diff *  (0.5 /noise_sigma2)))
    J = 2./logFx * ax#2.*np.sum(a**2,axis=0)/ sumax  #2. * (a.T@ax)#*1./axax 
    grad_neg_logpdf_result[:] = J * np.sum(diff)/noise_sigma2

@guvectorize([(float64[:],float64[:], float64[:,:], float64[:], float64[:],  float64[:],float64[:])],
             '(m),(), (n_mix,m), (), (n_obs),(m), (m)', nopython=True, cache=True)
def hess_neg_logpdf_action(x,noise_sigma2, a, n_obs,obs, y, result):
    """
    hess_neg_logpdf_action(eval_point, mu, sigma_inv, y)

    Compute the action of the Hessian, as evaluated at `eval_point`, on a
    vector `y`.

    Arguments
    =========
    - eval_point: Shape (n,); point where the Hessian is evaluated.
    - mu: Mean of the distribution, shape (n,) or (1,).
    - sigma_inv: Inverse covariance, same as above. If a vector, it is the
    diagonal of the inverse covariance matrix.
    - y: Vector to which H(eval_point) is applied, shape(n,).

    Returns
    =======
    H(eval_point) * y, shape (n,)
    """
    ax = a.T@(a@(a.T@(a@x)))
    logFx = x@ax
    Fx = np.log(logFx)
    # Fx = 2.*np.log(sumax)#axax#np.log(axax)
    J =2./logFx * ax #2. * (a.T@ax) #* 1./axax 
    # log_tmp = 2.*np.log(ad)- 2.*np.log(sumax)
    # result[:] = (n_obs /(noise_sigma2) * (J * (J @ y))) - 2.* np.sum(Fx-obs)/noise_sigma2 * y * np.exp(log_tmp)#* 2 * (a.T@(a@y))
    result[:] = (n_obs /(noise_sigma2) * (J * (J @ y))) + 2.*np.sum(Fx-obs)/noise_sigma2 * ( (a.T@(a@(a.T@(a@y))))-2.*( ax*(ax@y)) /logFx) #* 2 * (a.T@(a@y))

# @guvectorize([(float64[:], float64[:], float64[:], float64[:], float64[:])],
#              '(m), (m), (m), (m), ()', nopython=True, cache=True)
# def hess_neg_logpdf_quadratic_form(eval_point, a, obs, noise_sigma, hessaction_diff_result, result):
#     """
#     hess_neg_logpdf_quadratic_form(eval_point, sigma_inv, y, hessaction_diff_result, result)

#     Compute the action of the Hessian, as evaluated at `eval_point`, on a
#     vector `x`.

#     Arguments
#     =========
#     - mu: Mean of the distribution, shape (n,) or (1,).
#     - sigma_inv: Inverse covariance, same as above. If a vector, it is the
#     diagonal of the inverse covariance matrix.
#     - y: (y - eval_point) is the Vector to which H(eval_point) is applied, shape(n,).

#     Returns
#     =======
#     H(eval_point) * x, shape (n,)
#     """
#     Fx = np.sum(a*eval_point)
#     diff = Fx - obs
#     hess_action_diff = (diff / noise_sigma**2)
#     hessaction_diff_result[:] = hess_action_diff
#     result[0] = diff @ hess_action_diff

class SinLinearLikelihood(_Distribution):
    def __init__(self, prior, n_mix, n_obs, noise_sigma, init_allocations_size=1, comm=None):

        super().__init__(prior.dims, init_allocations_size=init_allocations_size, comm=comm)
        self._n_dim = prior.dims
        self._noise_sigma2 = noise_sigma**2
        self._noise_sigma = noise_sigma
        self._n_mix = n_mix

        self._obs_nobs = n_obs

        # local_x = prior.sample(self._obs_nobs)
        # self._x = prior.allgather(local_x) 
        if self.comm.Get_rank() == 0:
            self._x = prior.sample0(self._obs_nobs)
        else:
            self._x = np.zeros((self._obs_nobs,self._n_dim))
        self.comm.Bcast(self._x,root = 0)

        #this is the function that is discretized
        self._a = np.array([np.sin(r*np.arange(1,self._n_dim+1)* np.pi / self._n_dim)/self._n_dim for r in range(1,self._n_mix+1)])



        if self.comm.Get_rank() == 0:
            self._obs_noise = self._noise_sigma*random.normal(size=(self._obs_nobs))
        else:
            self._obs_noise = np.zeros(self._obs_nobs)
        self.comm.Bcast(self._obs_noise,root = 0)



        # ax = self._a@self._x
        self._obs_true = np.log(np.sum(np.sum(self._a**2,axis=0)*self._x,axis=1)**2)#ax@ax#np.log(ax@ax)
        #True observations (noisy)
        self.comm.barrier()
        print('shape noise', self._obs_noise.shape)
        print('obs_true noise',self._obs_noise.shape)
        self._obs = self._obs_true + self._obs_noise
        self.comm.barrier()

    @property
    def noise_sigma(self):
        return self._noise_sigma

    @property
    def noise_sigma2(self):
        return self._noise_sigma2

    def _neg_logpdf_impl(self, xs, result):
        neg_logpdf(xs, self._noise_sigma2,self._a,self._obs,result)

    def _grad_neg_logpdf_impl(self, xs, neg_logpdf_result, grad_neg_logpdf_result):
        grad_neg_logpdf(xs, self._noise_sigma2,self._a, self._obs, neg_logpdf_result, grad_neg_logpdf_result)

    def _hess_neg_logpdf_action_gn_impl(self, xs, ys, result, idx=None):
        hess_neg_logpdf_action(xs,self._noise_sigma2, self._a, self._obs_nobs, self._obs,ys, result)

    def _hess_neg_logpdf_action_impl(self, xs, ys, result, idx=None):
        hess_neg_logpdf_action(xs,self._noise_sigma2, self._a, self._obs_nobs, self._obs, ys, result)

    # def _hess_neg_logpdf_quadratic_form_impl(self, eval_point, y, hessaction_diffs_result, result, gaussnewton=False, idx=None):
    #     hess_neg_logpdf_quadratic_form(eval_point, self._a,self._obs, self._noise_sigma,y, hessaction_diffs_result, result)
