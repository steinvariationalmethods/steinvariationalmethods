#
# This file is part of stein variational inference methods class project.
#
# stein variational inference methods class project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein variational inference methods class project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein variational inference methods class project.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: .. 
#

import sys
import logging
import cProfile
import os
try:
    from mpi4py import MPI
except:
    pass
import numpy as np
from numpy import prod, ndarray
from functools import wraps
from scipy.sparse.linalg import LinearOperator


__all__ = ['allgather',
           'assign_indices',
           'assign_slice',
           'LOG_LEVEL',
           'logger',
           'setLogLevel', 
           'deprecate',
           'cprofile',
           'MPIFree',
           'np_float64_shared_array',
           'np_float64_increase_size_shared_array',
           'assign_size',
           'PriorCovarianceLinearOperator',
           'LowRankEigendecompLinearOperator',
           'load_from_file',
           'save_to_file',
           'Allreduce_mean_scalars',
           'Allreduce_scalars',
           'Allreduce_mean_axis_0',
           'Reduce_mean_scalars_to_rank_0',
           'pprint',
           'pprintsection'
           ]

####### LOGGING #########
LOG_LEVEL = logging.getLogger().getEffectiveLevel()

logger = logging.getLogger('SVI.semilattices.')
logger.propagate = False
ch = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter("%(asctime)s %(levelname)s:%(name)s: %(message)s",
                              "%Y-%m-%d %H:%M:%S")
ch.setFormatter(formatter)
logger.addHandler(ch)


def pprint(*args, comm=None):
    if comm is None or comm.Get_rank() == 0:
        print(*args)

def pprintsection(*args, comm=None):
    pprint(80*'#', comm=comm)
    pprint(*args, comm=comm)
    pprint(80*'#', comm=comm)

class PriorCovarianceLinearOperator(LinearOperator):
    def __init__(self, sigma_diag):
        dtype = sigma_diag.dtype
        N = sigma_diag.shape[0]
        shape = (N,N)
        super().__init__(dtype, shape)
        self._sigma_diag = sigma_diag
    def _matvec(self, vec):
        return self._sigma_diag * vec
    
class LowRankEigendecompLinearOperator(LinearOperator):
    def __init__(self, Ut, d):
        dtype = d.dtype
        assert(Ut.ndim ==2)
        N = Ut.shape[1]
        shape = (N,N)
        super().__init__(dtype, shape)
        self._Ut = Ut
        self._d = d
    def _matvec(self, vec):
        return self._Ut.T @ (self._d *(self._Ut @ vec))

def load_from_file(n_particles, dimension, comm):
    #Rank 0 load data, broadcast to all, into all_samples
    filename =str(n_particles)+"_"+str(dimension)+".npy"
    if os.path.exists(filename):
        if comm is None or comm.Get_rank() == 0:
            all_samples = np.load(filename)
        else:
            all_samples = np.empty((n_particles, dimension), dtype=np.float64)
        # Broadcast A from rank 0 to everybody
        if comm is not None and comm.Get_size() > 1 : comm.Bcast( [all_samples, MPI.DOUBLE], root=0)        
        return all_samples
    else:
        print("No samples exist to read from file.")
        raise ValueError("no existing file to read")

def save_to_file(x, comm):
    assert(x.ndim == 2)
    n_particles, dimension = x.shape
    if comm is None or comm.Get_rank() == 0:
        filename =str(n_particles)+"_"+str(dimension)+".npy"
        np.save(filename, x)
    if comm is not None and comm.Get_size()>1: comm.Barrier()


#all ranks 
def Allreduce_mean_scalars(quantity, comm):
    if comm is None or comm.Get_size() == 1:
        return np.mean(quantity)
    mean_cost_old = np.zeros(1, dtype=np.float64)
    local_mean_cost_old = np.mean(quantity)
    comm.Allreduce(local_mean_cost_old, mean_cost_old, op=MPI.SUM)
    mean_cost_old /= comm.Get_size()
    return mean_cost_old

def Allreduce_scalars(quantity, comm):
    # print('quantity = ',quantity)
    if comm is None or comm.Get_size() == 1:
        return quantity
    # sum_cost_old = np.zeros(1, dtype=np.float64)
    sum_cost_old = comm.allreduce(sendobj= quantity, op=MPI.SUM)
    return sum_cost_old


def Allreduce_mean_axis_0(quantity, comm):
    # print("quantity size ", quantity.shape)
    if comm is None or comm.Get_size() == 1:
        return np.mean(quantity, axis=0)
    # print('quantity shape',quantity.shape)
    mean_cost_old = np.zeros(quantity.shape[1:], dtype=np.float64)
    # print('mean cosst old shape',mean_cost_old.shape)
    local_mean_cost_old = np.mean(quantity, axis=0)
    local_mean_cost_old = np.ascontiguousarray(local_mean_cost_old)
    # print("ordering ",local_mean_cost_old.flags)
    # print("locallllllllll ",local_mean_cost_old.shape,local_mean_cost_old)
    comm.Allreduce(local_mean_cost_old, mean_cost_old, op=MPI.SUM)
    # print("globalllllllllll ",mean_cost_old)
    mean_cost_old /= comm.Get_size()
    return mean_cost_old

#mean goes onto rank 0
def Reduce_mean_scalars_to_rank_0(quantity, comm):
    if comm is None or comm.Get_size() == 1:
        return np.mean(quantity)
    mean_cost_old = np.array(0, dtype=np.float64) #is this ok???
    local_mean_cost_old = np.mean(quantity)
    comm.Reduce(local_mean_cost_old, mean_cost_old, root=0)
    mean_cost_old /= comm.Get_size()
    return mean_cost_old


########################################################################################################################################



def allgather(local_array, out, comm): 
    if comm is None or comm.Get_size() == 1:
        out[:] = local_array
    else:
        comm.Allgather( [local_array, MPI.DOUBLE], [out, MPI.DOUBLE] )
    return out

def setLogLevel(level):
    r""" Set the log level for all existing and new objects related to the semilattices module

    Args:
      level (int): logging level

    .. see:: the :module:`logging` module.
    """
    import sparse_multi_indices as SVI
    SVI.LOG_LEVEL = level
    for lname, logger in logging.Logger.manager.loggerDict.items():
        if "SVI." in lname:
            logger.setLevel(level)

###### END LOGGING #######

def deprecate(name, version, msg):
    def deprecate_decorator(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            logger.warning("%s DEPRECATED since v%s. %s" % (name, version, msg))
            return f(*args, **kwargs)
        return wrapped
    return deprecate_decorator

def cprofile(func):
    def profiled_func(*args, **kwargs):
        profile = cProfile.Profile()
        try:
            profile.enable()
            result = func(*args, **kwargs)
            profile.disable()
            return result
        finally:
            profile.print_stats(sort='time')
    return profiled_func

def MPIFree(mpi_window):
    if mpi_window is not None:
        MPI.Win.Free(mpi_window)
        
#For instantiating shared memory arrays for storing samples, also random vectors for randomized SVD
try:
    SIZE_OF_FLOAT64 = MPI.DOUBLE.Get_size()
except:
    pass
def np_float64_shared_array(shape, comm): #shape is assumed to be a 2d iterable
    # create a shared array of 'shape' elements of type np.float64
    if comm.Get_rank() == 0: 
        nbytes = prod(shape) * SIZE_OF_FLOAT64  #bytes in total 
    else: 
        nbytes = 0

    # on rank 0, create the shared block
    # on rank other get a handle to it (known as a window in MPI speak)
    mpi_window = MPI.Win.Allocate_shared(nbytes, SIZE_OF_FLOAT64, comm=comm)  
    buf, itemsize = mpi_window.Shared_query(0) # get the process local address for the shared memory
    comm.Barrier()
    return ndarray(buffer=buf, dtype='float64', shape=shape), mpi_window

def np_float64_increase_size_shared_array(shared_array, extra_shape, shared_array_mpi_window, comm):
    current_shape = shared_array.shape
    assert(current_shape[1:] == extra_shape[1:]) #last dimensions must match
    if comm.Get_rank() == 0: 
        nbytes = (prod(current_shape) + prod(extra_shape)) * SIZE_OF_FLOAT64  #bytes in total  #allocate more memory into new array
    else: 
        nbytes = 0

    shape = list(current_shape)
    shape[0] = shape[0] + extra_shape[0]
    shape = tuple(shape)

    # on rank 0, create the shared block
    # on rank other get a handle to it (known as a window in MPI speak)
    new_mpi_window = MPI.Win.Allocate_shared(nbytes, SIZE_OF_FLOAT64, comm=comm)#, Alloc_shared_noncontig=True) 
    buf, itemsize = new_mpi_window.Shared_query(0) # get the process local address for the shared memory
    new_larger_shared_array = ndarray(buffer=buf, dtype='float64', shape=shape) 
    if comm.Get_rank() ==0:
        new_larger_shared_array[0:current_shape[0]] = shared_array #copy the old array into the new larger array
        # del shared_array #delete the old array
    MPI.Win.Free(shared_array_mpi_window) #free the shared memory window
    comm.Barrier()
    print(new_larger_shared_array)
    return new_larger_shared_array, new_mpi_window

def assign_indices(N, comm):
    # N is the number of indices in the array we're indexing
    if comm is None or comm.Get_size() == 1:
        my_idxs = (0, N)
    else:
        comm_size, rank = comm.Get_size(), comm.Get_rank()
        if N > comm_size:
            num_idxs, remainder = divmod(N, comm_size) #give the last worker less work
            if remainder != 0:
                num_idxs +=1 
        else:
            if rank < N: 
                num_idxs = 1
            else:
                num_idxs = 0
        start_idx = num_idxs * rank
        if rank == comm_size - 1:
            end_idx = N
        else:
            end_idx = start_idx + num_idxs
        my_idxs =  start_idx, end_idx
        # print(my_idxs)
        comm.Barrier()
    # Return from this function is not synchronized
    return my_idxs

def assign_slice(N, comm):
    #assign numpy slicing for memory arrays
    start_idx, end_idx = assign_indices(N, comm)
    return np.s_[ start_idx : end_idx]


def assign_size(N, comm):
    # N is the number of indices in the array we're indexing
    if comm is None or comm.Get_size() == 1:
        num_idxs = N
    else:
        comm_size, rank = comm.Get_size(), comm.Get_rank()
        if N > comm_size:
            num_idxs, remainder = divmod(N, comm_size) #give the last worker less work
            if remainder != 0:
                num_idxs +=1 
            if rank == comm_size -1:
                N - num_idxs * ( comm_size - 1)
        else:
            if rank < N: 
                num_idxs = 1
            else:
                num_idxs = 0

        comm.Barrier()
    return num_idxs
