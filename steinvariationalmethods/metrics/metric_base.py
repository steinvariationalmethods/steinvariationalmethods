from abc import ABC, abstractmethod
import prngs.parallel as random
from misc import *
import numpy as np

class _Metric(ABC):
    r"""A Kernel takes a metric (inner product field) and 
    options, and allows for the evaluation of the Kernel function at pairs of particles which
    live in parameter space. Derivatives are also available."""
    def __init__(self, manifold, project, common_projector, init_allocations_size=1, low_rank=None, oversampling=1):
        self._manifold = manifold
        self._dims = manifold.dims
        self._project = project

        assert(low_rank is not None)
        assert(low_rank != 0)
        assert(low_rank <= self._dims)
        self._eigendecomp_rank = low_rank
        assert(oversampling >= 0)
        self._oversampling = oversampling
        self._random_projection_dims = low_rank + oversampling
        assert(self._random_projection_dims <= self._dims)
        self._random_projection = self._sample_random_projection(self._random_projection_dims)

        self._common_projector = common_projector
        
        #for eigendecomposed hessians
        self._d_s = np.zeros((init_allocations_size, self._eigendecomp_rank))
        self._Ut_s = np.zeros((init_allocations_size, self._eigendecomp_rank, self.dims))
            
        #For AOmega temporary matrix for randomized eigendecomposition
        self._hessaction_Omega = np.zeros((init_allocations_size, self._random_projection_dims, self.dims))

    

        self._matrix_calculated = False

    @property
    def random_projection(self):
        return self._random_projection
    
    @property
    def comm(self):
        return self._manifold._comm

    def _expand_array(self, array, extra_shape):
        #add zeros to the end of the array given a compatible "extra shape":
        return np.concatenate((array, np.zeros(extra_shape)), axis=0)

    def _assign_size(self, N):
        # assign size of local portion
        return assign_size(N, self.comm)

    def _low_rank_eigendecomposition_allocations_impl(self, eval_points, projection_rank, oversampling):
        N = eval_points.shape[0]
        allocated = False
        if N != self._d_s.shape[0]:
            self._d_s = np.zeros((N, projection_rank))
            allocated = True
        else:
            self._d_s[:] = 0
        if (N, projection_rank, self.dims) != self._Ut_s.shape:
            self._Ut_s = np.zeros((N, projection_rank, self.dims))
            allocated = True
        else:
            self._Ut_s[:] = 0
        if (N, projection_rank + oversampling, self.dims) != self._hessaction_Omega.shape:
            self._hessaction_Omega = np.zeros((N, projection_rank+oversampling, self.dims ))
            allocated = True
        else:
            self._hessaction_Omega[:] = 0
        if self.comm is not None: self.comm.Barrier()

    def _sample_random_projection(self, projection_dim):
        assert(projection_dim <=self.dims)
        np.random.seed(0)
        return np.random.normal(size=(projection_dim, self.dims))/self.dims #not shared random projection, since its not a lot of storage and not needed

    @property
    def dims(self):
        # if self._project:
        #     return self._eigendecomp_rank
        # else:
        return self._dims

    @property
    def manifold(self): 
        # the manifold which we take Hessians of to define the metric.
        # it will almost always be a Bayesian distribution (prior, likelihood, posterior)
        # but doesnt have to be. 
        return self._manifold
    
    def mults(self, eval_point, vecs, result, gaussnewton=False):
        self._mults_impl(eval_point, vecs, result, gaussnewton)
        return result

    def quadratic_forms(self, eval_points, ys, hessian_action_ys_result, result, gaussnewton=False, projections=False):
        self._quadratic_forms_impl(eval_points, ys, hessian_action_ys_result, result, gaussnewton, projections)
        #what about other normalizations?????
        # if self._project:
        #     print(self._eigendecomp_rank)
        #     result /= self._eigendecomp_rank
        #     hessian_action_ys_result[:] /= self._eigendecomp_rank
        # else:
        result /= self.dims #full space dimension or subspace dimension????

        hessian_action_ys_result[:] /= self.dims
        return result, hessian_action_ys_result


    @abstractmethod
    def _mults_impl(self, eval_point, vecs, result, gaussnewton):
        pass

    @abstractmethod
    def _quadratic_forms_impl(self, eval_points, ys, hessian_action_ys_result, result, gaussnewton):
        pass

