import numpy as np
from numba import guvectorize, float64

from .metric_base import _Metric
from algorithms.randomized_eigensolvers import randomized_double_pass_eigensolver,\
 randomized_double_pass_generalized_eigensolver, randomized_posterior_hess_eigendecomp, randomized_likelihood_hess_eigendecomp

from misc import *

@guvectorize([(float64[:], float64[:], float64[:],  float64[:, :], float64[:], float64[:])], '(dim), (dim), (r), (r, dim), (dim), ()',
        nopython=True, cache=True)
def low_rank_eigendecomposed_quadratic_forms(eval_point, y, d, Ut, hessian_action_diff, result):
    """
    Compute :math: `-0.5 diff.T U D U^T diff `, store in `result`. 
    Also store :math: U D U^T diff ` in `hessian_action_diff`.
    """
    diff = eval_point - y
    UdUtdiff = Ut.T @ ((d * Ut) @ diff)
    hessian_action_diff[:] += UdUtdiff
    result[0] += -0.5 * diff.T @ UdUtdiff



@guvectorize([(float64[:], float64[:], float64[:],  float64[:, :], float64[:],float64[:], float64[:])], '(dim), (dim), (r), (r, dim),(dim), (dim), ()',
        nopython=True, cache=True)
def low_rank_posterior_eigendecomposed_quadratic_forms(eval_point, y, d, Ut,sigma_inv, hessian_action_diff, result):
    """
    Compute :math: `-0.5 diff.T U D U^T diff `, store in `result`. 
    Also store :math: U D U^T diff ` in `hessian_action_diff`.
    """
    diff = eval_point - y
    sigma_invUdUtsigma_invdiff = ( Ut *sigma_inv).T @ ((Ut.T*d).T @ (diff*sigma_inv))
    hessian_action_diff[:] += sigma_invUdUtsigma_invdiff
    result[0] += -0.5 * diff.T @ sigma_invUdUtsigma_invdiff


# @guvectorize([(float64[:], float64[:], float64[:],  float64[:, :], float64[:],float64[:], float64[:])], '(dim), (dim), (r), (r, dim), (dim), (dim), ()',
#         nopython=True, cache=True)
def low_rank_posterior_eigendecomposed_quadratic_forms_cholesky(eval_point, ys, d, Ut,L, hessian_action_diff, result):
    """
    Compute :math: `-0.5 diff.T U D U^T diff `, store in `result`. 
    Also store :math: U D U^T diff ` in `hessian_action_diff`.
    """
    for i in range(ys.shape[0]):
        diff = eval_point - ys[i]
        sigma_inv_diff = (L@(L.T@diff))
        
        sigma_inv_UdUtsigma_inv_diff = L@ (L.T@  (Ut.T @ ((Ut.T*d).T @ sigma_inv_diff)))
        hessian_action_diff_val = sigma_inv_UdUtsigma_inv_diff + sigma_inv_diff
        hessian_action_diff[i] += hessian_action_diff_val
        result[i] += -0.5 * diff.T @ hessian_action_diff_val



@guvectorize([(float64[:], float64[:], float64[:, :], float64[:], float64[:] )], \
             '(dim), (dim), (dim, dim), (dim), ()',
        nopython=True, cache=True)
def matrix_quadratic_forms(eval_point, ys, matrix, hessian_action_diffs_result, result):
    """
    Compute :math: `-0.5 diff.T H diff `, store in `result`. 
    Also store :math: H diff ` in `hessian_action_diffs_result`.
    """
    diffs = eval_point - ys
    hessian_action_diffs = matrix @ diffs
    hessian_action_diffs_result[:] = hessian_action_diffs
    result[0] = -0.5* diffs @ hessian_action_diffs

# @guvectorize([(float64[:], float64[:], float64[:, :], float64[:])], \
#              '(dim), (dim), (dim, dim), (dim)',
#         nopython=True, cache=True)
# def matrix_mults(eval_point, ys, matrix, result):

#     diffs = eval_point - ys
#     result[:] = matrix @ diffs



class IdentityMetric(_Metric):
    """
    Define the Identity Metric: returns the vectors for mults and does the l2 norm for quadratic forms
    """
    def __init__(self, manifold, preconditioner=None, eigendecomp=False):
        #manifold will be the likelihood distribution, preconditioner will be the prior
        super().__init__(manifold, init_allocations_size=manifold._pdf.shape[0], eigendecomp=eigendecomp)

    def _mults_impl(self, eval_pt, vecs, result, *args):
        result[:] = vecs
    def _quadratic_forms_impl(self, eval_points, ys, hessian_action_ys_result, result, *args): 
        #split eval_points accross workers and then do the below!!!!

        #make this guvectorized
        result[:] = 0.
        for i in range(eval_points.shape[0]):
            p_diffs = eval_points[i] - ys
            hessian_action_ys_result[i] = p_diffs
            for j in range(ys.shape[0]):
                # result[j] += p_diffs[j].T @ p_diffs
                result[j] = np.sum(p_diffs[j]**2.)

class AverageEigendecompPosteriorHessianOperator():
    #average log posterior hessian action, from eigendecompositions of hte log posterior hessian ds, Uts, and the prior
    def __init__(self, ds, Uts, prior):
        self.ds = ds
        self.Uts = Uts
        self.prior = prior

    def mult(self, x):
        N = self.ds.shape[0]
        dim = self.Uts[0].shape[1]
        Mx_sum =  np.zeros(dim)
        ###localN########
        for i in range(N): #does this work for parallel/serial?????????????????????????
        ############code up##################
            Ut = self.Uts[i]
            d = self.ds[i]-1.
            ######communication between processors###########
            Mx_sum += self.prior._sigma_inv*x + self.prior._sigma_inv*( Ut.T @(d * (Ut@(self.prior._sigma_inv*x))))
            # print("hesssssss",np.diag(self.prior._sigma_inv) + np.diag(self.prior._sigma_inv)@( Ut.T @(np.diag(d) @ (Ut@(np.diag(self.prior._sigma_inv))))))
            
            # dfdfdfdfdf
        return Mx_sum/N




class HessianMetric(_Metric):
    """
    Define the metric M(p_i), with norm_squared for point p_i (p_hat)^T M(p_i) * (p_hat)
    and inner product for point p_i between two vectors phat_1 and phat_2 (phat_1)^T M(p_i) * (phat_2)
    Here the metric is different for each point, and defined by the Posterior Hessian at each point/the dimension of the space.
    """
    def __init__(self, manifold, project, common_projector = False, init_allocations_size=1, low_rank=None, oversampling=5):
        #manifold will be the likelihood distribution, preconditioner will be the prior
        super().__init__(manifold, project, common_projector, init_allocations_size=init_allocations_size, low_rank=low_rank, oversampling=oversampling)
        if project:
            # assert common_projector 
            self._manifold_hess_matrices = manifold.hess_neg_logpdf_matrices

            self._metrics = np.zeros((init_allocations_size, self._eigendecomp_rank, self._eigendecomp_rank))

        else:
            assert not common_projector 
    @property
    def matrices(self):
        if self._project:
            return self._metrics
        else:
            return self._metric_d_s, self._metric_Ut_s
    @matrices.setter
    def matrices(self, matrices):
        if self._project:
            self._hessians_setting_allocations_impl(matrices)
            self._metrics[:] = matrices
        else:
            #assign Ut and d
            self._metric_d_s, self._metric_Ut_s = matrices #matrices is Ut and d tuple
        self._matrix_calculated = True

    def _mults_impl(self, eval_pt, vecs, result, gaussnewton):
        """
        For each particle in the metric evaluation point, evaluate the hess action against 
        vecs
        """
        # if self.projections:
        #     result[:] = matrix @ vecs
        # else:
        #     result[:] = 


    def _hessians_setting_allocations_impl(self, matrices):
        if self._metrics.shape != matrices.shape:
            self._metrics = np.zeros((matrices.shape))

    def _quadratic_forms_impl(self, eval_points, ys, hessian_action_ys_result, result, gaussnewton, projections): #projections????
        if self._project:
            N = eval_points.shape[0]
            if not self._matrix_calculated:
                self._hessians_allocations_impl(eval_points)
                self._manifold_hess_matrices(eval_points, result=self._metrics, gaussnewton=gaussnewton) #fill in the metrics explicitly using low dimension hessians
                self.matrices = self._matrices 

            #Use self.metrics[i] to do the quadratic forms (matrix vecs essentially)
            if self._metrics.ndim is 2:
                print("metric ", self._metrics)
                for i in range(N):
                    matrix_quadratic_forms(eval_points.coeffs[i], ys.coeffs, self._metrics, hessian_action_ys_result[i], result[i])
            else:
                for i in range(N):
                    matrix_quadratic_forms(eval_points.coeffs[i], ys.coeffs, self._metrics[i], hessian_action_ys_result[i], result[i])
            self._matrix_calculated = False #reset the flag
        else:
            #If full space (no projection), then eigendecompse first, then use the eigendecomposition
            #to create a metric
            self._low_rank_eigendecomposition_allocations_impl(eval_points, self._eigendecomp_rank, self._oversampling)
            #Low-rank Eigendecompose the Hessians evaluated at the eval points and store in _d and _U
            randomized_posterior_hess_eigendecomp(self.manifold.prior, eval_points, self._random_projection,  self._eigendecomp_rank, self._d_s, self._Ut_s, self.manifold, gaussnewton, False)
            
            print("SVD HESSIAN", self._Ut_s[0].T @ (self._d_s[0] @ (self._Ut_s[0].T)))
            # print("FULL HESSIAN", self.manifold.hess_neg_logpdf_action(eval_point[0],))
            self.matrices = (np.abs(self._d_s), self._Ut_s)
            
            #Use self._d_s[i] and self._U_s[i] to do the quadratic forms

            for i, eval_point in enumerate(eval_points):
                # Notes for parallelism: 
                # looping through local eval_points, ys must be a copy of all the particles/points (on each rank), all other quantities are local
                low_rank_eigendecomposed_quadratic_forms(eval_point, ys, self._metric_d_s[i], self._metric_Ut_s[i], hessian_action_ys_result[i], result[i])
            if self.comm is not None: self.comm.Barrier()

    def _hessians_allocations_impl(self, eval_points):
        N = eval_points.shape[0]
        dims = eval_points.projected_dims
        if self._metrics.shape != (N, dims, dims):
            self._metrics = np.zeros((N, dims, dims))


class AveragedHessianMetric(HessianMetric):
    """
    Define the metric M(p_i), with norm_squared for point p_i (p_hat)^T M(p_i) * (p_hat)
    and inner product for point p_i between two vectors phat_1 and phat_2 (phat_1)^T M(p_i) * (phat_2)
    Here the metric is different for each point, and defined by the Posterior Hessian at each point/the dimension of the space.
    """
    def __init__(self, manifold, project, init_allocations_size=1, low_rank=None, oversampling=5):
        #manifold will be the posterior
        super().__init__(manifold, project, init_allocations_size=init_allocations_size, low_rank=low_rank, oversampling=oversampling)
        if project:
            #For building small matrices in r (low rank) dimensional subspace 
            self._manifold_hess_matrices = manifold.hess_neg_logpdf_matrices
            self._matrices = np.zeros((init_allocations_size, low_rank, low_rank))
        else:
            self._global_metric_d_s, self._global_metric_Ut_s = None, None
            # self._metrics = np.zeros((self._eigendecomp_rank, self._eigendecomp_rank))
    @property
    def matrices(self):
        if self._project:
            return self._matrices
        else:
            return self._d_s, self._Ut_s

    @matrices.setter
    def matrices(self, matrices):
        self._average_hessians_setting_allocations_impl(matrices)
        if self._project:
            self._hessians_setting_allocations_impl(matrices)
            self._matrices[:] = matrices
            if self.comm is None or self.comm.Get_size() == 1: #set all of the matrices to the mean on rank 0
                #All reduce mean! the matrices
                self._metrics = np.mean(matrices, axis=0)
            else:
                # print('Matrix before mean',matrices)
                matrices = np.mean(matrices, axis=0) #local means
                # print('Matrix after mean',matrices)
                self.comm.Allreduce(matrices, self._metrics)   #All reduce sum and divide by number of ranks to get the full average
                self._metrics /= self.comm.Get_size() 
            
        else:
            #all gather
            self._d_s, self._Ut_s = matrices
            metric_d_s, metric_Ut_s = matrices
            allgather(metric_d_s, self._global_metric_d_s ,self.manifold.prior.comm)
            allgather(metric_Ut_s, self._global_metric_Ut_s,self.manifold.prior.comm)
        
        #This flag is used by _quadratic_forms_impl to decide if one needs to calculate the hessian matrices
        self._matrix_calculated = True

    def _hessians_setting_allocations_impl(self, matrices):
        if self._metrics.shape != matrices[0].shape:
            self._metrics = np.zeros((matrices[0].shape))

    def _mults_impl(self, eval_pt, vecs, result, gaussnewton):
        """
        For each particle in the metric evaluation point, evaluate the hess action against 
        `its portion' of phat
        """
        pass
        # result[:] = matrix @ vecs

    def _average_hessians_setting_allocations_impl(self, matrices):
        if self._project:
            if self._matrices.shape != matrices.shape:
                self._matrices= np.zeros((matrices.shape))
        else:
            #allocate for self.
            r = self._eigendecomp_rank
            d = self.manifold.prior.dims
            N_local = matrices[0].shape[0]
            N = Allreduce_scalars(N_local, self.manifold.prior.comm)
            self._global_metric_d_s = np.zeros((N, r)) 
            self._global_metric_Ut_s = np.zeros((N, r, d))




    def _quadratic_forms_impl(self, eval_points, ys, hessian_action_ys_result, result, gaussnewton, projections): #projections????
        N = eval_points.shape[0]
        N_ys = ys.shape[0]
        if self._project:
            if not self._matrix_calculated:
                self._hessians_allocations_impl(eval_points)
                self._manifold_hess_matrices(eval_points, result=self._matrices, gaussnewton=gaussnewton) #fill in the metrics explicitly using low dimension hessians
                # print("get here matrices ", self._matrices)
                self.matrices = self._matrices 

            #Use self.metrics[i] to do the quadratic forms (matrix vecs essentially)
            if self._metrics.ndim is 2:
                # print("matrices", self.matrices)
                # print("metric ", np.linalg.det(self._metrics), self._metrics)
                for i in range(N):
                    matrix_quadratic_forms(eval_points.coeffs[i], ys.coeffs, self._metrics, hessian_action_ys_result[i], result[i])
                # print("result of quadratic", result)
            else:
                for i in range(N):
                    matrix_quadratic_forms(eval_points.coeffs[i], ys.coeffs, self._metrics[i], hessian_action_ys_result[i], result[i])
            self._matrix_calculated = False #reset the flag
        else:
            #If full space (no projection), then grab the metric_d_ds and metric_Ut_s from the metric which was set from stein.py...

            self._low_rank_eigendecomposition_allocations_impl(eval_points, self._eigendecomp_rank, self._oversampling)

            #_metric_d_s and _metric_Ut_s needs to be all of the d's and Ut's (all gather)

            AveH = AverageEigendecompPosteriorHessianOperator(self._global_metric_d_s, self._global_metric_Ut_s,self.manifold.prior)
            
            # metric_matrix = np.zeros((self.manifold.dims,self.manifold.dims))
            # for i in range(self.manifold.dims):
            #     idn = np.zeros(self.manifold.dims)
            #     idn[i] = 1.
            #     metric_matrix[i] = AveH.mult(idn)
            # print("metric_matrix", np.linalg.det(metric_matrix), metric_matrix)


            #Below should be correct as long as AveH is correct
            for k in range(N):
                # Notes for parallelism: 
                # looping through k = 1...N local eval_points, ys must be a copy of all the particles/points (on each rank), all other quantities are local
                diffs = eval_points[k] - ys
                for j in range(N_ys): #This loop will not be 1..N, it should be only looping through local particles
                    hessian_action_ys_result[k,j] = AveH.mult(diffs[j])
                    result[k,j] = -0.5*diffs[j].T @ (hessian_action_ys_result[k,j])
            # print("result of quadratic", result)



            #Low-rank Eigendecompose the Hessians evaluated at the eval points and store in _d and _U



            # randomized_likelihood_hess_eigendecomp(self.manifold.prior, eval_points, self._random_projection,  self._eigendecomp_rank, self._d_s, self._Ut_s, self.manifold, gaussnewton, False)
            # print('ds after',self._d_s)

            # self.matrices = (np.abs(self._d_s), self._Ut_s)
            # print('||ds|| after',np.linalg.norm(self._metric_d_s))
            # print('||U|| after',np.linalg.norm(self._metric_Ut_s))
            # #Use self._d_s[i] and self._U_s[i] to do the quadratic forms

            # if hasattr(self.manifold.prior,'L'):
            #     for i in range(eval_points.shape[0]):
            #         for j in range(self._metric_d_s.shape[0]):
            #             low_rank_posterior_eigendecomposed_quadratic_forms_cholesky(eval_points[i], ys, self._metric_d_s[j], self._metric_Ut_s[j], self.manifold.prior.L, hessian_action_ys_result[i], result[i])
            #     hessian_action_ys_result[:]*=  1./N_ys
            #     result[:] *= 1./N_ys
            # else:
            #     for i in range(eval_points.shape[0]):
            #         for j in range(self._metric_d_s.shape[0]):
            #             low_rank_posterior_eigendecomposed_quadratic_forms(eval_points[i], ys, self._metric_d_s[j], self._metric_Ut_s[j], self.manifold.prior.sigma_inv, hessian_action_ys_result[i], result[i])
            #     hessian_action_ys_result[:]*=  1./N_ys
            #     result[:] *= 1./N_ys
            # if self.comm is not None: self.comm.Barrier()


            # print('I hope this is nice',hessian_action_ys_result)
            # print('Dosds',result)