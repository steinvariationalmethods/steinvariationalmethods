import numpy as np

from misc import *


class splitspace_vectors:
    r"""
    Define a vector(s) that have a representation in terms of a coefficients in a subspace basis and a orthogonal complement (perp) 
    x = U (U, x) + x^{\perp}
    
    In code, this is done via x_proj = U  @ (Ut @ (R @ x)), where (Ut @ (R @ x)) is stored and x^{\perp} = x - U  @ (Ut  @ (R @ x))
    for xs, since xs is row major,    xs_proj = ((R@xs) @ U) @ Ut , xs_perp = xs - ((R@xs) @ U) @ Ut, where ((R@xs) @ U) are the coeffs
    """
    def __init__(self, xs=None, projectors=None,prior=None):
        #xs are all the points
        #projectors is either one projector or xs.shape[0] projectors, each (r, xs.shape[1])
        if prior is None:
            sigma_inv = 1.
        else:
            sigma_inv = prior._sigma_inv
        if xs is not None and projectors is not None:
            N = xs.shape[0]
            self._num_vecs = N

            if projectors.ndim == 2:    
                self._projected_dims, self._dims = projectors.shape #we're using a common projector for all particles 
            else:
                assert(projectors.ndim == 3)
                num_projectors, self._projected_dims, self._dims = projectors.shape   
                assert(num_projectors == N) # num projectors should be same as number of vectors
            assert(self._dims == xs.shape[1])

            self._projectors = projectors #referencing the projector from the outside, which is passed in

            self._coeffs = np.zeros((N, self.projected_dims)) 
            self._perp  = np.zeros((N, self.dims))
            self._proj = np.zeros((N, self.dims))
            self._full = np.zeros((N, self.dims))

            if self._projectors.ndim == 3:
                for i in range(N):
                    self._coeffs[i] = ((sigma_inv*xs[i] )@ self._projectors[i].T) 
                    self._proj[i] = self._coeffs[i].T @ self._projectors[i]
                    self._perp[i] = xs[i] - self._proj[i]
            else:
                for i in range(N):
                    self._coeffs[i] = ((sigma_inv*xs[i]) @ self._projectors.T) 
                    self._proj[i] = self._coeffs[i].T @ self._projectors
                    self._perp[i] = xs[i] - self._proj[i]

            self._shape = (self._num_vecs, self._dims)
        else:
            assert(xs is None and projectors is None)
            self._coeffs = None
            self._perp  = None
            self._proj = None
            self._full = None
            self._projectors = None
            self._projected_dims = None
            self._shape = None
            self._dims = None
            self._num_vecs = None
            
    def __iter__(self):
        return iter(self._full)
        
    def __iadd__(self, other):
        self.coeffs += other
        return self

    # def __setitem__(self,key,item):
    #     if projectors is None:

    def __getitem__(self,key):
        return self.coeffs[key]


    @property
    def shape(self):
        return self._shape

    @property
    def projectors(self):
        return self._projectors
    
    @projectors.setter
    def projectors(self, projectors):
        self._projectors = projectors

    @property
    def projected_dims(self):
        return self._projected_dims
    
    @property
    def coeffs(self):
        return self._coeffs

    @coeffs.setter
    def coeffs(self, coeffs):
        self._coeffs[:] = coeffs
        if self._projectors.ndim == 3:
            for i, coeff in enumerate(self._coeffs):
                self._proj[i] = coeff.T @ self.projectors[i]
        else:
            for i, coeff in enumerate(self._coeffs):
                self._proj[i] = coeff.T @ self.projectors
    @property
    def perp(self):
        return self._perp
    
    @property
    def proj(self):
        return self._proj
    
    @property
    def full(self):
        self._full = self._proj + self._perp
        return self._full

    @property
    def dims(self):
        return self._dims
    
    @property
    def num_vecs(self):
        return self._num_vecs
    
