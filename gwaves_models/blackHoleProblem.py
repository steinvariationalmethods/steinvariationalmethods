import autograd.numpy as np
from autograd import grad

class BlackHoleProblem:

	# note: non-precessing, single-detector with TaylorF2 (up to 2PN)
	def __init__(self, freq_array=None):
		self.param_list = ['m_1','m_2','chi_1','chi_2','t_c','phi_c','D_eff']
		if freq_array is None:
			f = open('bilby_gw_noise_curves_aLIGO_ZERO_DET_high_P_psd.txt')
			freq_file, psd = [],[]
			for line in f:
				freq_file.append(float(line.split()[0]))
				psd.append(float(line.split()[1]))
			self.freq_array = np.array(freq_file)
			self.psd = np.array(psd)
		else:
			self.freq_array = freq_array

		self.gll =  grad(self.log_likelihood)

	# returns h(f)
	def TF2(self, param_vals):
		param_dict = {}
		for i,key in enumerate(self.param_list):
			param_dict[key] = param_vals[i]

		M = param_dict['m_1'] + param_dict['m_2']
		eta = param_dict['m_1'] * param_dict['m_2'] / (M**2)
		Mc = eta**(3./5) * M
		sigma = 79./8 * eta * param_dict['chi_1'] * param_dict['chi_2']
		beta = 1./12 * (param_dict['chi_1']*(113*(param_dict['m_1']/M)**2 + 75*eta)\
				+ param_dict['chi_2']*(113*(param_dict['m_2']/M)**2 + 75*eta))

		A = Mc**(5./6) / param_dict['D_eff'] * 1e-20

		phi = 2*np.pi*self.freq_array * param_dict['t_c'] - param_dict['phi_c'] - np.pi/4\
				+ 3./128*(np.pi * Mc * self.freq_array)**(-5./3)\
				*(1 + 20./9*(743./336 + 11./4*eta)*(np.pi*M*self.freq_array)**(2./3)\
				- 4*(4*np.pi - beta)*(np.pi*M*self.freq_array) +\
				10*(3058673./1016064 + 5429./1008*eta + 617./144*eta**2 - sigma)*(np.pi*M*self.freq_array)**(4./3))

		h = A * self.freq_array**(-7./6) * np.cos(phi)
		return h

	def set_data(self, injection_params):
		self.data = self.TF2(injection_params)

	def inner_prod(self, h1, h2):
		weighted = h1 * h2 / self.psd
		return np.sum(weighted)

	def log_likelihood(self, param_vals):
		fun_val = self.TF2(param_vals)
		diff = fun_val - self.data
		return 0.5 * self.inner_prod(diff, diff)

	def grad_log_likelihood(self, param_vals):
		return self.gll(param_vals)
