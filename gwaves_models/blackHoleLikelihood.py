import numpy as np
import sys
sys.path.append('../')
from distributions.distribution_base import _Distribution
from blackHoleProblem import BlackHoleProblem


class taylorF2_Likelihood(_Distribution):

	def __init__(self, dims=7, init_allocations_size=1, comm=None):
		self.problem = BlackHoleProblem()
		super().__init__(dims, init_allocations_size, comm)

	def _pdf_impl(self, param_vals, result):
		out = []
		for theta in thetas:
			neg_loglike = self.problem.log_likelihood(theta)
			out.append(np.exp(-1 * neg_loglike))
		result[:] = out

	def _neg_logpdf_impl(self, thetas, result):
		out = []
		for theta in thetas:
			out.append(self.problem.log_likelihood(theta))
		result[:] = out

	def _grad_neg_logpdf_impl(self, thetas, neg_logpdf_result, grad_neg_logpdf_result):
		outputs, gradients = [],[]
		for theta in thetas:
			neg_logpdf = self.problem.log_likelihood(theta)
			grad_neg_logpdf = self.problem.grad_log_likelihood(theta)
			outputs.append(neg_logpdf)
			gradients.append(grad_neg_logpdf)

		neg_logpdf_result[:] = outputs
		grad_neg_logpdf_result[:] = gradients