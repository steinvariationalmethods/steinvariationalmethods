from distributions import Normal, Posterior, Laplace1D, MixtureDistribution
from harmonicoscillator import HarmOscDistr
import numpy as np

def pad_to_8(x, y, z):
    arr = np.zeros((8,))
    arr[0] = x
    arr[1] = y
    arr[2] = z
    return arr

MixtureWeights = [0.10091359135086529, 0.1069604021226033, 0.0955124026241248, 0.09857087439571578, 0.0929232421573577, 0.09578746387184309, 0.10897526401252772, 0.10023752114111818, 0.10367013314080166, 0.09644910518304235]
Covariances = [ np.array([[ 1.35857259, -0.43945712], \
                         [-0.43945712,  1.447238  ]]), 
                np.array([[ 0.10587465, -0.09119594], \
                         [-0.09119594,  0.6817462 ]]),
                np.array([[ 0.7735977 , -0.4268192 ], \
                         [-0.4268192 ,  0.84236674]]), 
                np.array([[ 2.34197806, -0.46190396], \
                         [-0.46190396,  0.5688675 ]]), 
                np.array([[1.28597596, 0.17113349], \
                         [0.17113349, 0.27561947]]), 
                np.array([[ 0.93152391, -0.1875246 ], \
                         [-0.1875246 ,  0.56002465]]), 
                np.array([[ 2.61580311, -0.28689598], \
                         [-0.28689598,  1.53408092]]),
                np.array([[ 1.44798589, -0.44041874], \
                         [-0.44041874,  1.23330637]]),  
                np.array([[ 1.64194141, -0.59515995], \
                         [-0.59515995,  0.51891557]]),
                np.array([[ 0.51208074, -0.32051853], \
                         [-0.32051853,  0.5980881 ]])]

Means = [ np.array([3.58638537, 6.34856182]), \
          np.array([3.06402991, 4.25320064]), \
          np.array([ 0.67770322, -2.75157609]), \
          np.array([-6.2517193 , -1.42637984]), \
          np.array([-0.80161989,  4.84915916]), \
          np.array([-2.17331107, -0.59097905]), \
          np.array([0.71997479, 2.52173257]), \
          np.array([-5.3174366, -0.4448168]), \
          np.array([6.82213674, 4.4171975 ]), \
          np.array([-7.02970499, -5.30378666])]
GaussiansList = [Normal(2, mu=mean/1.5, sigma=cov) for mean, cov in zip(Means, Covariances)]

all_dists = {
        'standard_normal_1d': Posterior(
            prior=Normal(1, mu=1., sigma=1.),
            likelihood = Normal(1, mu=-1., sigma=1.)),
        'normal_prior_oscillator_like_1d': Posterior(
            prior = Normal(1, mu=0., sigma=0.4),
            likelihood = HarmOscDistr(1, degree=10)),
        'normal_prior_oscillator_like_2d': Posterior(
            prior = Normal(2, mu=0., sigma=0.6),
            likelihood = HarmOscDistr(2, degree=10)),
        'normal_prior_oscillator_like_8d': Posterior(
            prior = Normal(8, mu=0., sigma=0.9),
            likelihood = HarmOscDistr(8, degree=10)),

        'laplace_prior_oscillator_like_1d': Posterior(
            prior = Laplace1D(mu=0., b=0.6),
            likelihood=HarmOscDistr(1, degree=10)),

        'mixture_prior_oscillator_like_1d': Posterior(
            prior = MixtureDistribution([Normal(1, mu=-2.5, sigma=0.5),
                Normal(1, mu=2.5, sigma=0.5)], [0.5, 0.5]),
            likelihood = HarmOscDistr(1, degree=10)),
        'mixture_prior_oscillator_like_2d': Posterior(
            prior = MixtureDistribution([
                Normal(2, mu=np.array([-2.5, -2.5]), sigma=0.8),
                Normal(2, mu=np.array([-2.5, 2.5]), sigma=0.8),
                Normal(2, mu=np.array([2.5, -2.5]), sigma=0.8),
                Normal(2, mu=np.array([2.5, 2.5]), sigma=0.8)],
                [0.25,0.25,0.25,0.25]),
            likelihood = HarmOscDistr(2, degree=10)),
        'mixture_prior_oscillator_like_8d': Posterior(
            prior = MixtureDistribution([
                Normal(8, mu=pad_to_8(x, y, z), sigma=0.95) for x in (-2.5, 2.5)
                for y in (-2.5, 2.5) for z in (-2.5, 2.5)],
                [1./8 for i in range(8)]),
            likelihood = HarmOscDistr(8, degree=10)),
        'normal_prior_gaussian_mixture_like_2d': Posterior(
            prior = Normal(2, mu=0.0, sigma=1.0),
            likelihood = MixtureDistribution(GaussiansList, MixtureWeights)
            )
}
