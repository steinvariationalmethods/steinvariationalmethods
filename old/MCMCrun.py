import numpy as np

from distributions import *
import prngs.parallel as random
from algorithms import *
from mcmc import *
from scipy.sparse import diags
from mpi4py import MPI 
comm = MPI.COMM_WORLD
rank = comm.Get_rank()


stochastic_newton_params = {
        'burn_in': 100,
        'nsamples': 100000, #100k samples
        'rank_reduction': 25,  #how much of the full rank do we want, for the neural network, probably rank 100-200? but this fraction (10 => 1/10th)
        'oversample': 1,
        'checkpoint_iter':100 #save mu/cov every 1000 MCMC samples
}


####################################################################################################################
def DiscreteLaplace(n_dim):
    diag = np.zeros((n_dim));
    lower = np.zeros((n_dim-1));

    h = 1/n_dim
    for i in range(n_dim):
        diag[i] = np.sqrt((i+2)/(i+1))
    for i in range(n_dim-1):
        lower[i] = - np.sqrt((i+1)/(i+2));
    diag /= h 
    lower /= h
    return diag,lower


n_dim = 128
n_mix = 20

diag,lower = DiscreteLaplace(n_dim)
mu = np.zeros(n_dim)
prior = LaplaceOperatorGaussian(mu, lower, diag,comm=comm)


likelihood = SinLinearLikelihood(prior,n_mix, n_obs=16, noise_sigma=1., comm=comm)
posterior = Posterior(prior,likelihood)

stochastic_newton_mcmc(prior, posterior, "LaplaceMCMC", return_chain=False, params=stochastic_newton_params)

##### CHECK THAT IT IS SAVING TO THE FOLDER, the means and covariances!!!