# from pairwise import Pairwvi pairiseDifferences
import numpy as np
from mpi4py import MPI
from misc import *
import prngs.parallel as random
from distributions import Gaussian, GaussianMixture
from metrics import HessianMetric
from kernels import GaussianKernel
comm = MPI.COMM_WORLD

npoints = 8
n_dim = 3

rank = comm.Get_rank()

print("Rank reporting:", rank)



def init_array(shape, comm): 
    if comm.Get_size() == 1:
        return np.zeros(shape=shape, dtype=np.float64)
    else:
        return np_float64_shared_array(shape, comm)

def assign_slice(N, comm):
    #assign numpy slicing for memory arrays
    _start_idx, _end_idx = assign_indices(N, comm)
    return np.s_[ _start_idx : _end_idx]

def random_mus_and_sigmas(n_mix,n_dim): #just testing random gaussians right now
    mus = random.uniform(-1.,1., size=(n_mix,n_dim))#np.random.normal(size=(n_mix,n_dim))
    sigma_invs = 3.*np.ones((n_mix,n_dim))#np.abs(np.random.normal(size=(n_mix,n_dim))) 
    return mus, sigma_invs

def random_mu_and_sigma(n_dim): #just testing random gaussians right now
    mu = random.uniform(-1.,1.,size=n_dim)#np.random.normal(size=(n_mix,n_dim))
    sigma_inv = 3.*np.ones(n_dim)#np.abs(np.random.normal(size=(n_mix,n_dim))) 
    return mu, sigma_inv

print(comm.Get_rank())
# # Points are stored in rows of an array
prior = Gaussian(*random_mu_and_sigma(n_dim), init_allocations_size=npoints, comm=comm)

start = MPI.Wtime()
xs = prior.sample(npoints)

# n_mix = 10
# posterior = GaussianMixture(*random_mus_and_sigmas(n_mix,n_dim), comm=comm)
print("Sampling Time:",MPI.Wtime()-start)
    
xs_non_shared = xs.copy()
print(comm.Get_rank())
print("\n",xs_non_shared,"\n")
# # print(comm.Get_rank(), ":", pts[0], pts[-1])

# # Differences are also stored in rows; number of differences
# # is n(n-1)//2 (for two indices i, j only diff(i, j) is stored,
# # not diff(j, i)).
# # Construct the differences class.
# # diffs = PairwiseDifferences(dim, npts=npoints)
# # print(diffs.shape)

# #since #pts changes, it reallocates the _diffs buffer here before comuting the differences
# # diffs.pts = pts
# # start = time.time()
# # diffs.compute_diffs()
# # print("TIme:",time.time()-start)
# # print(diffs.shape)
# # print(diffs.buffer)




# start_idx, end_idx = assign_indices(npoints, comm)
# diffs = prior.init_array(shape=(npoints, npoints, n_dim))
# # diffs = np.zeros((npoints, npoints, n_dim)) #my differences

# # start = time.time()
# # for i in range(start_idx, end_idx):
# # 	pt_i = pts[i, :]
# # 	for j in range(npoints):
# # 		diffs[i, j, :] = pt_i - pts[j, :]
# # print("Diffs time:",time.time()-start)
# # # print(comm.Get_rank(),":", diffs)
# # #now copy pts onto each processor and do the same thing:
# # pts = pts.copy()

# # diffs = np.zeros((npoints * (end_idx-start_idx), npoints, n_dim)) #my differences

# # start = time.time()
# # for i in range(start_idx, end_idx):
# # 	pt_i = xs[i, :]
# # 	for j in range(npoints):
# # 	   pt_i - xs[j, :]
# # print("Diffs copies time:",time.time()-start)

# print("calculating logpdf gradient")
# start = MPI.Wtime()
# gradient_neg_log_pdf_at_xs = posterior.grad_neg_logpdf(xs)
# print("Done calculating logpdf gradient. time:",MPI.Wtime()-start)

# print("calculating kernel gradient")
# metric = HessianMetric(posterior, preconditioner=prior)#, eigendecomp=10)
# kernel = GaussianKernel(metric, init_allocations_size=npoints)
# start = MPI.Wtime()
# grad_K = kernel.calculate_grad_gram_matrix(xs, xs)#, eigendecomp=10) #N^2 operation   
# print("Done calculating kernel gradient. time:",MPI.Wtime()-start)
