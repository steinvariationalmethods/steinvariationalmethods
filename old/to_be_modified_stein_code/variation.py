from __future__ import absolute_import, division, print_function

import numpy as np
from abc import ABC, abstractmethod

#from ..algorithms.lowRankOperator import LowRankOperator

import pickle

plot_valid = True
try:
    import matplotlib.pyplot as plt
except:
    plot_valid = False
    print("can not import pyplot")

if plot_valid:
    import os
    if not os.path.isdir("figure"):
        os.mkdir("figure")



class Variation:
    # compute the variation (gradient, Hessian) of the negative log likelihood function
    def __init__(self, posterior, particles, options):
        # if add particles, then use generalized eigenvalue decomposition
        # if options["add_rule"] > 0:
        #     options["low_rank_Hessian_misfit"] = 2

        self.posterior = posterior
        self.particles = particles  # particle class
        self.options = options
        # self.low_rank_Hessian_misfit = options["low_rank_Hessian_misfit"]
        # self.low_rank_Hessian_misfit_hold = options["low_rank_Hessian_misfit"]
        # self.rank_Hessian_misfit = options["rank_Hessian_misfit"]
        # self.gauss_newton_approx = options["gauss_newton_approx"]
        # self.gauss_newton_approx_hold = options["gauss_newton_approx"]
        # self.max_iter_gauss_newton_approx = options["max_iter_gauss_newton_approx"]

        # if options["add_number"] > 0:
        #     self.low_rank_Hessian_misfit = 2  # use generalized eigendecomposition if new particles are to be added
        #     self.low_rank_Hessian_misfit_hold = 2

        # if self.low_rank_Hessian_misfit:
        #     # generate Gaussian random vectors to be used for randomized SVD
        #     randomGen = Random(myid=0, nproc=dl.MPI.size(model.problem.Vh[PARAMETER].mesh().mpi_comm()))
        #     m = model.generate_vector(PARAMETER)
        #     Omega = MultiVector(m, self.rank_Hessian_misfit + 5)
        #     for i in range(self.rank_Hessian_misfit + 5):
        #         randomGen.normal(1., Omega[i])
        #     self.Omega = Omega
        #     self.d_average = None
        #     self.U_average = None

        # self.d = [None] * particle.number_particles_all
        # self.U = [None] * particle.number_particles_all
        # self.x_all = [None] * particle.number_particles_all
        # for n in range(particle.number_particles_all):
        #     self.x_all[n] = [model.generate_vector(STATE), model.generate_vector(PARAMETER),
        #                      model.generate_vector(ADJOINT)]

    def update(self, particles):
        pass
        # self.particles = particles

        # # if particle.number_particles_all > particle.number_particles_all_old:
        # #     self.d = [None] * particle.number_particles_all
        # #     self.U = [None] * particle.number_particles_all
        # #     self.x_all = [None] * particle.number_particles_all
        # #     for n in range(particle.number_particles_all):
        # #         self.x_all[n] = [self.model.generate_vector(STATE), self.model.generate_vector(PARAMETER),
        # #                          self.model.generate_vector(ADJOINT)]

        # # solve the forward and adjoint problems at all the particles
        # for n in range(particle.number_particles_all):
        #     # solve the state and adjoint problems
        #     self.x_all[n][PARAMETER].zero()
        #     self.x_all[n][PARAMETER].axpy(1.0, particle.particles[n])
        #     self.model.solveFwd(self.x_all[n][STATE], self.x_all[n])
        #     self.model.solveAdj(self.x_all[n][ADJOINT], self.x_all[n])

        # if self.low_rank_Hessian_misfit:
        #     # compute the low rank decomposition only for the particles used in constructing the transport map
        #     for n in range(particle.number_particles_all):
        #         # low rank decomposition of hessian misfit
        #         # self.model.setPointForHessianEvaluations(self.x_all[n], self.gauss_newton_approx)
        #         # hessian_misfit = ReducedHessian(self.model, misfit_only=True)
        #         # if self.low_rank_Hessian_misfit == 1:
        #         #     self.d[n], self.U[n] = doublePass(hessian_misfit, self.Omega, self.rank_Hessian_misfit, s=1)
        #         # elif self.low_rank_Hessian_misfit == 2:
        #         #     self.d[n], self.U[n] = doublePassG(hessian_misfit, self.model.prior.R, self.model.prior.Rsolver,
        #         #                                        self.Omega, self.rank_Hessian_misfit, s=1)
        #         # else:
        #         #     raise NotImplementedError("please choose between 0, 1, 2 for low_rank_Hessian_misfit")

        # if self.options['low_rank_Hessian_misfit']:
        #     hessian_misfit = HessianAverage(self.model, particle, self.d, self.U, self.low_rank_Hessian_misfit)
        #     if self.low_rank_Hessian_misfit == 1:
        #         self.d_average, self.U_average = doublePass(hessian_misfit, self.Omega, self.rank_Hessian_misfit, s=1)
        #     elif self.low_rank_Hessian_misfit == 2:
        #         self.d_average, self.U_average = doublePassG(hessian_misfit, self.model.prior.R, self.model.prior.Rsolver,
        #                                                      self.Omega, self.rank_Hessian_misfit, s=1)
        #     else:
        #         raise NotImplementedError("please choose between 0, 1, 2 for low_rank_Hessian_misfit")

    def gradient(self, n):
        return self.posterior.likelihood.gradlogpdf(self.particles[n])

    def hessian(self, phat, n):  # misfit Hessian action at particle pn in direction phat , H * phat = pout
        self.posterior.likelihood.hessian_eval_point = self.particles[n] 
        return self.posterior.likelihood.hess_logpdf_action(phat) #, self.gauss_newton_approx)

    # def save_eigenvalue(self, save_number=1, it=0):
    #     for n in range(save_number):
    #         filename = "data/eigenvalue_" + str(n) + '_iteration_' + str(it) + '.p'
    #         pickle.dump(self.d, open(filename, 'wb'))

    # def plot_eigenvalue(self, save_number=1, it=0):

    #     if plot_valid and self.low_rank_Hessian_misfit:

    #         for n in range(save_number):
    #             if n < self.particle.number_particles_all:
    #                 d = self.d[n]

    #                 fig = plt.figure()
    #                 if np.all(d > 0):
    #                     plt.semilogy(d, 'r.')
    #                 else:
    #                     indexplus = np.where(d > 0)[0]
    #                     # print indexplus
    #                     dplus, = plt.semilogy(indexplus, d[indexplus], 'ro')
    #                     indexminus = np.where(d < 0)[0]
    #                     # print indexminus
    #                     dminus, = plt.semilogy(indexminus, -d[indexminus], 'k*')

    #                     plt.legend([dplus, dminus], ["positive", "negative"])

    #                 plt.xlabel("n ", fontsize=12)
    #                 plt.ylabel("|$\lambda_n$|", fontsize=12)

    #                 plt.tick_params(axis='both', which='major', labelsize=12)
    #                 plt.tick_params(axis='both', which='minor', labelsize=12)

    #                 filename = "figure/eigenvalue_" + str(n) + '_iteration_' + str(it) + '.pdf'
    #                 fig.savefig(filename, format='pdf')
    #                 filename = "figure/eigenvalue_" + str(n) + '_iteration_' + str(it) + '.eps'
    #                 fig.savefig(filename, format='eps')

    #                 plt.close()
