# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

from __future__ import absolute_import, division, print_function

import numpy as np
import scipy.sparse as spspa

from abc import ABC, abstractmethod

#class PreconditionerCoupled:
#    # define the preconditioner for the coupled Newton system
#    def __init__(self, model, particles):
#        self.posterior = model
#        particles = particles  # num of particles
#        self.gls = GlobalLocalSwitch(self.posterior, particles)
#
#    def update(self, particles):
#        particles = particles
#        self.gls = GlobalLocalSwitch(self.posterior, particles)
#
#    def solve(self, phat, prhs):
#
#        prhs_subs = [self.posterior.generate_vector(PARAMETER) for n in range(particles.num_particles_all)]
#        phat_subs = [self.posterior.generate_vector(PARAMETER) for n in range(particles.num_particles_all)]
#
#        self.gls.global2local(prhs, prhs_subs)
#
#        for m in range(particles.num_particles_all):
#            self.posterior.prior.Rsolver.solve(phat_subs[m], prhs_subs[m])
#
#        self.gls.local2global(phat_subs, phat)

class IdentityPreconditioner(spspa.linalg.LinearOperator):
    def __init__(self, posterior):
        self.shape = None #define the shape when setting the hessian evaluation point

    def _matvec(self, phat):
        return phat

class DKLHessianOperator(ABC, spspa.linalg.LinearOperator):

    @abstractmethod
    def __init__(self, *args, **kwargs):
        pass
        
    @property
    def dim(self):
        return self._dim

    @property
    def num_particles(self):
        return self._num_particles

    @property
    def hess_eval_pt(self):
        return self._hess_eval_pt
    
    @hess_eval_pt.setter
    def hess_eval_pt(self, particles):
        self._hess_eval_pt = particles
        self._num_particles, self._dim = particles.shape
        d = self.dim * self.num_particles
        self.shape =  d, d
        self._hess_eval_pt_set = True

    @abstractmethod
    def _matvec(self, phat):
        pass

class IdentityDKLHessianOperator(DKLHessianOperator):
    r"""
    Identity Hessian (For use for gradient descent)
    """
    def __init__(self, posterior, options):
        self._hess_eval_pt = None
        self._hess_eval_pt_set = True 
        self._dim = None
        self._num_particles = None
        self.shape = None
     
    def _matvec(self, phat):
        return phat

class CoupledDKLHessianOperator(DKLHessianOperator):
    # construct Hessian action in a vector of length parameter_dimension * num_particles
    def __init__(self, posterior, options):
        self.options = options
        
        self._posterior = posterior
        self._hess_eval_pt = None
        self._hess_eval_pt_set = False
        self._dim = None
        self._num_particles = None
        self.shape = None #this has to not be private to fit interface of LinearOperator
        self.ncalls = 0
    
    def _matvec(self, phat):
        if not self._hess_eval_pt_set:
            raise Exception("Hessian evaluation point has not been set")

#        pout_subs = [self._posterior.generate_vector(PARAMETER) for n in range(particles.num_particles_all)]
#        phat_subs = [self._posterior.generate_vector(PARAMETER) for n in range(particles.num_particles_all)]
        pout = [np.zeros(self.dim) for i in range(self.num_particles)]
        phat_reshaped = phat.reshape(self.hess_eval_pt.shape)        #from flat vector to a matrix
#        print("phat_reshaped",phat_reshaped)
        for i, (kernel_evals_i, kernel_gradients_i, phat_reshaped_i, pout_i) in enumerate(zip(self.all_kernel_evals, self.all_kernel_gradients, phat_reshaped, pout)):
            
#            print("phat_reshaped_i",phat_reshaped_i)

            #FIXME
            if self.options['type_Hessian'] is "diagonal":
#                R_coeff = 0.  # take the covariance/regularization part out
                # loop over each particles to compute the expectation
                for particle_j, kernel_eval_ij, kernel_gradient_ij in zip(self.hess_eval_pt, kernel_evals_i, kernel_gradients_i):
                    self._posterior.hess_eval_pt = particle_j
                    hessian_phat = -self._posterior.hess_logpdf_action(phat_reshaped_i)
                    pout[i] += kernel_eval_ij ** 2*hessian_phat
                    pout[i] += kernel_gradient_ij.dot(phat_reshaped_i)*kernel_gradient_ij
#                    R_coeff += kernel_value_m[ie] ** 2
#
#                self._posterior.prior.R.mult(phat_subs[m], R_phat)
#                pout_subs[m].axpy(R_coeff, R_phat)
            
            #To Keyi: Can you check that this is correct? 
            elif self.options['type_Hessian'] is "lumped":
#                R_coeff = 0.  # take the covariance/regularization part out
                # loop over each particles to compute the expectation
#                x1 = 0.
#                x2 = 0.
                for particle_j, kernel_eval_ij, kernel_gradient_ij, kernel_evals_j, kernel_gradients_j  in zip(self.hess_eval_pt, kernel_evals_i, kernel_gradients_i, self.all_kernel_evals, self.all_kernel_gradients):
                    self._posterior.hess_eval_pt = particle_j
#                    print("self.options['gauss_newton_approx']",self.options['gauss_newton_approx'])
#                    print(phat_reshaped_i.shape,phat_reshaped_i)
                    hessian_phat_i = -self._posterior.hess_logpdf_action(phat_reshaped_i, gaussnewton=self.options['gauss_newton_approx'])
#                    print(hessian_phat_i)
                    # # lumped, summing over columns
                    # print("kernel_value_m[ie] shape",kernel_value_m[ie].shape,"hessian_phat",hessian_phat.shape,"sum",(np.sum(self.all_kernel_evals[ie])* kernel_value_m[ie] * hessian_phat).shape)
                    pout[i] += np.sum(kernel_evals_j)* kernel_eval_ij * hessian_phat_i #H_j * phat_i
#                    x1 += np.sum(kernel_evals_j)* kernel_eval_ij * hessian_phat_i
                    pout[i] += kernel_gradient_ij.dot(phat_reshaped_i)* np.sum(-kernel_gradients_j, axis=0 )
#                    x2 += kernel_gradient_ij.dot(phat_reshaped_i)* np.sum(-kernel_gradients_j, axis=0 )
#                print("x1,x2", x1,x2)
#                    R_coeff += self.kernel.value_sum[ie] * kernel_value_m[ie]
#
#                    if m > len(particles):
#                        pout_subs[m].axpy(kernel_value_m[ie] ** 2, hessian_phat)
#                        pout_subs[m].axpy(kernel_gradient_m[ie].inner(phat_subs[m]), kernel_gradient_m[ie])
##                        R_coeff += kernel_value_m[ie] ** 2
#                print("pout_i",pout_i)
#                print("pout_i_array",pout[i])
                # # also use the particles to compute the expectation
                # if m > len(particles):
                #     hessian_phat = self._posterior.generate_vector(PARAMETER)
                #     particles.num_particles_all.hessian(phat_subs[m], hessian_phat, m)
                #     pout_subs[m].axpy(self.kernel.value_sum[m] * kernel_value_m[m], hessian_phat)
                #     pout_subs[m].axpy(kernel_gradient_m[m].inner(phat_subs[m]), self.kernel.gradient_sum[m])
                #     R_coeff += self.kernel.value_sum[m] * kernel_value_m[m]
                #     pout_subs[m].axpy(kernel_value_m[m] ** 2, hessian_phat)
                #     pout_subs[m].axpy(kernel_gradient_m[m].inner(phat_subs[m]), kernel_gradient_m[m])
                #     R_coeff += kernel_value_m[m] ** 2

#                R_phat = self._posterior.generate_vector(PARAMETER)
#                self._posterior.prior.R.mult(phat_subs[m], R_phat)
#                pout_subs[m].axpy(R_coeff, R_phat)

            #FIXME
            elif self.options['type_Hessian'] is "full":  # assemble the full coupled Hessian system
                # loop for each Hessian H_mn for n = 1, ..., num_particles
                for kernel_evals_j, kernel_gradients_j, phat_reshaped_j in zip(self.all_kernel_evals, self.all_kernel_gradients):
#
#                    R_coeff = 0.  # take the covariance/regularization part out
                    # loop over each particles used for map construction to compute the expectation
                    for k in range(self.num_particles):
                        self._posterior.hess_eval_pt = particles[k]
                        hessian_phat = -self._posterior.hess_logpdf_action(phat_reshaped_j)

                        pout[i] += kernel_evals_i[k] ** kernel_evals_j[k]*hessian_phat
                        pout[i] += kernel_gradients_i[k].inner(phat_reshaped_j)* kernel_gradients_j[k]
#                        R_coeff += kernel_value_m[ie] * kernel_value_n[ie]

                    # # also use the particles to compute the expectation
                    # if m > len(particles):
                    #     hessian_phat = self._posterior.generate_vector(PARAMETER)
                    #     particles.num_particles_all.hessian(phat_subs[n], hessian_phat, m)
                    #
                    #     pout_subs[m].axpy(kernel_value_m[m] ** kernel_value_n[m], hessian_phat)
                    #     pout_subs[m].axpy(kernel_gradient_m[m].inner(phat_subs[n]), kernel_gradient_n[m])
                    #     R_coeff += kernel_value_m[m] * kernel_value_n[m]

#                    R_phat = self._posterior.generate_vector(PARAMETER)
#                    self._posterior.prior.R.mult(phat_subs[n], R_phat)
#                    pout_subs[m].axpy(R_coeff, R_phat)
#
#                # deal with block H_mm for the particles not used in the construction map
#                if m > len(particles):
#                    R_coeff = 0.  # take the covariance/regularization part out
#                    # loop over each particles used for map construction to compute the expectation
#                    for ie in range(self.num_particles):
#                        hessian_phat = self._posterior.generate_vector(PARAMETER)
#                        particles.num_particles_all.hessian(phat_subs[m], hessian_phat, ie)
#
#                        pout_subs[m].axpy(kernel_value_m[ie] ** kernel_value_m[ie], hessian_phat)
#                        pout_subs[m].axpy(kernel_gradient_m[ie].inner(phat_subs[m]), kernel_gradient_m[ie])
#                        R_coeff += kernel_value_m[ie] * kernel_value_m[ie]

                    # # also use the particles to compute the expectation
                    # hessian_phat = self._posterior.generate_vector(PARAMETER)
                    # particles.num_particles_all.hessian(phat_subs[m], hessian_phat, m)
                    # pout_subs[m].axpy(kernel_value_m[m] ** kernel_value_m[m], hessian_phat)
                    # pout_subs[m].axpy(kernel_gradient_m[m].inner(phat_subs[m]), kernel_gradient_m[m])
                    # R_coeff += kernel_value_m[m] * kernel_value_m[m]
#
#                    R_phat = self._posterior.generate_vector(PARAMETER)
#                    self._posterior.prior.R.mult(phat_subs[n], R_phat)
#                    pout_subs[m].axpy(R_coeff, R_phat)

            else:
                raise NotImplementedError("choose full, diagonal, or lumped")
            
        self.ncalls += 1
#        print("pout_subs", pout_subs.dtype)
#        print(pout)
        return np.reshape(np.array(pout),self.hess_eval_pt.shape)/ self.num_particles







class CoupledNewton:
    # solve the optimization problem by Newton method with coupled linear system
    # The problem is discretized with the kernel basis, and the posterior is the external potential
    def __init__(self, posterior, kernel, DKL_Hessian_Operator, options):
        self.posterior = posterior
        self.kernel = kernel
        self.options = options

#        self.preconditioner = PreconditionerCoupled(self.posterior, particles)
        self.DKL_hessian_operator = DKL_Hessian_Operator(self.posterior, self.options)
        self.preconditioner_operator = IdentityPreconditioner(self.posterior)

#        self.rank = dl.MPI.rank(self.posterior.prior.R.mpi_comm())
        self.rank = 0 #just use this for now
        self.save_num = 0
        self.add_num = 0

    def coupled_D_KL_gradient(self, particles):
        """compute gradient in a vector of length dimension * num_particles

        Return shape: (#particles * dimension, 1),
                      (),
                      (#particles, #particles)
                      (#particles, #particles, dimension)
        """
        # particle_misfits = -np.apply_along_axis(self.posterior.logpdf,axis=1, arr=particles)
        # print("check all misfits are positive- min misfit:", np.min(particle_misfits))
        
        # (#particles, dim)
        gradients_particle_misfits = -np.apply_along_axis(self.posterior.gradlogpdf,axis=1, arr=particles)
#        print("gradients_particle_misfits", gradients_particle_misfits.shape)
#        print("PRINTING DIAGNOSTICS FOR D_KL Gradient:")
#        print("misfits norm",np.linalg.norm(gradients_particle_misfits, ord=2))
        gradients_particle_misfits = np.tile(gradients_particle_misfits[np.newaxis,:,:],(len(particles),1,1))
        all_kernel_gradients, \
        all_p_diffs, \
        all_kernel_evals, \
        all_M_p_diffs = self.kernel.all_gradients(particles)

#        print("max all_kernel_gradients norm",max([np.linalg.norm(kernel_gradients,ord=2) for kernel_gradients in all_kernel_gradients]))
        all_kernel_evals_expanded = all_kernel_evals[:,:,np.newaxis]
#        print("gradient before average",(all_kernel_evals_expanded * gradients_particle_misfits + all_kernel_gradients).shape)
        gradient = np.mean(-all_kernel_evals_expanded * gradients_particle_misfits + all_kernel_gradients, axis = 1)
#        print("gradient after average",gradient.shape)
        gradient = np.reshape(gradient,(np.prod(particles.shape),)) #shape compatible with scipy
        gradient_norm = np.linalg.norm(gradient, ord=2) #2 norm
#        print("gradient norm",gradient_norm)
        return gradient, gradient_norm, all_kernel_evals, all_kernel_gradients
    
    def logpdfs(self, particles):
        """
        Return shape: (), (), ()
        """
        neg_prior_log_pdf = np.mean([-self.posterior.prior.logpdf(particle) for particle in particles])
        neg_like_log_pdf = np.mean([-self.posterior.likelihood.logpdf(particle) for particle in particles])
        neg_post_log_pdf = neg_prior_log_pdf + neg_like_log_pdf
        return neg_prior_log_pdf, neg_like_log_pdf, neg_post_log_pdf

    def solve(self, particles, **kwargs):
        r""" 
        Solve the optimization problem to minimize the KL Divergence (D_KL) 
        between prior and posterior with respect to posterior use the Newton-CG method

        Return: None, TODO: return particles, and a logger, that records history of variables, such as particles history
        """
        history_callback_fn = kwargs.get('history_callback_fn', lambda it, particles : None)
        convergence_callback_fn = kwargs.get('convergence_callback_fn', lambda particles : np.inf)

        ###########################
        # OPTIMIZATION PARAMETERS #
        ###########################
        rel_tol = self.options["rel_tolerance"]
        abs_tol = self.options["abs_tolerance"]
        inner_tol = self.options["inner_rel_tolerance"]
        line_search = self.options["line_search"]
        print_level = self.options["print_level"]
        cg_coarse_tolerance = self.options["cg_coarse_tolerance"]
        c_armijo = self.options["c_armijo"]
        max_backtracking_iters = self.options["max_backtracking_iters"]

        if self.options['save_num']:

            #TO JOSH: implement saving of particles next, so we can plot the trajectories of the particles in time.
            pass
#            self.kernel.save_values(self.save_num, it)
#            particles.save(self.save_num, it)
#            particles.save_eigenvalue(self.save_num, it)
#            particles.plot_eigenvalue(self.save_num, it)
       
        ###################
        # INITIALIZATIONS #
        ###################
        gradient, \
        gradient_norm, \
        all_kernel_evals, \
        all_kernel_gradients = self.coupled_D_KL_gradient(particles)

        neg_prior_log_pdf, \
        neg_like_log_pdf, \
        neg_post_log_pdf = self.logpdfs(particles)
               
        gradient_norm_0 = gradient_norm
        tol_newton = max(abs_tol, gradient_norm_0 * rel_tol)
        total_cg_iters = 0
        int_error = convergence_callback_fn(particles)

        if (self.rank == 0) and (print_level >= -1):
            print("\n{0:5} {1:8} {2:15} {3:15} {4:15} {5:15} {6:14} {7:14} {8:14}".format(
                "It", "cg_it", "cost", "misfit", "reg", "(g,dm)", "||g||L2", "alpha", "tolcg", "int_err"))
            print("{0:3d} {1:3} {2:15e} {3:15e} {4:15e} {5:15} {6:14e} {7:14} {8:14} {9:14e}".format(
                0, "   -", neg_post_log_pdf, neg_like_log_pdf, neg_prior_log_pdf,
                "        -", gradient_norm, "         -", "         -", int_error))

        #####################
        # NEWTON ITERATIONS #
        #####################         
        for it in range(1, self.options["max_iter"] + 1):

            ################################
            # Check Termination Conditions #
            ################################
            if int_error < self.options['integration_error_tol']:
                converged = True
                reason = 4 #REASON 4 is: particles produced integral to within desired accuracy
                break
            # Convergence check based on gradient_inner_phat (necessary condition)
            if it > 1 and -gradient_inner_phat <= self.options["gdm_tolerance"]:
                converged = True
                reason = 3
                break
            # Convergence check based on norm of the gradient (necessary condition)
            if gradient_norm <= tol_newton:
                converged = True
                reason = 1
                break

            #########################################
            # UPDATE LHS OPERATORS/SOLVE PARAMETERS #
            #########################################
            #Update Hessian Operator with point for evaluation, as well as Kernel calculations
            #Also set the preconditioner
            self.DKL_hessian_operator.hess_eval_pt = particles
            self.preconditioner_operator.eval_point = particles
            self.preconditioner_operator.shape = self.DKL_hessian_operator.shape
            # self.DKL_hessian_operator.update_kernel_operator(all_kernel_evals, all_kernel_gradients)
            self.DKL_hessian_operator.all_kernel_evals = all_kernel_evals
#            print(all_kernel_evals)
            self.DKL_hessian_operator.all_kernel_gradients = all_kernel_gradients
            self.DKL_hessian_operator.ncalls = 0    #reset number of hessian calls

            # set tolerance for CG iteration fpr solving Hessian system
            tol_cg = 1e-4#min(cg_coarse_tolerance, np.sqrt(gradient_norm / gradient_norm_0))
            
            #set hessian operator options? this is a bit too implicit, should update it explicitly
#            self.options['gauss_newton_approx'] = (it < self.options['max_iter_gauss_newton_approx'])
                                             #or self.options['gauss_newton_approx_hold'])

            #############################
            # CONJUGATE GRADIENTS SOLVE #
            #############################  
#            phat_flattened, cg_summary_info = spspa.linalg.cg(self.DKL_hessian_operator,
#                                        -gradient,
#                                        x0= -gradient,
#                                        M =self.preconditioner_operator,
#                                        tol=tol_cg,
#                                        maxiter=self.options['max_cg_iters'])           #zero initial guess? print level?
            phat_flattened, cg_summary_info = spspa.linalg.cg(self.DKL_hessian_operator,
                                                  gradient,
                                                  M =self.preconditioner_operator,
                                                  tol=1e-4,
                                                  maxiter=self.options['max_cg_iters'])
            total_cg_iters += self.DKL_hessian_operator.ncalls

            # replace Hessian with its Gauss Newton low_rank_Hessian_misfit if CG reached a negative direction
            # if #cg_summary_info (if we see negative curvature)...
            #     hessian_operator.low_rank_Hessian_misfit = False
            #     hessian_operator.gauss_newton_approx = True
            #     solver.set_operator(hessian)
            #     solver.solve(phat, -gradient)

            #####################$
            # DEFINE NEWTON STEP #
            ######################
            # Particles update step: particles_step(x)_j = sum_i phat_i k_ij(x)
            phat = phat_flattened.reshape((self.DKL_hessian_operator.num_particles, self.DKL_hessian_operator.dim))
            particles_step = all_kernel_evals.dot(phat)

            ###########################################
            # GLOBALIZATION: BACKTRACKING LINE SEARCH #
            ###########################################  
            gradient_inner_phat = -gradient.dot(particles_step.flatten())
            alpha = 1 # make this into an Option!
            
            if line_search:
                for n_backtrack in range(max_backtracking_iters):
                    # Update backtracking line search for particles
                    particles_new = particles + alpha * particles_step

                    # Calculate log pdfs at NEW particles
                    neg_prior_log_pdf_new, \
                    neg_like_log_pdf_new, \
                    neg_post_log_pdf_new = self.logpdfs(particles_new)

                    # Check sufficient descent / if armijo conditions are satisfied
                    if (neg_post_log_pdf_new < neg_post_log_pdf + alpha * c_armijo * gradient_inner_phat) or \
                        (-gradient_inner_phat <= self.options["gdm_tolerance"]):
                        #Sufficient descent, exit armijo line search and use this alpha for taking a step

                        ############################################
                        # UPDATE NEWTON STEP WITHOUT GLOBALIZATION #
                        ############################################
                        particles += alpha * particles_step
                        neg_post_log_pdf, \
                        neg_like_log_pdf, \
                        neg_prior_log_pdf = neg_post_log_pdf_new, \
                                            neg_like_log_pdf_new, \
                                            neg_prior_log_pdf_new
                        break
                    else:
                        #Insufficient descent, backtrack
                        alpha *= .5
                else:
                    # Backtracking failure: reached max_backtracking_iters, exit newton with convergence = False (algorithm failure)
                    converged = False
                    reason = 2
                    break
            else:
                #####################################
                # NEWTON STEP WITHOUT GLOBALIZATION #
                #####################################
                # Update particles, i.e. Take a newton step - Move all particles in new directions `alpha` amount: p_i = p_i + alpha * sum_j phat[j] * k(p_i, p_j)
                particles += alpha * particles_step

                # Calculate new log pdfs at particles
                neg_prior_log_pdf, \
                neg_like_log_pdf, \
                neg_post_log_pdf = self.logpdfs(particles)

            # Calculate / Update gradient at new particles
            gradient, \
            gradient_norm, \
            all_kernel_evals, \
            all_kernel_gradients = self.coupled_D_KL_gradient(particles)
            self.DKL_hessian_operator._hess_eval_pt_set = False
            
            int_error = convergence_callback_fn(particles)

            #####################
            # LOGGING / OUTPUTS #
            #####################
            if (self.rank == 0) and (print_level >= -1):
                print("{0:3d} {1:3d} {2:15e} {3:15e} {4:15e} {5:15e} {6:14e} {7:14e} {8:14e} {9:14e}".format(
                    it, self.DKL_hessian_operator.ncalls, neg_post_log_pdf, neg_like_log_pdf, neg_prior_log_pdf,
                    gradient_inner_phat, gradient_norm, alpha, tol_cg, int_error))

            # save the particles for visualization and plot the eigenvalues at each particles
            if self.save_num and np.mod(it, self.save_step) == 0:
                # self.kernel.save_values(self.save_num, it)
                particles.save(self.save_num, it)
                # particles.save_eigenvalue(self.save_num, it)
                # particles.plot_eigenvalue(self.save_num, it)

            ##########################################
            # ADAPTATION OF PARTICLES/DISCRETIZATION #
            ##########################################
            # add new particles if needed, try different adding criteria, e.g., np.max(tol_cg) < beta^{-t}
            # if self.add_num and np.mod(it, self.add_step) == 0:
            #     particles.add(particles.num_particles_all)
            #     self.gls.update(particles)
            #     self.preconditioner.update(particles)

            # update particles.num_particles_all, kernel, and hessian with new particles before solving the Newton linear system
            # particles.gauss_newton_approx = (it < self.options['max_iter_gauss_newton_approx'])
                                                 #or self.options['gauss_newton_approx_hold'])

#            particles.num_particles_all.update(particles)
            # self.kernel.delta_kernel = (it < self.kernel.max_iter_delta_kernel) or self.kernel.delta_kernel_hold
#            self.kernel.update(particles, particles.num_particles_all)
#            self.DKL_hessian_operator.update(particles, particles.num_particles_all, self.kernel)
            history_callback_fn(it, particles)
        else:
            #####################
            # REACHED MAX ITERS #
            #####################
            # Evaluate gradient/Check convergence
            gradient, \
            gradient_norm, \
            all_kernel_evals, \
            all_kernel_gradients = self.coupled_D_KL_gradient(particles)

            # Convergence check based on integration error
            if convergence_callback_fn(particles) < self.options['integration_error_tol']:
                converged = True
                reason = 4
            # Convergence check based on gradient_inner_phat (necessary condition)
            elif it > 0 and -gradient_inner_phat <= self.options["gdm_tolerance"]:
                converged = True
                reason = 3
            # Convergence check based on norm of the gradient (necessary condition)
            elif gradient_norm <= tol_newton:
                converged = True
                reason = 1                
            # Algorithm did not converge within max_iter #rename to max_newton_iters
            else:
                converged = False
                reason = 0

        #####################
        # POST OPTIMIZATION #
        #####################  
        print("Converged: ", converged)
        print("Termination reason: ", self.options["termination_reasons"][reason])
        print("Total Newton iters", it)
        print("Total CG iters",total_cg_iters)
        # print("\n {0:3} {1:15} {2:15} {3:15}".format("ip", "cost", "misfit", "reg"))
        return reason
        # for m in range(particles.num_particles_all):
        #     print("{0:3d} {1:15e} {2:15e} {3:15e}".format(m,
        #                                 self.post_log_pdf_new_set[m], self.misfit_new_set[m], self.reg_new_set[m]))
