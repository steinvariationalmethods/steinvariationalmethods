from __future__ import absolute_import, division, print_function

import numpy as np
from abc import ABC, abstractmethod

import os
if not os.path.isdir("data"):
    os.mkdir("data")
import pickle

class AbstractKernelBase(ABC):
    r"""A Kernel takes a (possibly parameter space varying ) metric (inner product field) and 
    options, and allows for the evaluation of the Kernel function at pairs of particles which
    live in parameter space. Derivatives are also available."""
    def __init__(self, metric, options):
        self.metric = metric  # posterior object
#        self.variation = variation  # variation object
        self.options = options      # options dictionary

    @abstractmethod
    def eval(self, p_i, p_j):
        pass

    @abstractmethod
    def evals(self, p_i, particles):
        pass

    @abstractmethod
    def gradient(self, p_i, p_j):
        pass

    @abstractmethod
    def gradients(self, p_i, particles):
        pass

class GaussianKernel(AbstractKernelBase):
    r"""A Gaussian Kernel, K(p_i,p_j) = exp(-(p_i-p_j)^T * M * (p_i-p_j)), with precision defined by a metric M (in general a function of p_i)"""
    def __init__(self, metric, options):
        super(GaussianKernel, self).__init__(metric, options)
#        self.save_kernel = options["save_kernel"]
#        self.delta_kernel = options["delta_kernel"]
#        self.delta_kernel_hold = options["delta_kernel"]
#        self.max_iter_delta_kernel = options["max_iter_delta_kernel"]

    def differences(self, particles_i, particles):
        r"""Calculate particles_i - particles

        Return shape: (#particles_i, #particles, dimension) or (#particles, dimension) if #particles_i is 1
        """
        tiled_particles_i = np.transpose(np.tile(particles_i[np.newaxis,:,:],(len(particles),1,1)),(1,0,2))
        expanded_particles = particles[np.newaxis,:,:]
        diffs = tiled_particles_i - expanded_particles
        if diffs.shape[0] is 1:
            diffs = np.squeeze(diffs,axis=0)
        return diffs

    def all_differences(self, particles):
        r"""Calculate p_i - p_j, \forall p_i, p_j in particles

        Return shape: (#particles, #particles, dimension)
        """
        all_differences = self.differences(particles, particles)
        return all_differences

    def eval(self, p_i, p_j, p_i_diff=None, Mi_p_diff=None, particles=None): #Mi_p_diff=None
        """
        Evaluate the kernel k(p_i,p_j) = exp(-(p_i-p_j)^T * M(p_i) * (p_i-p_j))

        Return shape:  (),(dimension,),(dimension,)
        """
        if p_i_diff is None:
            p_i_diff = p_i - p_j
        
        # Calculate and return the kernel value, defined in terms of the norm_squared_squared
        if Mi_p_diff is None:
            Mi_p_diff = self.metric.mult(p_i_diff, eval_metric_at=p_i, particles=particles)

        p_diff_Mi_p_diff = self.metric.inner(p_i_diff, Mi_p_diff, weighted=False)
        return np.exp(-p_diff_Mi_p_diff), Mi_p_diff, p_i_diff

    def evals(self, p_i, particles, p_i_diffs=None, Mi_p_diffs=None): 
        """
        Evaluate the kernel k(p_i,p_j) = exp(-(p_i-p_j)^T * M(p_i) * (p_i-p_j)) forall all p_j \in particles 
        
        Return shape: (#particles,),
                      (#particles, dimension), 
                      (#particles, dimension)
        """
        if p_i_diffs is None:
            p_i_diffs = self.differences(p_i, particles._data)


        # evaluate the K(p_i, p_j) for given p_i and all p_j \in particles
        
        if Mi_p_diffs is None:   
#            f = lambda p_i_diff, p_i : self.metric.mult(p_i_diff, eval_metric_at=p_i)
#            Mi_p_diffs = np.apply_along_axis(f, axis=1, arr=p_i_diffs)
            Mi_p_diffs = np.array([ self.metric.mult(p_i_diff, eval_metric_at=p_i, particles=particles) for p_i, p_i_diff in zip(particles, p_i_diffs)])
                
        evals_Mi_p_diffs = [ self.eval(p_i, p_j, p_i_diff=p_i_diff, Mi_p_diff=Mi_p_diff)[0:2] for p_j, p_i_diff, Mi_p_diff in zip(particles, p_i_diffs, Mi_p_diffs) ]
        evals, Mi_p_diffs = zip(*evals_Mi_p_diffs)
        evals, Mi_p_diffs = np.array(evals), np.array(Mi_p_diffs)

        return evals, Mi_p_diffs, p_i_diffs 

    def all_evals(self, particles, all_p_diffs=None, all_M_p_diffs=None):
        """
        Evaluate the kernel k(p_i,p_j) = exp(-(p_i-p_j)^T * M(p_i) * (p_i-p_j)) forall all pi, p_j \in particles         
        
        Return shape: (#particles, #particles), 
                      (#particles, #particles, dimension), 
                      (#particles, #particles, dimension)
        """
        if all_p_diffs is None:
            all_p_diffs = self.all_differences(particles._data)

        if all_M_p_diffs is None:
#            f = lambda p_i_diff, p_i : self.metric.mult(p_i_diff, eval_metric_at=p_i)
#            all_M_p_diffs = np.array([np.array([ self.metric.mult(p_i_diff, eval_metric_at=p_i) for p_i, p_i_diff in zip(particles, p_i_diffs)])] )
            all_M_p_diffs = np.array([[ self.metric.mult(p_i_diff, eval_metric_at=p_i, particles=particles) for p_i_diff in p_i_diffs] for p_i_diffs, p_i in zip(all_p_diffs,particles)])
        
        all_evals_all_Mi_p_diffs = [self.evals(p_i, particles, p_i_diffs=p_i_diffs, Mi_p_diffs=Mi_p_diffs)[0:2] for p_i, p_i_diffs, Mi_p_diffs in zip(particles, all_p_diffs, all_M_p_diffs)]
        all_evals, all_Mi_p_diffs = zip(*all_evals_all_Mi_p_diffs)
        all_evals, all_Mi_p_diffs = np.array(all_evals), np.array(all_Mi_p_diffs)
        return all_evals, all_M_p_diffs, all_p_diffs

    def gradient(self, p_i, p_j, p_i_diff=None, kernel_eval=None, M_p_diff=None, particles=None):
        """
        Evaluate the gradient of kernel at (p_i, p_j) with respect to p_j, grad K(p_i, p_j) = 2 * k(p_i, p_j) * M(p_i) * (p_i - p_j)
        
        Return shape: (1, dimension), 
                      (), 
                      (1, dimension), 
                      (1, dimension) 
        """
        # Calculate K(p_i, p_j)
        if p_i_diff is None:
            p_i_diff = p_i - p_j

        if Mi_p_diff is None:
            Mi_p_diff = self.metric.mult(p_i_diff, eval_metric_at=p_i, particles=particles)

        if kernel_eval is None:
            kernel_eval, Mi_p_diff, p_i_diff = self.eval(p_i, p_j, p_i_diff=p_i_diff, Mi_p_diff=Mi_p_diff, particles=particles) #The metric has been updated, so we dont need to reupdate it for the metric.mult

        gradient = 2. * kernel_eval * M_p_diff
        return gradient, p_i_diff, kernel_eval, M_p_diff

    def gradients(self, p_i, particles, p_i_diffs=None, kernel_evals=None, M_p_diffs=None):
        """
        Evaluate the gradients of kernel at (p_i, p_j) with respect to p_j, for all p_j in particles
        
        return shape should be  (#partices, dimension), 
                                (#partices, dimension), 
                                (#particles, ), 
                                (#particles, dimension)
        """

        if p_i_diffs is None:
            p_i_diffs = self.differences(p_i, particles._data)

        if M_p_diffs is None:
            M_p_diffs = np.array([ self.metric.mult(p_i_diff, eval_metric_at=p_i, particles=particles) for p_i, p_i_diff in zip(particles, p_i_diffs)])

        if kernel_evals is None:
            #Calculate K(p_i, p_j) \forall p_j in particles
            kernel_evals = self.evals(p_i, particles, p_i_diffs=p_i_diffs, Mi_p_diffs=M_p_diffs)[0]

        # Calculate and return the gradient of K(p_i, p_j) with respect to p_j, for all p_j in particles
        kernel_evals_expanded = np.expand_dims(kernel_evals, axis=1)

        gradients = 2. * kernel_evals_expanded * M_p_diffs
        return gradients,  p_i_diffs, kernel_evals, M_p_diffs

    def all_gradients(self, particles, all_p_diffs=None, all_kernel_evals=None, all_M_p_diffs=None):
        """
        Evaluate the gradients of kernel at (p_i, p_j) with respect to p_j, for all p_j in particles for all p_i in particles
        
        return shape should be (#particles, #particles, dimension), 
                               (#particles, #particles, dimension), 
                               (#particles, #particles), 
                               (#particles, #particles, dimension)
        """
        if all_p_diffs is None:
            all_p_diffs = self.all_differences(particles._data)

        if all_M_p_diffs is None:
            all_M_p_diffs = np.array([[ self.metric.mult(p_i_diff, eval_metric_at=p_i, particles=particles) for p_i_diff in p_i_diffs] for p_i_diffs, p_i in zip(all_p_diffs,particles)])

        if all_kernel_evals is None:
            #Calculate K(p_i, p_j) \forall p_j in particles
            all_kernel_evals = self.all_evals(particles, all_p_diffs=all_p_diffs, all_M_p_diffs=all_M_p_diffs)[0]

        all_gradients = np.array([ self.gradients(p_i, particles, 
                                        p_i_diffs=p_i_diffs, 
                                        kernel_evals=kernel_evals, 
                                        M_p_diffs=M_p_diffs)[0]
                                            for (p_i, p_i_diffs, kernel_evals, M_p_diffs) in 
                                                zip(particles, all_p_diffs, all_kernel_evals, all_M_p_diffs)
                        ])
        return all_gradients, \
                all_p_diffs, \
                all_kernel_evals, \
                all_M_p_diffs

    def hessian(self):
        raise NotImplementedError(" the hessian needs to be implemented ")

#    def save_values(self, save_number, it):
#        for n in range(save_number):
#            filename = 'data/kernel_' + str(n) + '_iteration_' + str(it) + '.p'
#            pickle.dump(self.evaluations_set, open(filename, 'wb'))
