from __future__ import absolute_import, division, print_function

import numpy as np
from abc import ABC, abstractmethod

import os
if not os.path.isdir("data"):
    os.mkdir("data")

import pickle

class ParticlesBase(ABC):
    r"""
    Abstract data structure that contains particles, and has methods 
    that allow one to iterate through particles, index on them, 
    move them. 

    Particles are stored as a row-major numpy array that is of shape (#particles, dim)
    Indexing on a ParticlesBase object reveals individual particles
    """
    def __init__(self, *args, **kwargs):
        len_args = len(args)
        if len_args > 0:
            assert(len_args is 1)
            self._data = args[0]
        else:
            self._data = np.empty((0,0))

    @property
    def dim(self):
        return self._data.shape[1]

    @property
    def shape(self):
        return self._data.shape
    
    def __str__(self):
        return str(self._data)

    def flatten(self):
        r"""Returns a numpy vector (#particles * dim, 1) of the particles"""
        return np.reshape(self, (np.prod(self.shape),1))

    def __len__(self):
        return self._data.shape[0]
        
    def __iter__(self):
        return iter(self._data)

    def __getitem__(self, key):
        r""""
        Returns a particle
        
        Return shape: (1, dimension)
        """
        return self._data[[key]]


    def __getstate__(self):
        # This extracts the information necessary to serialize/copy the object.
        dd = dict()
        dd['data'] = self._data
        return dd

    def __setstate__(self, dd):
        self._data = dd['data']

    def copy(self):
        r""" A copy of the particles
        """
        return self.__copy__()

    def __copy__(self):
        return self.__deepcopy__({})

    def __deepcopy__(self, memo):
        return pickle.loads( pickle.dumps( self ) )

    # The following methods allow us to update particles by adding/subtracting other particle steps
    def __iadd__(self, other):
        if self.shape != other.shape:
            raise Exception("Shape mismatch of particles!") #also print the shapes
        try:
            self._data += other._data
        except AttributeError: #other is a numpy array?
            self._data += other 
        return self

    def __radd__(self, other):
        return self + other

    def __add__(self, other):
        new_particle = ParticlesBase(self._data)
        try:
            new_particle._data += other._data
        except AttributeError:  #other is a numpy array?
            new_particle._data += other
        return new_particle

    def __isub__(self, other):
        if self.shape != other.shape:
            raise Exception("Shape mismatch of particles!") #also print the shapes
        try:
            self._data -= other._data
        except AttributeError: #other is a numpy array?
            self._data -= other 
        return self

    def __rsub__(self, other):
        return other - self

    def __sub__(self, other):
        try:
            new_particle = ParticlesBase(other._data)
        except:
            new_particle = ParticlesBase(other)
        new_particle._data -= self._data
        return new_particle

    def __imul__(self, alpha):
        #alpha must be a scalar......
        self._data *= alpha

    def __mul__(self, alpha):
        #alpha must be a scalar......
        new_particle = ParticlesBase(self._data)
        new_particle._data *= alpha
        return new_particle

    def __rmul__(self, alpha):
        #alpha must be a scalar......
        return self.__mul__(alpha)

class SampledParticles(ParticlesBase):
    r"""
    Particles sampled from a distribution
    """ 
    def __init__(self, sampling_distribution, options):
        self._sampling_distribution = sampling_distribution
        self.options = options

        #two sets for the particles

        self.initial_num_data = options["num_particles"]
        self.num_particles_old = self.initial_num_data

        self.num_particles_add = options["num_particles_add"]
        self.num_particles_all = self.initial_num_data + self.num_particles_add
        self.num_particles_all_old = self.num_particles_all

        # sample a bunch of particles from the prior
        data = self.sampling_distribution._samplemany(self.initial_num_data)
        super().__init__(data)

    @property
    def sampling_distribution(self):
        return self._sampling_distribution
    

#Define a mixture distribution from the particles, sample from that, return a Distribution of Particles (hierarchy?? define a tree structure))

class MixtureDistributionSampledParticles(SampledParticles):
    #can sample from the particles and produce a new set of particles (hierarchical set of particles??)
    pass

#        if self.num_particles_add > 0:
#            for n in range(self.num_particles_add):
#                particle = self.model.generate_vector(PARAMETER)
#                parRandom.normal(1., noise)
#                self.model.prior.sample(noise, particle, add_mean=True)
#                self.particles.append(particle)
#
#    def add(self, variation):
#        # add new particles by Laplace distribution at each of the particle for the transport map construction
#        if self.add_rule:
#            for n in range(self.num_particles):
#                sampler = GaussianLRPosterior(self.model.prior, variation.d[n], variation.U[n], mean=self.particles[n])
#                noise = dl.Vector()
#                self.model.prior.init_vector(noise, "noise")
#                for m in range(self.add_num):
#                    s_prior = self.model.generate_vector(PARAMETER)
#                    s_posterior = self.model.generate_vector(PARAMETER)
#                    parRandom.normal(1., noise)
#                    self.model.prior.sample(noise, s_prior, add_mean=False)
#                    sampler.sample(s_prior, s_posterior, add_mean=True)
#                    index = self.num_particles_all_old-self.num_particles_add + n * self.num_particles + m
#                    self.particles.insert(index, s_posterior)
#
#            # update the num of particles used for constructing the transport map
#            if self.add_rule == 1:  # add all the new particles for construction
#                self.num_particles_old = self.num_particles
#                self.num_particles += self.num_particles_old * self.add_num
#            elif self.add_rule == 2:  # only add the ones added in the previous step
#                self.num_particles_old = self.num_particles
#                self.num_particles = self.num_particles_all - self.num_particles_add
#            else:
#                pass
#
#            # update the total num of particles
#            self.num_particles_all_old = self.num_particles_all
#            self.num_particles_all += self.num_particles_old * self.add_num
#
#    def save(self, save_num=1, it=0):
#        # save samples for visualization
#        if save_num > self.num_particles_all:
#            save_num = self.num_particles_all
#
#        for n in range(save_num):
#            particle_fun = dl.Function(self.model.problem.Vh[PARAMETER], name='particle')
#            particle_fun.vector().axpy(1.0, self.particles[n])
#            filename = 'data/particle_' + str(n) + '_iteration_' + str(it) + '.xdmf'
#            if dlversion() <= (1, 6, 0):
#                dl.File(self.model.prior.R.mpi_comm(), filename) << particle_fun
#            else:
#                xf = dl.XDMFFile(self.model.prior.R.mpi_comm(), filename)
#                xf.write(particle_fun)
