from mpi4py import MPI
import numpy as np 

comm = MPI.COMM_WORLD 
print(comm.rank)
#useful for indexing up to 4million (for our samples for instance)
def np_uint32_shared_array(num_elements, comm):
    # create a shared array of 'num_elements' elements of type np.uint64

    size_of_uint32 = MPI.UNSIGNED.Get_size()  #bytes per number
    if comm.Get_rank() == 0: 
        nbytes = num_elements * size_of_uint32        #bytes in total 
    else: 
        nbytes = 0

    # on rank 0, create the shared block
    # on rank other get a handle to it (known as a window in MPI speak)
    mpi_window = MPI.Win.Allocate_shared(nbytes, size_of_uint32, comm=comm) 
    buf, itemsize = mpi_window.Shared_query(0) # get the process local address for the shared memory
    return np.ndarray(buffer=buf, dtype='uint32', shape=(num_elements,)) 

#useful for storing samples, also random vectors for randomized SVD
def np_float64_shared_array(num_elements, comm):
    # create a shared array of 'num_elements' elements of type np.float64

    size_of_float64 = MPI.DOUBLE.Get_size()  #bytes per number
    if comm.Get_rank() == 0: 
        nbytes = num_elements * size_of_float64        #bytes in total 
    else: 
        nbytes = 0

    # on rank 0, create the shared block
    # on rank other get a handle to it (known as a window in MPI speak)
    mpi_window = MPI.Win.Allocate_shared(nbytes, size_of_float64, comm=comm) 
    buf, itemsize = mpi_window.Shared_query(0) # get the process local address for the shared memory
    return np.ndarray(buffer=buf, dtype='float64', shape=(num_elements,)) 

#useful for storing samples, also random vectors for randomized SVD
def np_float64_shared_2d_array(shape, comm): #shape is assumed to be a 2d iterable
    # create a shared array of 'size' elements of type np.float64

    size_of_float64 = MPI.DOUBLE.Get_size()  #bytes per number
    if comm.Get_rank() == 0: 
        nbytes = shape[0] * shape[1] * size_of_float64        #bytes in total 
    else: 
        nbytes = 0

    # on rank 0, create the shared block
    # on rank other get a handle to it (known as a window in MPI speak)
    mpi_window = MPI.Win.Allocate_shared(nbytes, size_of_float64, comm=comm) 
    buf, itemsize = mpi_window.Shared_query(0) # get the process local address for the shared memory
    return np.ndarray(buffer=buf, dtype='float64', shape=shape) 





#####################################################################
# SHARED MEMORY - USED FOR CREATING PRIOR SAMPLES/INITIAL PARTICLES #
#####################################################################

# create a numpy array whose data points to the shared mem
dimension = 1000000

num_pts_per_rank = 2500
N = comm.size * num_pts_per_rank

total_size = N * dimension

shared_uint_array = np_uint32_shared_array(total_size, comm)
shared_double_array = np_float64_shared_array(total_size, comm)

# On process rank 1:
# write the numbers 0.0,1.0,..,4.0 to the first 5 elements of the array
i = comm.rank
start = num_pts_per_rank * i * dimension
end = num_pts_per_rank * (i+1) * dimension

#FOR INDICES:
shared_uint_array[start:end] = np.arange(start,end)

#FOR INITIAL PRIOR SAMPLES, NEWTON STEPS, ETC
shared_double_array[start:end] = np.random.normal(size=(num_pts_per_rank* dimensionduk,))
# wait in process rank 0 until all processes have written to the array
comm.Barrier() 

# check that the array is actually shared and process 0 can see
# the changes made in the array by all processes minus 0
if comm.rank == 0: 
    print(shared_uint_array)
    print(shared_double_array)

#################################################
# SHARED MEMORY - GENERATION OF RANDOM MATRICES #
#################################################
low_rank = 2
shape = (total_size, low_rank)

shared_double_2d_array = np_float64_shared_2d_array(shape, comm)

# On process rank i:
# write the numbers to its portion of the array

local_shape = (num_pts_per_rank, low_rank)

#FOR RANDOM MATRICES
shared_double_2d_array[start:end,:] = np.random.normal(size=local_shape)
# wait in process rank 0 until all processes have written to the array
comm.Barrier() 

# check that the array is actually shared and process 0 can see
# the changes made in the array by all processes minus 0
if comm.rank == 0: 
    print(shared_double_2d_array)


#KERNEL MATRIX SHOULD BE SHARED MEMORY OR COPIED?? (N x N )




