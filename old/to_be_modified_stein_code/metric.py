from __future__ import absolute_import, division, print_function

import numpy as np
from abc import ABC, abstractmethod

class LinearOperator( ABC):
    
    def inner(self, v1, v2, save_matvec=False, weighted=True):
        if weighted:
            matvec = self.mult(v2)
            if not save_matvec:
                return np.sum(v1*matvec) #assumes phat and temp are (n,1), we want a scalar output
            else:
                return np.sum(v1*matvec), matvec
        else:
            return np.sum(v1*v2)

    @abstractmethod
    def mult(self, vec, **kwargs): #assumes vec is (n,1), we want a (n,1) output
        pass

# class AverageHessian(LinearOperator):
#     r"""The Average Hessian operator. enables Hessian action that is based on the average likelihood (or is it posterior) hesss of N particles
#     """
#     def __init__(self, posterior, particles):
#         self.posterior = posterior
#         self.particles = particles

#     def mult(self, phat, **kwargs):
#         # returns average Hessian action: 1/N * \sum_n H_n phat hess(self, phat, n)
#         averageHessian_phat = 0.0
#         for particle in self.particles: 
#             self.posterior.hess_eval_pt = particle
#             averageHessian_phat += self.posterior.hess_logpdf_action(phat)
#         return averageHessian_phat/self.number_particles

class MetricBase(LinearOperator): 
    r"""A `metric` which is really a (possibly spatially varying) inner product.
    One should be able to multiply the inner product weight with a vector,
    take inner products, as well as norms"""
    def __init__(self, manifold, options):
        self.manifold = manifold
        self._dim = manifold.dim
        self.options = options
    
    @property
    def dim(self):
        return self._dim
    
    @property
    def metric_eval_pt(self):
        return self.manifold.hess_eval_pt

    @metric_eval_pt.setter
    def metric_eval_pt(self, particle):
        self.manifold.hess_eval_pt = particle

    def norm_squared(self, phat, eval_metric_at=None):
        #Returns metric weighted norm of phat, as well as M*phat
        if eval_metric_at is not None:
            self.metric_eval_pt = eval_metric_at
        return self.inner(phat, phat, save_matvec=True)

class IdentityMetric(MetricBase):
    r"""
    Define the Identity Metric.
    """
    def __init__(self, manifold, options):
        super(IdentityMetric, self).__init__(manifold, options)

    def mult(self, phat, **kwargs):
        return phat

class PriorHessianMetric(MetricBase): #Right now this assumes a Gaussian Prior
    r"""
    Define the metric M(p_i), with norm_squared for point p_i: (p_hat)^T M(p_i) * (p_hat)
    and inner product for point p_i: (phat_1, phat_2)  = (phat_1)^T M(p_i) * (phat_2)
    Here the metric is the prior Precision/the dimension of the space. Thus, it is not point dependent
    """
    def __init__(self, prior, options):
        super(PriorHessianMetric, self).__init__(prior, options)

    def mult(self, phat, **kwargs): #Prior Precision * phat
        if kwargs.get('eval_metric_at') is not None:
            self.metric_eval_pt = eval_metric_at
        M_p_hat = self.manifold.hess_logpdf_action(phat, gaussnewton=self.options['gauss_newton_approx']) 

        # 2. rescaling by dimension
        if self.options['rescaling']:
            M_p_hat *= (1/self.dim)
        return M_p_hat

class PosteriorHessianMetric(MetricBase):
    r"""
    Define the metric M(p_i), with norm_squared for point p_i (p_hat)^T M(p_i) * (p_hat)
    and inner product for point p_i between two vectors phat_1 and phat_2 (phat_1)^T M(p_i) * (phat_2)
    Here the metric is different for each point, and defined by the Posterior Hessian at each point/the dimension of the space.
    """
    def __init__(self, posterior, options):
        super(PosteriorHessianMetric, self).__init__(posterior, options)

    def mult(self, phat, **kwargs):
        r"""
        For each particle in the metric evaluation point, evaluate the hess action against 
        `its portion' of phat
        """
        if kwargs.get('eval_metric_at') is not None:
            self.metric_eval_pt = eval_metric_at
        M_p_hat = self.manifold.hess_logpdf_action(phat, gaussnewton=self.options['gauss_newton_approx'])

        # 2. rescaling by dimension
        if self.options['rescaling']:
            M_p_hat *= (1/self.dim)
        return M_p_hat

class AveragedPosteriorHessianMetric(MetricBase):
    # define the metric M for the kernel k(pn, pm) = exp(-(pn-pm)^T * M * (pn - pm))
    def __init__(self, posterior, options):
        super(AveragedPosteriorHessianMetric, self).__init__(posterior, options)

    def mult(self, phat, **kwargs):
        # REQUIRES particles keyword argument
        # returns average Hessian action: 1/N * \sum_n H_n phat hess(self, phat, n)
        particles = kwargs['particles']
        averageHessian_phat = np.zeros(self.dim)
        for particle in particles:  #can be MPI Parallelized, then all reduced
            self.metric_eval_pt = particle
            averageHessian_phat += -self.manifold.hess_logpdf_action(phat, gaussnewton=self.options['gauss_newton_approx'])
        averageHessian_phat/=len(particles)
        
        # 2. rescaling by dimension
        if self.options['rescaling']:
            averageHessian_phat *= (1/self.dim)
        return averageHessian_phat


        # pout.zero()
        # covariance/regularization
#        # phelp = self.model.generate_vector(PARAMETER)
#        temp = self.posterior.prior.hess_logpdf_action(phat)
#        # pout.axpy(1.0, phelp)
#        # negative log likelihood function
#        if self.low_rank_Hessian_misfit == 2:
#            # phelp_1 = self.model.generate_vector(PARAMETER)
#            hess_misfit = LowRankOperator(self.variation.d_average, self.variation.U_average)
#            temp = hess_misfit.mult(pout)
#            H_phat = self.posterior.prior.hess_logpdf_action(temp)
#
#            # # 1. no rescaling
#
#            # # 2. rescaling by dimension
#            H_phat /= self.model.problem.Vh[PARAMETER].dim()
#
#            # # 3. rescaling by the trace = trace(I) + trace(H), the following is the correct trace
#            # H_phat / = (self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d_average))
#
#            # print("dim = ", self.model.problem.Vh[PARAMETER].dim(), "trace", np.sum(variation.d[n]))
#        else:
#            if self.low_rank_Hessian_misfit == 1:
#                hess_misfit = LowRankOperator(self.variation.d_average, self.variation.U_average)
#                hess_misfit.mult(phat, phelp)
#            else:
#                for n in range(self.particle.number_particles):
#                    self.variation.hess(phat, phelp, n)
#                    pout.axpy(1.0/self.particle.number_particles, phelp)
#
#            # # 1. no rescaling
#
#            # # 2. rescaling by dimension
#            H_phat /= self.model.problem.Vh[PARAMETER].dim()
#
#            # # 3. rescaling by the trace = trace(I) + trace(H), however the following is not the right trace
#            # pout[:] *= 1/(self.model.problem.Vh[PARAMETER].dim() + np.sum(variation.d_average))
#
#            # print("dim = ", self.model.problem.Vh[PARAMETER].dim(), "trace", np.sum(variation.d[n]))
