from __future__ import absolute_import, division, print_function

import numpy as np
from abc import ABC, abstractmethod

 
likelihood_hess_log_pdf_action = #partial func? the hessian action at the point specified, either hte Gauss newton or normal Hessian
k = 

# all the hessian actions shoudl just be numpy partial funcs or lambad functions
# that operate on many vectors at the same time.


if #no preconditioning
    d, U = double_pass_eigensolver(A=likelihood_hess_log_pdf_action, Omega=Omega, k=)
elif #Prior preconditioned...
    d, U = randomized_double_pass_generalized_eigensolver(A=hessian_misfit, B=self.model.prior.R, Binv=self.model.prior.Rsolver,
                                       Omega=Omega, k=) 
return LowRankOperator(eigs=d,U=U)


#Jit class or something here? this should just be fast copmiled

class LowRankOperator(_Metric):
    """
    This class model the action of a low rank operator :math:`A = U D U^T`.
    Here :math:`D` is a diagonal (vector), and the columns of U are orthonormal.
    """
    def __init__(self, d, U, my_init_vector=None):
        """
        Construct the low rank operator given :code:`d` and :code:`U`.
        """
        self.d = d
        self.U = U
        
    # def mult(self, x):
    #     """
    #     Compute :math:`y = Ax = U D U^T x`
    #     """
    #     return U @ (d* (self.U.T @ x))

    # def inner(self, x, y): 
    #     """
    #     Compute :math: `y.T U D U^T x`
    #     """
    #     Utx = U.T @ x
    #     Uty = U.T @ y
    #     return np.sum(self.d*Utx*Uty)   
    
    def quadratic_form(self, xs): 
        """
        Compute :math: `xs.T U D U^T xs`
        """
        Utx = U.T @ x
        return np.sum(self.d*Utx*Utx)   


#exp(-diff.T M(i) diff)

#What is M(i):
#Let C^-1 be the prior hessian
# THen, what we have is:

#C^{-1} Hess_neg_log_like_pdf(x_i)

# Hess_neg_log_like_pdf is eiether pre decomposed,
# i.e. via eigendecomposition, or we have its action 



# class AverageHessian(LinearOperator):
#     r"""The Average Hessian operator. enables Hessian action that is based on the average likelihood (or is it posterior) hesss of N particles
#     """
#     def __init__(self, posterior, particles):
#         self.posterior = posterior
#         self.particles = particles

#     def mult(self, phat, **kwargs):
#         # returns average Hessian action: 1/N * \sum_n H_n phat hess(self, phat, n)
#         averageHessian_phat = 0.0
#         for particle in self.particles: 
#             self.posterior.hess_eval_pt = particle
#             averageHessian_phat += self.posterior.hess_logpdf_action(phat)
#         return averageHessian_phat/self.number_particles


class _Metric(LinearOperator): 
    """A metric used for Kernels
    """

    def (self, eval_pt, pt):
        #Returns metric weighted norm of phat, as well as M*phat
        return self.inner(phat, phat, save_matvec=True)


class IdentityMetric(_Metric):
    r"""
    Define the Identity Metric.
    """
    def __init__(self):
        super(IdentityMetric, self).__init__(manifold, options)

    def mult(self, phat, **kwargs):
        return phat

class DiagonalHessianMetric(_Metric): 
#For example prior precision
    r"""
    Define the metric M(p_i), with quadratic form for point p_i: (p_hat)^T M(p_i) * (p_hat)
    and inner product for point p_i: (phat_1, phat_2)  = (phat_1)^T M(p_i) * (phat_2)
    Here the metric is the prior Precision/the dimension of the space. Thus, it is not point dependent
    """
    def __init__(self, prior, options):
        super(_Metric, self).__init__(prior, options)

    # def mult(self, phat, **kwargs): #Prior Precision * phat
    #     if kwargs.get('eval_metric_at') is not None:
    #         self.metric_eval_pt = eval_metric_at
    #     M_p_hat = self.manifold.hess_logpdf_action(phat, gaussnewton=self.options['gauss_newton_approx']) 

    #     # 2. rescaling by dimension
    #     if self.options['rescaling']:
    #         M_p_hat *= (1/self.dim)
    #     return M_p_hat
    def mult(self, eval_point, )
    def quadratic_form

#new particles
#get gradients, which gives neglogpdfs as well
#reduce on 0 to get 'cost'

#calculate the diffs on the last rank
#Since the Kernel.metric.quadratic_form is updated by the gradient
#Kernel.grad_gram_matrix(diffs), which gives gram_matrix for free
#
#1) 'vectorized?'
#which in the processes computes the either metric.quadratic forms
#via either metric.quadratic_forms(diffs) (which goes throug hall hessians Nx N-1/2)
#or if the option of 
#metric has to have the ability to do eigendecomposition
#LowRankHessianMetric(), these objects should be very lightweight objects, practically
#jit compiled. 

#2) 
#Only need mult(eval_pt, diffs), which we wont really use,
#and quadrtic form, which will store mults as well
#Then the kernel will use the mults part of the metric.quadraticforms
#to calculate the grad_gram_matrix(diffs)  (two separate functions called within grad_gram_matrix? )

#3) These grad/gram matrix entires etc will be stored in Kernel

#4) define product and grad_product functions? or just do it with numpy/ what about the row sums which will be parallel accros columns???

class HessianMetric(MetricBase):
    r"""
    Define the metric M(p_i), with norm_squared for point p_i (p_hat)^T M(p_i) * (p_hat)
    and inner product for point p_i between two vectors phat_1 and phat_2 (phat_1)^T M(p_i) * (phat_2)
    Here the metric is different for each point, and defined by the Posterior Hessian at each point/the dimension of the space.
    """
    def __init__(self, posterior, options):
        super(PosteriorHessianMetric, self).__init__(posterior, options)

    def mult(self, phat, **kwargs):
        r"""
        For each particle in the metric evaluation point, evaluate the hess action against 
        `its portion' of phat
        """
        if kwargs.get('eval_metric_at') is not None:
            self.metric_eval_pt = eval_metric_at
        M_p_hat = self.manifold.hess_logpdf_action(phat, gaussnewton=self.options['gauss_newton_approx'])

        # 2. rescaling by dimension
        if self.options['rescaling']:
            M_p_hat *= (1/self.dim)
        return M_p_hat

#This is instantiated on each worker
class AveragedPosteriorHessianMetric(MetricBase):
    # define the metric M for the kernel k_ij = exp(-diff_ij.TT * M * diff_ij)
    def __init__(self, posterior, options):
        super(AveragedPosteriorHessianMetric, self).__init__(posterior, options)

    def mult(self, diff, **kwargs):
        # REQUIRES particles keyword argument
        # returns average Hessian action: 1/N * \sum_n H_n phat hess(self, phat, n)

        averageHessian_phat = np.zeros_like(diff)
        for particle in particles: 
            averageHessian_phat += self.manifold.hess_neg_logpdf_action(particle, diff, gaussnewton=self.options['gauss_newton_approx'])
        

        for particle in particles: 
            averageHessian_phat += 
            averageHessian_phat/=len(particles)
        
        # 2. rescaling by dimension
        if self.options['rescaling']:
            averageHessian_phat *= (1/self.dim)
        return averageHessian_phat


        # pout.zero()
        # covariance/regularization
#        # phelp = self.model.generate_vector(PARAMETER)
#        temp = self.posterior.prior.hess_logpdf_action(phat)
#        # pout.axpy(1.0, phelp)
#        # negative log likelihood function
#        if self.low_rank_Hessian_misfit == 2:
#            # phelp_1 = self.model.generate_vector(PARAMETER)
#            hess_misfit = LowRankOperator(self.variation.d_average, self.variation.U_average)
#            temp = hess_misfit.mult(pout)
#            H_phat = self.posterior.prior.hess_logpdf_action(temp)
#
#            # # 1. no rescaling
#
#            # # 2. rescaling by dimension
#            H_phat /= self.model.problem.Vh[PARAMETER].dim()
#
#            # # 3. rescaling by the trace = trace(I) + trace(H), the following is the correct trace
#            # H_phat / = (self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d_average))
#
#            # print("dim = ", self.model.problem.Vh[PARAMETER].dim(), "trace", np.sum(variation.d[n]))
#        else:
#            if self.low_rank_Hessian_misfit == 1:
#                hess_misfit = LowRankOperator(self.variation.d_average, self.variation.U_average)
#                hess_misfit.mult(phat, phelp)
#            else:
#                for n in range(self.particle.number_particles):
#                    self.variation.hess(phat, phelp, n)
#                    pout.axpy(1.0/self.particle.number_particles, phelp)
#
#            # # 1. no rescaling
#
#            # # 2. rescaling by dimension
#            H_phat /= self.model.problem.Vh[PARAMETER].dim()
#
#            # # 3. rescaling by the trace = trace(I) + trace(H), however the following is not the right trace
#            # pout[:] *= 1/(self.model.problem.Vh[PARAMETER].dim() + np.sum(variation.d_average))
#
#            # print("dim = ", self.model.problem.Vh[PARAMETER].dim(), "trace", np.sum(variation.d[n]))
