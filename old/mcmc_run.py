from __future__ import absolute_import, division, print_function

import tensorflow as tf
import numpy as np
import time
import sys
import os


# Parse run specifications

from distributions import *
import prngs.parallel as random
from algorithms import *
from mcmc import *



comm = None
rank = 0
def xavier_gaussian_prior(likelihood,comm ):
    w_dimension = likelihood.dims
    n_layers = likelihood.n_layers
    w_dims = likelihood.w_dims

    # Mean vector for weights
    mu = np.zeros(w_dimension)
    # Diagonal covariance for weights (represented as vector)
    varinv_coefficients = []
    weight_var = 1.
    bias_var = 1.
    for i in range(n_layers):
        varinv_coefficients.append(weight_var)
        varinv_coefficients.append(bias_var)
        weight_var *=0.5
        bias_var *= 0.5

#     print(varinv_coefficients)
    sigma_inv = np.zeros(w_dimension)
    index = 0
    for coef,dim in zip(varinv_coefficients,w_dims):
        sigma_inv[index:index+dim] = coef*np.ones(dim)
        index += dim
    return  Gaussian(mu,sigma_inv,comm = comm)

N_data = 500


sys.path.append('../nn_stein/')
from mnist import load_mnist
from tfLikelihood import tfLikelihood
from neuralNetwork import GenericCED, GenericCAE 

likelihood = tfLikelihood(settings,GenericCED,data,comm = comm)
# Sample from "Xavier Gaussian Prior"
prior = xavier_gaussian_prior(likelihood,comm)
n_dim = likelihood.dims





stochastic_newton_params = {
        'burn_in': args.burn_in ,
        'nsamples': 100000, #100k samples
        'rank_reduction': args.rank_reduction,  #how much of the full rank do we want, for the neural network, probably rank 100-200? but this fraction (10 => 1/10th)
        'oversample': 0,
        'checkpoint_iter':100 #save mu/cov every 1000 MCMC samples
}


post = Posterior(prior=prior, likelihood=likelihood)

name = "nn_mcmc_"+args.data_set+str(args.n_filters[0])+'_'+str(len(args.n_filters))

stochastic_newton_mcmc(prior, post, name , return_chain=False, params=stochastic_newton_params)

##### CHECK THAT IT IS SAVING TO THE FOLDER, the means and covariances!!!