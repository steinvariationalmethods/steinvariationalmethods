from __future__ import absolute_import, division, print_function
import math
import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys
from scipy.spatial.distance import cdist
import numpy.matlib
import time


def SVN_H(x, stepsize, itermax, model,preconditioned=False):
    
    N = x.shape[1]
    
    # Initialise particle maximum shifts
    maxshift = np.zeros(N)
    maxmaxshift_old = np.inf
    
    # Initialise average computational time
    timeave = 0;
    
    # Identity matrix
    I = np.identity(model.dim)
    #print('   step norm        mean           min            median' )
    log_posterior = np.zeros(N)
    for k in range(itermax):
        tic = time.time()
        
        g_mlpt = np.zeros((model.dim, N))
        gnH = np.zeros((model.dim,model.dim,N))
        
        for j in range(N):
            logpost_pdf,logpost_grad,logpost_hess = model.logpost_stat(x[:,j])
            g_mlpt[:,j] = -logpost_grad
            gnH[:,:,j]  = -logpost_hess
        
        # Averaging Hessian approximation
        sEH = np.mean(gnH,axis=2)  / model.dim
        
        x_copy = x.copy()
        Q = np.zeros( (model.dim, N) )
        kernel = np.zeros((N,N))
        gkernel = np.zeros((N,N,model.dim))
        for i in range(N):
            sign_diff = x[:,i,np.newaxis] - x_copy
            Msd   = np.matmul(sEH, sign_diff)
            kernel[i,:]  = np.exp( - 0.5 * np.sum( sign_diff * Msd, 0 ) )
            gkernel[i,:,:] = (Msd * kernel[i,:]).T
        sum_k = 1.#np.sum(kernel,axis=1)
        sum_gkernel = np.sum(gkernel,axis = 0)/np.sum(kernel,axis=1)[:,np.newaxis]
        for i in range(N):       
            kern  = kernel[i,:]#np.exp( - 0.5 * np.sum( sign_diff * Msd, 0 ) )
            gkern = gkernel[i,:,:].T #Msd * kern

            mgJ = np.mean(- g_mlpt * kern + gkern , 1)
            HJ  = np.mean(gnH * kern * sum_k, 2) + np.matmul(gkern, sum_gkernel) /N
            if preconditioned:
                HJ = np.dot(model.prior_C0,HJ)
                mgJ = np.dot(model.prior_C0,mgJ)
            Q[:,i] = np.linalg.solve(HJ, mgJ)
            maxshift[i] = np.linalg.norm(Q[:,i], np.inf)
        for i in range(N):
            kern_i = kernel[i,:]
            step = np.sum(Q * np.tile(kern_i[np.newaxis,:],[model.dim,1]),axis = 1)/np.sum(kernel,axis=1)[i]
                
            x[:,i] += step
            log_posterior[i],_,_ = model.logpost_stat(x[:,i])
            
            
#         for i in range(N):
#             step = 0.
#             sign_diff = x[:,i,np.newaxis] - x
#             Msd   = np.matmul(sEH, sign_diff)
#             kern = np.exp( - 0.5 * np.sum( sign_diff * Msd, 0 ) )
#             for j in range(N):
#                 step +=Q[:,j] *kern[j]
                
#             x[:,i] += step /np.sum(kernel,axis=1)[i]
#             log_posterior[i] = model.log_post(x[:,[i]])

 
        maxmaxshift = np.max(maxshift);
        #print('Maximum shift is', maxmaxshift)
        
        
        
        print(k,np.linalg.norm(Q),np.mean(log_posterior))
        #print(k,np.linalg.norm(Q),np.mean(log_posterior),log_posterior.min(),np.median(log_posterior))
        
        if np.isnan(maxmaxshift) or maxmaxshift > 1e50:
            stepsize = 0.1*stepsize
            print('Step size too large; scaling it by factor 10.\n epsilon = ', stepsize)
            print('Reset particles... \n')
            x = model.prior_m[:,np.newaxis] + np.dot(model.prior_C0sqrt,np.random.randn(model.dim,N))
        
        # Update stepsize
        if maxmaxshift >= maxmaxshift_old:
            stepsize = 0.9*stepsize
        elif np.abs(maxmaxshift - maxmaxshift_old) < 1e-6:
            stepsize = 1.01*stepsize
    
        maxmaxshift_old = maxmaxshift
        
        # Last iteration
        if k == itermax:
            print('Maximum number of iterations has been reached.\n')
    timeave += time.time() - tic

    timeave = timeave / itermax;
    return x, stepsize, timeave


    