
from __future__ import absolute_import, division, print_function
import math
import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys
from scipy.spatial.distance import cdist 
import numpy.matlib
import time

class ModelSetup(object):
    
    def __init__(self,design):
        # set up fwd function parameters
        self.num_component = design[0]
        self.dim = design[1]
        self.weights = design[2]
        self.m = 1
        
        
        # set up prior 
        #self.prior_m = np.zeros((self.dim,1))
        self.prior_m = design[3]
        self.prior_sigma = design[4]
        self.prior_C0 = self.prior_sigma * np.identity(self.dim)
        self.prior_C0sqrt  = np.sqrt(self.prior_C0) 
        self.prior_C0i = np.linalg.inv(self.prior_C0)
        self.prior_C0isqrt = np.sqrt(self.prior_C0i)
        
        # # set up observations
        # # Number of independent observations
        # self.obs_nobs = 1
        # # Noise standard deviation
        # self.obs_std = 0.3
        # # Nose variance
        # self.obs_std2 = self.obs_std**2
        # # Noise
        # self.obs_noise = self.obs_std*np.random.randn(self.m, self.obs_nobs) 

        self.post_m = design[5]
        self.post_sigma = design[6]
        # self.post_C = self.post_sigma * np.identity(self.dim)
        # self.post_Ci = np.linalg.inv(self.post_C)

        # Real parameter vector
        # self.obs_u_true = np.random.rand(self.dim,1)
        # self.obs_y_true = self.fwd(self.obs_u_true)
        # # Observations
        # self.obs_y = self.obs_y_true + self.obs_noise
        
        #self.obs_u_true = np.array([0.7203,.0001])
        #self.obs_y = 3.1007
        
    # def fwd(self, u):
    #     f = np.log(np.sum((self.a - u)**2 /self.h) + 1.)
    #     return f
    
    # def fwd_solve(self, u):
    #     Fx = self.fwd(u)
        
    #     exp_F = np.exp(Fx)
    #     J = 2./ exp_F *(u-self.a)/self.h

    #     return Fx,J

    
    
    # def grad_mllkd(self, Fx, J):
    #     g_mllkd = np.sum(np.dot(J,(Fx - self.obs_y)),axis=1) / self.obs_std2
    #     return g_mllkd.reshape(self.dim,1)

    def post_stat(self,x):

        pdf = 0.
        grad = 0.
        hess = 0.

        for mu,weight,sigma in zip(self.post_m,self.weights,self.post_sigma):
            post_C = sigma * np.identity(self.dim)
            post_Ci = np.linalg.inv(post_C)
            diff = x - mu
            # y = np.exp(-0.5*np.dot(diff,diff)/self.post_sigma)
            # y = np.exp(-0.5*np.dot(np.dot(diff,self.prior_C0i),diff))
            y = np.exp(-0.5*np.dot(diff/sigma,diff))
            c = ((2.*np.pi)**self.dim*np.linalg.det(post_C))**(-0.5)
            pdf_tmp = y*c
            pdf += weight*pdf_tmp 
            grad_tmp = -diff/sigma*pdf_tmp
            grad += weight*grad_tmp
            hess_tmp = (-post_Ci)*pdf_tmp +np.dot(grad_tmp[:,np.newaxis], (-diff/sigma).reshape(1,self.dim)) 
            hess += weight*hess_tmp
        post_pdf = pdf #/ self.num_component
        post_grad = grad #/ self.num_component
        post_hess = hess #/ self.num_component
        return post_pdf,post_grad , post_hess

    def logpost_stat(self,x):
         post_pdf,post_grad, post_hess = self.post_stat(x)
         logpost_pdf = np.log(post_pdf)
         logpost_grad = post_grad / post_pdf
         logpost_hess = -post_pdf**(-2) * np.dot(post_grad[:,np.newaxis],post_grad[np.newaxis,:]) + 1./post_pdf*post_hess  
         return logpost_pdf,logpost_grad,logpost_hess

    def post_moments(self):
        post_mean = np.sum(self.post_m*self.weights[:,np.newaxis],axis=0)
        post_c_mean = self.post_m - post_mean
        post_var =  np.diag(np.sum(self.post_sigma*self.weights[:,np.newaxis],axis=0))+np.einsum("ni,nj->ij", post_c_mean * self.weights[:, np.newaxis], post_c_mean)
        #np.sum((self.post_m**2+self.post_sigma)*self.weights[:,np.newaxis],axis=0)-post_mean**2
        return post_mean,post_var

    def sample_moments(self,x):
        sample_mean = np.mean(x,axis=1)
        sample_var = np.var(x,axis=1)
        return sample_mean,sample_var
        

    
    # def grad_mlpt(self, x): 
    #     diff = x-self.mu  
    #     g_mlpt = self.pdf(x) * (x-self.mu)
    #     return g_mlpt
    
    
    def postdcontour(self, x_2d, dim,mcn):

        post = np.zeros(x_2d.shape[1]);
        for j in range(x_2d.shape[1]):
            
            for nn in range(mcn):
                
                xj = self.prior_m + np.dot(self.prior_C0sqrt,np.random.randn(self.dim))    
                xj[dim] = x_2d[:,j]
        
                pdf = 0.

                for mu,weight,sigma in zip(self.post_m,self.weights,self.post_sigma):
                    post_C = sigma * np.identity(self.dim)
                    post_Ci = np.linalg.inv(post_C)
                    diff = xj - mu
                    y = np.exp(-0.5*np.dot(diff/sigma,diff))
                    c = (2.*np.pi*np.linalg.det(post_Ci))**(-self.dim/2.)
                    pdf += weight*y*c
                post[j] += pdf
                #print(j,np.exp( -(mllkd + mlprr) ))
        post = post / mcn
        return post
    
    def post2contour(self, x):
    
        post = np.zeros((x.shape[1], 1));
    
        for j in range(x.shape[1]):
        
            pdf = 0.

            for mu,weight,sigma in zip(self.post_m,self.weights,self.post_sigma):
                post_C = sigma * np.identity(self.dim)
                post_Ci = np.linalg.inv(post_C)
                diff = x[:,j]- mu
                y = np.exp(-0.5*np.dot(diff/sigma,diff))
                c = (2.*np.pi*np.linalg.det(post_Ci))**(-self.dim/2.)
                pdf += weight*y*c
            post[j] = pdf
        return post  
        
    # def misfit_cost(self,x):
    #     cost = .5 * np.sum((x-self.obs_y)**2 )/ self.obs_std2
    #     return cost

    # def prior_cost(self,x):
    #     cost = 0.5*np.dot(np.dot((x - self.prior_m).transpose(),self.prior_C0i),(x - self.prior_m))
    #     return cost
    
    
    # def log_post(self,x):
    #     log_prior = self.prior_cost(x)
        
    #     Fx = self.fwd(x)
 
    #     log_misfit = self.misfit_cost(Fx)
         
    #     y = -log_prior - log_misfit
        
    #     return y
    
    
    
    