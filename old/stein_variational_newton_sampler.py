from __future__ import absolute_import, division, print_function
import math
import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys
from scipy.spatial.distance import cdist
import numpy.matlib
import time


def SVN_H(x, stepsize, itermax, model):
    # Number of particles
    N = x.shape[1]
    
    # Initialise particle maximum shifts
    maxshift = np.zeros(N)
    maxmaxshift_old = np.inf
    
    # Initialise average computational time
    timeave = 0;
    
    # Identity matrix
    I = np.identity(model.n)
    
    
    for k in range(itermax):
        tic = time.time()
        
        g_mlpt = np.zeros((model.n, N))
        gnH = np.zeros((model.n,model.n,N))
        
        for j in range(N):
            #Fx,J = model.fwd_solve(x[:,[j]])
            #g_mlpt[:,[j]] = model.grad_mlpt(x[:,[j]], Fx, J)
            #gnH[:,:,j]  = model.prior_C0i + np.dot(J.transpose(),J) / model.obs_std2
            Fx,J = model.fwd_solve(x[:,[j]])
            g_mlpt[:,[j]] = model.grad_mlpt(x[:,[j]], Fx, J)
            gnH[:,:,j]  = model.prior_C0i + model.obs_nobs*np.dot(J,J.transpose()) / model.obs_std2
        
        # Averaging Hessian approximation
        sEH = np.mean(gnH,axis=2) / model.n
        
        x_copy = x.copy()
        log_posterior = 0.
        
        for i in range(N):
            sign_diff = x[:,[i]] - x_copy
            
            kern = np.exp(-0.5*np.sum((np.dot(sign_diff.transpose(),sEH)*sign_diff.transpose()), axis = 1))
            
            g_kern = np.dot(sEH,sign_diff) * kern
            
            mgrad_J = np.mean( -kern * g_mlpt + g_kern, axis=1)
            
            H_J = np.mean( np.transpose( np.tile(kern[:,np.newaxis],[model.n,1,model.n]), (0, 2, 1)  ) * gnH , axis=2 ) + sEH*np.mean(kern)
            
            Q = np.linalg.solve(H_J, mgrad_J)
            
            x[:,i] = x[:,i] + stepsize*Q
            
            log_posterior += model.log_post(x[:,[i]])
            
            maxshift[i] = np.linalg.norm(Q, np.inf)
        print(Q,log_posterior/N,mgrad_J)
    
        maxmaxshift = np.max(maxshift);
        #print('Maximum shift is', maxmaxshift)
        #print(np.linalg.norm(Q))
        
        if np.isnan(maxmaxshift) or maxmaxshift > 1e50:
            stepsize = 0.1*stepsize
            print('Step size too large; scaling it by factor 10.\n epsilon = ', stepsize)
            print('Reset particles... \n')
            x = model.prior_m + np.dot(model.prior_C0sqrt,np.random.randn(model.n,N)) 
        
        # Update stepsize
        if maxmaxshift >= maxmaxshift_old:
            stepsize = 0.9*stepsize
        elif np.abs(maxmaxshift - maxmaxshift_old) < 1e-6:
            stepsize = 1.01*stepsize

        maxmaxshift_old = maxmaxshift
    
        # Last iteration
        if k == itermax:
            print('Maximum number of iterations has been reached.\n')
        timeave += time.time() - tic

    timeave = timeave / itermax;

    return x, stepsize, timeave







def SVN_H_full(x, stepsize, itermax, model):

    N = x.shape[1]
    
    # Initialise particle maximum shifts
    maxshift = np.zeros(N)
    maxmaxshift_old = np.inf
    
    # Initialise average computational time
    timeave = 0;
    
    # Identity matrix
    I = np.identity(model.n)
    
    
    for k in range(itermax):
        tic = time.time()
        
        g_mlpt = np.zeros((model.n, N))
        gnH = np.zeros((model.n,model.n,N))
        
        for j in range(N):
            Fx,J = model.fwd_solve(x[:,[j]])
            g_mlpt[:,[j]] = model.grad_mlpt(x[:,[j]], Fx, J)
            gnH[:,:,j]  = model.prior_C0i + model.obs_nobs*np.dot(J,J.transpose()) / model.obs_std2
        
        # Averaging Hessian approximation
        sEH = np.mean(gnH,axis=2) / model.n
        
        x_copy = x.copy()
        

        dims = N*model.n

        H_J_full = np.zeros((dims,dims))
        H_J_full_m = np.zeros((dims,dims))
        H_J_full_d = np.zeros((dims,dims))
        mgrad_J_full = np.zeros(dims)

        for i in range(N):
            sign_diff = x[:,[i]] - x_copy

            kern = np.exp(-0.5*np.sum((np.dot(sign_diff.transpose(),sEH)*sign_diff.transpose()), axis = 1))

            g_kern = np.dot(sEH,sign_diff) * kern

            mgrad_J = np.mean( -kern * g_mlpt + g_kern, axis=1)

            Mx = np.dot(sEH,sign_diff)

            res = np.mean(kern*np.tile(Mx[:,np.newaxis,:],[1,model.n,1])*np.tile(Mx[np.newaxis,:,:],[model.n,1,1]),axis = 2)

            index1 = i*model.n
            index2 = (i+1)*model.n

            H_J = np.transpose( np.tile(kern[:,np.newaxis],[model.n,1,model.n]), (0, 2, 1)  ) * gnH  + np.tile(sEH[:,:,np.newaxis],[1,1,N])*np.tile(kern[np.newaxis,np.newaxis,:],[model.n,model.n,1]) 

            for j in range(N):
                index_1 = j*model.n
                index_2 = (j+1)*model.n
                H_J_full[index1:index2,index_1:index_2] = H_J[:,:,j]/np.float(N)
            H_J_full_m[index1:index2,index1:index2] = np.mean(H_J,axis = 2)
            #H_J_full_d[index1:index2,index1:index2] = H_J[:,:,i]/np.float(N)
            mgrad_J_full[index1:index2] = mgrad_J
        H_J_full_m_inv = np.linalg.inv(H_J_full_m)
        #H_J_full_d_inv = np.linalg.inv(H_J_full_d)
        #H_inv = np.diag(np.diag(H_J_full_m))
        Q,info = scipy.sparse.linalg.bicgstab(H_J_full, mgrad_J_full,tol=1e-01, M = H_J_full_m_inv)
        #Q,info = scipy.sparse.linalg.cg(H_J_full, mgrad_J_full,tol=1e-1)


        log_posterior = 0.

        for i in range(N):

            index1 = i*model.n
            index2 = (i+1)*model.n

            x[:,i] = x[:,i] + stepsize*Q[index1:index2]

            log_posterior += model.log_post(x[:,[i]])
        print(Q[index1:index2],info,log_posterior/N,mgrad_J)

    
        # Last iteration
        if k == itermax:
            print('Maximum number of iterations has been reached.\n')
        timeave += time.time() - tic

    timeave = timeave / itermax;
    return x, stepsize, timeave
