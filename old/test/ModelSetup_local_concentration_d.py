
from __future__ import absolute_import, division, print_function
import math
import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys
from scipy.spatial.distance import cdist 
import numpy.matlib
import time

class ModelSetup(object):
    
    def __init__(self):

        a = 1.
    
        para_dim = 4

        #h = 0.1
        h = np.ones((para_dim,1))
        for i in range(para_dim):
            h[i] = 0.1*(i+1.)**2

        nobs = 1
        design = [a,h,para_dim,nobs]
        # set up fwd function parameters
        self.dim = design[2]
        self.m = 1
        
        self.a = design[0]
        self.h = design[1]
        
        # set up prior 
        #self.prior_m = np.zeros((self.dim,1))
        self.prior_m = -np.ones((self.dim,1))
        self.prior_C0 = np.identity(self.dim)
        self.prior_C0sqrt  = np.sqrt(self.prior_C0) 
        self.prior_C0i = np.linalg.inv(self.prior_C0)
        self.prior_C0isqrt = np.sqrt(self.prior_C0i)
        
        # set up observations
        # Number of independent observations
        self.obs_nobs = design[3]
        # Noise standard deviation
        self.obs_std = 0.3
        # Nose variance
        self.obs_std2 = self.obs_std**2
        # Noise
        self.obs_noise = self.obs_std*np.random.randn(self.m, self.obs_nobs) 
        # Real parameter vector
        self.obs_u_true = np.random.rand(self.dim,1)
        self.obs_y_true = self.fwd(self.obs_u_true)
        # Observations
        self.obs_y = self.obs_y_true + self.obs_noise
        
        #self.obs_u_true = np.array([0.7203,.0001])
        #self.obs_y = 3.1007
        
    def fwd(self, u):
        f = np.log(np.sum((self.a - u)**2 /self.h) + 1.)
        return f
    
    def fwd_solve(self, u):
        Fx = self.fwd(u)
        
        exp_F = np.exp(Fx)
        J = 2./ exp_F *(u-self.a)/self.h

        return Fx,J

    
    
    def grad_mllkd(self, Fx, J):
        g_mllkd = np.sum(np.dot(J,(Fx - self.obs_y)),axis=1) / self.obs_std2
        return g_mllkd.reshape(self.dim,1)
    
    def grad_mlpt(self, x, Fx, J):   
        g_mlpt = np.dot(self.prior_C0i,(x - self.prior_m)) + self.grad_mllkd(Fx, J)
        return g_mlpt

    def hess_pt(self, J):

        hess= self.prior_C0i + self.obs_nobs*np.dot(J,J.transpose()) / self.obs_std2
        return hess

    def get_grad_hess(self,x):
        Fx,J = self.fwd_solve(x)
        g_mlpt = self.grad_mlpt(x, Fx, J)
        gnH = self.hess_pt(J)
        return g_mlpt,gnH

    
    
    def postdcontour(self, x_2d, dim,mcn):

        post = np.zeros(x_2d.shape[1]);
        for j in range(x_2d.shape[1]):
            
            for nn in range(mcn):
                
                xj = self.prior_m + np.dot(self.prior_C0sqrt,np.random.randn(self.dim,1))    
                xj[dim] = x_2d[:,[j]]
        
                mlprr = 0.5*np.dot(np.dot((xj - self.prior_m).transpose(),self.prior_C0i),(xj - self.prior_m))
        
                Fu = self.fwd(xj)
 
                misfit = (self.obs_y - Fu) / self.obs_std
                mllkd  = 0.5*np.sum(misfit**2)
                post[j] = post[j] + np.exp( -(mllkd + mlprr) )
                #print(j,np.exp( -(mllkd + mlprr) ))
        post = post / mcn
        return post
    
    def post2contour(self, x):
    
        post = np.zeros((x.shape[1], 1));
    
        for j in range(x.shape[1]):
        
            mlprr = 0.5*np.dot(np.dot((x[:,[j]] - self.prior_m).transpose(),self.prior_C0i),(x[:,[j]] - self.prior_m))
        
            Fu = self.fwd(x[:,[j]])
 
            misfit = (self.obs_y - Fu) / self.obs_std
            mllkd  = 0.5*np.sum(misfit**2)
            post[j] = np.exp( -(mllkd + mlprr) )
        return post
        
    def misfit_cost(self,x):
        cost = .5 * np.sum((x-self.obs_y)**2 )/ self.obs_std2
        return cost

    def prior_cost(self,x):
        cost = 0.5*np.dot(np.dot((x - self.prior_m).transpose(),self.prior_C0i),(x - self.prior_m))
        return cost
    
    
    def log_post(self,x):
        log_prior = self.prior_cost(x)
        
        Fx = self.fwd(x)
 
        log_misfit = self.misfit_cost(Fx)
         
        y = -log_prior - log_misfit
        
        return y
    
    
    
    