# Stein-Variational-samplers
ArXiv: https://arxiv.org/abs/1806.03085 -- NIPS 2018

MATLAB and Python code for Stein Variational Newton method and comparisons with Stein Variational Gradient Descent.

Test cases:
- Double banana -- two-dimensional, multi-modal banana shaped.
- Non-linear regression -- two-dimension, skewed shaped.
- Conditional diffusion -- 100 dimensional, Brownian path reconstruction out of noisy observations of the solution of a Langevin stochastic   differential equation.
- Bayesian Neural Network -- 2951 dimensional on a real data-set with 308 data points.
