# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

from __future__ import absolute_import, division, print_function

import numpy as np
# from ..modeling.variables import PARAMETER
# from .metric import MetricPrior, MetricPosteriorSeparate, MetricPosteriorAverage #,MetricPosteriorAverageLowrank

import os
# if not os.path.isdir("data"):
#     os.mkdir("data")
# import pickle

class Kernel:
    # evaluate the value and the gradient of the kernel at given particles
    # def __init__(self):

        # self.metric = metric
        # self.options = options
        # self.save_kernel = options["save_kernel"]
        # self.type_Hessian = options["type_Hessian"]
        # self.type_metric = options["type_metric"]

        # # metric M in k(pn,pm) = exp(-(pn-pm)^T * M * (pn-pm))
        # if self.type_metric is 'prior':
        #     self.metric = MetricPrior(model, particle, variation, options)
        # elif self.type_metric is 'posterior_average_lowrank':
        #     self.metric = MetricPosteriorAverageLowrank(model, particle, variation, options)
        # elif self.type_metric is 'posterior_average':
        #     self.metric = MetricPosteriorAverage(model, particle, variation, options)
        # elif self.type_metric is 'posterior_separate':
        #     self.metric = MetricPosteriorSeparate(model, particle, variation, options)
        # else:
        #     raise NotImplementedError("required metric is not implemented")

        # self.value_set = []
        # for n in range(particle.number_particles_all):
        #     values = np.zeros(particle.number_particles_all)
        #     self.value_set.append(values)

        # if self.save_kernel:
        #     self.gradient_set = []
        #     for n in range(particle.number_particles_all):
        #         gradients = [None] * particle.number_particles_all
        #         self.gradient_set.append(gradients)

        # self.value_sum = np.zeros(particle.number_particles)
        # self.gradient_sum = [self.model.generate_vector(PARAMETER) for ie in range(particle.number_particles)]

        # self.phelp = model.generate_vector(PARAMETER)

    # def update(self, particle, variation):

    #     self.particle = particle
    #     self.variation = variation
    #     self.metric.update(particle, variation)

    #     # update the size of the data if new particles are added
    #     if particle.number_particles_all > particle.number_particles_all_old:
    #         self.value_set = []
    #         for n in range(particle.number_particles_all):
    #             values = np.zeros(particle.number_particles_all)
    #             self.value_set.append(values)

    #         if self.save_kernel:
    #             self.gradient_set = []
    #             for n in range(particle.number_particles_all):
    #                 gradients = [None] * particle.number_particles_all
    #                 self.gradient_set.append(gradients)

    #     # save the evaluations of the kernel and its gradient if needed
    #     if self.save_kernel:
    #         self.value_sum = np.zeros(particle.number_particles_all)
    #         self.gradient_sum = [self.model.generate_vector(PARAMETER) for ie in range(particle.number_particles_all)]
    #         # evaluate the values and gradients of the kernel
    #         for n in range(particle.number_particles_all):
    #             values = self.values(n)
    #             # print("kernel values = ", values)
    #             gradients = self.gradients(n)

    #             self.value_set[n] = values
    #             self.gradient_set[n] = gradients

    #             if n < particle.number_particles:
    #                 for ie in range(particle.number_particles_all):
    #                     self.value_sum[ie] = self.value_sum[ie] + values[ie]
    #                     self.gradient_sum[ie].axpy(1.0, gradients[ie])
    def value(self, sign_diff, sEH):
        # evaluate the kernel k(x,y) = exp(-(x-y)^T * M * (x-y))
        value = np.exp(-np.sum((np.dot(sign_diff.transpose(),sEH)*sign_diff.transpose()), axis = 1))

        return value

#     def values(self, n=0):
#         # evaluate the kernel at (pn, pm) for given n and all m = 1, ..., N
#         values = np.zeros(self.particle.number_particles_all)
#         for m in range(self.particle.number_particles_all):
#             if m != n and self.delta_kernel:  # set zero of small kernels
#                 values[m] = 0
#             else:
#                 values[m] = self.value(n=n, m=m)
# #        print("kernel values = ", values)
#         self.value_set[n] = values

#         return values

    def gradient(self, kern_i,sign_diff,sEH):
        kern_g =  2.*np.transpose((np.dot(sEH,sign_diff) * kern_i),(1,0))

        return kern_g

    # def gradients(self, n=0):
    #     # evaluate the gradients of kernel at (pn, pm) with respect to pm, for all m = 1, ..., N
    #     gradients = [self.model.generate_vector(PARAMETER) for m in range(self.particle.number_particles_all)]
    #     for m in range(self.particle.number_particles_all):
    #         if m != n and self.delta_kernel:
    #             gradients[m].zero()
    #         else:
    #             gradients[m].axpy(1.0, self.gradient(n=n, m=m))

    #     if self.save_kernel:
    #         self.gradient_set[n] = gradients

    #     return gradients

    # def save_values(self, save_number, it):
    #     for n in range(save_number):
    #         filename = 'data/kernel_' + str(n) + '_iteration_' + str(it) + '.p'
    #         pickle.dump(self.value_set, open(filename, 'wb'))

    # def hessian(self):
    #     raise NotImplementedError(" the hessian needs to be implemented ")
