# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

# A dictionary of options for dynamic stein variational methods

options = {
    # 1. options for initializing and dynamically adding particles in the optimization process
    "number_particles": 1,
    # number of initial particles used to construct the transport map
    "number_particles_add": 0,
    # number of additional particles not used in constructing the transport map, but used
    # e.g., to compute the normalization constant as independent samples pushed to posterior
    "add_number": 0,
    # if 0, do not add particles
    # the number of particles to be added sampled from Laplace distribution at each particle, 1, 2, 3, ...
    "add_step": 10,
    # add particles every n steps of optimization, n = add_step,
    # this should be changed to more suitable criteria
    "add_rule": 1,
    # if 1, add particles and use all of them to construct the transport map
    # if 2, add particles but only add the ones added in the previous step to construct the transport map
    # if 3, add particles but do NOT use them to construct the transport map

    # 2. options to construct the operator/Hessian of the linear system in each Newton step
    "type_Hessian": "lumped",
    # "full": assemble the fully coupled Hessian system, only work for NewtonCoupled
    # "lumped": add the Hessian in the same row to the diagonal one, work for both NewtonCoupled and NewtonSeparated
    # "diagonal": only use the diagonal part of the Hessian system, work for both NewtonCoupled and NewtonSeparated

    # 3. options for the Hessian misfit term
    "low_rank_Hessian_misfit": 1,  # choices are 0, 1, 2
    # if not 0, use low rank approximation of the Hessian of the misfit H
    # if 1, solve the eigenvalue problem for the Hessian of the misfit H
    # if 2, solve the generalized eigenvalue problem (H, R), R is the inverse of prior covariance
    "low_rank_Hessian_average": False,
    # if True, use low rank approximation of the average of the Hessian misfit
    "rank_Hessian_misfit": 20,
    # the rank of the Hessian of the misfit term
    "gauss_newton_approx": False,
    # if True, use Gauss Newton approximation of the Hessian of the misfit term, to make the system well-posed to solve
    "max_iter_gauss_newton_approx": 10,
    # the maximum number n of optimization steps to use gauss_newton_approx
    # if not 0, "gauss_newton_approx" is automatically set to True before reaching n

    # 4. options for the kernel
    "type_metric": "posterior_average",
    # "prior":  the prior covariance
    # "posterior_average": the average of the posterior covariance at all current particles;
    # "posterior_separate": the posterior covariance at each of the current particle;
    "type_scaling": 1,
    # 0: no scaling
    # 1: scale the metric by the parameter dimension
    # 2: scale the metric by the trace of the negative log posterior

    # "save_kernel": True,
    # if True, save the evaluations of the kernel and its gradient at every particle
    # set False for large numbers of both parameter dimension and particles, due to prohibitive memory usage
    
    # "delta_kernel": False,
    # if True, set k(pn, pm) = 0 for pn != pm, and k(pn, pn) = 1, in order to accelerate moving particles to posterior
    # "max_iter_delta_kernel": 0,
    # the maximum number n of optimization steps to use delta kernel
    # if not 0, "delta_kernel" is automatically set to True before reaching n

    # 5. options for saving data and stopping optimization
    # "save_step": 10,
    # save data every n steps
    # "save_number": 1,
    # the number of particles, kernels, eigenvalues to be generated and saved, if 0, do not save
    "rel_tolerance": 1e-4,
    # stop when sqrt(g,g)/sqrt(g_0,g_0) <= rel_tolerance
    "abs_tolerance": 1e-12,
    # stop when sqrt(g,g) <= abs_tolerance
    "gdm_tolerance": 1e-18,
    # stop when (g,dm) <= gdm_tolerance
    "inner_rel_tolerance": 1e-9,
    # relative tolerance used for the solution of the forward, adjoint, and incremental (fwd,adj) problems
    "max_iter": 100,
    # maximum number of iterations for the optimization
    "cg_coarse_tolerance": .5,
    # Coarsest tolerance for the CG method (Eisenstat-Walker)
    "line_search": True,
    # do line search if True
    "c_armijo": 1e-4,
    # Armijo constant for sufficient reduction
    "max_backtracking_iter": 10,
    # Maximum number of backtracking iterations
    "print_level": -1,
    # Control verbosity of printing screen
    "termination_reasons": [
        "Maximum number of Iteration reached",  # 0
        "Norm of the gradient less than tolerance",  # 1
        "Maximum number of backtracking reached",  # 2
        "Norm of (g, dm) less than tolerance"  # 3
    ]
    # the reasons for terminating the optimization
}
