# contact: Peng Chen, peng@ices.utexas.edu, written on Novemeber 19, 2018

from __future__ import absolute_import, division, print_function

import numpy as np
# from ..modeling.variables import PARAMETER


class Metric:
    def __init__(self, model):
        self.model = model

    def value(self, gnH):
        return np.mean(gnH,axis=2) / self.model.dim




# class HessianAverage:
#     # to compute one low rank decomposition of the metric
#     def __init__(self, model, particle, d, U, rank_Hessian_misfit):
#         self.model = model
#         self.number_particles = particle.number_particles
#         self.d = d
#         self.U = U
#         self.rank_Hessian_misfit = rank_Hessian_misfit
    
#     def mult(self, phat, pout):
#         phelp = self.model.generate_vector(PARAMETER)
#         if self.rank_Hessian_misfit == 1:  # pout = 1/N * \sum_n U_n Sigma_n U_n^T phat
#             for n in range(self.number_particles):
#                 hessian_misfit = LowRankOperator(self.d[n], self.U[n])
#                 hessian_misfit.mult(phat, phelp)
#                 pout.axpy(1.0 / self.number_particles, phelp)
    
#         elif self.rank_Hessian_misfit == 2:
#             phelp_1 = self.model.generate_vector(PARAMETER)
#             for n in range(self.number_particles):  # pout = 1/N * \sum_n R U_n Sigma_n U_n^T R phat
#                 self.model.prior.R.mult(phat, phelp)
#                 hessian_misfit = LowRankOperator(self.d[n], self.U[n])
#                 hessian_misfit.mult(phelp, phelp_1)
#                 self.model.prior.R.mult(phelp_1, phelp)
#                 pout.axpy(1.0 / self.number_particles, phelp)
#         else:
#             raise NotImplementedError("please choose between 0, 1, 2 for low_rank_Hessian_misfit")






# class MetricPrior:
#     # define the metric M for the kernel k(pn, pm) = exp(-(pn-pm)^T * M * (pn - pm))
#     def __init__(self, model, particle, variation, options):
#         self.model = model  # forward model
#         self.particle = particle
#         self.variation = variation
#         self.low_rank_Hessian_misfit = options["low_rank_Hessian_misfit"]
#         self.type_scaling = options["type_scaling"]

#     def mult(self, phat, pout, n):

#         pout.zero()
#         # covariance/regularization
#         phelp = self.model.generate_vector(PARAMETER)
#         self.model.prior.R.mult(phat, phelp)
#         pout.axpy(1.0, phelp)

#         if self.type_scaling == 1:
#             # rescaling by dimension
#             pout[:] *= 1. / self.model.problem.Vh[PARAMETER].dim()
#         elif self.type_scaling == 2:
#             # rescaling by the trace = trace(I) + trace(H), however the following is not the right trace
#              pout[:] *= 1./(self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d[n]))

#     def inner(self, phat1, phat2, n):

#         phelp = self.model.generate_vector(PARAMETER)
#         self.mult(phat1, phelp, n)

#         return phat2.inner(phelp)

#     def update(self, particle, variation):
#         self.particle = particle
#         self.variation = variation


# class MetricPosteriorSeparate:
#     # define the metric M for the kernel k(pn, pm) = exp(-(pn-pm)^T * M * (pn - pm))
#     def __init__(self, model, particle, variation, options):
#         self.model = model  # forward model
#         self.particle = particle
#         self.variation = variation
#         self.type_scaling = options["type_scaling"]

#     def mult(self, phat, pout, n):

#         pout.zero()
#         # covariance/regularization
#         phelp = self.model.generate_vector(PARAMETER)
#         self.model.prior.R.mult(phat, phelp)
#         pout.axpy(1.0, phelp)
#         # negative log likelihood function
#         self.variation.hessian(phat, phelp, n)
#         pout.axpy(1.0, phelp)

#         if self.type_scaling == 1:
#             # rescaling by the parameter dimension = trace(I)
#             pout[:] *= 1. / self.model.problem.Vh[PARAMETER].dim()
#         elif self.type_scaling == 2:
#             # rescaling by the trace = trace(I) + trace(H)
#              pout[:] *= 1./(self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d[n]))

#         # print("dim = ", self.model.problem.Vh[PARAMETER].dim(), "trace", np.sum(variation.d[n]))

#     def inner(self, phat1, phat2, n):

#         phelp = self.model.generate_vector(PARAMETER)
#         self.mult(phat1, phelp, n)

#         return phat2.inner(phelp)

#     def update(self, particle, variation):
#         self.particle = particle
#         self.variation = variation


# class MetricPosteriorAverage:
#     # define the metric M for the kernel k(pn, pm) = exp(-(pn-pm)^T * M * (pn - pm))
#     def __init__(self, model, particle, variation, options):
#         self.model = model  # forward model
#         self.particle = particle
#         self.variation = variation
#         self.type_scaling = options["type_scaling"]
#         self.low_rank_Hessian_average = options["low_rank_Hessian_average"]
#         self.low_rank_Hessian_misfit = options["low_rank_Hessian_misfit"]

#     def mult(self, phat, pout, n):

#         pout.zero()
#         # covariance/regularization
#         phelp = self.model.generate_vector(PARAMETER)
#         self.model.prior.R.mult(phat, phelp)
#         pout.axpy(1.0, phelp)
# #        pout.axpy(1.0, phat)
#         # negative log likelihood function
# #<<<<<<< HEAD
# #        if self.low_rank_Hessian_misfit == 2:
# #            phelp_1 = self.model.generate_vector(PARAMETER)
# #            hessian_misfit = LowRankOperator(self.variation.d_average, self.variation.U_average)
# #            hessian_misfit.mult(pout, phelp_1)
# #            self.model.prior.R.mult(phelp_1, phelp)
# #            pout.axpy(1.0, phelp)
# #
# #            # # 1. no rescaling
# #
# #            # # 2. rescaling by dimension
# #            pout[:] *= 1. / self.model.problem.Vh[PARAMETER].dim()
# #
# #            # # 3. rescaling by the trace = trace(I) + trace(H), the following is the correct trace
# #            # pout[:] *= 1./(self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d_average))
# #
# #            # print("dim = ", self.model.problem.Vh[PARAMETER].dim(), "trace", np.sum(variation.d[n]))
# #=======
#         if self.low_rank_Hessian_average and self.low_rank_Hessian_misfit:
#             self.variation.hessian_average(phat, pout)
# #>>>>>>> master
#         else:
#             for n in range(self.particle.number_particles):
#                 self.variation.hessian(phat, phelp, n)
#                 pout.axpy(1.0 / self.particle.number_particles, phelp)

# #                self.model.prior.R.mult(pout, phelp)
# #                pout.axpy(1.0, phelp)

#         if self.type_scaling == 1:
#             # rescaling by the parameter dimension = trace(I)
#             pout[:] *= 1. / self.model.problem.Vh[PARAMETER].dim()
#         elif self.type_scaling == 2:
#             # rescaling by the trace = trace(I) + trace(H)
#              pout[:] *= 1./(self.model.problem.Vh[PARAMETER].dim() + np.sum(self.variation.d[n]))

#         # print("dim = ", self.model.problem.Vh[PARAMETER].dim(), "trace", np.sum(variation.d[n]))

#     def inner(self, phat1, phat2, n):

#         phelp = self.model.generate_vector(PARAMETER)
#         self.mult(phat1, phelp, n)

#         return phat2.inner(phelp)

#     def update(self, particle, variation):
#         self.particle = particle
#         self.variation = variation
