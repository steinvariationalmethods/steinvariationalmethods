from __future__ import absolute_import, division, print_function
import math
import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys
from scipy.spatial.distance import cdist
import numpy.matlib
import time
from metric import Metric
from kernel import Kernel


def SVN_H(x, stepsize, itermax, model):
    # Number of particles
    N = x.shape[1]
    
    # Initialise particle maximum shifts
    maxshift = np.zeros(N)
    maxmaxshift_old = np.inf
    
    # Initialise average computational time
    timeave = 0;
    
    # Identity matrix
    I = np.identity(model.dim)
    
    
    for k in range(itermax):
        tic = time.time()
        
        g_mlpt = np.zeros((model.dim, N))
        gnH = np.zeros((model.dim,model.dim,N))
        
        for j in range(N):
            g_mlpt[:,[j]], gnH[:,:,j]  = model.get_grad_hess(x[:,[j]])
        # Averaging Hessian approximation
        sEH = np.mean(gnH,axis=2) / model.dim
        
        x_copy = x.copy()
        log_posterior = 0.
        
        for i in range(N):
            sign_diff = x[:,[i]] - x_copy
            
            kern = np.exp(-0.5*np.sum((np.dot(sign_diff.transpose(),sEH)*sign_diff.transpose()), axis = 1))
            
            g_kern = np.dot(sEH,sign_diff) * kern
            
            mgrad_J = np.mean( -kern * g_mlpt + g_kern, axis=1)
            
            H_J = np.mean( np.transpose( np.tile(kern[:,np.newaxis],[model.dim,1,model.dim]), (0, 2, 1)  ) * gnH , axis=2 ) + sEH*np.mean(kern)
            
            Q = np.linalg.solve(H_J, mgrad_J)
            
            x[:,i] = x[:,i] + stepsize*Q
            
            log_posterior += model.log_post(x[:,[i]])
            
            maxshift[i] = np.linalg.norm(Q, np.inf)
        print(k,np.linalg.norm(Q),log_posterior/N)
    
        maxmaxshift = np.max(maxshift);
        #print('Maximum shift is', maxmaxshift)
        #print(np.linalg.norm(Q))
        
        if np.isnan(maxmaxshift) or maxmaxshift > 1e50:
            stepsize = 0.1*stepsize
            print('Step size too large; scaling it by factor 10.\n epsilon = ', stepsize)
            print('Reset particles... \n')
            x = model.prior_m + np.dot(model.prior_C0sqrt,np.random.randn(model.dim,N)) 
        
        # Update stepsize
        if maxmaxshift >= maxmaxshift_old:
            stepsize = 0.9*stepsize
        elif np.abs(maxmaxshift - maxmaxshift_old) < 1e-6:
            stepsize = 1.01*stepsize

        maxmaxshift_old = maxmaxshift
    
        # Last iteration
        if k == itermax:
            print('Maximum number of iterations has been reached.\n')
        timeave += time.time() - tic

    timeave = timeave / itermax;

    return x, stepsize, timeave







def SVN_H_full(x, stepsize, itermax, model):

    N = x.shape[1]
    
    # Initialise particle maximum shifts
    maxshift = np.zeros(N)
    maxmaxshift_old = np.inf
    
    # Initialise average computational time
    timeave = 0;
    
    # Identity matrix
    I = np.identity(model.dim)
    
    
    for k in range(itermax):
        tic = time.time()
        
        g_mlpt = np.zeros((model.dim, N))
        gnH = np.zeros((model.dim,model.dim,N))
        
        for j in range(N):
            Fx,J = model.fwd_solve(x[:,[j]])
            g_mlpt[:,[j]] = model.grad_mlpt(x[:,[j]], Fx, J)
            gnH[:,:,j]  = model.prior_C0i + model.obs_nobs*np.dot(J,J.transpose()) / model.obs_std2
        
        # Averaging Hessian approximation
        sEH = np.mean(gnH,axis=2) / model.dim
        
        x_copy = x.copy()
        

        dims = N*model.dim

        H_J_full = np.zeros((dims,dims))
        H_J_full_m = np.zeros((dims,dims))
        H_J_full_d = np.zeros((dims,dims))
        mgrad_J_full = np.zeros(dims)

        for i in range(N):
            sign_diff = x[:,[i]] - x_copy

            kern = np.exp(-0.5*np.sum((np.dot(sign_diff.transpose(),sEH)*sign_diff.transpose()), axis = 1))

            g_kern = np.dot(sEH,sign_diff) * kern

            mgrad_J = np.mean( -kern * g_mlpt + g_kern, axis=1)

            Mx = np.dot(sEH,sign_diff)

            res = np.mean(kern*np.tile(Mx[:,np.newaxis,:],[1,model.dim,1])*np.tile(Mx[np.newaxis,:,:],[model.dim,1,1]),axis = 2)

            index1 = i*model.dim
            index2 = (i+1)*model.dim

            H_J = np.transpose( np.tile(kern[:,np.newaxis],[model.dim,1,model.dim]), (0, 2, 1)  ) * gnH  + np.tile(sEH[:,:,np.newaxis],[1,1,N])*np.tile(kern[np.newaxis,np.newaxis,:],[model.dim,model.dim,1]) 

            for j in range(N):
                index_1 = j*model.dim
                index_2 = (j+1)*model.dim
                H_J_full[index1:index2,index_1:index_2] = H_J[:,:,j]/np.float(N)
            H_J_full_m[index1:index2,index1:index2] = np.mean(H_J,axis = 2)
            #H_J_full_d[index1:index2,index1:index2] = H_J[:,:,i]/np.float(N)
            mgrad_J_full[index1:index2] = mgrad_J
        H_J_full_m_inv = np.linalg.inv(H_J_full_m)
        #H_J_full_d_inv = np.linalg.inv(H_J_full_d)
        #H_inv = np.diag(np.diag(H_J_full_m))
        #Q,info = scipy.sparse.linalg.bicgstab(H_J_full, mgrad_J_full,tol=1e-01, M = H_J_full_m_inv)
        #Q,info = scipy.sparse.linalg.cg(H_J_full, mgrad_J_full,tol=1e-1)
        Q,info = scipy.sparse.linalg.gmres(H_J_full, mgrad_J_full,tol=1e-1, M = H_J_full_m_inv)

        log_posterior = 0.

        for i in range(N):

            index1 = i*model.dim
            index2 = (i+1)*model.dim

            x[:,i] = x[:,i] + stepsize*Q[index1:index2]

            log_posterior += model.log_post(x[:,[i]])
        print(k,np.linalg.norm(Q[index1:index2]),info,log_posterior/N)

    
        # Last iteration
        if k == itermax:
            print('Maximum number of iterations has been reached.\n')
        timeave += time.time() - tic

    timeave = timeave / itermax;
    return x, stepsize, timeave





def SVN_H_New(x, stepsize, itermax, model,tol0=1e-1,maxiter0=5):
    N = x.shape[1]

    # Initialise particle maximum shifts
    maxshift = np.zeros(N)
    maxmaxshift_old = np.inf
    
    # Initialise average computational time
    timeave = 0;
    
    # Identity matrix
    I = np.identity(model.dim)
    #print('   step norm        mean           min            median' )
    
    for k in range(itermax):
        tic = time.time()
        
        g_mlpt = np.zeros((model.dim, N))
        gnH = np.zeros((model.dim,model.dim,N))
        
        for j in range(N):
            g_mlpt[:,[j]], gnH[:,:,j]  = model.get_grad_hess(x[:,[j]])
        # Averaging Hessian approximation
        metric = Metric(model)
        sEH = metric.value(gnH)
#        sEH = I
        x_copy = x.copy()
        
        kernel = Kernel()
        kern = np.zeros((N,N))
        g_kern = np.zeros((N,N,model.dim))
        for i in range(N):
            sign_diff = x[:,[i]] - x_copy
            kern[i,:] = kernel.value(sign_diff,sEH)
            g_kern[i,:,:] = kernel.gradient(kern[i,:], sign_diff,sEH)
        
        sum_kern = np.sum(kern,axis = 1)#=/np.float(N)
        sum_g_kern = np.sum(g_kern,axis = 0) #/ np.tile(sum_kern[:,np.newaxis],[1,model.dim])
        
        log_posterior = np.zeros(N)
        Qs = []
        for i in range(N):
            
            kern_tmp = kern[i,:]*sum_kern
            
            g_kern_i = np.transpose(g_kern[i,:,:],(1,0))
            
            mgrad_J = np.mean( -kern[i,:] * g_mlpt + g_kern_i, axis=1)
            

            kern_i = kern[i,:]
            H_J = np.mean( np.transpose( np.tile(kern_tmp[:,np.newaxis],[model.dim,1,model.dim]), (0, 2, 1)  ) * gnH , axis=2 ) + np.mean(np.tile(sum_g_kern.transpose()[:,np.newaxis,:],[1,model.dim,1])*np.tile(g_kern_i[np.newaxis,:,:],[model.dim,1,1]), axis = 2)
            Q,info = scipy.sparse.linalg.gmres(H_J, mgrad_J,tol=tol0,maxiter=maxiter0)
            # print(info)
            Qs.append(Q)
            maxshift[i] = np.linalg.norm(Q, np.inf)
            #Q = np.linalg.solve(H_J, mgrad_J)
        
        for i in range(N):
            x[:,i] = x[:,i] + np.sum(Qs*np.tile(kern[i,:][:,np.newaxis],[1,model.dim]),axis = 0)/sum_kern[i]
            log_posterior[i] = model.log_post(x[:,[i]])
 
        maxmaxshift = np.max(maxshift);
        #print('Maximum shift is', maxmaxshift)
        
        
        
        print(k,np.linalg.norm(Q),np.mean(log_posterior))
        #print(k,np.linalg.norm(Q),np.mean(log_posterior),log_posterior.min(),np.median(log_posterior))
        
        if np.isnan(maxmaxshift) or maxmaxshift > 1e50:
            stepsize = 0.1*stepsize
            print('Step size too large; scaling it by factor 10.\n epsilon = ', stepsize)
            print('Reset particles... \n')

            # x = model.prior_m + np.dot(model.prior_C0sqrt,np.random.randn(model.dim,N))
            x = model.prior.sample(N).T.copy()
        # Update stepsize
        if maxmaxshift >= maxmaxshift_old:
            stepsize = 0.9*stepsize
        elif np.abs(maxmaxshift - maxmaxshift_old) < 1e-6:
            stepsize = 1.01*stepsize
    
        maxmaxshift_old = maxmaxshift
        
        # if convergence_callback(x.T):
        #     return x, True #particles, whether or not converged to within epsilon of integral
        # Last iteration
        if k == itermax:
            print('Maximum number of iterations has been reached.\n')
    timeave += time.time() - tic

    timeave = timeave / itermax;

    return x, stepsize, timeave
    # if convergence_callback(x.T):
    #     return x, True 
    # else:
        # return x, False #stepsize #, timeave

def SVN_H_New_full(x, stepsize, itermax, model,tol0=1e-1,maxiter0=5,block_diag=False):
    N = x.shape[1]
    
    # Initialise particle maximum shifts
    maxshift = np.zeros(N)
    maxmaxshift_old = np.inf
    
    # Initialise average computational time
    timeave = 0;
    
    # Identity matrix
    I = np.identity(model.dim)
    
    
    for k in range(itermax):
        tic = time.time()
        
        g_mlpt = np.zeros((model.dim, N))
        gnH = np.zeros((model.dim,model.dim,N))
        
        for j in range(N):
            Fx,J = model.fwd_solve(x[:,[j]])
            g_mlpt[:,[j]] = model.grad_mlpt(x[:,[j]], Fx, J)
            gnH[:,:,j]  = model.prior_C0i + model.obs_nobs*np.dot(J,J.transpose()) / model.obs_std2
        
        # Averaging Hessian approximation
        sEH = np.mean(gnH,axis=2) / model.dim
        
        x_copy = x.copy()
        
        kern = np.zeros((N,N))
        g_kern = np.zeros((N,N,model.dim))
        for i in range(N):
            sign_diff = x[:,[i]] - x_copy
            kern[i,:] = np.exp(-0.5*np.sum((np.dot(sign_diff.transpose(),sEH)*sign_diff.transpose()), axis = 1))
            g_kern[i,:,:] = np.transpose((np.dot(sEH,sign_diff) * kern[i,:]),(1,0))
            
        sum_kern = np.sum(kern,axis = 1)#=/np.float(N)
        sum_g_kern = np.sum(g_kern,axis = 0) / np.tile(sum_kern[:,np.newaxis],[1,model.dim])
        dims = N*model.dim

        H_J_full = np.zeros((dims,dims))
        H_J_full_m = np.zeros((dims,dims))
        H_J_full_m_inv = np.zeros((dims,dims))
        H_J_full_d = np.zeros((dims,dims))
        mgrad_J_full = np.zeros(dims)

        for i in range(N):
            kern_tmp = kern[i,:]*sum_kern

            g_kern_i = np.transpose(g_kern[i,:,:],(1,0))

            mgrad_J = np.mean( -kern[i,:] * g_mlpt + g_kern_i, axis=1)
            
            kern_i = kern[i,:]

            Mx = np.dot(sEH,sign_diff)

            index1 = i*model.dim
            index2 = (i+1)*model.dim
            H_J_all = 0.
            for j in range(N):
                kern_p = kern[i,:]*kern[j,:]/sum_kern
                g_kern_j = np.transpose(g_kern[j,:,:],(1,0))/sum_kern
                H_J = np.mean( np.transpose( np.tile(kern_p[:,np.newaxis],[model.dim,1,model.dim]), (0, 2, 1)  ) * gnH , axis=2 ) + np.mean(np.tile(g_kern_j[:,np.newaxis,:],[1,model.dim,1])*np.tile(g_kern_i[np.newaxis,:,:],[model.dim,1,1]), axis = 2)

                index_1 = j*model.dim
                index_2 = (j+1)*model.dim
                H_J_full[index1:index2,index_1:index_2] = H_J
                H_J_all = H_J_all + H_J
            H_J_full_m[index1:index2,index1:index2] = H_J_all
            H_J_full_m_inv[index1:index2,index1:index2] = np.linalg.inv(H_J_all)
            #H_J_full_d[index1:index2,index1:index2] = H_J[:,:,i]/np.float(N)
            mgrad_J_full[index1:index2] = mgrad_J
        #H_J_full_d_inv = np.linalg.inv(H_J_full_d)
        #H_inv = np.diag(np.diag(H_J_full_m))
        #Q,info = scipy.sparse.linalg.bicgstab(H_J_full, mgrad_J_full,tol=1e-2,maxiter = 4, M = H_J_full_m_inv)
        if block_diag:
            Q,info = scipy.sparse.linalg.gmres(H_J_full_m, mgrad_J_full,tol=tol0,maxiter=maxiter0)
        else:
            Q,info = scipy.sparse.linalg.cg(H_J_full, mgrad_J_full,tol=tol0,maxiter=maxiter0)


        log_posterior = 0.

        for i in range(N):

            index1 = i*model.dim
            index2 = (i+1)*model.dim

            x[:,i] = x[:,i] + stepsize*Q[index1:index2]

            log_posterior += model.log_post(x[:,[i]])
        print(k,np.linalg.norm(Q[index1:index2]),info,log_posterior/N)

    
        # Last iteration
        if k == itermax:
            print('Maximum number of iterations has been reached.\n')
        timeave += time.time() - tic

    timeave = timeave / itermax;
    return x, stepsize, timeave
