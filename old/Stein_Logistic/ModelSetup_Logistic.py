
from __future__ import absolute_import, division, print_function
import math
import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys
from scipy.spatial.distance import cdist 
import numpy.matlib
import time

class ModelSetup(object):
    
    def __init__(self,design):
        # set up fwd function parameters
        self.dim = design[0]
        # self.m = 1
        # if design[1] == None:
        #     self.x = design[1]
        # else:
        
        # set up prior 
        self.prior_m = np.zeros(self.dim)
        self.prior_sigma2 = 1./np.random.gamma(1, 100, self.dim)#np.random.uniform(1.,10.,self.dim)
        self.prior_sigma = np.sqrt(self.prior_sigma2)
        self.prior_C0 = self.prior_sigma2 * np.identity(self.dim)
        self.prior_C0sqrt  = np.sqrt(self.prior_C0) 
        self.prior_C0i = np.linalg.inv(self.prior_C0)
        self.prior_C0isqrt = np.sqrt(self.prior_C0i)
        
        # # set up observations
        # Number of independent observations
        self.obs_nobs = design[1]
        # Noise standard deviation
        self.obs_std = 0.3
        # Nose variance
        self.obs_std2 = self.obs_std**2
        # Noise
        self.obs_noise = self.obs_std*np.random.randn(self.obs_nobs) 

        self.x = np.random.uniform(-1.,1.,(self.obs_nobs,self.dim))
        self.x[:,0] = 1.

        # self.post_m = design[5]
        # self.post_sigma = design[6]
        # self.post_C = self.post_sigma * np.identity(self.dim)
        # self.post_Ci = np.linalg.inv(self.post_C)

        # Real parameter vector
        self.obs_u_true = np.random.rand(self.dim)
        # self.obs_y_true = self.fwd(self.obs_u_true)
        # Observations
        self.obs_y = self.fwd_obs(self.obs_u_true)

    def beta_x(self,beta): # x.shape = (n,dim)
        # xnew = np.insert(self.x, 0, 1,,axis = 1)
        if self.dim == 1:
            y = np.sum(beta*self.x)
        else:
            y = np.sum(beta*self.x,axis = 1)

        return y
        
        
    def fwd(self, u):
        betax = self.beta_x(u)
        if betax >= 0:
            log_y = -np.log(1.+np.exp(-betax))
        else:
            log_y = betax  - np.log(1.+np.exp(betax))
        prob = np.exp(log_y)  
        y = np.random.binomial(1, prob, size=None)
        return y

    def fwd_obs(self, u):
        betax = self.beta_x(u) + self.obs_noise
        if betax >= 0:
            log_y = -np.log(1.+np.exp(-betax))
        else:
            log_y = betax  - np.log(1.+np.exp(betax))
        prob = np.exp(log_y)  
        y = np.random.binomial(1, prob, size=None)
        return y
    
    # def fwd_solve(self, u):
    #     Fx = self.fwd(u)
        
    #     exp_F = np.exp(Fx)
    #     J = 2./ exp_F *(u-self.a)/self.h

    #     return Fx,J

    
    
    # def grad_mllkd(self, Fx, J):
    #     g_mllkd = np.sum(np.dot(J,(Fx - self.obs_y)),axis=1) / self.obs_std2
    #     return g_mllkd.reshape(self.dim,1)

    def logpost_stat(self,beta):
        betax = self.beta_x(beta)
        if betax < 0:
            exp_betax = np.exp(betax)
            beta_diff = beta-self.prior_m
            logpost_pdf = np.sum(-0.5*beta_diff**2/self.prior_sigma2)+np.sum(self.obs_y*betax-np.log(1.+exp_betax))
            logpost_grad = - beta_diff/self.prior_sigma2 + np.sum(self.obs_y[:,np.newaxis]*self.x-exp_betax[:,np.newaxis]*self.x/(1.+exp_betax[:,np.newaxis]),axis = 0)
            logprior_hess = -self.prior_C0i
            loglike_hess = 0.
            for i in range(self.obs_nobs):
                xi = self.x[0,:][:,np.newaxis]
                loglike_hess += -exp_betax[i]*np.dot(xi,xi.T)/(1.+exp_betax[i])**2

            logpost_hess =   logprior_hess + loglike_hess
        else:
            exp_betax = np.exp(-betax)
            beta_diff = beta-self.prior_m
            logpost_pdf = np.sum(-0.5*beta_diff**2/self.prior_sigma2)+np.sum(self.obs_y*betax-betax-np.log(1.+exp_betax))
            logpost_grad = - beta_diff/self.prior_sigma2 + np.sum(self.obs_y[:,np.newaxis]*self.x-self.x/(1.+exp_betax[:,np.newaxis]),axis = 0)
            logprior_hess = -self.prior_C0i
            loglike_hess = 0.
            for i in range(self.obs_nobs):
                xi = self.x[0,:][:,np.newaxis]
                loglike_hess += -np.dot(xi,xi.T)/(1.+exp_betax[i])**2

            logpost_hess =   logprior_hess + loglike_hess            
        return logpost_pdf,logpost_grad , logpost_hess,loglike_hess

    # def logpost_stat(self,x):
    #      post_pdf,post_grad, post_hess = self.post_stat(x)
    #      logpost_pdf = np.log(post_pdf)
    #      logpost_grad = post_grad / post_pdf
    #      logpost_hess = -post_pdf**(-2) * np.dot(post_grad[:,np.newaxis],post_grad[np.newaxis,:]) + 1./post_pdf*post_hess  
    #      return logpost_pdf,logpost_grad,logpost_hess

    # def post_moments(self):
    #     post_mean = np.sum(self.post_m*self.weights[:,np.newaxis],axis=0)
    #     post_c_mean = self.post_m - post_mean
    #     post_var =  np.diag(np.sum(self.post_sigma*self.weights[:,np.newaxis],axis=0))+np.einsum("ni,nj->ij", post_c_mean * self.weights[:, np.newaxis], post_c_mean)
    #     #np.sum((self.post_m**2+self.post_sigma)*self.weights[:,np.newaxis],axis=0)-post_mean**2
    #     return post_mean,post_var

    def sample_moments(self,x):
        sample_mean = np.mean(x,axis=1)
        sample_var = np.var(x,axis=1)
        return sample_mean,sample_var
        

    
    # def grad_mlpt(self, x): 
    #     diff = x-self.mu  
    #     g_mlpt = self.pdf(x) * (x-self.mu)
    #     return g_mlpt
    
    
    def postdcontour(self, x_2d, dim,mcn):

        post = np.zeros(x_2d.shape[1]);
        for j in range(x_2d.shape[1]):
            
            for nn in range(mcn):
                
                xj = self.prior_m + np.dot(self.prior_C0sqrt,np.random.randn(self.dim))    
                xj[dim] = x_2d[:,j]

        
                logpost,_,_,_ = self.logpost_stat(xj)
                post[j] += np.exp(logpost)
                #print(j,np.exp( -(mllkd + mlprr) ))
        post = post / mcn
        return post
    
    def post2contour(self, x):
    
        post = np.zeros((x.shape[1], 1));
    
        for j in range(x.shape[1]):
        
            logpost,_,_,_ = self.logpost_stat(x[:,j])
            post[j] = np.exp(logpost)
        return post  
        
    # def misfit_cost(self,x):
    #     cost = .5 * np.sum((x-self.obs_y)**2 )/ self.obs_std2
    #     return cost

    # def prior_cost(self,x):
    #     cost = 0.5*np.dot(np.dot((x - self.prior_m).transpose(),self.prior_C0i),(x - self.prior_m))
    #     return cost
    
    
    # def log_post(self,x):
    #     log_prior = self.prior_cost(x)
        
    #     Fx = self.fwd(x)
 
    #     log_misfit = self.misfit_cost(Fx)
         
    #     y = -log_prior - log_misfit
        
    #     return y
    
    
    
    