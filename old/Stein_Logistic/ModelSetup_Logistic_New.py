
from __future__ import absolute_import, division, print_function
import math
import numpy as np
import scipy
import matplotlib.pyplot as plt
import sys
from scipy.spatial.distance import cdist 
import numpy.matlib
import time

def sigmoid(x,theta): 
        y = 1./(1.+np.exp(-np.dot(x,theta)))
        return y
    
class ModelSetup(object):
    
    def __init__(self,design):
        # set up fwd function parameters
        self.dim = design[0]
 
        # set up prior 
        self.prior_m = np.zeros(self.dim)
        self.prior_sigma2 = np.ones(self.dim)
        self.prior_sigma = np.sqrt(self.prior_sigma2)
        self.prior_C0 = self.prior_sigma2 * np.identity(self.dim)
        self.prior_C0sqrt  = np.sqrt(self.prior_C0) 
        self.prior_C0i = np.linalg.inv(self.prior_C0)
        self.prior_C0isqrt = np.sqrt(self.prior_C0i)
        
        # # set up observations
        self.n_obs = design[1]
        self.x = design[2]
        self.obs_y = design[3]

        
 
    def logpost_stat(self,theta):
        z = sigmoid(self.x,theta)
        dz = z*(1.-z)
#         print("z", z)
        misfit = self.obs_y*z + (1.-self.obs_y) * (1.-z) + 1e-3
#         print("misfit", misfit)
#         prior = np.exp(-0.5*np.sum((theta-self.prior_m)**2/self.prior_sigma2))
        logpost_pdf = np.sum(np.log(misfit))  -0.5*np.sum((theta-self.prior_m)**2/self.prior_sigma2)#np.log(prior)
        loglike_grad = 1./misfit*(2.*self.obs_y-1.)*dz
        logpost_grad = np.sum(loglike_grad[:,np.newaxis]*self.x,axis=0) + (-theta/self.prior_sigma2)
        loglike_hess = np.zeros((self.dim,self.dim))
        for i in range(self.n_obs):
            xx = np.dot(self.x[i].reshape(self.dim,1),self.x[i].reshape(1,self.dim))
            loglike_hess += -(loglike_grad[i])**2 * xx
            loglike_hess += loglike_grad[i] * (1.-z[i]) * xx
            loglike_hess += loglike_grad[i] * (-z[i]) * xx
        logprior_hess = -self.prior_C0i
        logpost_hess =   logprior_hess + loglike_hess                
        
        return logpost_pdf,logpost_grad , logpost_hess,loglike_hess



    def sample_moments(self,x):
        sample_mean = np.mean(x,axis=1)
        sample_var = np.var(x,axis=1)
        return sample_mean,sample_var
        

    
    
    def postdcontour(self, x_2d, dim,mcn):

        post = np.zeros(x_2d.shape[1]);
        for j in range(x_2d.shape[1]):
            
            for nn in range(mcn):
                
                xj = self.prior_m + np.dot(self.prior_C0sqrt,np.random.randn(self.dim))    
                xj[dim] = x_2d[:,j]

        
                logpost,_,_,_ = self.logpost_stat(xj)
                post[j] += np.exp(logpost)
                #print(j,np.exp( -(mllkd + mlprr) ))
        post = post / mcn
        return post
    
    def post2contour(self, x):
    
        post = np.zeros((x.shape[1], 1));
    
        for j in range(x.shape[1]):
        
            logpost,_,_,_ = self.logpost_stat(x[:,j])
            post[j] = np.exp(logpost)
        return post  
        
    # def misfit_cost(self,x):
    #     cost = .5 * np.sum((x-self.obs_y)**2 )/ self.obs_std2
    #     return cost

    # def prior_cost(self,x):
    #     cost = 0.5*np.dot(np.dot((x - self.prior_m).transpose(),self.prior_C0i),(x - self.prior_m))
    #     return cost
    
    
    # def log_post(self,x):
    #     log_prior = self.prior_cost(x)
        
    #     Fx = self.fwd(x)
 
    #     log_misfit = self.misfit_cost(Fx)
         
    #     y = -log_prior - log_misfit
        
    #     return y
    
    
    
    