#
# This file is part of stein_variational_inference package
#
# stein_variational_inference is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# stein_variational_inference is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with stein variational inference methods class project.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: Sean McBane, Joshua Chen, Keyi Wu
# Contact: ...
#

import numpy as np
from scipy.integrate import quad
from math import isclose
from abc import ABC, abstractmethod

def require_parameters(*args, **kwargs):
    for arg in args:
        if not arg in kwargs:
            raise Exception('Required parameter "%s" not found in kwargs'%(arg))

class _Distribution(ABC): #consider numba jit classes, 
    "Abstract base class for (generally non-normalized, i.e. non-probability) distributions"
    def __init__(self, dim):
        """
        All child classes should call this base constructor
        (via super().__init__) to set the class members common to all
        _Distribution objects.
        """
        if type(dim) is not int:
            raise Exception("`dim` arg to _Distribution.__init__ must be integer")
        self._dim = dim
        self._hess_eval_pt_set = False
        self.reset_count_evals()
       
    @property
    def logpdf_evaluations(self):
        return self._logpdf_evals

    @logpdf_evaluations.setter
    def logpdf_evaluations(self, n):
        self._logpdf_evals = n

    @property
    def gradlogpdf_evaluations(self):
        return self._gradlogpdf_evals

    @gradlogpdf_evaluations.setter
    def gradlogpdf_evaluations(self, n):
        self._gradlogpdf_evals = n

    @property
    def hesslogpdf_evaluations(self):
        return self._hesslogpdf_evals

    def reset_count_evals(self):
        self._logpdf_evals = 0
        self._gradlogpdf_evals = 0
        self._hesslogpdf_evals = 0
               
    @property
    def fn_evals(self):
        return self.logpdf_evaluations + self.gradlogpdf_evaluations + 2*self.hesslogpdf_evaluations

    @hesslogpdf_evaluations.setter
    def hesslogpdf_evaluations(self, n):
        self._hesslogpdf_evals = 0
       
    def _check_x(self, x):
        pass
        return True
        "Make sure x has correct dimensionality"
#        if self._dim == 1:
#            if not np.isscalar(x):
#                if x.shape != (self._dim,):
#                    raise Exception("""
#                                Expected x to have shape ()
#                                """)
#        elif x.shape != (self._dim,):
#            raise Exception("""
#                            Expected x to have shape (%d,)
#                            """%(self._dim))
        pass
    def sample(self, nsamples=1):
        if nsamples is 1:
            return self._sample1()
        else:
            return self._samplemany(nsamples)
    
    def _sample1(self):
        raise Exception("Child classes has not implemented _sample1 so one can not use this method for this distribution")

    def _samplemany(self, nsamples):
        raise Exception("Child classes has not implemented _samplemany(self, nsamples), so one cannot use this method for this distribution")

    @property
    def dim(self):
        "Dimension of the distribution"
        return self._dim

    def what_am_i(self):
        "Tell caller that this object is a distribution"
        return 'Distribution'

    @abstractmethod
    def pdf(self, x):
        "Evaluate the PDF of this distribution at point x"
        raise Exception("Child classes should implement pdf(self, x) if appropriate")

    # @abstractmethod
    def gradpdf(self, x):
        "Evaluate the gradient of the PDF of this distribution at x"
        raise Exception("Child classes should implement gradpdf(self, x) if known")

    def hess_pdf_action(self, x, gaussnewton=False):
        "Action of the hess of the distribution evaluated at a point on vector x"
        if self._hess_eval_pt_set:
            return self.hess_pdf_action_impl(x, gaussnewton=gaussnewton)
        else:
            raise Exception("hess evaluation point not set")

    # @abstractmethod
    def hess_pdf_action_impl(self, x, gaussnewton=False):
        raise Exception("Child classes should implement hess_pdf_action_impl")
       
    # Implement these methods in child classes if analytical or more efficient
    # forms are known
    def logpdf(self, x):
        self.logpdf_evaluations += 1
        return np.log(self.pdf(x))
    
    def gradlogpdf(self, x):
        self.gradlogpdf_evaluations += 1
        return self.gradpdf(x) / self.pdf(x)
   
    @property
    def hess_eval_pt(self):
        return self.m

    @hess_eval_pt.setter
    def hess_eval_pt(self, x):
        self.x = x
        self._hess_eval_pt_set = True

    def hess_logpdf_action(self, x, gaussnewton=True):
        if self._hess_eval_pt_set:
            return self.hess_logpdf_action_impl(x, gaussnewton=gaussnewton)
        else:
            raise Exception("hess evaluation point not set")
    
    def hess_logpdf_action_impl(self, x, gaussnewton=True):
        self.hesslogpdf_evaluations += 1
        p = self.pdf(self.hess_eval_pt)
        g = self.gradpdf(self.hess_eval_pt)
        if gaussnewton:
            return -g * np.dot(g, x) / p**2
        else:
            return -g * np.dot(g, x) / p**2 + self.hess_pdf_action(x, gaussnewton=gaussnewton) / p

class Posterior(_Distribution):
    def __init__(self, **kwargs):
        require_parameters('prior', 'likelihood', **kwargs)
        self.prior = kwargs['prior']
        self.likelihood = kwargs['likelihood']
        if (self.prior.what_am_i() is not 'Distribution' or 
                self.likelihood.what_am_i() is not 'Distribution'):
            raise Exception('Prior and likelihood must be distributions')
        dim = self.prior.dim
        if self.likelihood.dim != dim:
            raise Exception('Dimensions of prior and likelihood must match "dim"')
        super().__init__(dim)
    
    def pdf(self, x):
        "Unnormalized pdf of the posterior"
        self._check_x(x)
        return self.prior.pdf(x)*self.likelihood.pdf(x)

    def gradpdf(self, x):
        "Unnormalized gradient of the pdf of the posterior"
        self._check_x(x)
        return self.prior.gradpdf(x)*self.likelihood.pdf(x) + self.prior.pdf(x)*self.likelihood.gradpdf(x)

    def hess_pdf_action_impl(x):
        self._check_x(x)
        return self.prior.hess_pdf_action_impl(x)*self.likelihood.pdf(x) \
               + 2* self.prior.gradpdf(x)*self.likelihood.gradpdf(x) \
               + self.prior.pdf(x)*self.likelihood.hess_pdf_action_impl(x) \

    def logpdf(self, x):
        "Unnormalized log of the pdf of the posterior"
        self.logpdf_evaluations += 1
        return self.prior.logpdf(x) + self.likelihood.logpdf(x)
    
    def gradlogpdf(self, x):
        "Unnormalized gradient of the log of the pdf of the posterior"
        self.gradlogpdf_evaluations += 1
        return self.prior.gradlogpdf(x) + self.likelihood.gradlogpdf(x)

    @property
    def hess_eval_pt(self):
        "Point at which to evaluate the hess action of the posterior"
        return self.prior.hess_eval_pt

    @hess_eval_pt.setter
    def hess_eval_pt(self, x):
        # Need to call hess_eval_pt() on member distributions so that they
        # flag the evaluation point as set.
        self.prior.hess_eval_pt = x
        self.likelihood.hess_eval_pt = x
        self._hess_eval_pt_set = True
       
    def hess_logpdf_action_impl(self, x, gaussnewton=False):
#        self._check_x(x)
        self.hesslogpdf_evaluations += 1
        return self.prior.hess_logpdf_action(x, gaussnewton=gaussnewton) \
                + self.likelihood.hess_logpdf_action(x, gaussnewton=gaussnewton)


#Maybe we can have _1D_Distribution, since there are several interesting 1D ones that don't have generalizations to multiD, at least not known.

class Uniform(_Distribution):
    """
    A uniform distribution on a hypercube centered at the origin with sides of
    length 2*sz
    """
    def __init__(self, dim, **kwargs):
        require_parameters('sz', **kwargs)
        super().__init__(dim)
        self._sz = kwargs['sz']
        
    def pdf(self, x):
        self._check_x(x)
        if np.all( (x < self._sz) * (x > -self._sz) ):
            return 1. / (2*self._sz)**self._dim
        else:
            return 0.0
    
    def gradpdf(self, x):
        self._check_x(x)
        return np.zeros(self._dim)
    
    def hess_pdf_action_impl(x, gaussnewton=False):
        self._check_x(x)
        return np.zeros(self._dim)
        
    def gradlogpdf(self, x):
        self.gradlogpdf_evaluations += 1
        return np.zeros(self._dim)
    
    def hess_logpdf_action_impl(x, gaussnewton=False):
        self.hesslogpdf_evaluations += 1
        return np.zeros(self._dim)
        
    def _sample1(self):
        return 2*self._sz * np.random.rand(self._dim) - self._sz
    
    def _samplemany(self, nsamples):
        return 2*self._sz * np.random.rand(nsamples, self._dim) - self._sz



#Consider optionally providing a sparse cholesky factor, or calculating it first if one is given the covariance operator, and then storing it.  so that our solves are always fast (back substitution).
#Consider splitting off into 1DNormal and MultivariateNormal, this way, we dont have as many cases 
#in 1D, store one_over_sigma, we can also store the normalization factor: 2*np.pi)**self._dim
# as well as  ((2*np.pi)**self._dim * np.linalg.det(self.sigma)) ** 0.5, etc

#Basically, we need MultivariateNormal to be veryvery fast, so that we can do a 1Million dimensional Normal (with decaying covariance eignevlaues of course)
# Also, consider for all distributions both "log_pdf" and "unnormalized log pdf", and possibly the same for 'pdf', since when performing
# optimization, we dont care about the constant factors. the "unnoramlzied log pdf" gets used to check the 'cost', but not for calculations of the graident or hessian though, so this is simply for speeding up 
# evaluations which tell us metrics for how well the optimization is doing.


#Also, i think we should just have special cases of the Normal: IsotropicNormal (scalar sigma)
# ScaledNormal (diagonal sigma)
# RotatedNormal (or some other name, that takes a non diagonal sigma) 
# This way, we no longer have to have if/else statements.
# Anyways, these objects are instantiated once, its wasteful to keep checking if we only instantiate them once per problem, and
# the parameters are fixed.
class Normal(_Distribution):
    def __init__(self, dim, **kwargs):
        super().__init__(dim)
        require_parameters('mu', 'sigma', **kwargs)

        self.mu = kwargs['mu']
        self.sigma = kwargs['sigma']

        if np.isscalar(self.mu):
            self.mean_type = 'scalar'
        elif self.mu.shape != (dim,):
            raise Exception('Non-scalar mean must have shape (dim,)')
        else:
            self.mean_type = 'vector'

        if np.isscalar(self.sigma):
            self.std_type = 'scalar'
        elif self.sigma.shape == (dim,):
            self.std_type = 'vector'
        elif self.sigma.shape == (dim, dim):
            if not np.allclose(self.sigma, self.sigma.T):
                raise Exception('Covariance matrix should be symmetric')
            self.std_type = 'covariance'
        else:
            raise Exception("""
                            sigma should be a scalar, a vector of standard
                            deviations, or a covariance matrix
                            """)        

    # np.random.multivariate_normal seems to not take a scipy sparse sigma,
    # if not, we can simply draw N i.i.d. Gaussian normral random vectors and hit Sigma against them (unwhiten them)
    # this would be fast.
    def _sample1(self):
        if self.mean_type is 'scalar':
            if self.std_type is 'scalar':
                return np.random.normal(self.mu, self.sigma, self._dim)
            elif self.std_type is 'vector':
                return np.random.normal(self.mu, self.sigma)
            else:
                return np.random.multivariate_normal(
                    np.repeat(self.mu, self._dim), self.sigma
                )
        else:
            if self.std_type is 'scalar' or self.std_type is 'vector':
                return np.random.normal(self.mu, self.sigma)
            else:
                return np.random.multivariate_normal(self.mu, self.sigma)

    def _samplemany(self, nsamples):
        if self.mean_type is 'scalar':
            if self.std_type is 'scalar' or self.std_type is 'vector':
                return np.random.normal(
                        self.mu, self.sigma, (nsamples, self._dim)
                )
            else:
                return np.random.multivariate_normal(
                        np.repeat(self.mu, self._dim), self.sigma, nsamples
                )
        else:
            if self.std_type is 'scalar' or self.std_type is 'vector':
                return np.random.normal(
                        self.mu, self.sigma, (nsamples, self._dim)
                )
            else:
                return np.random.multivariate_normal(
                        self.mu, self.sigma, nsamples
                )

    def pdf(self, x):
        """
        Return shape: ()
        """
        self._check_x(x)
        diff = x - self.mu
        if self.std_type is 'scalar':
            num = np.exp(-0.5 * np.dot(diff, (1/self.sigma)*diff))
            den = ((2*np.pi)**self._dim * self.sigma**self._dim)**0.5
            return num / den
        elif self.std_type is 'vector':
            num = np.exp(-0.5 * np.dot(diff, (1/self.sigma)*diff))
            den = ((2*np.pi)**self._dim * np.prod(self.sigma))**0.5
            return num / den
        else:
            num = np.exp(-0.5 * np.dot(diff,
                                       np.linalg.solve(self.sigma, diff))
                        )
            den = ((2*np.pi)**self._dim * np.linalg.det(self.sigma)) ** 0.5
            return num / den

    def gradpdf(self, x):
        """
        Return shape: (dimensions,)
        """
        self._check_x(x)
        diff = x - self.mu
        if self.std_type is 'scalar' or self.std_type is 'vector':
            return -self.pdf(x) * diff / self.sigma
        else:
            return -self.pdf(x) * np.linalg.solve(self.sigma, diff)

    def hess_pdf_action_impl(self, x, gaussnewton=False):
        """
        Return shape: (dimensions,)
        """
        self._check_x(x)
        if gaussnewton:
            if self.std_type is 'scalar' or self.std_type is 'vector':
                return -self.pdf(self.hess_eval_pt) * x / self.sigma
            else:
                return -self.pdf(self.hess_eval_pt) * np.linalg.solve(self.sigma, x)
        else:
            diff = self.hess_eval_pt - self.mu
            g = self.gradpdf(self.hess_eval_pt)
            if self.std_type is 'scalar' or self.std_type is 'vector':
                return -g * np.dot(diff/self.sigma, x) - self.pdf(self.hess_eval_pt) * x / self.sigma
            else:
                return -g * np.dot(np.linalg.solve(self.sigma, diff), x) - self.pdf(self.hess_eval_pt) * np.linalg.solve(self.sigma, x)
               
    def logpdf(self, x):
        """
        Return shape: ()
        """
        self.logpdf_evaluations += 1
        diff = x - self.mu
#        if self.std_type is 'scalar':
#            den = np.sqrt((2*np.pi)**self._dim * self.sigma**self._dim)
#            return -np.log(den) - 0.5 * np.dot(diff, diff / self.sigma)
#        elif self.std_type is 'vector':
#            den = np.sqrt((2*np.pi)**self._dim * np.prod(self.sigma))
#            return -np.log(den) - 0.5 * np.dot(diff, diff / self.sigma)
#        else:
#            den = np.sqrt((2*np.pi)**self._dim * np.linalg.det(self.sigma))
#            return -np.log(den) - 0.5 * np.dot(diff,
#                                               np.linalg.solve(self.sigma, diff)
#                                              )
        if self.std_type is 'scalar':
            return - 0.5 * np.dot(diff, diff / self.sigma)
        elif self.std_type is 'vector':
            return  - 0.5 * np.dot(diff, diff / self.sigma)
        else:
            return  - 0.5 * np.dot(diff,np.linalg.solve(self.sigma, diff)) #consider scipy.linalg.cho_solve
            #and scipy.linalg.cholesky_banded to get the cholesky factor.
            # we should operate, i htink, on the case where the Covariance is BANDED. all our test problems hsould have this structure then.
            # the covariance magnitudes should decay with the ordering of the matrix, so that we get the structure we want...
            #https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.linalg.cho_solve.html

    def gradlogpdf(self, x):
        """
        Return shape: (dimensions,)
        """
        self.gradlogpdf_evaluations += 1
        if self.std_type is 'scalar' or self.std_type is 'vector':
            return -(x - self.mu) / self.sigma
        else:
            return -np.linalg.solve(self.sigma, x - self.mu)

    def hess_logpdf_action_impl(self, x, gaussnewton=False):
        """
        Return shape: (dimensions,)
        """
#        self._check_x(x)
        self.hesslogpdf_evaluations += 1
        if self.std_type is 'scalar' or self.std_type is 'vector':
            return -x / self.sigma
        else:
            return -np.linalg.solve(self.sigma, x)
   
    # I implement this method here instead of using the default because we
    # don't need the check that the evaluation point is set - the hess is the
    # same everywhere.
    def hess_logpdf_action(self, x, gaussnewton=False):
        return self.hess_logpdf_action_impl(x)

class LPercentSparseDistribution(_Distribution):
    "l-percent sparse distribution for sampling i.i.d. l-percent sparse vectors per Tan Bui's work. Used for RandomisedSVD"
    def __init__(self, dim, **kwargs):
        super().__init__(dim)
        require_parameters('mu', 'l', **kwargs)
        self.mu = kwargs['mu']
        self.l = kwargs['l'] # percent sparsity

class MixtureDistribution(_Distribution):
    "Represents a convex combination of distributions"
    def __init__(self, distrs, weights):
        """
        __init__(self, distrs, weights)
        
        distrs: Iterable of distributions that will be combined.

        weights: Iterable of the weights associated to each distribution
        in the convex combination. sum(weights) == 1 is enforced to make random
        sampling easier.

        We assume now that distrs each have the same dim (Otherwise this would be invalid)
        """
        dim = distrs[0]._dim
        super().__init__(dim)
        
        if len(distrs) != len(weights):
            raise Exception("len(distrs) != len(weights)")
        
        for distr in distrs:
            if not distr.what_am_i() == 'Distribution':
                raise Exception("distr is not a _Distribution")

        if not isclose(sum(weights), 1.0, rel_tol=1e-11):
            raise Exception("sum(weights) != 1")

        self._distrs = tuple(distrs)
        self._weights = np.array(list(weights))

        self._distrs_and_weights = ((distr, weight) for distr, weight in zip(distrs, weights))
        self._num_components = len(distrs)

    @property
    def distrs(self):
        return self._distrs
    
    @property
    def weights(self):
        return self._weights
    
    @property
    def distrs_and_weights(self):
        return self._distrs_and_weights
    
    @property
    def num_components(self):
        return self._num_components
    
    def _choose_distribution(self):
        return np.random.choice(self._distrs, p=self._weights)[0]

    def _sample1(self):
        return self._choose_distribution().sample()

    def _samplemany(self, nsamples):
        chosen_n_distrs = np.random.choice(self._distrs, size=nsamples, p=self._weights)
        samples = np.zeros((nsamples, self._dim))
        for i, distr in enumerate(chosen_n_distrs):
            samples[i, :] = distr._sample1()
        return samples

    def pdf(self, x):
        self._check_x(x) #consider removing all checks and we can allow things to break, only because we want things to be fast
        return sum(distr.pdf(x) * weight for distr, weight in zip(self._distrs, self._weights))

    def gradpdf(self, x):
        self._check_x(x)
        return sum(distr.gradpdf(x) * weight
                    for distr, weight in zip(self._distrs, self._weights))

    def hess_pdf_action_impl(self, x, gaussnewton=False): 
        self._check_x(x)
        return sum(distr.hess_pdf_action(x, gaussnewton=gaussnewton) * 
                   weight for distr, weight in zip(self._distrs, self._weights))

    @property
    def hess_eval_pt(self):
        return self._distrs[0].hess_eval_pt

    @hess_eval_pt.setter
    def hess_eval_pt(self, x):
        for distr in self._distrs:
            distr.hess_eval_pt = x
        self._hess_eval_pt_set = True


class BananaModelLikelihood(_Distribution):
    def __init__(self, design, **kwargs):
        super().__init__(2)
        self.a = design[0]
        self.b = design[1]

        self.obs_nobs = 1
        self.obs_std  = 0.3
        self.obs_std2 = self.obs_std ** 2
        self.obs_noise = self.obs_std * np.random.randn(self.obs_nobs, 1)

        # I commented out the lines that randomize instances of the distribution
        # so that experiments are repeatable

        #self.obs_u_true = np.random.rand(self.n, 1)
        self.obs_u_true = np.array([0.7203, 0.0001])
        self.obs_y_true = self.fwd(self.obs_u_true)
        #self.obs_y = self.obs_y_true + self.obs_noise
        self.obs_y = 3.1007

    def fwd(self, u):
        return np.log((self.a - u[0])**2 + self.b*(u[1] - u[0]**2)**2)

    def fwd_solve(self, u):
        Fx = self.fwd(u)
        exp_F = np.exp(Fx)

        J1 = 2.*(u[0]-self.a-2.*self.b*u[0]*(u[1]-u[0]**2)) / exp_F
        J2 = 2.*self.b*(u[1]-u[0]**2) / exp_F

        return Fx, np.array([J1, J2])

    def logpdf(self, u):
        """
        Return shape: ()
        """
        self.logpdf_evaluations += 1
        Fx = self.fwd(u)
        diff = Fx - self.obs_y
        return -0.5 * np.dot(diff, diff) / self.obs_std2

    # Not normalized!
    def pdf(self, u):
        """
        Return shape: ()
        """
        return np.exp(self.logpdf(u))

    def gradlogpdf(self, u):
        """
        Return shape: (2,)
        """
        self.gradlogpdf_evaluations += 1
        Fx, J = self.fwd_solve(u)
        grad = np.sum((J.transpose() * (Fx - self.obs_y)).reshape((2,1)), axis=1) \
                / self.obs_std2
        return -grad.reshape(2,)

    def hess_logpdf_action_impl(self, u, gaussnewton=True):
        """
        Return shape: (2,)
        """
        self.hesslogpdf_evaluations += 1
        _, J = self.fwd_solve(self.hess_eval_pt)
        J = J.reshape((2,1))
        if not gaussnewton:
            raise Exception("Didn't work out full Hessian")
        else:
#            Fx = self.fwd(u)
#            return -J * np.dot(J, u) / self.obs_std2
#            return -J * np.dot(J, Fx - self.obs_y) / self.obs_std2
            return -np.dot(np.dot(J,J.transpose()) / self.obs_std2,u)

class Laplace1D(_Distribution):
    def __init__(self, **kwargs):
        require_parameters('mu', 'b', **kwargs)
        super().__init__(1)
        self.mu = kwargs['mu']
        self.b  = kwargs['b']
    
    def pdf(self, x):
        return np.exp(-np.abs(x - self.mu) / self.b) / 2 / self.b

    def gradpdf(self, x):
        g = self.pdf(self.hess_eval_pt)
        return g * self.gradlogpdf(x)

    def hess_pdf_action_impl(self, x, gaussnewton=False):
        h = (self.hess_eval_pt / self.b)**2 * self.pdf(self.hess_eval_pt)
        if gaussnewton:
            return h * x
        else:
            if self.hess_eval_pt < self.mu:
                return (h + self.pdf(self.hess_eval_pt)/self.b) * x
            else:
                return (h - self.pdf(self.hess_eval_pt)/self.b) * x

    def logpdf(self, x):
        self.logpdf_evaluations += 1
        return -np.log(2*self.b) - np.abs(x - self.mu) / self.b

    def gradlogpdf(self, x):
        self.gradlogpdf_evaluations += 1
        if x < self.mu:
            return x / self.b
        else:
            return -x / self.b

    def hess_logpdf_action_impl(self, x, gaussnewton=True):
        self.hesslogpdf_evaluations += 1
        if self.hess_eval_pt < self.mu:
            return x / self.b
        else:
            return -x / self.b

    def _sample1(self):
        return np.random.laplace(self.mu, self.b, 1)
        

    def _samplemany(self, nsamples):
        return np.random.laplace(self.mu, self.b, (nsamples, 1))

#https://docs.scipy.org/doc/scipy/reference/stats.html for different multivariate distirbutions

#https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.arcsine.html#scipy.stats.arcsine
# class Arcsine(_ProbabilityDistribution): 1/(pi * sqrt(1- x^2))
#d/dx : x/(pi*(1-x^2)^{3/2})
#d^2/dx^2 1\pi * (3x^2/(1-x^2)^{5/2} + 1/(1-x^2)^{3/2})

#The arcsine distribution, which is the same as the classical harmonic oscillator (I believe we can only sample from it in 1D thorugh scipy)
# we can use it as a prior in 1D or as a likelihood ni any D. the above must be norms \|x\| for higher dimensions

# class Dirichlet(_ProbabilityDistribution): #Generalization of Beta to multi dimensions






# Sean, I have below two methods to sample from a multivariate Laplce distirbution
# One is for sort of a tensorized Laplace (although possibly rotated)
# And another is for a sort of elliptical Laplace, thie picture below in the link gives intuition.
# The elliptical laplace has the property that all conditionals are Gaussian.
# I've tride these in matplotlib and seen that they look correct. 
#
class TensorizedLaplace(_Distribution):
    def __init__(self, dim, **kwargs):
        super().__init__(dim)
        require_parameters('mu', 'sigma', **kwargs)

        self.mu = kwargs['mu']
        self.sigma = kwargs['sigma']

        if np.isscalar(self.mu):
            self.mean_type = 'scalar'
        elif self.mu.shape != (dim,):
            raise Exception('Non-scalar mean must have shape (dim,)')
        else:
            self.mean_type = 'vector'

        if np.isscalar(self.sigma):
            self.std_type = 'scalar'
        elif self.sigma.shape == (dim,):
            self.std_type = 'vector'
        elif self.sigma.shape == (dim, dim):
            if not np.allclose(self.sigma, self.sigma.T):
                raise Exception('Covariance matrix should be symmetric')
            self.std_type = 'covariance'
        else:
            raise Exception("""
                            sigma should be a scalar, a vector of standard
                            deviations, or a covariance matrix
                            """)

    def _sample1(self):
        if self.mean_type is 'scalar':
            if self.std_type is 'scalar':
                return np.random.laplace(self.mu, self.sigma,self._dims)
            elif self.std_type is 'vector':
                U = np.random.uniform(-0.5,0.5,self._dims)
                return self.mu[:,np.newaxis]  - self.sigma*np.sign(U)*np.log(1-2*np.abs(U))
            else:
                return self.mu - np.dot(self.sigma,np.sign(U)*np.log(1-2*np.abs(U)))
        else:
            if self.std_type is 'scalar' or self.std_type is 'vector':
                return np.random.normal(self.mu, self.sigma)
            else:
                return np.random.multivariate_normal(self.mu, self.sigma)

    def _samplemany(self, nsamples):
        if self.mean_type is 'scalar':
            if self.std_type is 'scalar':
                return np.random.laplace(self.mu, self.sigma, np.random.laplace(self.mu,self.sigma,[self._dims,nsamples]))
            else:   
                U = np.random.uniform(-0.5,0.5,[self._dims,nsamples])
                if self.std_type is 'vector':
                    return self.mu - self.sigma * np.sign(U)*np.log(1-2*np.abs(U))
                else:
                    return self.mu - np.dot(self.sigma, np.sign(U)*np.log(1-2*np.abs(U)))
        else:
            if self.std_type is 'scalar' or self.std_type is 'vector':
                return  self.mu[:,np.newaxis] - self.sigma * np.sign(U)*np.log(1-2*np.abs(U))
            else:
                return  self.mu[:,np.newaxis] - np.dot(self.sigma, np.sign(U)*np.log(1-2*np.abs(U)))

    def pdf(self, x):
        diff = x - self.mu
        if self.std_type is 'scalar':
            num = np.exp((-np.sqrt(2)/self.sigma)*diff)
            den = (self.sigma**self._dim)**0.5
            return num / den
        elif self.std_type is 'vector':
            num = np.exp((-np.sqrt(2)/self.sigma)*diff)
            den = (self._dim * np.prod(self.sigma))**0.5
            return num / den
        else:
            pass
            # num = np.exp(-0.5 * np.dot(diff,
            #                            np.linalg.solve(self.sigma, diff))
            #             )
            # den = ((2*np.pi)**self._dim * np.linalg.det(self.sigma)) ** 0.5
            # return num / den

    def logpdf(self, x):
        pass
        # diff = x - self.mu
        # if self.std_type is 'scalar':
        #     den = np.sqrt((2*np.pi)**self._dim * self.sigma**self._dim)
        #     return -np.log(den) - 0.5 * np.dot(diff, diff / self.sigma)
        # elif self.std_type is 'vector':
        #     den = np.sqrt((2*np.pi)**self._dim * np.prod(self.sigma))
        #     return -np.log(den) - 0.5 * np.dot(diff, diff / self.sigma)
        # else:
        #     den = np.sqrt((2*np.pi)**self._dim * np.linalg.det(self.sigma))
        #     return -np.log(den) - 0.5 * np.dot(diff,
        #                                        np.linalg.solve(self.sigma, diff)
        #                                       )

    def gradlogpdf(self, x):
        pass
        # diff = x - self.mu
        # if self.std_type is 'scalar' or self.std_type is 'vector':
        #     return diff / self.sigma
        # else:
        #     return np.linag.solve(self.sigma, diff)

    def hess_logpdf_action_impl(self, x):
        pass
        # if self.std_type is 'scalar' or self.std_type is 'vector':
        #     return x / self.sigma
        # else:
        #     return np.linalg.solve(self.sigma, diff)

    def hess_logpdf_action(self, x):
        return self.hess_logpdf_action_impl(x)


# Simple tests seem to show that this works if we want rotated independnet Laplace distributions: mu - np.dot(B,np.sign(U)*np.log(1-2*np.abs(U))), where U are i.i.d. uniform [-1/2,1/2] samples., 
# mu is whatever mean we want, and B is whatever covariance we want.
#See picture below for intuition,
#https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=imgres&cd=&cad=rja&uact=8&ved=2ahUKEwjxxOe7yrveAhUBD60KHa2gC9oQjRx6BAgBEAU&url=https%3A%2F%2Fwww.slideshare.net%2FDaichiKitamura%2Fblind-source-separation-based-on-independent-lowrank-matrix-analysis-and-its-extension-to-students-tdistribution&psig=AOvVaw0nzXibvwLkUmEF4kGPFMFo&ust=1541449871487390


#What we really may want is `spherical' or ellipsoidal Laplace distribution, whci I guess is harder to sample from.
#See equation 6 in https://ieeexplore.ieee.org/document/1618702 for how, I think ..
# mu -  np.dot(Sqrt(Z),np.dot(Gamma^{1/2},np.random.normal(0,1,[N,10000])) works for 10k samples of a Laplace distirbution random variable
#with mean mu and Covariance ?



#Sinc/Laplace custom likelihood: 
#exp(-|x| + sin(30|x|)/(|x|+1))
#d/dx :
#d^2/dx^2
